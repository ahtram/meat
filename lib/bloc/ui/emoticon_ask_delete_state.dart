part of 'emoticon_ask_delete_cubit.dart';

@immutable
abstract class EmoticonAskDeleteState {}

class EmoticonAskDeleteInitial extends EmoticonAskDeleteState {}

class EmoticonAskDeleteShow extends EmoticonAskDeleteState {
  EmoticonAskDeleteShow(this.url);
  final String url;
}
