import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_plurk_delete_state.dart';

class TimelinePlurkDeleteCubit extends Cubit<TimelinePlurkDeleteState> {
  TimelinePlurkDeleteCubit() : super(TimelinePlurkDeleteInitial());

  request(int plurkId) async {
    Plurdart.Base responseBase = await Plurdart.timelinePlurkDelete(Plurdart.TimelinePlurkDelete(plurkId: plurkId));
    if (responseBase != null) {
      if(responseBase.hasSuccess()) {
        emit(TimelinePlurkDeleteSuccess(plurkId));
      } else {
        emit(TimelinePlurkDeleteFailed());
      }
    } else {
      emit(TimelinePlurkDeleteFailed());
    }
  }
}
