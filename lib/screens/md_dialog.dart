import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';

void show(BuildContext context, String title, String mdContent, Function(bool) onAccept) {
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _MDDialog(title, mdContent, onAccept);
  }));
}

class _MDDialog extends StatefulWidget {
  _MDDialog(this._title, this._mdContent, this._onAccept);

  final String _title;
  final String _mdContent;
  final Function(bool) _onAccept;

  @override
  _MDDialogState createState() => _MDDialogState();
}

class _MDDialogState extends State<_MDDialog> {

  ScrollController _scrollController = ScrollController();
  bool _acceptButtonEnable = false;

  @override
  void initState() {
    super.initState();
    _scrollController
      ..addListener(() {
        if (_scrollController.position.pixels >=
            _scrollController.position.maxScrollExtent) {
          //Enable accept button.
          setState(() {
            _acceptButtonEnable = true;
          });
        } else {
          //Disable accept button.
          setState(() {
            _acceptButtonEnable = false;
          });
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    return AcrylicScaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
          elevation: 0,
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          actions: [
            AppBarActionButton(Icons.close, onTap: () {
              Navigator.pop(context);
            }),
            SizedBox(
              width: 2,
            )
          ],
          title: Center(
            child: Text(
              widget._title,
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
            ),
          ),
        ),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [

              //TOS viewer
              Expanded(
                child: SingleChildScrollView(
                  controller: _scrollController,
                  child: Container(
                    color: Theme.of(context).canvasColor.withOpacity(0.4),
                    padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                    child: MarkdownBody(
                      data: widget._mdContent,
                    ),
                  ),
                ),
              ),

              //Accept button.
              Container(
                color: Theme.of(context).canvasColor.withOpacity(0.4),
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                child: ElevatedButton(
                  child: Text('Accept'),
                  onPressed: (!_acceptButtonEnable) ? null : () {
                    Navigator.pop(context);
                    widget._onAccept(true);
                }),
              ),

            ],
          ),
        ));
  }
}
