import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/widgets/plurk/plurk_cards_viewer.dart';

class SearchPlurks extends StatefulWidget {
  SearchPlurks({Key key}) : super(key: key);

  @override
  SearchPlurksState createState() => SearchPlurksState();
}

class SearchPlurksState extends State<SearchPlurks> with AutomaticKeepAliveClientMixin<SearchPlurks> {

  //The search term we are using.
  String _searchTerm = '';
  GlobalKey<PlurkCardsViewerState> _plurkCardsViewerGlobalKey = GlobalKey<PlurkCardsViewerState>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Builder(
      builder: (context) {
        //Do we have a search term?
        if (_searchTerm.isEmpty != true) {
          //We have a search term.
          //Display the search PlurkViewer.
          return PlurkCardsViewer(
            PlurkCardsViewerArgs(
              BrowsingType.Search,
              true,
              hasAppBar: false,
              searchTerm: _searchTerm,
            ),
            key: _plurkCardsViewerGlobalKey,
          );
        } else {
          return Container(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    MdiIcons.imageSearchOutline,
                    size: 96,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    'Looking for something?',
                    style: Theme.of(context).textTheme.caption.copyWith(fontSize: 14),
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  runSearch(String searchTerm) {
    if (_searchTerm.isEmpty == true) {
      //First time. Just setState will do the work
      setState(() {
        _searchTerm = searchTerm;
      });
    } else {
      //Not first time. Will need to refreshSearchTerm.
      _searchTerm = searchTerm;
      if (_plurkCardsViewerGlobalKey.currentState != null) {
        _plurkCardsViewerGlobalKey.currentState.refreshSearchTerm(_searchTerm);
      }
    }
  }

}