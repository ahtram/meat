part of 'responses_response_add_cubit.dart';

@immutable
abstract class ResponsesResponseAddState {}

class ResponsesResponseAddInitial extends ResponsesResponseAddState {}

class ResponsesResponseAddSuccess extends ResponsesResponseAddState {
  ResponsesResponseAddSuccess(this.response);
  final Plurdart.Response response;
}

class ResponsesResponseAddFailed extends ResponsesResponseAddState {

}