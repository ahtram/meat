import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'polling_get_unread_count_state.dart';

class PollingGetUnreadCountCubit extends Cubit<PollingGetUnreadCountState> {
  PollingGetUnreadCountCubit() : super(PollingGetUnreadCountInitial());

  request() async {
    Plurdart.PollingUnreadCount pollingUnreadCount = await Plurdart.pollingGetUnreadCount();
    if (pollingUnreadCount != null) {
      emit(PollingGetUnreadCountSuccess(pollingUnreadCount));
    } else {
      emit(PollingGetUnreadCountFailed());
    }
  }
}
