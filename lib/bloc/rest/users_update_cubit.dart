import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'users_update_state.dart';

class UsersUpdateCubit extends Cubit<UsersUpdateState> {
  UsersUpdateCubit() : super(UsersUpdateInitial());

  Future pinPlurk(int plurkId) async {
    Plurdart.User user = await Plurdart.usersUpdate(Plurdart.UsersUpdate(
      pinnedPlurkId: plurkId
    ));

    //Collect Users to global User Cache.
    Static.addToUserCache(user);

    if (user != null) {
      if (!user.hasError()) {
        print('UsersUpdatePinPlurkSuccess[' + plurkId.toString() + ']');
        emit(UsersUpdatePinPlurkSuccess(user));
      } else {
        print('UsersUpdatePinPlurkFailed[' + user.errorText +']');
        emit(UsersUpdatePinPlurkFailed(user.errorText));
      }
    } else {
      print('UsersUpdatePinPlurkFailed');
      emit(UsersUpdatePinPlurkFailed('Unknown error'));
    }
  }

  Future unpinPlurk() async {
    Plurdart.User user = await Plurdart.usersUpdate(Plurdart.UsersUpdate(
        pinnedPlurkId: 0
    ));

    //Collect Users to global User Cache.
    Static.addToUserCache(user);

    if (user != null) {
      if (!user.hasError()) {
        emit(UsersUpdateUnpinPlurkSuccess(user));
      } else {
        emit(UsersUpdateUnpinPlurkFailed(user.errorText));
      }
    } else {
      emit(UsersUpdateUnpinPlurkFailed('Unknown error'));
    }
  }

}
