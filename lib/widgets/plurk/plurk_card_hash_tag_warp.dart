import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/theme/app_themes.dart';

class PlurkCardHashTagWarp extends StatelessWidget {
  PlurkCardHashTagWarp(this.hashTags, {this.onHashtagTap});

  final List<PlurkCardHashTag> hashTags;
  final Function onHashtagTap;

  @override
  Widget build(BuildContext context) {
    List<Widget> chipChildren = [];

    Color hasTagColor = Theme.of(context).accentColor;
    //hashtag color is tricky: Pure theme should be override with grey color.
    if (Static.settingsAppTheme == AppTheme.Pure) {
      hasTagColor = Colors.grey;
    }

    hashTags.forEach((hashTag) {
      chipChildren.add(GestureDetector(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 3),
          child: Text(
            '#' + hashTag.term,
            style: TextStyle(
              color: hasTagColor,
              fontSize: 14,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
        onTap: () {
          onHashtagTap?.call(hashTag.term);
        },
      ));
    });

    return Wrap(
      children: chipChildren,
    );
  }
}
