part of 'friends_fans_get_friends_by_offset_cubit.dart';

@immutable
abstract class FriendsFansGetFriendsByOffsetState {}

class FriendsFansGetFriendsByOffsetInitial extends FriendsFansGetFriendsByOffsetState {}

class FriendsFansGetFriendsByOffsetSuccess extends FriendsFansGetFriendsByOffsetState {
  FriendsFansGetFriendsByOffsetSuccess(this.users);
  final List<Plurdart.User> users;
}

class FriendsFansGetFriendsByOffsetFailed extends FriendsFansGetFriendsByOffsetState {

}
