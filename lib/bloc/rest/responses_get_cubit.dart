import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'responses_get_state.dart';

//[Dep]: Replaced by ResponsesCollector.
class ResponsesGetCubit extends Cubit<ResponsesGetState> {
  ResponsesGetCubit() : super(ResponsesGetState(null));

  void get(int plurkId, int fromResponse, int count) async {
    Plurdart.Responses responses = await Plurdart.responsesGet(Plurdart.ResponsesGet(
      plurkId: plurkId,
      fromResponse: fromResponse,
      count: count,
    ));

    //Collect Users to global User Cache.
    if (responses != null) {
      responses.friends.forEach((key, value) {
        Static.addToUserCache(value);
      });
    }

    emit(ResponsesGetState(responses));
  }
}
