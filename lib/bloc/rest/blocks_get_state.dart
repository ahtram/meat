part of 'blocks_get_cubit.dart';

@immutable
abstract class BlocksGetState {}

class BlocksGetInitial extends BlocksGetState {}

class BlocksGetSuccess extends BlocksGetState {
  BlocksGetSuccess(this.users);
  final List<Plurdart.User> users;
}

class BlocksGetFailed extends BlocksGetState {}
