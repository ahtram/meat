import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'blocks_unblock_state.dart';

class BlocksUnblockCubit extends Cubit<BlocksUnblockState> {
  BlocksUnblockCubit() : super(BlocksUnblockInitial());

  request(int userId) async {
    Plurdart.Base base = await Plurdart.blocksUnblock(Plurdart.UserID(userId: userId));

    if (base != null) {
      if (base.hasSuccess()) {
        emit(BlocksUnblockSuccess());
      } else {
        emit(BlocksUnblockFailed(base.errorText));
      }
    } else {
      emit(BlocksUnblockFailed(base.errorText));
    }

  }
}
