import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'home_drawer_item.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/theme/app_themes.dart';

class HomeDrawerFooter extends StatelessWidget {
  HomeDrawerFooter({this.onSettingsTap, this.onQRCodeTap});

  final Function onSettingsTap;
  final Function onQRCodeTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Static.settingsAcrylicEffect == true
              ? Theme.of(context).lowContrastPrimaryColor.withOpacity(0.5)
              : Theme.of(context).lowContrastPrimaryColor,
          padding: EdgeInsets.fromLTRB(8, 4, 16, 4),
          child: Row(
            children: <Widget>[
              Expanded(
                child: HomeDrawerItem(
                  Icons.settings,
                  FlutterI18n.translate(context, 'menuSettings'),
                  onTap: onSettingsTap,
                ),
              ),
              InkWell(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: Icon(
                    MdiIcons.qrcode,
                    color: Theme.of(context).iconTheme.color,
                  ),
                ),
                onTap: onQRCodeTap,
              ),
            ],
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).viewInsets.bottom,
        ),
      ],
    );
  }
}
