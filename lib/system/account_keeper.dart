import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:meat/models/app/account_store.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meat/system/define.dart' as Define;

//This is the static APIs provider of manage account relative stuffs like:
//AccountStore / meId

//The global accountStore object. Should be loaded when the app start...
AccountStore _accountStore;

//Initialize the AccountKeeper.
initialize() async {
  //Try read the json string from secure storage.
  // print('[Account Keeper Initialize]');

  FlutterSecureStorage storage = FlutterSecureStorage(aOptions: AndroidOptions(
    encryptedSharedPreferences: true,
  ));
  //Read the account stuffs...
  String accountStore = '';
  https://github.com/mogol/flutter_secure_storage/issues/354
  try {
    accountStore = await storage.read(key: Define.accountStoreStorageKey);
  } on PlatformException catch (e) {
    // Workaround for https://github.com/mogol/flutter_secure_storage/issues/43
    await storage.deleteAll();
  }

  if (accountStore?.isEmpty ?? true) {
    // print('accountStore not exist! Creating a new one.');
    _accountStore = AccountStore();
  } else {
    //Try decode the json content.
    try {
      _accountStore = AccountStore.fromJson(json.decode(accountStore));
    } catch (e) {
      print('Oops! AccountStore deserialize failed! Creating a new one...');
      _accountStore = AccountStore();
    }
  }
}

//Try save the current _accountStore data.
Future<bool> save() async {
  if (_accountStore != null) {
    FlutterSecureStorage storage = FlutterSecureStorage(aOptions: AndroidOptions(
      encryptedSharedPreferences: true,
    ));
    String jsonStr = json.encode(_accountStore.toJson());
    // print('AccountStore serialize: ' + jsonStr);
    await storage.write(key: Define.accountStoreStorageKey, value: jsonStr);
    return true;
  } else {
    return false;
  }
}

//I doubt we will ever need this.
release() {
  _accountStore = null;
}

//Try add an account.
addAccount(String nickName, int userId, String accessToken, String tokenSecret, { bool andSetActive = true } ) async {
  if (_accountStore != null) {
    _accountStore.addAccount(nickName, userId, accessToken, tokenSecret, andSetActive: andSetActive);
    await save();
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
  }
}

//Set active an account.
Future<bool> setAccountActive(String nickName) async {
  if (_accountStore != null) {
    bool result = _accountStore.setAccountActive(nickName);
    if (result) {
      await save();
    }
    return result;
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return false;
  }
}

//Set an account active.
Future<bool> setIndexActive(int index) async {
  if (_accountStore != null) {
    bool result = _accountStore.setIndexActive(index);
    if (result) {
      await save();
    }
    return result;
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return false;
  }
}

//Is an account with the nickName already exist?
bool isAccountExist(String nickName) {
  if (_accountStore != null) {
    return _accountStore.isAccountExist(nickName);
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return false;
  }
}

//Try remove an account.
Future<bool> removeAccount(String nickName) async {
  if (_accountStore != null) {
    bool result = _accountStore.removeAccount(nickName);
    if (result) {
      await save();
    }
    return result;
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return false;
  }
}

//Try remove to activated account.
Future<bool> removeActivatedAccount() async {
  if (_accountStore != null) {
    bool result = _accountStore.removeActivatedAccount();
    if (result) {
      await save();
    }
    return result;
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return false;
  }
}

//Get the activated account index.
int getActivatedAccountIndex() {
  if (_accountStore != null) {
    return _accountStore.getActivatedAccountIndex();
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return 0;
  }
}

//Get the activated account or any usable account if the active one is out of index. (possible return null)
AccountTrunk getActivatedOrAnUsableAccount() {
  if (_accountStore != null) {
    return _accountStore.getActivatedOrAnUsableAccount();
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return null;
  }
}

//Get the activated account id. (possible 0)
int meId() {
  AccountTrunk at = getActivatedOrAnUsableAccount();
  if (at != null) {
    return at.userId;
  } else {
    print('meId() return 0');
    return 0;
  }
}

//Get the activated account nickName. (possible empty)
String meNickName() {
  AccountTrunk at = getActivatedOrAnUsableAccount();
  if (at != null) {
    return at.nickName;
  } else {
    print('meNickName() return Empty');
    return '';
  }
}

//Get the full nick name list of the logged in accounts.
List<String> getAccountNickNameList() {
  if (_accountStore != null) {
    return _accountStore.getAccountNickNameList();
  } else {
    print('Oops! _accountStore is null! Have you done the initialize?');
    return [];
  }
}
