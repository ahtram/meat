import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'plurk_post_complete_state.dart';

//For Home to decide if it should refresh the current Plurk feed after a new Plurk is sent.
class PlurkPostCompleteCubit extends Cubit<PlurkPostCompleteState> {
  PlurkPostCompleteCubit() : super(PlurkPostCompleteInitial());

  success(Plurdart.Plurk plurk) async {
    emit(PlurkPostCompleteSuccess(plurk));
  }
}
