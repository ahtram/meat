import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'expire_token_state.dart';

class ExpireTokenCubit extends Cubit<ExpireTokenState> {
  ExpireTokenCubit() : super(ExpireTokenState(null));

  void expireToken() async {
    Plurdart.CheckedExpiredToken checkedExpiredToken = await Plurdart.expireToken();
    emit(ExpireTokenState(checkedExpiredToken));
  }
}
