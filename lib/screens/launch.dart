import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/services.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/models/app/app.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/account_keeper.dart' as AccountKeeper;
import 'package:meat/models/app/account_store.dart';
import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';

// import 'package:facebook_audience_network/facebook_audience_network.dart';

class Launch extends StatefulWidget {
  Launch();
  @override
  _LaunchState createState() => _LaunchState();
}

class _LaunchState extends State<Launch> with AfterLayoutMixin {
  @override
  void afterFirstLayout(BuildContext context) async {
    // This will check credential to see we should launch into login or main.
    await _launchSetupClient();
    // Launch the Test Page.
    // _launchToThemePlayground();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: Theme.of(context).canvasColor,
        appBar: null,
        resizeToAvoidBottomInset: false,
        body: Container(
          color: Colors.transparent,
        ),
      ),
    );
  }

  //--

  //This will try setup client use exist token/secret
  _launchSetupClient() async {
    print('[_launchSetupClient]');

    //Shit! Auth_Ninja is dead.
    //[Preserve for debug hard-coded token/secret]
    //These are for when stupid Plurk login page is broken...(hard code token/secret from my Plurk test console)
//    String readToken = 'yABXHPMRevhu';
//    String readSecret = 'yBcDsuGL5j1Q6NuNRJYFCmeOAvt3qDoH';

    //Read and decode the app stuff from asset file...
    App app;
    try {
      String jsonStr = await rootBundle.loadString('assets/app/app.json');
      dynamic json = jsonDecode(jsonStr);
      app = App.fromJson(json);
    } catch (e) {
      print(e.toString());
    }

    //Read account store.
    await AccountKeeper.initialize();
    //This will get our saved user credentials.
    AccountTrunk at = AccountKeeper.getActivatedOrAnUsableAccount();

    // Check these credentials by API checkToken().
    bool plurdartSetupRestoreSuccess = false;
    if ((app != null) &&
        (at != null) &&
        !(at.accessToken?.isEmpty ?? true) &&
        !(at.tokenSecret?.isEmpty ?? true)) {
      // setup client.
      Plurdart.setupClient(app.key, app.secret, at.accessToken, at.tokenSecret);
      plurdartSetupRestoreSuccess = true;

      //Hmm... This could slow down the app opening time. I'll do this in Home instead...
//      // Test an API call to see if the client still work...
//      Plurdart.CheckedExpiredToken checkedExpiredToken = await Plurdart.checkToken();
//      if (checkedExpiredToken != null) {
////        _showSnackBarMessage('[Credential restored. Plurdart has been setup]');
//        print('[Credential restored. Plurdart has been setup]');
//        plurdartSetupRestoreSuccess = true;
//      } else {
////        _showSnackBarMessage('[Credential expired. Re-Auth is required.]');
//        print('[Credential expired. Re-Auth is required.]');
//        plurdartSetupRestoreSuccess = false;
//      }
    } else {
//      _showSnackBarMessage('[Cannot restore credential. Auth is required.]');
      print('[Cannot restore credential. Auth is required.]');
      plurdartSetupRestoreSuccess = false;
    }

    //Initial admob stuffs here...
    WidgetsFlutterBinding.ensureInitialized();

    //[I screwed this]
    // await MobileAds.instance.initialize();
    // await MobileAds.instance.updateRequestConfiguration(
    //   RequestConfiguration(
    //       tagForChildDirectedTreatment:
    //       TagForChildDirectedTreatment.unspecified,
    //       testDeviceIds: <String>["927f6bd1-33d2-41ff-8b93-6f4146486ba0"]),
    // );

    // FacebookAudienceNetwork.init(
    //     testingId: 'd7a46403-4626-4e4f-aab2-c5c6045864be'
    // );

    //Alarm manager plus (Android only)
    if (Platform.isAndroid) {
      await AndroidAlarmManager.initialize();
    }

    //Load any system static define data here...
    await Static.emoticonCollection.loadDice();

    // Open route by the setup credential result.
    if (!plurdartSetupRestoreSuccess) {
      Navigator.pushReplacementNamed(context, '/welcome_login');
    } else {
      Navigator.pushReplacementNamed(context, '/home');
    }
  }

  // _launchToThemePlayground() {
  //   print('[_launchToThemePlayground]');
  //   Navigator.pushNamed(context, '/theme_playground');
  // }
}
