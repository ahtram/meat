import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'profile_get_own_profile_state.dart';

class ProfileGetOwnProfileCubit extends Cubit<ProfileGetOwnProfileState> {
  ProfileGetOwnProfileCubit() : super(ProfileGetOwnProfileInitial());

  request() async {
    Plurdart.Profile profile = await Plurdart.profileGetOwnProfile(Plurdart.ProfileGetOwnProfile());

    if (profile != null) {
      if (!profile.hasError()) {
        emit(ProfileGetOwnProfileSuccess(profile));
      } else {
        emit(ProfileGetOwnProfileFailed(profile.errorText));
      }
    } else {
      emit(ProfileGetOwnProfileFailed('Unknown error'));
    }
  }

}
