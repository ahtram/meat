
class LivingChannel {
  String channelID;
  String channelThumbnailUrl;
  String videoID;
  String videoThumbnailUrl;
  String owner;
  String title;
  int viewingCount;

  LivingChannel(
      {this.channelID,
        this.channelThumbnailUrl,
        this.videoID,
        this.videoThumbnailUrl,
        this.owner,
        this.title,
        this.viewingCount});

  LivingChannel.fromJson(Map<String, dynamic> json) {
    channelID = json['ChannelID'];
    channelThumbnailUrl = json['ChannelThumbnailUrl'];
    videoID = json['VideoID'];
    videoThumbnailUrl = json['VideoThumbnailUrl'];
    owner = json['Owner'];
    title = json['Title'];
    viewingCount = json['ViewingCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ChannelID'] = this.channelID;
    data['ChannelThumbnailUrl'] = this.channelThumbnailUrl;
    data['VideoID'] = this.videoID;
    data['VideoThumbnailUrl'] = this.videoThumbnailUrl;
    data['Owner'] = this.owner;
    data['Title'] = this.title;
    data['ViewingCount'] = this.viewingCount;
    return data;
  }
}