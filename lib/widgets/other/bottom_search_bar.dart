import 'package:flutter/material.dart';
import 'package:meat/widgets/hack/HTextField.dart';

class BottomSearchBar extends StatefulWidget {
  BottomSearchBar({Key key, this.onSearchEnter, this.onSearchChanged, this.withSearchButton = true}) : super(key: key);

  final Function(String) onSearchEnter;
  final Function(String) onSearchChanged;
  final bool withSearchButton;

  @override
  BottomSearchBarState createState() => BottomSearchBarState();
}

class BottomSearchBarState extends State<BottomSearchBar> {
  TextEditingController commentController = TextEditingController();
  GlobalKey<TextFieldState> commentTextFieldKey = GlobalKey<TextFieldState>();
  FocusNode commentFocus = FocusNode();

  @override
  Widget build(BuildContext context) {

    List<Widget> rowChildren = [];

    //TextField
    rowChildren.add(
      Expanded(
        child: TextField(
          key: commentTextFieldKey,
          showCursor: true,
          controller: commentController,
          focusNode: commentFocus,
          cursorColor: Theme.of(context).accentColor,
          keyboardAppearance: Theme.of(context).brightness,
          decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            filled: true,
            contentPadding: EdgeInsets.fromLTRB(8, 0, 8, 2),
            hintText: 'Search',
          ),
          onSubmitted: _onSubmitted,
          onChanged: (value) {
            if (widget.onSearchChanged != null) {
              widget.onSearchChanged(value);
            }
            setState(() {});
          },
        ),
      ),
    );

    rowChildren.add(Container(
      height: 28,
      child: commentController.text.isEmpty
          ? null
          : GestureDetector(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: Icon(
            Icons.clear,
            size: 24,
          ),
        ),
        onTap: () {
          clear();
        },
      ),
    ));

    if (widget.withSearchButton) {
      rowChildren.add(SizedBox(
        width: 4,
      ));

      //Button
      rowChildren.add(SizedBox(
        height: 28,
        child: ElevatedButton(
          child: Icon(
            Icons.search,
            size: 24,
          ),
          onPressed: () {
            setState(() {
              FocusScope.of(context)
                  .unfocus(disposition: UnfocusDisposition.scope);
            });

            //Callback the term.
            _onSubmitted(commentController.text);
          },
        ),
      ));
    }

    return BottomAppBar(
      elevation: 0,
      color: Colors.transparent,
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // Input UIs here...
            Row(
              mainAxisSize: MainAxisSize.max,
              children: rowChildren,
            ),

            SizedBox(
              height: MediaQuery.of(context).viewInsets.bottom,
            ),
          ],
        ),
      ),
    );
  }

  _onSubmitted(String searchTerm) {
    if (searchTerm.isEmpty == false && widget.onSearchEnter != null) {
      widget.onSearchEnter(searchTerm);
    }
  }

  setTerm(String term) {
    commentController.text = term;
  }

  focus() {
    commentFocus.requestFocus();
  }

  clear() {
    commentController.clear();
    if (widget.onSearchChanged != null) {
      widget.onSearchChanged('');
    }
    setState(() {});
  }

}
