import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/ui/acrylic_background_switch_cubit.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/bloc/ui/image_links_viewer_cubit.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/screens/image_links_viewer.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/plurk_coin_sign.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/plurk_paste_view.dart';
import 'package:meat/widgets/plurk/response_card_sub_header.dart';
import 'package:meat/bloc/ui/report_abuse_dialog_cubit.dart';

import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/models/plurk/plurk_content_parts.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';

import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:clipboard/clipboard.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/screens/send_gift.dart' as SendGift;

class ResponseCard extends StatefulWidget {
  ResponseCard(
    this.plurk,
    this.response,
    this.user,
    this.floor,
    this.onNameTap,
    this.onAvatarTap,
    this.onWantEditResponse,
    this.onWantDeleteResponse,
    this.onLongPressed,
    this.highlighting,
    this.imageCollectionType, {
    Key key,
    this.keepAliveWhenFoundImageLinks = false,
    this.keepAliveWhenFoundPlurkPaste = true,
  }) : super(key: key);

  final Plurdart.Plurk plurk;
  final Plurdart.Response response;
  final Plurdart.User user;
  final int floor;

  final Function(Plurdart.Response, Plurdart.User) onNameTap;
  final Function(Plurdart.User) onAvatarTap;
  final Function(Plurdart.Response, Plurdart.User) onWantEditResponse;
  final Function(Plurdart.Response) onWantDeleteResponse;
  //This returns the display name (or handle of response when it's available)
  final Function(String) onLongPressed;
  final bool highlighting;

  //Sometimes we want a very long card just keep alive. (In plurk_viewer)
  final bool keepAliveWhenFoundImageLinks;
  final bool keepAliveWhenFoundPlurkPaste;

  final ImageCollectionType imageCollectionType;

  @override
  _ResponseCardState createState() => _ResponseCardState();
}

class _ResponseCardState extends State<ResponseCard>
    with PlurkContentParts, AfterLayoutMixin, AutomaticKeepAliveClientMixin {
  _ResponseCardState();

  BuildContext _blocProviderContext;
  GlobalKey<PopupMenuButtonState> _popupMenuKey =
      GlobalKey<PopupMenuButtonState>();

  //Should this card self keep alive?
  bool _keepAlive = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    if (mounted) {
      // Pre process all display elements.
      preProcess(context, widget.response.content,
          Static.settingsRemoveLinksInPlurkContent);

      //Check if I should keep myself alive.
      if ((widget.keepAliveWhenFoundImageLinks && plurkImageLinks.length > 0) ||
          (widget.keepAliveWhenFoundPlurkPaste && plurkPasteLinks.length > 0)) {
        _keepAlive = true;
        updateKeepAlive();
      }
    }
  }

  @override
  bool get wantKeepAlive {
    return _keepAlive;
  }

  @override
  void didUpdateWidget(covariant ResponseCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    // print('[didUpdateWidget]');

    // Pre process all display elements.
    preProcess(context, widget.response.content,
        Static.settingsRemoveLinksInPlurkContent);

    //Try switch acrylic container image.
    //Try set background image.
    // Uint8List backgroundImage = tryGetABackgroundImage();
    //Possible null. Will set a null image for empty background.
    // print('plurk [' + plurk.plurkId.toString() + ']' 'active a AcrylicBackground [' + tryGetABackgroundImageSource() + ']');

    //This one is for AcrylicContainer.
    // _blocProviderContext
    //     .read<AcrylicBackgroundSwitchCubit>()
    //     .active(backgroundImage);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // Pre process all display elements.
    preProcess(context, widget.response.content,
        Static.settingsRemoveLinksInPlurkContent);

    //We need this for get InheritedWidget's context right...
    context.dependOnInheritedWidgetOfExactType();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    //Build our beautiful PlurkCard here.
    return AcrylicContainer(
      Builder(
        builder: (context) {
          //Save the context because we need it for switching background.
          _blocProviderContext = context;
          return InkWell(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      Define.horizontalPadding * 0.5,
                      Define.verticalPadding * 0.5,
                      Define.horizontalPadding * 0.5,
                      0),
                  child: _buildHeaderPart(widget.response, widget.user,
                      widget.floor, widget.onNameTap, widget.onAvatarTap),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: _buildContentParts(widget.response, widget.user),
                ),
                Divider(height: 0.5, thickness: 0.5),
              ],
            ),
            onTap: () {
              // Reply or copy content?
            },
            onLongPress: () {
              String displayNameStr =
                  (widget.response != null && widget.response.handle != null)
                      ? widget.response.handle
                      : widget.user.displayName;
              widget.onLongPressed?.call(displayNameStr);
              //[Dep]: Replaced with highlight user.
              // Vibrator.vibrateShortNote();
              // _popupMenuKey.currentState.showButtonMenu();
            },
          );
        },
      ),
      initialImage: MemoryImage(kTransparentImage),
      mode: AcrylicContainerMode.None,
      backgroundColor:
          widget.highlighting ? Theme.of(context).primaryColorLight : null,
      backgroundOpacity: widget.highlighting ? 0.25 : 0.5,
    );
  }

  // This will decide which part will be displayed on the Card.
  // Some of the part maybe empty.
  List<Widget> _buildContentParts(
      Plurdart.Response response, Plurdart.User user) {
    List<Widget> partWidgets = [];

    partWidgets.add(SizedBox(height: Define.verticalPadding * 0.5));

    //== Text Content == (Maybe empty)
    Widget textContentPart = buildTextContentPart(textContent);
    if (textContentPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: textContentPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //[Dep]: We have decide not to move HashTag from text centent.
    //== Hash tags == (Maybe empty)
    // Widget hashTagsPart = buildHashTagsPart(hashTags);
    // if (hashTagsPart != null) {
    //   partWidgets.add(hashTagsPart);
    // }

    //== Youtube Video Content == (Maybe empty)
    Widget youtubeVideoPart = buildYoutubeVideoPart(youtubeVideoLinks);
    if (youtubeVideoPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: youtubeVideoPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Image Collection Content == (Maybe empty)
    Widget imageCollectionPart =
        buildImageCollectionPartByType(plurkImageLinks, (heroTag) {
      //Get the initial index
      int initialIndex = 0;
      for (int i = 0; i < plurkImageLinks.length; ++i) {
        if (plurkImageLinks[i].getHeroTag() == heroTag) {
          initialIndex = i;
          break;
        }
      }

      context.read<ImageLinksViewerCubit>().enter(ImageLinksViewerArgs(
            plurkImageLinks,
            initialIndex,
          ));
    }, () {
      // for (int i = 0; i < plurkImageLinks.length; ++i) {
      //   if (plurkImageLinks[i].getImageRawInfo() != null) {
      //     _blocProviderContext
      //         .read<AcrylicBackgroundSwitchCubit>()
      //         .active(plurkImageLinks[i].getImageRawInfo().uint8list);
      //     break;
      //   }
      // }
    }, widget.imageCollectionType);

    if (imageCollectionPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: imageCollectionPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    } else {
      //Change to a transparent image
      // _blocProviderContext
      //     .watch<AcrylicBackgroundSwitchCubit>()
      //     .active(kTransparentImage);
    }

    //== Plurk Paste Content == (Maybe empty)
    Widget plurkPastePart = buildPlurkPastePart(
        plurkPasteLinks, PlurkPasteViewType.Expanded, (uri) {
      BlocProvider.of<OpenUriCubit>(context).request(uri);
    });
    if (plurkPastePart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: plurkPastePart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Link Stack Content == (Maybe empty)
    Widget linkStackPart = buildLinkStackPart(urlLinks, (uri) {
      BlocProvider.of<OpenUriCubit>(context).request(uri);
    });
    if (linkStackPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: linkStackPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    return partWidgets;
  }

  // Header
  Widget _buildHeaderPart(
      Plurdart.Response response,
      Plurdart.User user,
      int floor,
      Function(Plurdart.Response, Plurdart.User) onNameTap,
      Function(Plurdart.User) onAvatarTap) {
    List<Widget> rowChildren = [];

    //Header.
    rowChildren.add(Expanded(
      child: ResponseCardSubHeader(
        response,
        user,
        floor,
        false,
        onAvatarTap: (user) {
          onAvatarTap?.call(user);
        },
        onNameTap: (response, user) {
          onNameTap?.call(response, user);
        },
      ),
    ));

    //Coin icon
    //Plurk coin icon.
    if (widget.response.coins != null && widget.response.coins > 0) {
      rowChildren.add(SizedBox(
        width: 12,
      ));

      rowChildren.add(PlurkCoinSign(widget.response.coins));
    }

    //Arrow button.
    //This is the best solution.
    rowChildren.add(PopupMenuButton(
        key: _popupMenuKey,
        child: SizedBox(
          width: 36,
          height: 36,
          child: Icon(
            Icons.expand_more,
            size: 24,
            color: Theme.of(context).iconTheme.color,
          ),
        ),
        onSelected: (result) {
          ResponseMenuAction action = result;
          //These are very plain actions so we'll just do it here...
          switch (action.actionType) {
            case ResponseMenuActionType.Mention:
              //Let's do this in the cheap way...
              onNameTap?.call(response, user);
              Vibrator.vibrateShortNote();
              break;
            case ResponseMenuActionType.CopyTheContent:
              FlutterClipboard.copy(widget.response.contentRaw);
              context
                  .read<FlushBarCubit>()
                  .request(Icons.content_copy, 'Success!', 'Content copied.');
              Vibrator.vibrateShortNote();
              break;
            case ResponseMenuActionType.SendGift:
              //Open the SendGift dialog.
              SendGift.show(
                  context,
                  plurkId: widget.response.plurkId,
                  userId: widget.response.userId,
                  responseId: widget.response.id
              );
              Vibrator.vibrateShortNote();
              break;
            case ResponseMenuActionType.Edit:
              widget.onWantEditResponse?.call(response, user);
              Vibrator.vibrateShortNote();
              break;
            case ResponseMenuActionType.Delete:
              widget.onWantDeleteResponse?.call(response);
              Vibrator.vibrateShortNote();
              break;
            case ResponseMenuActionType.ReportAbuse:
              //Open report abuse. (Not sure if the API is suit for report user)
              print('Open report abuse.');
              context
                  .read<ReportAbuseDialogCubit>()
                  .reportResponse(action.payload);
              break;
          }
        },
        itemBuilder: (BuildContext context) {
          List<PopupMenuEntry> returnWidgets = [];

          returnWidgets.add(menuItem(
              ResponseMenuAction(ResponseMenuActionType.Mention),
              Icons.alternate_email,
              FlutterI18n.translate(context, 'dropdownMention')));

          returnWidgets.add(menuItem(
              ResponseMenuAction(ResponseMenuActionType.CopyTheContent),
              Icons.content_copy,
              FlutterI18n.translate(context, 'dropdownCopyTheContent')));

          if (Static.canSendGift()) {
            returnWidgets.add(menuItem(
                ResponseMenuAction(ResponseMenuActionType.SendGift),
                MdiIcons.giftOutline,
                FlutterI18n.translate(context, 'dropdownSendGift')));
          }

          //No edit? Maybe the premium user and do edit?
          //Tricky: this is what I found work correctly!
          bool canEditResponse = false;
          if (widget.onWantEditResponse != null && Static.me.premium) {
            if (widget.response.myAnonymous != null &&
                widget.response.myAnonymous) {
              canEditResponse = true;
            } else {
              canEditResponse = widget.response.userId == Static.meId;
            }
          }

          //Is this plurk mine? (Delete button)
          if (canEditResponse) {
            returnWidgets.add(menuItem(
                ResponseMenuAction(ResponseMenuActionType.Edit),
                Icons.edit,
                FlutterI18n.translate(context, 'dropdownEdit')));
          }

          //Is this response mine?
          if (widget.onWantDeleteResponse != null &&
              (widget.response.userId == Static.meId ||
                  widget.plurk.ownerId == Static.meId)) {
            //That's mine. I can delete this if I want.
            returnWidgets.add(menuItem(
                ResponseMenuAction(ResponseMenuActionType.Delete),
                Icons.delete,
                FlutterI18n.translate(context, 'dropdownDelete')));
          }

          returnWidgets.add(menuItem(
              ResponseMenuAction(ResponseMenuActionType.ReportAbuse,
                  payload: widget.response.userId),
              MdiIcons.alertOctagramOutline,
              FlutterI18n.translate(context, 'dropdownReportAbuse')));

          return returnWidgets;
        }));

    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: rowChildren,
    );
  }

  PopupMenuItem menuItem(dynamic value, IconData icon, String text) {
    return PopupMenuItem(
      value: value,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(icon),
          SizedBox(
            width: 8,
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 5), child: Text(text)),
        ],
      ),
    );
  }
}

enum ResponseMenuActionType {
  Mention,
  CopyTheContent,
  SendGift,
  Edit,
  Delete,
  ReportAbuse,
}

//For a response menu drop down content.
class ResponseMenuAction {
  ResponseMenuAction(this.actionType, {this.payload});
  ResponseMenuActionType actionType;
  dynamic payload;
}
