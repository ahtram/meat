import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'profile_get_public_profile_state.dart';

class ProfileGetPublicProfileCubit extends Cubit<ProfileGetPublicProfileState> {
  ProfileGetPublicProfileCubit() : super(ProfileGetPublicProfileInitial());

  request(String nickName) async {
    Plurdart.Profile profile = await Plurdart.profileGetPublicProfile(Plurdart.ProfileGetPublicProfile(
      nickName: nickName,
    ));

    if (profile != null) {
      if (!profile.hasError()) {
        emit(ProfileGetPublicProfileSuccess(profile));
      } else {
        emit(ProfileGetPublicProfileFailed(profile.errorText));
      }
    } else {
      emit(ProfileGetPublicProfileFailed('Unknown error'));
    }
  }

}
