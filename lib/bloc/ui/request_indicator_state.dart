part of 'request_indicator_cubit.dart';

@immutable
abstract class RequestIndicatorState {}

class RequestIndicatorInitial extends RequestIndicatorState {}

class RequestIndicatorShow extends RequestIndicatorState {}

class RequestIndicatorHide extends RequestIndicatorState {}
