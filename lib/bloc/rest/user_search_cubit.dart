import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'user_search_state.dart';

class UserSearchCubit extends Cubit<UserSearchState> {
  UserSearchCubit() : super(UserSearchInitial());

  request(String query, int offset) async {
    Plurdart.UserSearch userSearch = await Plurdart.userSearchSearch(Plurdart.Search(
      query: query,
      offset: offset,
    ));
    if (userSearch != null) {
      if (!userSearch.hasError()) {
        emit(UserSearchSuccess(userSearch));
      } else {
        emit(UserSearchFailed(userSearch.errorText));
      }
    } else {
      emit(UserSearchFailed('Unknown error'));
    }
  }

  trackBack(String query, int offset) async {
    Plurdart.UserSearch userSearch = await Plurdart.userSearchSearch(Plurdart.Search(
      query: query,
      offset: offset,
    ));

    if (userSearch != null) {
      if (!userSearch.hasError()) {
        emit(UserSearchTrackBackSuccess(userSearch));
      } else {
        emit(UserSearchTrackBackFailed(userSearch.errorText));
      }
    } else {
      emit(UserSearchTrackBackFailed('Unknown error'));
    }
  }

}
