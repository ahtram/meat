import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/rest/timeline_mark_as_read_cubit.dart';
import 'package:meat/bloc/rest/users_me_cubit.dart';
import 'package:meat/bloc/ui/refresh_unread_count_cubit.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/theme/app_themes.dart';
import 'package:meat/widgets/other/themed_badge.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

class HomeAppBarTitle extends StatefulWidget {
  HomeAppBarTitle(this.isEnable, this.onFilterChanged, this.getCurrentOffset, {Key key})
      : super(key: key);

  final Function isEnable;
  final Function onFilterChanged;
  final Function getCurrentOffset;

  @override
  _HomeAppBarTitleState createState() => _HomeAppBarTitleState();
}

class _HomeAppBarTitleState extends State<HomeAppBarTitle> with AfterLayoutMixin {
  //A container for store how many notify count we have for each filter.

  //Match Official plurk unread order.
  //{"all": 2, "my": 1, "private": 1, "responded": 0}
  Plurdart.PollingUnreadCount _pollingUnreadCount;
  bool _isRefreshUnreadCount = false;
  bool _isMarkingAllRead = false;

  @override
  Widget build(BuildContext context) {
    return _buildTitle();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    //Try focus on comment.
    //Do the first refresh unread count.
    _refreshUnreadCount();
  }

  _refreshUnreadCount() async {
    if (!_isRefreshUnreadCount) {
      _isRefreshUnreadCount = true;
      _pollingUnreadCount = await Plurdart.pollingGetUnreadCount();
      if (_pollingUnreadCount != null && !_pollingUnreadCount.hasError()) {
        // print('_refreshUnreadCount: ' + prettyJson(_pollingUnreadCount.toJson()));
        setState(() {});
      }
      _isRefreshUnreadCount = false;
    }
  }

  _markAllAsRead() async {
    //Huh? ok... the web api is not working.
    //https://www.plurk.com/Responses/markAllRead
    // String result = await Plurdart.responsesMarkAllRead();
    // if (result != null) {
    //   print('Mark all read result: ' + result);
    //   await _refreshUnreadCount();
    // }

    //Ok here's the thing:
    //We get the current time offset from Home.
    //Use it to timelineGetUnreadPlurks() for limit getUnreadCount(Static.PlurksMainFilter.AllPlurks) to get the all unread plurk ids.
    //And then use timelineMarkAsRead() to mark them all read.
    //Easy?
    if (!_isMarkingAllRead) {
      setState(() {
        _isMarkingAllRead = true;
      });
      DateTime currentOffset = widget.getCurrentOffset();
      if (currentOffset != null) {
        //Try to get all plurks that is unread.
        Plurdart.Plurks plurks = await Plurdart.timelineGetUnreadPlurks(Plurdart.TimelineGetPlurks(
          // offset: currentOffset,
          filter: Static.usingPlurkFilter(),
          favorersDetail: false,
          limitedDetail: false,
          replurkersDetail: false,
        ));

        if (plurks != null && !plurks.hasError()) {
          print('plurks length: ' + plurks.plurks.length.toString());

          //Wait for
          BlocProvider.of<TimelineMarkAsReadCubit>(context).mark(plurks.plurks.map((plurk) => plurk.plurkId).toList());

          //[Dep]
          // Plurdart.Base base = await Plurdart.timelineMarkAsRead(Plurdart.TimelineMarkAsRead(
          //   ids: plurks.plurks.map((plurk) => plurk.plurkId).toList(),
          //   notePosition: true,
          // ));

          // if (base != null && !base.hasError()) {
          //   //Do a refresh count again.
          //   _refreshUnreadCount();
          // }
        }
      }
      setState(() {
        _isMarkingAllRead = false;
      });
    }
  }

  //I guess this is not the right way...
  // _subFromUnreadCount(int count) {
  //   if (_pollingUnreadCount != null) {
  //     setState(() {
  //       // print('_subOneFromUnreadCount: ' + Static.prefPlurksMainFilter().toString());
  //       switch (Static.prefPlurksMainFilter()) {
  //         case Static.PlurksMainFilter.AllPlurks:
  //           if (_pollingUnreadCount.all > 1) {
  //             _pollingUnreadCount.all -= count;
  //           }
  //           break;
  //         case Static.PlurksMainFilter.MyPlurks:
  //           _pollingUnreadCount.my -= count;
  //           if (_pollingUnreadCount.my < 0) {
  //             _pollingUnreadCount.my = 0;
  //           }
  //           break;
  //         case Static.PlurksMainFilter.Private:
  //           _pollingUnreadCount.private -= count;
  //           if (_pollingUnreadCount.private < 0) {
  //             _pollingUnreadCount.private = 0;
  //           }
  //           break;
  //         case Static.PlurksMainFilter.Responded:
  //           _pollingUnreadCount.responded -= count;
  //           if (_pollingUnreadCount.responded < 0) {
  //             _pollingUnreadCount.responded = 0;
  //           }
  //           break;
  //         case Static.PlurksMainFilter.Mentioned:
  //           _pollingUnreadCount.mentioned -= count;
  //           if (_pollingUnreadCount.mentioned < 0) {
  //             _pollingUnreadCount.mentioned = 0;
  //           }
  //           break;
  //         case Static.PlurksMainFilter.Liked:
  //           _pollingUnreadCount.favorite -= count;
  //           if (_pollingUnreadCount.favorite < 0) {
  //             _pollingUnreadCount.favorite = 0;
  //           }
  //           break;
  //         case Static.PlurksMainFilter.Replurked:
  //           _pollingUnreadCount.replurked -= count;
  //           if (_pollingUnreadCount.replurked < 0) {
  //             _pollingUnreadCount.replurked = 0;
  //           }
  //           break;
  //         default:
  //           break;
  //       }
  //     });
  //   }
  // }

  int getUnreadCount(Static.PlurksMainFilter plurksMainFilter) {
    if (_pollingUnreadCount != null) {
      switch (plurksMainFilter) {
        case Static.PlurksMainFilter.AllPlurks:
          return _pollingUnreadCount.all;
        case Static.PlurksMainFilter.MyPlurks:
          return _pollingUnreadCount.my;
        case Static.PlurksMainFilter.Private:
          return _pollingUnreadCount.private;
        case Static.PlurksMainFilter.Responded:
          return _pollingUnreadCount.responded;
        case Static.PlurksMainFilter.Mentioned:
          return _pollingUnreadCount.mentioned;
        case Static.PlurksMainFilter.Liked:
          return _pollingUnreadCount.favorite;
        case Static.PlurksMainFilter.Replurked:
          return _pollingUnreadCount.replurked;
        default:
          return 0;
      }
    }
    return 0;
  }

  Widget _buildTitle() {
    return MultiBlocListener(
      listeners: [
        BlocListener<RefreshUnreadCountCubit,
            RefreshUnreadCountState>(
          listener: (context, state) {
            if (state is RefreshUnreadCountRequest) {
              _refreshUnreadCount();
            }
          },
        ),
        BlocListener<TimelineMarkAsReadCubit, TimelineMarkAsReadState>(
          listener: (context, state) {
            if (state is TimelineMarkAsReadSuccess) {
              //Just do a refresh on any success...
              _refreshUnreadCount();
              //Nope.
              // _subFromUnreadCount(state.plurkIDs.length);
            }
          },
        ),
      ],
      child: BlocBuilder<UsersMeCubit, UsersMeState>(
        builder: (context, state) {
          //Check me.
          if (Static.me != null) {
            //Dropdown items.
            List<DropdownMenuItem<Static.PlurksMainFilter>> dropdownItems = [];

            //Hard-coded
            dropdownItems.add(_filterDropdownItem(
                Icons.home,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterAllPlurks'),
                getUnreadCount(Static.PlurksMainFilter.AllPlurks),
                Static.PlurksMainFilter.AllPlurks,
                false,
            ));
            dropdownItems.add(_filterDropdownItem(
                Icons.person,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterMyPlurks'),
                getUnreadCount(Static.PlurksMainFilter.MyPlurks),
                Static.PlurksMainFilter.MyPlurks,
                false,
            ));
            dropdownItems.add(_filterDropdownItem(
                Icons.lock,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterPrivate'),
                getUnreadCount(Static.PlurksMainFilter.Private),
                Static.PlurksMainFilter.Private,
                false,
            ));
            dropdownItems.add(_filterDropdownItem(
                Icons.message,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterResponded'),
                getUnreadCount(Static.PlurksMainFilter.Responded),
                Static.PlurksMainFilter.Responded,
                false,
            ));
            dropdownItems.add(_filterDropdownItem(
                Icons.cached,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterReplurked'),
                getUnreadCount(Static.PlurksMainFilter.Replurked),
                Static.PlurksMainFilter.Replurked,
                false,
            ));
            dropdownItems.add(_filterDropdownItem(
                Icons.favorite,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterLiked'),
                getUnreadCount(Static.PlurksMainFilter.Liked),
                Static.PlurksMainFilter.Liked,
                false,
            ));

            //Only for premium user...
            if (Static.me != null && Static.me.premium != null) {
              if (Static.me.premium) {
                dropdownItems.add(_filterDropdownItem(
                    Icons.alternate_email,
                    Theme.of(context).iconTheme.color,
                    FlutterI18n.translate(context, 'filterMentioned'),
                    getUnreadCount(Static.PlurksMainFilter.Mentioned),
                    Static.PlurksMainFilter.Mentioned,
                    false,
                ));
              }
            }

            //Extra friend filter need getCompletion to work correctly.
            //Extra filter: Friends
            dropdownItems.add(_filterDropdownItem(
                MdiIcons.accountMultiple,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterFriends'),
                0,
                Static.PlurksMainFilter.Friends,
                false,
            ));

            //Extra filter: Fans
            dropdownItems.add(_filterDropdownItem(
                MdiIcons.eye,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterFollowing'),
                0,
                Static.PlurksMainFilter.Following,
                false,
            ));

            //Extra filter: My Private
            dropdownItems.add(_filterDropdownItem(
                MdiIcons.notebook,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterMyPrivate'),
                0,
                Static.PlurksMainFilter.MyPrivate,
                false,
            ));

            //Extra filter: Spy
            dropdownItems.add(_filterDropdownItem(
                MdiIcons.magnifyPlus,
                Theme.of(context).iconTheme.color,
                FlutterI18n.translate(context, 'filterSpy'),
                0,
                Static.PlurksMainFilter.Spy,
                false,
            ));

            //Mark all as read button
            dropdownItems.add(_filterDropdownItem(
              Icons.check_circle,
              Theme.of(context).iconTheme.color,
              FlutterI18n.translate(context, 'fliterMarkAllAsRead'),
              0,
              Static.PlurksMainFilter.MarkAllAsRead,
              true,
            ));

            return DropdownButton(
                elevation: 8,
                value: Static.prefPlurksMainFilter(),
                underline: Container(),
                iconSize: 0,
                items: dropdownItems,
                dropdownColor: Theme.of(context).cardColor,
                onTap: () {
                  HapticFeedback.lightImpact();
                },
                onChanged: (value) {
                  //Special case: Some of the filter is actually not a filter.
                  if (value == Static.PlurksMainFilter.MarkAllAsRead) {
                    HapticFeedback.lightImpact();
                    _markAllAsRead();
                    return;
                  }

                  if (value != Static.prefPlurksMainFilter()) {
                    setState(() {
                      Static.savePlurksMainFilter(value);
                    });
                    widget.onFilterChanged(value);
                  }
                });
          } else {
            //Static.me not ready yet.
            // print('Static.me is null?!');
            return Container();
          }
        },
      ),
    );
  }

  // PopupMenuItem popupMenuItem(IconData icon, Color iconColor, String text, dynamic value,) {
  //   return PopupMenuItem(
  //     value: value,
  //     child: Container(
  //       child: Row(
  //         mainAxisSize: MainAxisSize.min,
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //         children: [
  //           Icon(icon,
  //             color: Theme.of(context).iconTheme.color,),
  //           SizedBox(
  //             width: 8,
  //           ),
  //           Container(
  //               child: Text(text)),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  //Build a dropdown item widget.
  DropdownMenuItem<Static.PlurksMainFilter> _filterDropdownItem(
      IconData icon,
      Color iconColor,
      String text,
      int unreadCount,
      Static.PlurksMainFilter value,
      bool buttonStyle,
      ) {

    Widget unreadNotify = Container();

    if (unreadCount > 0) {
      Color dotColor = Theme.of(context).lowContrastPrimaryColor;

      Widget markingAllReadIndicator = Container();

      if (_isMarkingAllRead) {
        markingAllReadIndicator = Container(
          width: 12,
          height: 12,
          child: Center(
            child: CircularProgressIndicator(
              strokeWidth: 3,
            ),
          ),
        );
      }

      unreadNotify = Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          ThemedBadge(dotColor, unreadCount.toString()),
          Positioned.fill(
            right: -22,
            child: Align(
                alignment: Alignment.centerRight,
                child: markingAllReadIndicator),
          )
        ],
      );
    }

    //With backgroundColor?
    Color backgroundColor = Colors.transparent;
    if (buttonStyle) {
      backgroundColor = Theme.of(context).lowContrastPrimaryColor;
    }

    return DropdownMenuItem<Static.PlurksMainFilter>(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(3),
        ),
        width: 230,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                icon,
                color: iconColor,
              ),
              SizedBox(
                width: 8,
              ),
              Flexible(
                child: Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 4), child: Text(text, overflow: TextOverflow.ellipsis,)),
              ),
              SizedBox(
                width: 8,
              ),
              unreadNotify
            ],
          ),
        ),
      ),
      value: value,
    );
  }
}
