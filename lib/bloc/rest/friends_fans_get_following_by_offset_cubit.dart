import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'friends_fans_get_following_by_offset_state.dart';

class FriendsFansGetFollowingByOffsetCubit extends Cubit<FriendsFansGetFollowingByOffsetState> {
  FriendsFansGetFollowingByOffsetCubit() : super(FriendsFansGetFollowingByOffsetInitial());

  request(int offset, int limit) async {
    List<Plurdart.User> users = await Plurdart.friendsFansGetFollowingByOffset(Plurdart.FriendsFansGetFollowingByOffset(
      offset: offset,
      limit: limit,
    ));

    if (users != null) {
      emit(FriendsFansGetFollowingByOffsetSuccess(users));
    } else {
      emit(FriendsFansGetFollowingByOffsetFailed());
    }
  }
}
