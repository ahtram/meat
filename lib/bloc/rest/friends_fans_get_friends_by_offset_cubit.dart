import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'friends_fans_get_friends_by_offset_state.dart';

class FriendsFansGetFriendsByOffsetCubit extends Cubit<FriendsFansGetFriendsByOffsetState> {
  FriendsFansGetFriendsByOffsetCubit() : super(FriendsFansGetFriendsByOffsetInitial());

  request(int userId, int offset, int limit) async {
    List<Plurdart.User> users = await Plurdart.friendsFansGetFriendsByOffset(Plurdart.FriendsFansGetFriendsFansByOffset(
      userId: userId,
      offset: offset,
      limit: limit,
    ));

    if (users != null) {
      emit(FriendsFansGetFriendsByOffsetSuccess(users));
    } else {
      emit(FriendsFansGetFriendsByOffsetFailed());
    }
  }
}
