part of 'users_me_cubit.dart';

@immutable
abstract class UsersMeState {}

class UsersMeInitial extends UsersMeState {}

class UsersMeSuccess extends UsersMeState {
  UsersMeSuccess(this.user);
  final Plurdart.User user;
}

class UsersMeFailed extends UsersMeState {}
