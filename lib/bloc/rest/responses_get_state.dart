part of 'responses_get_cubit.dart';

@immutable
class ResponsesGetState {
  ResponsesGetState(this.responses);
  final Plurdart.Responses responses;
}
