import 'package:flutter/cupertino.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:tuple/tuple.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

class ResponsesCollector {
  ResponsesCollector(
      {@required int plurkID, @required int initialResponseCount}) {
    _plurkID = plurkID;
    _topFloorIndex = 0;
    formatCacheSpace(initialResponseCount);
    _bottomCeilIndex = _responsesCache.length;
    // print('ResponsesCollector initialResponseCount[' + initialResponseCount.toString() + '] _topFloorIndex[' + _topFloorIndex.toString() + '] _bottomCeilIndex[' + _bottomCeilIndex.toString() + ']');
  }

  //This will be the iniitial load margin both for top and bottom sections.
  static const int LOAD_INITIAL_MARGIN = 5;
  static const int LOAD_MORE_COUNT = 50;

  //A Map which index by response's Id. This could be use to check any duplicate response.
  //This is mainly for handle Plurk's crazy responseGet API which give weird behavior.
  Map<int, Plurdart.Response> _responseMap = Map<int, Plurdart.Response>();

  //The actual displayable data. Possible contains null data which means the Response is not loaded yet...
  List<Tuple2<Plurdart.Response, Plurdart.User>> _responsesCache = [];

  //The two important loaded progress indicator.
  int _topFloorIndex;
  int _bottomCeilIndex;

  int _plurkID;

  bool _isLoading = false;

  //Ensure _responsesCache has enough space for the required space.
  formatCacheSpace(int space) {
    _responsesCache = [];
    for (int i = 0; i < space; ++i) {
      _responsesCache.add(null);
    }
  }

  bool isLoading() {
    return _isLoading;
  }

  //This will clear and re-setup the ResponseCollector.
  loadInitial() async {
    //Reset the index.
    _topFloorIndex = 0;
    _bottomCeilIndex = _responsesCache.length;

    //Two kind of situation:
    //If the known total responses count is greater than LOAD_INITIAL_MARGIN x 2
    //tt means we can just load once from top, otherwise we need to load top and bottom separately.
    if (_responsesCache.length <= LOAD_INITIAL_MARGIN * 2) {
      //Just load once (top -> down)
      await loadAll();
    } else {
      //Load twice (top -> down) and (bottom -> up)
      await loadTopDown(LOAD_INITIAL_MARGIN);
      await loadBottomUp(LOAD_INITIAL_MARGIN);
    }
  }

  //Load more from the top index down.
  loadTopDown([int count]) async {
    if (count == null) {
      count = LOAD_MORE_COUNT;
    }

    _isLoading = true;
    Plurdart.Responses responses = await _loadResponses(_topFloorIndex, count);
    _isLoading = false;

    if (responses != null) {
      List<Plurdart.Response> responsesList = responses.responses;
      // print('responsesList responsesList.length: ' + responsesList.length.toString());
      //Process the responses to map and cache.
      for (int p = 0; p < responsesList.length; ++p) {
        int responseIndex = _topFloorIndex + p;
        if (!_responseMap.containsKey(responsesList[p].id)) {
          //Not exist yet! Add this
          //Map
          _responseMap[responsesList[p].id] = responsesList[p];
          //FeedCache
          Plurdart.User responseOwner =
              responses.friends[responsesList[p].userId];

          //Expand one element.
          if (_responsesCache.length <= responseIndex) {
            _responsesCache.add(null);
          }

          //Check if this one is occupied.
          if (_responsesCache[responseIndex] != null) {
            // print('Oops! responseIndex[' +
            //     responseIndex.toString() +
            //     '] is occupied! Are you sure this is right?');
          }
          _responsesCache[responseIndex] =
              Tuple2(responsesList[p], responseOwner);
        } else {
          //Already exist! Ignore this one.
          //This maybe due to someone removed a response. In that case we should remove one empty element.
          if (responseIndex < _responsesCache.length &&
              _responsesCache[responseIndex] == null) {
            _responsesCache.removeAt(responseIndex);
          }
        }
      }
      //Move the top floor index down.
      _topFloorIndex += responsesList.length;
      // print('_topFloorIndex is now [' + _topFloorIndex.toString() + ']');
    }
  }

  loadAll() async {
    _isLoading = true;
    Plurdart.Responses responses = await _loadResponses(_topFloorIndex, 0);
    _isLoading = false;

    if (responses != null) {
      List<Plurdart.Response> responsesList = responses.responses;
      // print('responsesList responsesList.length: ' + responsesList.length.toString());
      //Process the responses to map and cache.
      for (int p = 0; p < responsesList.length; ++p) {
        int responseIndex = _topFloorIndex + p;
        if (!_responseMap.containsKey(responsesList[p].id)) {
          //Not exist yet! Add this
          //Map
          _responseMap[responsesList[p].id] = responsesList[p];
          //FeedCache
          Plurdart.User responseOwner =
          responses.friends[responsesList[p].userId];

          //Expand one element.
          if (_responsesCache.length <= responseIndex) {
            _responsesCache.add(null);
          }

          //Check if this one is occupied.
          if (_responsesCache[responseIndex] != null) {
            print('Oops! responseIndex[' +
                responseIndex.toString() +
                '] is occupied! Are you sure this is right?');
          }
          _responsesCache[responseIndex] =
              Tuple2(responsesList[p], responseOwner);
        } else {
          //Already exist! Ignore this one.
          //This maybe due to someone removed a response. In that case we should remove one empty element.
          if (responseIndex < _responsesCache.length &&
              _responsesCache[responseIndex] == null) {
            _responsesCache.removeAt(responseIndex);
          }
        }
      }
      //Move the top floor index down.
      _topFloorIndex += responsesList.length;
      // print('_topFloorIndex is now [' + _topFloorIndex.toString() + ']');

      //[Maybe?]
      //Check and remove empty element from cache. (The empty element maybe caused by removed responses)

    }
  }

  //Load more from the bottom index up.
  loadBottomUp([int count]) async {
    if (count == null) {
      count = LOAD_MORE_COUNT;
    }

    int bottomTrackBackIndex = _bottomCeilIndex - count;
    if (bottomTrackBackIndex < 0) {
      //Special fix for count and trackBackIndex;
      count += bottomTrackBackIndex;
      bottomTrackBackIndex = 0;
    }

    _isLoading = true;
    Plurdart.Responses responses =
        await _loadResponses(bottomTrackBackIndex, count);
    _isLoading = false;

    if (responses != null) {
      List<Plurdart.Response> responsesList = responses.responses;
      // print('loadBottomUp responsesList.length: ' + responsesList.length.toString());
      if (responsesList.length != count) {
        print('Oops! loadBottomUp got responsesList.length[' +
            responsesList.length.toString() +
            '] is not equal to count[' +
            count.toString() +
            ']. Cancel the load responses...');
      }
      //Process the responses to map and cache.
      for (int p = 0; p < responsesList.length; ++p) {
        int responseIndex = bottomTrackBackIndex + p;
        if (!_responseMap.containsKey(responsesList[p].id)) {
          //Not exist yet! Add this
          //Map
          _responseMap[responsesList[p].id] = responsesList[p];
          //FeedCache
          Plurdart.User responseOwner =
          responses.friends[responsesList[p].userId];

          //Expand one element.
          if (_responsesCache.length <= responseIndex) {
            _responsesCache.add(null);
          }

          //Check if this one is occupied.
          if (_responsesCache[responseIndex] != null) {
            // print('Oops! responseIndex[' +
            //     responseIndex.toString() +
            //     '] is occupied! Are you sure this is right?');
          }
          _responsesCache[responseIndex] =
              Tuple2(responsesList[p], responseOwner);
        } else {
          //Already exist! Ignore this one.
          //This maybe due to someone removed a response. In that case we should remove one empty element.
          if (responseIndex < _responsesCache.length &&
              _responsesCache[responseIndex] == null) {
            _responsesCache.removeAt(responseIndex);
          }
        }
      }
      //Move the bottom ceil index up.
      _bottomCeilIndex -= responsesList.length;
      // print('_bottomCeilIndex is now [' + _bottomCeilIndex.toString() + ']');
    }
  }

  //Load more from the end down.  Return the total response count.
  Future<int> loadBottomEndMore() async {
    int endIndex = _responsesCache.length;

    _isLoading = true;
    Plurdart.Responses responses = await _loadResponses(endIndex, LOAD_MORE_COUNT);
    _isLoading = false;

    if (responses != null) {
      List<Plurdart.Response> responsesList = responses.responses;
      // print('loadBottomEndMore responsesList.length: ' + responsesList.length.toString());
      //Process the responses to map and cache.
      for (int p = 0; p < responsesList.length; ++p) {
        int responseIndex = endIndex + p;
        if (!_responseMap.containsKey(responsesList[p].id)) {
          //Not exist yet! Add this
          // print('responsesList[p].id not exist! Add this one: [' + responsesList[p].id.toString() + ']');
          //Map
          _responseMap[responsesList[p].id] = responsesList[p];
          //FeedCache
          Plurdart.User responseOwner =
          responses.friends[responsesList[p].userId];

          //Expand one element.
          if (_responsesCache.length <= responseIndex) {
            _responsesCache.add(null);
          }

          //Check if this one is occupied.
          if (_responsesCache[responseIndex] != null) {
            // print('Oops! responseIndex[' +
            //     responseIndex.toString() +
            //     '] is occupied! Are you sure this is right?');
          }
          _responsesCache[responseIndex] =
              Tuple2(responsesList[p], responseOwner);
        } else {
          //Already exist! Ignore this one.
          // print('responsesList[p].id already exist! skip this one: [' + responsesList[p].id.toString() + ']');
        }
      }
      // print('_responsesCache length is now [' + _responsesCache.length.toString() + ']');
    }
    return _responsesCache.length;
  }

  //Will return the processed count
  Future<Plurdart.Responses> _loadResponses(int fromIndex, int count) async {
    Plurdart.Responses responses =
        await Plurdart.responsesGet(Plurdart.ResponsesGet(
      plurkId: _plurkID,
      fromResponse: fromIndex,
      count: count,
    ));

    //Collect Users to global User Cache.
    if (responses != null) {
      //Debug info
      // print('responsesGet: ' + prettyJson(responses.toJson()));

      //Try catch users.
      responses.friends.forEach((key, value) {
        Static.addToUserCache(value);
      });

      if (responses.hasError()) {
        print('OOps! responsesGet got error [' + responses.errorText + ']');
        return null;
      } else {
        return responses;
      }
    } else {
      return null;
    }
  }

  //Will return the total response count after deleted
  Future<int> requestDeleteResponse(Plurdart.Response response) async {
    _isLoading = true;
    Plurdart.Base base = await Plurdart.responsesResponseDelete(Plurdart.ResponsesResponseDelete(
      plurkId: response.plurkId,
      responseId: response.id,
    ));
    _isLoading = false;

    if (base != null) {
      if (!base.hasError()) {
        //_responseMap
        _responseMap.removeWhere((key, value) => value.id == response.id);

        //_responsesCache
        for (int i = _responsesCache.length - 1 ; i >= 0 ; --i) {
          if (_responsesCache[i] != null && _responsesCache[i].item1.id == response.id) {
            _responsesCache.removeAt(i);
            //Note that we should take care of
            // _topFloorIndex
            // and
            // _bottomCeilIndex
            if (i <= _topFloorIndex) {
              _topFloorIndex -= 1;
            }

            if (i <= _bottomCeilIndex) {
              _bottomCeilIndex -= 1;
            }
            break;
          }
        }
      }
    }

    return _responsesCache.length;
  }

  //Just update edited data for the caller
  updateEditResponse(int responseId, Plurdart.ResponseEditResult responseEditResult) {
    //Update map
    if (_responseMap.containsKey(responseId)) {
      Plurdart.Response response = _responseMap[responseId];
      if (response != null) {
        //Update the content.
        response.content = responseEditResult.content;
        response.contentRaw = responseEditResult.contentRaw;
        response.lastEdited = responseEditResult.lastEdited;
        response.editability = responseEditResult.editability;
      }
    }

    //_responsesCache
    for (int i = _responsesCache.length - 1 ; i >= 0 ; --i) {
      if (_responsesCache[i].item1.id == responseId) {
        //Update the content.
        _responsesCache[i].item1.content = responseEditResult.content;
        _responsesCache[i].item1.contentRaw = responseEditResult.contentRaw;
        _responsesCache[i].item1.lastEdited = responseEditResult.lastEdited;
        _responsesCache[i].item1.editability = responseEditResult.editability;
      }
    }
  }

  // Clear all.
  void clear() {
    _responseMap.clear();
    eraseCache();
  }

  //Erase the cache but keep the space.
  eraseCache() {
    for (int i = 0 ; i < _responsesCache.length ; ++i) {
      _responsesCache[i] = null;
    }
  }

  // A convenient function for getting the necessary info for display home feed.
  List<Tuple2<Plurdart.Response, Plurdart.User>> getFeed() {
    return _responsesCache;
  }

  // How many Responses do we have now?
  int getResponsesCount() {
    return _responsesCache.length;
  }
}
