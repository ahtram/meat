import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/plurk/response_card_sub_header.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:enum_to_string/enum_to_string.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/widgets/hack/HTextField.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

import 'emoticon_browser.dart';

enum QualifierMode {
  Plurk,
  Response,
}

abstract class BaseEditor {
  bool isInEmoticonsMode = false;
  bool isShowingEmoticonView = false;

  bool isUploadingPictures = false;

  FocusNode commentFocus = FocusNode();
  TextEditingController commentController = TextEditingController();

  //The qualifier selector.
  GlobalKey qualifierButtonKey = GlobalKey();
  OverlayEntry qualifierSelectorOverlayEntry;

  //Mention suggestion. (I have to hack to TextField to expose TextFieldState)
  GlobalKey<TextFieldState> commentTextFieldKey = GlobalKey<TextFieldState>();
  OverlayEntry mentionSuggestionOverlayEntry;
  String mentionQueryTerm = '';

  //For display upload progress LinearProgressIndicator
  double uploadProgressValue = 0.0;

  //This is stupid...but I think this'll work.
  //We need the actual soft keyboard height to use as Emoticon's height.
  //The system doesn't give the actual number so we are better measure it ourselves.
  double measuredSoftKeyboardHeight = kMinInteractiveDimension;
  QualifierMode qualifierMode = QualifierMode.Plurk;

  //TextField
  Widget buildCommentTextField(
    BuildContext context,
    bool showCharacterCounter,
    bool expands,
    bool readonly,
    bool enable,
    bool showProgressIndicator, {
    int minLines,
    int maxLines,
    Function onTap,
  }) {
    List<Widget> stackChildren = [];

    String hintText = '';
    if (!enable) {
      hintText = FlutterI18n.translate(context, 'labelResponseContentDisabled');
    } else {
      if (qualifierMode == QualifierMode.Plurk) {
        hintText = FlutterI18n.translate(context, 'labelPlurkContent');
      } else {
        hintText = FlutterI18n.translate(context, 'labelResponseContent');
      }
    }

    //Comment TextField.
    //(I have to hack to TextField to expose TextFieldState)
    stackChildren.add(TextField(
      key: commentTextFieldKey,
      expands: expands,
      readOnly: readonly, //isInEmoticonsMode,
      showCursor: true,
      enabled: enable, //!isSendingResponse && !isUploadingPictures,
      controller: commentController,
      focusNode: commentFocus,
      cursorColor: Theme.of(context).colorScheme.secondary,
      keyboardAppearance: Theme.of(context).brightness,
      decoration: InputDecoration(
        isDense: true,
        border: InputBorder.none,
        filled: true,
        contentPadding: EdgeInsets.fromLTRB(8, 0, 8, 2),
        hintText: hintText,
        counterText: showCharacterCounter
            ? (getCalculatedCharacterCount().toString() +
                '/' +
                Define.PLURK_MAX_CHARACTER_COUNT.toString())
            : null,
      ),
      minLines: minLines,
      maxLines: maxLines,
//        maxLength: MAX_CHARACTER_COUNT,
      onChanged: (value) {
        //Hide qualifier overlay on any changed.
        hideQualifierSelectorOverlayEntry();

        //Check and display mention suggestion.
        checkAndProvideMentionSuggestion(context);

        //For update the character count display.
        requestSetState(() {});
      },
      onTap: () {
        //Hide qualifier overlay on any changed.
        hideQualifierSelectorOverlayEntry();

        //Check and display mention suggestion.
        checkAndProvideMentionSuggestion(context);

        //Plurk poster will need this.
        onTap?.call();
      },
    ));

    //When a response is sending, show a circular indicator.
    if (showProgressIndicator) {
      stackChildren.add(Container(
        margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
        width: 16,
        height: 16,
        child: CircularProgressIndicator(),
      ));
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: stackChildren,
      ),
    );
  }

  //The tool bar.
  Widget buildUtilityBar(
      BuildContext context,
      IconData switchButtonIcon,
      Function onSwitchButtonPress,
      Function onQualifierButtonPress,
      Function onSendButtonPress,
      {Plurdart.PlurkQualifier forcePlurkQualifier}) {
    return Container(
      height: 36,
      padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //Switch button: color decide by (isInEmoticonsMode) (hard-coded)
          utilityButton(context, switchButtonIcon, onSwitchButtonPress,
              color: isInEmoticonsMode
                  ? Theme.of(context).colorScheme.secondary.withOpacity(0.3)
                  : null),

          SizedBox(
            width: 3,
          ),
          utilityButton(context, Icons.image, () {
            //Hide overlay.
            hideQualifierSelectorOverlayEntry();

            //Open image picker.
            pickAndUploadImages(context);

            Vibrator.vibrateShortNote();
          }),
          SizedBox(
            width: 3,
          ),
          VerticalDivider(
            width: 8,
          ),
          utilityButton(context, Icons.format_bold, () {
            //Hide overlay.
            hideQualifierSelectorOverlayEntry();
            if (sandwichSelectionWith(commentController, '**')) {
              Vibrator.vibrateShortNote();
            }
          }),
          SizedBox(
            width: 3,
          ),
          utilityButton(context, Icons.format_italic, () {
            //Hide overlay.
            hideQualifierSelectorOverlayEntry();
            if (sandwichSelectionWith(commentController, '*')) {
              Vibrator.vibrateShortNote();
            }
          }),
          SizedBox(
            width: 3,
          ),
          utilityButton(context, Icons.format_underlined, () {
            //Hide overlay.
            hideQualifierSelectorOverlayEntry();
            if (sandwichSelectionWith(commentController, '__')) {
              Vibrator.vibrateShortNote();
            }
          }),
          SizedBox(
            width: 3,
          ),
          utilityButton(context, Icons.format_strikethrough, () {
            //Hide overlay.
            hideQualifierSelectorOverlayEntry();
            if (sandwichSelectionWith(commentController, '--')) {
              Vibrator.vibrateShortNote();
            }
          }),
          VerticalDivider(
            width: 8,
          ),

          (qualifierMode == QualifierMode.Plurk)
              ? (
                  //Plurk mode.
                  Expanded(
                  flex: 1,
                  child: ElevatedButton(
                    key: qualifierButtonKey,
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Define.getResponseQualifierColor(
                          Static.prefResponseQualifier)
                          .withOpacity(0.4)),
                      padding: MaterialStateProperty.all(EdgeInsets.all(0)),
                    ),
                    child: Text(
                      forcePlurkQualifier == null
                          ? FlutterI18n.translate(
                              context,
                              EnumToString.convertToString(
                                  Static.prefPlurkQualifier))
                          : FlutterI18n.translate(
                              context,
                              EnumToString.convertToString(
                                  forcePlurkQualifier)),
                      style: TextStyle(
                        fontSize: 13,
                      ),
                    ),
                    onPressed: forcePlurkQualifier == null
                        ? onQualifierButtonPress
                        : null,
                  ),
                ))
              : (
                  //Response mode.
                  Expanded(
                  flex: 1,
                  child: ElevatedButton(
                    key: qualifierButtonKey,
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Define.getResponseQualifierColor(
                          Static.prefResponseQualifier)
                          .withOpacity(0.4)),
                      padding: MaterialStateProperty.all(EdgeInsets.all(0)),
                    ),
                    child: Text(
                      FlutterI18n.translate(
                          context,
                          EnumToString.convertToString(
                              Static.prefResponseQualifier)),
                      style: TextStyle(
                        fontSize: 13,
                      ),
                    ),
                    onPressed: onQualifierButtonPress,
                  ),
                )),

          Expanded(
            flex: 1,
            child: ElevatedButton(
              child: Text(
                FlutterI18n.translate(context, 'btnLabelSend'),
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              onPressed: onSendButtonPress,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildEmoticonView() {
    return Container(
      //The height is actually measured in runtime.. my beautiful hack.
      height: measuredSoftKeyboardHeight - kMinInteractiveDimension,
      child: EmoticonBrowser((String textCode) {
        if (!commentFocus.hasFocus) {
          commentFocus.requestFocus();
        }

        //Insert emoticon logic
        insertText(textCode);
        Vibrator.vibrateShortNote();
      }, () {
        if (!commentFocus.hasFocus) {
          commentFocus.requestFocus();
        }

        //Backspace logic
        tryBackspace();
        Vibrator.vibrateShortNote();
      }, () {
        if (!commentFocus.hasFocus) {
          commentFocus.requestFocus();
        }

        //Space tap
        insertText(' ');
        Vibrator.vibrateShortNote();
      }, () {
        if (!commentFocus.hasFocus) {
          commentFocus.requestFocus();
        }

        //Return tap
        insertText('\n');
        Vibrator.vibrateShortNote();
      }),
    );
  }

  bool sandwichSelectionWith(
      TextEditingController textEditingController, String toastText) {
    TextSelection textSelection = textEditingController.selection;

    if (textSelection.isValid) {
      textEditingController.text = textEditingController.text.replaceRange(
          textSelection.start,
          textSelection.end,
          toastText +
              textSelection.textInside(textEditingController.text) +
              toastText);
      textEditingController.selection = TextSelection(
        baseOffset: textSelection.start + toastText.length,
        extentOffset: textSelection.end + toastText.length,
      );
      requestSetState(() {});
      return true;
    }
    return false;
  }

  insertText(String text) {
    TextSelection textSelection = commentController.selection;
    if (textSelection.isValid) {
      commentController.text = commentController.text
          .replaceRange(textSelection.start, textSelection.end, text);
      commentController.selection = TextSelection(
        baseOffset: textSelection.start + text.length,
        extentOffset: textSelection.start + text.length,
      );
      requestSetState(() {});
    }
  }

  bool tryBackspace() {
    TextSelection textSelection = commentController.selection;
    if (textSelection.isValid) {
      //Check if it's 0 range.
      if (textSelection.start == textSelection.end) {
        //Is 0 range.
        //Try remove 1 character.
        if (textSelection.start > 0) {
          commentController.text = commentController.text
              .replaceRange(textSelection.start - 1, textSelection.start, '');
          commentController.selection = TextSelection(
            baseOffset: textSelection.start - 1,
            extentOffset: textSelection.start - 1,
          );
          requestSetState(() {});
          return true;
        }
        return false;
      } else {
        //Is selecting something.
        //Just remove the stuffs in the range.
        commentController.text = commentController.text
            .replaceRange(textSelection.start, textSelection.end, '');
        commentController.selection = TextSelection(
          baseOffset: textSelection.start,
          extentOffset: textSelection.start,
        );
        requestSetState(() {});
        return true;
      }
    }
    return false;
  }

  bool tryInsertNickNameStr(String nickNameStr) {
    //Check if the TextSelection is non select or 0 range.
    TextSelection textSelection = commentController.selection;

    if (textSelection.isValid) {
      //The thing we gonna insert into the TextField.
      String completeStrToInsert;
      //Check if we need to insert additional space prefix.
      if (textSelection.start > 0) {
        if (commentController.text
                .substring(textSelection.start - 1, textSelection.start) ==
            ' ') {
//          print('found leading space');
          completeStrToInsert = nickNameStr + ' ';
        } else {
//          print('leading space not found');
          completeStrToInsert = ' ' + nickNameStr + ' ';
        }
      } else {
        //At the start
        completeStrToInsert = nickNameStr + ' ';
      }

      commentController.text = commentController.text.replaceRange(
          textSelection.start, textSelection.end, completeStrToInsert);

      commentController.selection = TextSelection(
        baseOffset: textSelection.start + completeStrToInsert.length,
        extentOffset: textSelection.start + completeStrToInsert.length,
      );
      requestSetState(() {});

      //0 range?
//      if(textSelection.start == textSelection.end) {
//        print('is 0 range');
//      } else {
//        print('selecting something...');
//      }
    } else {
      //Not focus on the TextField so just append to the end of the TextField.
      //The thing we gonna insert into the TextField.
      String completeStrToInsert;
      if (commentController.text.length > 0) {
        if (commentController.text.substring(commentController.text.length - 2,
                commentController.text.length - 1) ==
            ' ') {
//          print('found leading space');
          completeStrToInsert = nickNameStr + ' ';
        } else {
//          print('leading space not found');
          completeStrToInsert = ' ' + nickNameStr + ' ';
        }
      } else {
        completeStrToInsert = nickNameStr + ' ';
      }
      requestSetState(() {});
      commentController.text += completeStrToInsert;
    }

    return true;
  }

  int getPlurkImageLinkMatchCount() {
    RegExp plurkImageRegExp = RegExp(
        r'https?:\/\/(images\.plurk\.com\/)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)');
    Iterable<RegExpMatch> allMatches =
        plurkImageRegExp.allMatches(commentController.text);
//    print('_getPlurkImageLinkMatchCount: ' + allMatches.length.toString());
    return allMatches.length;
  }

  int getCalculatedCharacterCount() {
    int matchCount = getPlurkImageLinkMatchCount();
    int linksTreatedCharacterCount =
        matchCount * Define.PLURK_IMAGE_LINK_TREATED_CHARACTER_COUNT;
    int linksActualCharacterCount =
        matchCount * Define.PLURK_IMAGE_LINK_ACTUAL_CHARACTER_COUNT;
    return commentController.text.length -
        linksActualCharacterCount +
        linksTreatedCharacterCount;
  }

  //Check the current word count to verify available space for image links.
  int availableSpaceForImageLinks() {
    //This could be complicate....
    return (Define.PLURK_MAX_CHARACTER_COUNT - getCalculatedCharacterCount()) ~/
        Define.PLURK_IMAGE_LINK_TREATED_CHARACTER_COUNT;
  }

  bool tryAppendImageLinkToComment(String link) {
    //Insert this link.
    commentController.text = commentController.text + '\n';
    commentController.text = commentController.text + link;
    commentController.text = commentController.text + '\n';
    return true;
  }

  static Widget utilityButton(
      BuildContext context, IconData iconData, Function onTap,
      {double width, Color color}) {
    return Container(
      width: width != null ? width : null,
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).hintColor.withOpacity(0.3),
          width: 0.5,
        ),
        borderRadius: BorderRadius.circular(4),
        color: color,
      ),
      child: InkWell(
        child: Container(
          padding: EdgeInsets.all(4),
          child: Center(
            child: Icon(
              iconData,
              size: 20,
            ),
          ),
        ),
        onTap: () {
          onTap?.call();
        },
      ),
    );
  }

  void tryUnfocusCommentTextField() {
    commentFocus?.unfocus(disposition: UnfocusDisposition.scope);
  }

//  void showUtilityBar(bool b) {
//    setState(() {
//      isShowingUtilityBar = b;
//    });
//  }

  //-- Qualifier selector overlay.

  showQualifierSelectorOverlayEntry(BuildContext context) {
    if (qualifierSelectorOverlayEntry == null &&
        qualifierButtonKey != null &&
        qualifierButtonKey.currentContext != null) {
      qualifierSelectorOverlayEntry =
          createQualifierSelectorOverlayEntry(context);
      Overlay.of(context).insert(qualifierSelectorOverlayEntry);
    }
  }

  hideQualifierSelectorOverlayEntry() {
    if (qualifierSelectorOverlayEntry != null) {
      qualifierSelectorOverlayEntry.remove();
      qualifierSelectorOverlayEntry = null;
    }
  }

  //Create the entry for select qualifier.
  OverlayEntry createQualifierSelectorOverlayEntry(BuildContext context) {
    RenderBox buttonRenderBox =
        qualifierButtonKey.currentContext.findRenderObject();
    Size buttonSize = buttonRenderBox.size;

    Offset tlOffset = buttonRenderBox.localToGlobal(Offset.zero);
//    Offset centerOffset = Offset(tlOffset.dx + buttonSize.width * 0.5, tlOffset.dy + buttonSize.height * 0.5);

    double menuWidth = buttonSize.width;
    double menuHeight = 280;

    return OverlayEntry(
        builder: (context) => Positioned(
              left: tlOffset.dx,
              top: tlOffset.dy - menuHeight - 4,
              width: menuWidth,
              height: menuHeight,
              child: Material(
                color: Colors.transparent,
                child: AcrylicContainer(
                  ClipRRect(
                    borderRadius: BorderRadius.circular(3),
                    child: ListView(
                      padding: EdgeInsets.all(0),
                      children: qualifierOverlayChildren(context, (value) {
                        if (qualifierMode == QualifierMode.Plurk) {
                          //Check if we should actually change.
                          if (onWantChangePlurkQualifier(value)) {
                            Static.savePlurkQualifier(value);
                          }
                        } else {
                          Static.saveResponseQualifier(value);
                        }

                        requestSetState(() {
                          hideQualifierSelectorOverlayEntry();
                        });
                        Vibrator.vibrateShortNote();
                      }),
                    ),
                  ),
                  initialImage: MemoryImage(kTransparentImage),
                  transparentBackLayer: false,
                  mode: AcrylicContainerMode.BackdropFilter,
                ),
              ),
            ));
  }

  List<Widget> qualifierOverlayChildren(
      BuildContext context, Function onQualifierTap) {
    List<Widget> widgets = [];

    //This is stupid but should work.
    if (qualifierMode == QualifierMode.Plurk) {
      //Plurk Mode.
      Plurdart.PlurkQualifier.values.forEach((qualifier) {
        widgets.add(GestureDetector(
          child: Container(
            height: 28,
            color: Define.getPlurkQualifierColor(qualifier).withOpacity(0.5),
            child: Center(
              child: Text(
                FlutterI18n.translate(
                    context, EnumToString.convertToString(qualifier)),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1.color,
                  fontSize: 13,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          onTap: () {
            onQualifierTap?.call(qualifier);
          },
        ));
      });
    } else {
      //Response Mode.
      Plurdart.ResponseQualifier.values.forEach((qualifier) {
        widgets.add(GestureDetector(
          child: Container(
            height: 28,
            color: Define.getResponseQualifierColor(qualifier).withOpacity(0.5),
            child: Center(
              child: Text(
                FlutterI18n.translate(
                    context, EnumToString.convertToString(qualifier)),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1.color,
                  fontSize: 13,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          onTap: () {
            onQualifierTap?.call(qualifier);
          },
        ));
      });
    }

    return widgets;
  }

  //-- Mention suggestion

  //This will replace a portion from an '@' character with a user's nickName.
  completeMentionSuggestion(Plurdart.User user) {
    if (commentController != null) {
      TextSelection textSelection = commentController.selection;
      //Check if the TextSelection is non select or 0 range.
      if (textSelection.isValid && textSelection.start == textSelection.end) {
        String strBeforeSelection =
            commentController.text.substring(0, textSelection.start);
        int atIndex = strBeforeSelection.lastIndexOf('@');

        if (atIndex != -1) {
          //Find start and end index to replace.
          int replaceRangeStart = atIndex + 1;

          int replaceRangeEnd = commentController.text
              .indexOf(RegExp(r'[ @\n\t-]'), textSelection.start);
          bool hasRangeEnd = false;
          if (replaceRangeEnd == -1) {
            replaceRangeEnd = textSelection.start;
            hasRangeEnd = false;
          } else {
            hasRangeEnd = true;
          }

          if (hasRangeEnd) {
            commentController.text = commentController.text.replaceRange(
                replaceRangeStart, replaceRangeEnd, user.nickName);
            commentController.selection = TextSelection(
              baseOffset: atIndex + 1 + user.nickName.length,
              extentOffset: atIndex + 1 + user.nickName.length,
            );
          } else {
            commentController.text = commentController.text.replaceRange(
                replaceRangeStart, replaceRangeEnd, user.nickName + ' ');
            commentController.selection = TextSelection(
              baseOffset: atIndex + 1 + user.nickName.length + 1,
              extentOffset: atIndex + 1 + user.nickName.length + 1,
            );
          }

          requestSetState(() {});
        }
      } //Ignore the case textSelection has a range.
    }
  }

  checkAndProvideMentionSuggestion(BuildContext context) {
    // (delay for one frame to get to the correct position)
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (commentController != null) {
        TextSelection textSelection = commentController.selection;
        //Check if the TextSelection is non select or 0 range.
        if (textSelection.isValid && textSelection.start == textSelection.end) {
          String strBeforeSelection =
              commentController.text.substring(0, textSelection.start);
          int atIndex = strBeforeSelection.lastIndexOf('@');
          if (atIndex != -1) {
            //Found an '@' someplace before cursor.
            //mentionTerm should be the term after '@'
            String mentionTerm = strBeforeSelection.substring(atIndex + 1);
            if (mentionTerm != null &&
                !mentionTerm.contains(' ') &&
                !mentionTerm.contains('\n')) {
              //This is a legal mention suggestion hint term.
//              print('[Legal mention suggestion term]: [' + mentionTerm + ']');
              provideMentionSuggestionOverlay(context, mentionTerm);
              return;
            }
          }
        } //Ignore the case textSelection has a range.
      }

      //No legal mention suggestion term found.
//      print('[No legal mention suggestion term]');
      hideMentionSuggestionOverlay();
    });
  }

  provideMentionSuggestionOverlay(BuildContext context, String mentionTerm) {
    mentionQueryTerm = mentionTerm;
    if (commentTextFieldKey != null &&
        commentTextFieldKey.currentContext != null) {
      if (mentionSuggestionOverlayEntry != null) {
        mentionSuggestionOverlayEntry.remove();
        mentionSuggestionOverlayEntry = null;
      }

      mentionSuggestionOverlayEntry =
          createMentionSuggestionOverlayEntry(context);
      Overlay.of(context).insert(mentionSuggestionOverlayEntry);
    }
  }

  hideMentionSuggestionOverlay() {
    mentionQueryTerm = '';
    if (mentionSuggestionOverlayEntry != null) {
      mentionSuggestionOverlayEntry.remove();
      mentionSuggestionOverlayEntry = null;
    }
  }

  OverlayEntry createMentionSuggestionOverlayEntry(BuildContext context) {
    RenderBox commentRenderBox =
        commentTextFieldKey.currentContext.findRenderObject();
    Size rootSize = commentRenderBox.size;

    Offset commentRenderBoxTLOffset =
        commentRenderBox.localToGlobal(Offset.zero);

//    print('tlOffset: ' + tlOffset.toString());

    List<Plurdart.User> suggestedUsers =
        Static.querySuggestionsByNickNameTerm(mentionQueryTerm);
    double itemHeight = 28;

    double menuWidth = rootSize.width;
    double menuHeight = suggestedUsers.length * itemHeight;

    //Limit the max menu height to n items.
    int limitItemCount = 4;
    double limitMenuHeight = itemHeight * limitItemCount;
    if (menuHeight > limitMenuHeight) {
      menuHeight = limitMenuHeight;
    }

    if (suggestedUsers.length > 0) {
      menuHeight += 2;
    }

    //The default is above comment render box's top
    Offset overlayTLOffset = Offset(commentRenderBoxTLOffset.dx,
        commentRenderBoxTLOffset.dy - menuHeight + 8);

    //I have to hack my way to get the renderEditable position.... Voodoo magic!
    RenderEditable renderEditable = commentTextFieldKey
        .currentState.editableTextKey.currentState.renderEditable;
    if (renderEditable != null) {
      TextPosition currentTextPosition =
          TextPosition(offset: renderEditable.selection.baseOffset);
      Rect rect = renderEditable.getLocalRectForCaret(currentTextPosition);

      // print('left: [' + rect.left.toString() + '] | top: [' + rect.top.toString() + ']');
      //Replace the top of tlOffet!!
      Offset cursorTLOffset =
          Offset(overlayTLOffset.dx, overlayTLOffset.dy + rect.top);

      if (cursorTLOffset.dy > 80) {
        //Over lay on top
        overlayTLOffset = Offset(cursorTLOffset.dx, cursorTLOffset.dy);
      } else {
        //Over lay on bottom
        overlayTLOffset =
            Offset(cursorTLOffset.dx, cursorTLOffset.dy + menuHeight + 20);
      }
    }

    return OverlayEntry(
        builder: (context) => Positioned(
              left: overlayTLOffset.dx,
              top: overlayTLOffset.dy,
              width: menuWidth,
              height: menuHeight,
              child: Material(
                color: Colors.transparent,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 8),
                  decoration: BoxDecoration(
                    border: Border.all(
                        width: 1, color: Theme.of(context).hintColor),
                  ),
                  child: AcrylicContainer(
                    Scrollbar(
                      child: SingleChildScrollView(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: mentionSuggestionOverlayChildren(
                              context, suggestedUsers, (user) {
                            //Complete mention suggestion.
                            completeMentionSuggestion(user);
                            hideMentionSuggestionOverlay();
                          }),
                        ),
                      ),
                    ),
                    initialImage: MemoryImage(kTransparentImage),
                    transparentBackLayer: false,
                    mode: AcrylicContainerMode.BackdropFilter,
                  ),
                ),
              ),
            ));
  }

  List<Widget> mentionSuggestionOverlayChildren(BuildContext context,
      List<Plurdart.User> users, Function onSuggestionTap) {
    List<Widget> children = [];

//    print('user length: ' + users.length.toString());

    users.forEach((user) {
      children.add(Material(
        color: Colors.transparent,
        child: InkWell(
          child: Container(
            height: 28,
            child: Row(
              children: [
                //NickName
                Text(
                  '@' + user.nickName,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 13,
                  ),
                ),

                SizedBox(
                  width: 12,
                ),

                //DisplayName (mention list don't display anonymous handle so null response)
                ResponseCardSubHeader.buildUserDisplayName(
                    context, null, user, false, null),
              ],
            ),
          ),
          onTap: () {
            onSuggestionTap?.call(user);
          },
        ),
      ));
    });

    return children;
  }

  //----- Upload images

  //This will open the image selector.
  pickAndUploadImages(BuildContext context) async {
    //Check if we still have space for image links.
    if (availableSpaceForImageLinks() <= 0) {
      //Not enough space. Notify the user.
      context.read<FlushBarCubit>().request(
          Icons.warning, 'Oops!', 'Not enough space for image links. :(');
      return;
    }

    try {
      ImagePicker _picker = ImagePicker();
      List<XFile> imageXFiles = await _picker.pickMultiImage();

      //Don't do anything if nothing selected.
      if (imageXFiles.length > 0) {
        //Display the progress bar.
        //Disable the comment TextField.

        List<String> links = [];

        // await Future.delayed(Duration(milliseconds: 300));

        //Cropping all images here.
        List<CroppedFile> croppedFiles = [];
        for (int i = 0; i < imageXFiles.length; ++i) {
          CroppedFile croppedFile = await ImageCropper().cropImage(
              sourcePath: imageXFiles[i].path,
              aspectRatioPresets: [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ],
              uiSettings: [
                AndroidUiSettings(
                    toolbarTitle: 'Cropper',
                    toolbarColor: Theme.of(context).primaryColorDark,
                    toolbarWidgetColor: Colors.white,
                    initAspectRatio: CropAspectRatioPreset.original,
                    lockAspectRatio: false),
                IOSUiSettings(
                  minimumAspectRatio: 1.0,
                )
              ],
          );

          if (croppedFile != null) {
            croppedFiles.add(croppedFile);
          }
        }

        requestSetState(() {
          isUploadingPictures = true;
          uploadProgressValue = 0.0;
        });

        //Upload the selected.
        for (int i = 0; i < croppedFiles.length; ++i) {

          //Update the progress bar progression.
          requestSetState(() {
            uploadProgressValue = (i + 1) / croppedFiles.length;
          });

          Uint8List imageBytes = await croppedFiles[i].readAsBytes();

          Plurdart.UploadPicture uploadPicture =
              await Plurdart.timelineUploadPicture(
                  Plurdart.TimelineUploadPicture(
            byteData: imageBytes.buffer.asByteData(),
            filename: imageXFiles[i].name,
          ));

          if (uploadPicture != null) {
            if (!uploadPicture.hasError()) {
              links.add(uploadPicture.full);
              print('Upload image success! [' +
                  imageXFiles[i].name +
                  '] [' +
                  uploadPicture.full +
                  ']');
            } else {
              print('Oops! Upload image failed! [' +
                  imageXFiles[i].name +
                  '] Reason: [' +
                  uploadPicture.errorText +
                  ']');
              context.read<FlushBarCubit>().request(Icons.warning,
                  'Oops! Upload image failed!', uploadPicture.errorText);
            }
          } else {
            print('Oops! Upload image failed! [' + imageXFiles[i].name + ']');
            context.read<FlushBarCubit>().request(Icons.warning,
                'Oops! Upload image failed!', imageXFiles[i].name);
          }
        }

        requestSetState(() {
          //Hide the progress bar.
          isUploadingPictures = false;
          uploadProgressValue = 0.0;
          //Put all the links onto the comment TextField.
          links.forEach((link) {
            if (!tryAppendImageLinkToComment(link)) {
              context.read<FlushBarCubit>().request(Icons.warning, 'Oops!',
                  'Max character limit reach! Cannot insert an image link!');
            }
          });
        });

        HapticFeedback.lightImpact();
        await Future.delayed(Duration(milliseconds: 200));

        //Focus the comment TextField.
        commentFocus.requestFocus();

//      print('[_pickAndUploadImages End][' + imageAssets.length.toString() + ']');
      }
    } on Exception catch (e) {
      print(e.toString());
      requestSetState(() {
        isUploadingPictures = false;
      });
    }
  }

  //----- Emoticon relative

  //A hacky way to see if the virtual is showing.
  bool isShowingKeyboard() {
    if (WidgetsBinding.instance.window.viewInsets.bottom < 200) {
      return false;
    } else {
      return true;
    }
  }

  toggleEmoticonMode(bool b) {
    if (b != isInEmoticonsMode) {
      requestSetState(() {
        isInEmoticonsMode = !isInEmoticonsMode;
      });

      if (b) {
        if (!commentFocus.hasFocus && !isShowingKeyboard()) {
          isShowingEmoticonView = true;
        }
      } else {
        if (!commentFocus.hasFocus && !isShowingKeyboard()) {
          isShowingEmoticonView = false;
        }
      }
    }
  }

  //--

  //The child class should override these.
  void requestSetState(Function fn);
  bool onWantChangePlurkQualifier(Plurdart.PlurkQualifier plurkQualifier) {
    return true;
  }
}
