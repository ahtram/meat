import 'package:flutter/material.dart';
import 'package:meat/screens/login.dart';
import 'package:meat/widgets/other/acrylic_container.dart';

class WelcomeLogin extends StatefulWidget {
  WelcomeLogin({Key key}) : super(key: key);

  @override
  _WelcomeLoginState createState() => _WelcomeLoginState();
}

class _WelcomeLoginState extends State<WelcomeLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      floatingActionButton: null,
      body: AcrylicContainer(
        Login(
          //On login success.
          () {
            Navigator.pushNamedAndRemoveUntil(
                context, '/home', (route) => false);
          },
        ),
        initialImage: AssetImage('assets/images/background001.jpg'),
      ),
    );
  }
}
