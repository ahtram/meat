import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';

void show(BuildContext context, {int plurkId, int responseId}) {
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _ReportAbuseDialog(plurkId, responseId);
  }));
}

class _ReportAbuseDialog extends StatefulWidget {
  _ReportAbuseDialog(this._reportPlurkId, this._reportResponseId);

  final int _reportPlurkId;
  final int _reportResponseId;

  @override
  _ReportAbuseDialogState createState() => _ReportAbuseDialogState();
}

class _ReportAbuseDialogState extends State<_ReportAbuseDialog> {
  bool _isSendingReport = false;

  TextEditingController _reasonController = TextEditingController();
  Plurdart.TimelineReportAbuse _timelineReportAbuse;

  @override
  void initState() {
    _timelineReportAbuse = Plurdart.TimelineReportAbuse(
      plurkId: widget._reportPlurkId,
      responseId: widget._reportResponseId,
      abuseCategory: Plurdart.AbuseCategory.Others,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return !_isSendingReport;
      },
      child: AcrylicScaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
            elevation: 0,
            titleSpacing: 0.0,
            leading: Container(),
            automaticallyImplyLeading: true,
            actions: [
              AppBarActionButton(Icons.close, onTap: () {
                if (!_isSendingReport) {
                  Navigator.pop(context);
                }
              }),
              SizedBox(
                width: 2,
              ),
            ],
            title: Text(
              'Report Abuse',
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
            ),
          ),
          child: Container(
            color: Theme.of(context).canvasColor.withOpacity(0.4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Report UI
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          //Type
                          ListTile(
                            title: Text(
                              'Abuse Type',
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            subtitle: Text(
                              EnumToString.convertToString(
                                  _timelineReportAbuse.abuseCategory),
                            ),
                            onTap: _showAbuseTypeDialog,
                          ),

                          //Input field
                          TextField(
                            controller: _reasonController,
                            keyboardAppearance: Theme.of(context).brightness,
                            readOnly: _isSendingReport,
                            showCursor: true,
                            decoration: InputDecoration(
                              isDense: true,
                              border: InputBorder.none,
                              filled: true,
                              contentPadding: EdgeInsets.fromLTRB(8, 0, 8, 2),
                              hintText: 'Reason',
                            ),
                            keyboardType: TextInputType.multiline,
                            minLines: 1,
                            maxLines: 5,
                          ),

                          Divider(),

                          //Info text.
                          Text(
                            'The report will be reviewed according to Plurk Terms of Service and we do not guarantee the plurk will eventually be removed. Please be advise that abusive reports might get your Karma decreased.',
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                //Report button.
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                  child: ElevatedButton(
                      child: Text('Report'),
                      onPressed: () async {
                        //Send Report.
                        if (_reasonController.text.isEmpty == false) {
                          setState(() {
                            _isSendingReport = true;
                            _timelineReportAbuse.reason = _reasonController.text;
                            FocusScope.of(context)
                                .unfocus(disposition: UnfocusDisposition.scope);
                          });

                          //We don't rely on Cubit here.
                          Plurdart.Base base = await Plurdart.timelineReportAbuse(_timelineReportAbuse);

                          setState(() {
                            _isSendingReport = false;
                          });

                          //Check the result.
                          if (base.hasError()) {
                            context.read<FlushBarCubit>().request(
                                Icons.warning,
                                'Oops! Cannot send report!',
                                'Keyword: ' + base.errorText);
                          } else {
                            Navigator.pop(context);
                            context.read<FlushBarCubit>().request(
                                Icons.check_circle,
                                'Success!',
                                'Report Sent');
                          }
                        }
                      }),
                ),
              ],
            ),
          )),
    );
  }

  _showAbuseTypeDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          List<Widget> dialogChildren = [];
          for (int i = 0; i < Plurdart.AbuseCategory.values.length; ++i) {
            dialogChildren.add(ListTile(
              title: Text(EnumToString.convertToString(
                  Plurdart.AbuseCategory.values[i])),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  _timelineReportAbuse.abuseCategory =
                      Plurdart.AbuseCategory.values[i];
                });
              },
            ));
          }
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: dialogChildren,
            // [
            //   //On
            //   ListTile(
            //     leading: Icon(
            //       (true == Static.settingsAcrylicEffect)
            //           ? Icons.radio_button_checked
            //           : Icons.radio_button_unchecked,
            //       color: Colors.grey,
            //     ),
            //     title: Text(settingsStrOnOff[true]),
            //     onTap: () {
            //       Navigator.pop(context);
            //       if (Static.settingsAcrylicEffect != true) {
            //         Static.saveAcrylicEffect(true);
            //         BlocProvider.of<SettingsAcrylicChangedCubit>(context).success();
            //       }
            //     },
            //   ),
            //   //Off
            //   ListTile(
            //     leading: Icon(
            //       (false == Static.settingsAcrylicEffect)
            //           ? Icons.radio_button_checked
            //           : Icons.radio_button_unchecked,
            //       color: Colors.grey,
            //     ),
            //     title: Text(settingsStrOnOff[false]),
            //     onTap: () {
            //       Navigator.pop(context);
            //       if (Static.settingsAcrylicEffect != false) {
            //         setState(() {
            //           Static.saveAcrylicEffect(false);
            //         });
            //         BlocProvider.of<SettingsAcrylicChangedCubit>(context).success();
            //       }
            //     },
            //   ),
            //
            // ],
          );
        });
  }
}
