import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'stats_top_favorites_state.dart';

class StatsTopFavoritesCubit extends Cubit<StatsTopFavoritesState> {
  StatsTopFavoritesCubit() : super(StatsTopFavoritesInitial());

  request() async {
    Plurdart.TopPlurks topPlurks = await Plurdart.statsTopFavorites(Plurdart.StatsTop(lang: Static.apiLanguage));
    if (topPlurks != null) {
      emit(StatsTopFavoritesSuccess(topPlurks));
    } else {
      emit(StatsTopFavoritesFailed());
    }
  }
}
