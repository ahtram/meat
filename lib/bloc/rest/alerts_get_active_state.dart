part of 'alerts_get_active_cubit.dart';

@immutable
abstract class AlertsGetActiveState {}

class AlertsGetActiveInitial extends AlertsGetActiveState {}

class AlertsGetActiveSuccess extends AlertsGetActiveState {
  AlertsGetActiveSuccess(this.alerts);
  final List<Plurdart.Alert> alerts;
}

class AlertsGetActiveFailed extends AlertsGetActiveState {

}
