//[Dep]

// import 'dart:io';
//
// import 'package:after_layout/after_layout.dart';
// import 'package:flutter/material.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:meat/system/define.dart' as Define;
// import 'package:shimmer/shimmer.dart';
//
// class CommentAdWidget extends StatefulWidget {
//   CommentAdWidget({Key key}) : super(key: key);
//
//   @override
//   _CommentAdWidgetState createState() => _CommentAdWidgetState();
// }
//
// class _CommentAdWidgetState extends State<CommentAdWidget>
//     with AfterLayoutMixin {
//   BannerAd _anchoredAdaptiveAd;
//   bool _isAdLoaded = false;
//   AdSize _adSize;
//
//   double get _adWidth => MediaQuery.of(context).size.width;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   void afterFirstLayout(BuildContext context) async {
//     await _loadAd();
//   }
//
//   Future<void> _loadAd() async {
//     await _anchoredAdaptiveAd?.dispose();
//
//     if (!mounted) return;
//
//     setState(() {
//       _anchoredAdaptiveAd = null;
//     });
//
//     // Get an inline adaptive size for the current orientation.
//     AdSize size =
//         await AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
//             _adWidth.truncate());
//
//     if (size == null) {
//       print('Unable to get height of anchored banner.');
//       return;
//     }
//
//     if (!mounted) return;
//
//     //Load banner.
//     if (mounted) {
//       _anchoredAdaptiveAd = BannerAd(
//         adUnitId: Define.commentADUnitID(),
//         size: size,
//         request: AdRequest(),
//         listener: BannerAdListener(
//           onAdLoaded: (Ad ad) async {
//             // After the ad is loaded, get the platform ad size and use it to
//             // update the height of the container. This is necessary because the
//             // height can change after the ad is loaded.
//             BannerAd bannerAd = (ad as BannerAd);
//             final AdSize size = await bannerAd.getPlatformAdSize();
//             if (size == null) {
//               print('Error: getPlatformAdSize() returned null for $bannerAd');
//               return;
//             }
//
//             _anchoredAdaptiveAd = bannerAd;
//             _isAdLoaded = true;
//             _adSize = size;
//
//             if (mounted) {
//               setState(() {
//
//               });
//             }
//           },
//           onAdFailedToLoad: (Ad ad, LoadAdError error) {
//             ad.dispose();
//           },
//         ),
//       );
//       await _anchoredAdaptiveAd.load();
//     }
//   }
//
//   @override
//   void dispose() {
//     //Dispose banner.
//     _anchoredAdaptiveAd?.dispose();
//     _anchoredAdaptiveAd = null;
//     _isAdLoaded = false;
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Widget ad;
//     if (_anchoredAdaptiveAd != null && _isAdLoaded) {
//       ad = AdWidget(ad: _anchoredAdaptiveAd);
//     } else {
//       ad = _shimmerLoading();
//     }
//
//     double adHeight = 64;
//     if (Platform.isIOS) {
//       adHeight = 59;
//     }
//     if (_adSize != null) {
//       adHeight = _adSize.height.toDouble();
//     }
//
//     return Column(
//       mainAxisSize: MainAxisSize.min,
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         Container(
//           height: adHeight,
//           child: ad,
//         ),
//         Divider(height: 0.5, thickness: 0.5),
//       ],
//     );
//   }
//
//   Widget _shimmerLoading() {
//     return Shimmer.fromColors(
//         child: Container(
//           padding: EdgeInsets.all(2),
//           decoration: BoxDecoration(
//             border: Border.all(
//               color: Colors.white,
//               width: 4,
//             ),
//           ),
//           child: Column(
//             children: [
//               Flexible(
//                 flex: 3,
//                 child: Row(
//                   children: [
//                     Flexible(
//                       flex: 2,
//                       child: Container(
//                         margin: EdgeInsets.all(2),
//                         color: Colors.white,
//                       ),
//                     ),
//                     Flexible(
//                       flex: 3,
//                       child: Column(
//                         children: [
//                           Flexible(
//                             child: Container(
//                               margin: EdgeInsets.all(2),
//                               color: Colors.white,
//                             ),
//                             flex: 1,
//                           ),
//                           Flexible(
//                             child: Container(
//                               margin: EdgeInsets.all(2),
//                               color: Colors.white,
//                             ),
//                             flex: 1,
//                           ),
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//         baseColor: Theme.of(context).colorScheme.primary.withOpacity(0.4),
//         highlightColor: Theme.of(context).colorScheme.secondary);
//   }
// }
