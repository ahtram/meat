import 'package:after_layout/after_layout.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/ui/add_account_cubit.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/bloc/ui/plurk_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/plurk_post_complete_cubit.dart';
import 'package:meat/system/routegen.dart';
import 'package:meat/bloc/ui/theme_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/theme/app_themes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:meat/system/static_stuffs.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:meat/system/static_stuffs.dart' as Static;

import 'bloc/rest/blocks_block_cubit.dart';
import 'bloc/rest/blocks_unblock_cubit.dart';
import 'bloc/rest/emoticons_add_from_url_cubit.dart';
import 'bloc/rest/emoticons_delete_cubit.dart';
import 'bloc/rest/emoticons_get_cubit.dart';
import 'bloc/rest/timeline_get_plurks_cubit.dart';
import 'bloc/rest/timeline_mark_as_read_cubit.dart';
import 'bloc/rest/timeline_plurk_delete_cubit.dart';
import 'bloc/rest/users_me_cubit.dart';
import 'bloc/ui/alert_number_changed_cubit.dart';
import 'bloc/ui/emoticon_ask_add_cubit.dart';
import 'bloc/ui/emoticon_ask_delete_cubit.dart';
import 'bloc/ui/emoticon_notify_owned_cubit.dart';
import 'bloc/ui/flush_bar_cubit.dart';
import 'bloc/ui/home_refresh_cubit.dart';
import 'bloc/ui/image_links_viewer_cubit.dart';
import 'bloc/ui/login_cubit.dart';
import 'bloc/ui/logout_cubit.dart';
import 'bloc/ui/plurk_got_new_response_cubit.dart';
import 'bloc/ui/plurk_viewer_cubit.dart';
import 'bloc/ui/refresh_unread_count_cubit.dart';
import 'bloc/ui/report_abuse_dialog_cubit.dart';
import 'bloc/ui/response_edit_complete_cubit.dart';
import 'bloc/ui/select_account_cubit.dart';
import 'bloc/ui/user_viewer_cubit.dart';
import 'bloc/ui/users_browser_cubit.dart';
import 'bloc/ui/users_selected_cubit.dart';
import 'bloc/ui/youtube_fullscreen_cubit.dart';
import 'package:meat/bloc/ui/settings_acrylic_changed_cubit.dart';
import 'package:meat/bloc/ui/settings_download_full_image_changed_cubit.dart';

void main() {
  runApp(BahKutTeh());
}

class BahKutTeh extends StatefulWidget {
  BahKutTeh();

  @override
  _BahKutTehState createState() => _BahKutTehState();
}

class _BahKutTehState extends State<BahKutTeh>
    with AfterLayoutMixin, WidgetsBindingObserver {
  _BahKutTehState();

  Future<ThemeChanged> _restoreThemeFuture;
//  ThemeMode _rootMaterialAppThemeMode = ThemeMode.system;
  BuildContext _materialAppContext;

  @override
  void afterFirstLayout(BuildContext context) async {
    //These should only be done once. Set and lock to portrait orientation.
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    //timeAgo need to set support language...
    //I'll just load them all for now.
    timeago.setLocaleMessages('en', timeago.EnMessages());
    timeago.setLocaleMessages('en_US', timeago.EnMessages());
    timeago.setLocaleMessages('zh', timeago.ZhMessages());
    timeago.setLocaleMessages('zh_TW', timeago.ZhMessages());
    timeago.setLocaleMessages('zh_CN', timeago.ZhCnMessages());
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    // Get the future which will read the saved AppTheme.
    _restoreThemeFuture = _tryPrepareInitialThemeBlocEvent();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    //Called when app pause or resume...
    // This could solve a stupid flutter bug...
    // (light theme Navigation buttons turned white when resume)
    if (state == AppLifecycleState.resumed) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (_materialAppContext != null) {
          _setSystemUiOverlayStyle(_materialAppContext);
        }
      });
    }
  }

  Future<ThemeChanged> _tryPrepareInitialThemeBlocEvent() async {
    //The theme setting is store in prefs so this should be our first time load prefs.
    await Static.loadPrefs();
    return ThemeChanged(
        appTheme: Static.settingsAppTheme,
        themeMode: Static.settingsThemeMode,
        textScale: Static.settingsTextScale,
        canvasColor: Static.settingsCanvasColor);
  }

  @override
  Widget build(BuildContext context) {
    // We may need a future builder to make sure restore theme before ThemeBloc()
    // is construct to prevent additional widget rebuild.
    return FutureBuilder(
      future: _restoreThemeFuture,
      builder: (BuildContext context, AsyncSnapshot<ThemeChanged> snapShot) {
        if (snapShot.connectionState == ConnectionState.done) {
          // Finish reading AppTheme.
          return MultiBlocProvider(
            providers: [
              BlocProvider<ThemeBloc>(
                create: (BuildContext context) => ThemeBloc(),
              ),
              BlocProvider<LoginCubit>(
                create: (BuildContext context) => LoginCubit(),
              ),
              BlocProvider<LogoutCubit>(
                create: (BuildContext context) => LogoutCubit(),
              ),
              BlocProvider<AddAccountCubit>(
                create: (BuildContext context) => AddAccountCubit(),
              ),
              BlocProvider<SelectAccountCubit>(
                create: (BuildContext context) => SelectAccountCubit(),
              ),
              BlocProvider<UsersMeCubit>(
                create: (BuildContext context) => UsersMeCubit(),
              ),
              BlocProvider<YoutubeFullscreenCubit>(
                create: (BuildContext context) => YoutubeFullscreenCubit(),
              ),
              BlocProvider<ImageLinksViewerCubit>(
                create: (BuildContext context) => ImageLinksViewerCubit(),
              ),
              BlocProvider<PlurkViewerCubit>(
                create: (BuildContext context) => PlurkViewerCubit(),
              ),
              BlocProvider<UserViewerCubit>(
                create: (BuildContext context) => UserViewerCubit(),
              ),
              BlocProvider<UsersBrowserCubit>(
                create: (BuildContext context) => UsersBrowserCubit(),
              ),
              BlocProvider<ReportAbuseDialogCubit>(
                create: (BuildContext context) => ReportAbuseDialogCubit(),
              ),
              BlocProvider<EmoticonsGetCubit>(
                  create: (BuildContext context) => EmoticonsGetCubit()),
              BlocProvider<EmoticonAskAddCubit>(
                  create: (BuildContext context) => EmoticonAskAddCubit()),
              BlocProvider<EmoticonAskDeleteCubit>(
                  create: (BuildContext context) => EmoticonAskDeleteCubit()),
              BlocProvider<EmoticonNotifyOwnedCubit>(
                  create: (BuildContext context) => EmoticonNotifyOwnedCubit()),
              BlocProvider<EmoticonsAddFromUrlCubit>(
                  create: (BuildContext context) => EmoticonsAddFromUrlCubit()),
              BlocProvider<EmoticonsDeleteCubit>(
                  create: (BuildContext context) => EmoticonsDeleteCubit()),
              BlocProvider<FlushBarCubit>(
                  create: (BuildContext context) => FlushBarCubit()),
              BlocProvider<PlurkPostCompleteCubit>(
                  create: (BuildContext context) => PlurkPostCompleteCubit()),
              BlocProvider<PlurkEditCompleteCubit>(
                  create: (BuildContext context) => PlurkEditCompleteCubit()),
              BlocProvider<ResponseEditCompleteCubit>(
                  create: (BuildContext context) =>
                      ResponseEditCompleteCubit()),
              BlocProvider<UsersSelectedCubit>(
                  create: (BuildContext context) => UsersSelectedCubit()),
              BlocProvider<TimelinePlurkDeleteCubit>(
                create: (BuildContext context) => TimelinePlurkDeleteCubit(),
              ),
              BlocProvider<SettingsAcrylicChangedCubit>(
                create: (BuildContext context) => SettingsAcrylicChangedCubit(),
              ),
              BlocProvider<SettingsDownloadFullImageChangedCubit>(
                create: (BuildContext context) =>
                    SettingsDownloadFullImageChangedCubit(),
              ),
              BlocProvider<AlertNumberChangedCubit>(
                create: (BuildContext context) => AlertNumberChangedCubit(),
              ),
              BlocProvider<HomeRefreshCubit>(
                create: (BuildContext context) => HomeRefreshCubit(),
              ),
              BlocProvider<BlocksBlockCubit>(
                create: (BuildContext context) => BlocksBlockCubit(),
              ),
              BlocProvider<BlocksUnblockCubit>(
                create: (BuildContext context) => BlocksUnblockCubit(),
              ),
              BlocProvider<OpenUriCubit>(
                create: (BuildContext context) => OpenUriCubit(),
              ),
              BlocProvider<RefreshUnreadCountCubit>(
                create: (BuildContext context) => RefreshUnreadCountCubit(),
              ),
              BlocProvider<PlurkGotNewResponseCubit>(
                create: (BuildContext context) => PlurkGotNewResponseCubit(),
              ),
              BlocProvider<TimelineMarkAsReadCubit>(
                create: (context) => TimelineMarkAsReadCubit(),
              ),
              BlocProvider<TimelineGetPlurksCubit>(
                create: (BuildContext context) => TimelineGetPlurksCubit(),
              ),
            ],
            child: BlocBuilder<ThemeBloc, ThemeState>(
              builder: _buildMaterialAppWithTheme,
            ),
          );
        } else {
          // Reading saved AppTheme...(waiting screen)
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: Scaffold(
              backgroundColor: Colors.black,
              appBar: null,
              resizeToAvoidBottomInset: true,
              body: Container(),
            ),
          );
        }
      },
    );
  }

  Widget _buildMaterialAppWithTheme(BuildContext context, ThemeState state) {
    //Get theme data for later use.
    ThemeData lightTheme = getThemeData(
        Brightness.light, state.appTheme, state.canvasColor, state.textScale);
    ThemeData darkTheme = getThemeData(
        Brightness.dark, state.appTheme, state.canvasColor, state.textScale);

    //This is stupid but I cannot find a better way for now.
    rootMaterialAppThemeMode = state.themeMode;

    print('[_buildMaterialAppWithTheme][' +
        EnumToString.convertToString(lightTheme.platform) +
        '][' +
        EnumToString.convertToString(darkTheme.platform) +
        ']');

    return MaterialApp(
      localizationsDelegates: [
        FlutterI18nDelegate(
          translationLoader: FileTranslationLoader(
              useCountryCode: true,
              basePath: 'assets/locales',
              fallbackFile: 'en'),
          missingTranslationHandler: (key, locale) {
            print(
                "--- Missing Key: $key, languageCode: ${locale.languageCode}");
          },
        ),
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('en', 'US'),
        const Locale('zh', ''),
        const Locale('zh', 'TW'),
        const Locale('zh', 'CN'),
        // ... other locales the app supports
      ],
      builder: _rootBuilder,
      // This will decide the looks of the app.
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: state.themeMode,
      // This will start the app in Launch page.
      initialRoute: '/',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: generateRoute,
    );
  }

  Widget _rootBuilder(BuildContext context, Widget child) {
    //Cache the context...
    _materialAppContext = context;

    // This should be done AFTER BUILD. (1 frame after build)
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _setSystemUiOverlayStyle(context);
    });

    //Run i18nAppRootBuilder (localization)
    TransitionBuilder i18nAppRootBuilder = FlutterI18n.rootAppBuilder();
    return i18nAppRootBuilder(context, child);
  }

  _setSystemUiOverlayStyle(BuildContext context) {
    //Set the navigationBar / statusBar color.
    bool useDarkTheme = false;
    if (rootMaterialAppThemeMode == ThemeMode.dark) {
      useDarkTheme = true;
    } else if (rootMaterialAppThemeMode == ThemeMode.light) {
      useDarkTheme = false;
    } else {
      //system
      Brightness brightnessValue = MediaQuery.of(context).platformBrightness;
      if (brightnessValue == Brightness.dark) {
        useDarkTheme = true;
      } else {
        useDarkTheme = false;
      }
    }

    ThemeData currentTheme = Theme.of(context);
    // Actually set the theme of navigationBar and statueBar.
    if (useDarkTheme) {
      // print('[Dark]');
      SystemUiOverlayStyle newStyle = SystemUiOverlayStyle(
        systemNavigationBarColor: currentTheme.canvasColor.withOpacity(0.5),
        systemNavigationBarDividerColor:
            currentTheme.colorScheme.secondary.withOpacity(0.5),
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarColor: currentTheme.canvasColor.withOpacity(0.5),
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
      );
      SystemChrome.setSystemUIOverlayStyle(newStyle);
    } else {
      // print('[Light]');
      SystemUiOverlayStyle newStyle = SystemUiOverlayStyle(
        systemNavigationBarColor: currentTheme.canvasColor.withOpacity(0.5),
        systemNavigationBarDividerColor:
            currentTheme.colorScheme.secondary.withOpacity(0.5),
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarColor: currentTheme.canvasColor.withOpacity(0.5),
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
      );
      SystemChrome.setSystemUIOverlayStyle(newStyle);
    }
  }
}
