import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'request_indicator_state.dart';

class RequestIndicatorCubit extends Cubit<RequestIndicatorState> {
  RequestIndicatorCubit() : super(RequestIndicatorInitial());

  void show() {
    emit(RequestIndicatorShow());
  }

  void hide() {
    emit(RequestIndicatorHide());
  }
}
