import 'package:enum_to_string/enum_to_string.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:plurdart/plurdart.dart' as Plurdart;

//The will form a bahKutTeh url.
class NotificationPayload {
  Plurdart.AlertType alertType;
  Map<String, dynamic /*String|Iterable<String>*/ > queryParameters;

  NotificationPayload(
      this.alertType,
      {
        this.queryParameters,
      }
  );

  //Check if this uri is a valid payload.
  static bool isValid(Uri uri) {
    Plurdart.AlertType alertType = EnumToString.fromString(Plurdart.AlertType.values,  uri.host);
    if (alertType != null) {
      return true;
    }
    return false;
  }

  NotificationPayload.fromUri(Uri uri) {
    alertType = EnumToString.fromString(Plurdart.AlertType.values,  uri.host);
    queryParameters = uri.queryParameters;
  }

  NotificationPayload.fromString(String url) {
    Uri uri = Uri.parse(url);
    alertType = EnumToString.fromString(Plurdart.AlertType.values,  uri.host);
    queryParameters = uri.queryParameters;
  }

  //Uri example: bahkutteh://notifications/?data1=str1&data2=str2
  Uri toUri() {
    // print('toUri host [' + this.host.toString() + ']');
    return Uri(scheme: Define.bktScheme, host: EnumToString.convertToString(this.alertType), queryParameters: this.queryParameters);
  }

  String toString() {
    // print('toString host [' + toUri().toString() + ']');
    return toUri().toString();
  }

}