import 'package:after_layout/after_layout.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:http/http.dart' as Http;
import 'package:universal_html/html.dart' as Html;
import 'package:http/http.dart';
import 'package:universal_html/parsing.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

Http.Client _client = Http.Client();

enum PlurkPasteViewType {
  Summary,
  Expanded
}

class PlurkPasteView extends StatefulWidget {
  const PlurkPasteView(this.plurkCardPasteLink, this.type, this.onLinkOpen, {Key key}) : super(key: key);

  final PlurkCardPasteLink plurkCardPasteLink;
  final PlurkPasteViewType type;
  final Function(Uri) onLinkOpen;

  @override
  _PlurkPasteViewState createState() => _PlurkPasteViewState();
}

class _PlurkPasteViewState extends State<PlurkPasteView> with AfterLayoutMixin {
  String _htmlContent;
  bool _fetchDone = false;

  @override
  void afterFirstLayout(BuildContext context) {
    if (widget.type == PlurkPasteViewType.Expanded) {
      _fetchPasteHtmlContent();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.type == PlurkPasteViewType.Expanded) {
      if (!_fetchDone) {
        _fetchPasteHtmlContent();
      }
    }
  }

  _fetchPasteHtmlContent() async {
    try {
      Response res = await _client.get(widget.plurkCardPasteLink.uri);
      if (mounted) {
        if (res.statusCode == HttpStatus.ok) {
          Html.HtmlDocument htmlDocument = parseHtmlDocument(res.body);
          Html.Element codeElement = htmlDocument.querySelector('.syntax');
          if (codeElement != null) {
            // print('[Found code element!]: ' + codeElement.tagName + ' | ' + codeElement.className + ' : ' + codeElement.innerHtml);
            //This is the essential html text content we want.
            setState(() {
              _htmlContent = codeElement.innerHtml;
            });
          }
        } else {
          print('Oops! _fetchPasteHtmlContent got status [' +
              res.statusCode.toString() +
              ']');
        }
      }
      _fetchDone = true;
    } catch (e) {
      //The server doesn't define any error code for now!
      print('Oops! _fetchPasteHtmlContent got error [' + e.toString() + ']');
    }
  }

  @override
  Widget build(BuildContext context) {
    // Widget
    Widget contentWidget = Container();
    if (widget.type == PlurkPasteViewType.Expanded) {
      if (_fetchDone) {
        contentWidget = HtmlWidget(
          _htmlContent,
          enableCaching: true,
          textStyle: Theme.of(context).textTheme.bodyText1,
          onTapUrl: (url) {
            //!!!!!!!!!! Warning!!! This thing is very tricky! It only store the first closure !!!!!!!!!!
            //!!!!!!!!!! You can consider this thing static! We'll try make a static StreamController for this!!!!!!!!
            //https://stackoverflow.com/questions/15717681/dart-how-to-create-listen-and-emits-custom-event

            // print('Static onTapUrl: ' + url);
            Static.htmlWidgetUrlTapController.add(url);
            return true;
          },
        );
      } else {
        //Still fetching...
        contentWidget = Container(
          child: Center(
            child: SizedBox(
              width: 128,
              height: 128,
              child: FlareActor(
                "assets/rive/MeatLoader.flr",
                animation: "Untitled",
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
        );
      }
    } else {
      contentWidget = Text(widget.plurkCardPasteLink.title);
    }

    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            width: 0.5,
            color: Theme.of(context).hintColor,
          ),
          color: Theme.of(context).canvasColor.withOpacity(0.33),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            //Upper row
            Container(
              color: Theme.of(context).primaryColor.withOpacity(0.5),
              padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
              child: Text('Plurk Paste'),
            ),

            //Bottom content.
            Container(
              padding: EdgeInsets.fromLTRB(16, 4, 16, 10),
              child: contentWidget
            ),
          ],
        ),
      ),
      onTap: () async {
        //User tap this tag. Open the url for him.
        widget.onLinkOpen?.call(widget.plurkCardPasteLink.uri);
      }
    );
  }
}
