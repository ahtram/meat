part of 'friends_fans_get_following_by_offset_cubit.dart';

@immutable
abstract class FriendsFansGetFollowingByOffsetState {}

class FriendsFansGetFollowingByOffsetInitial extends FriendsFansGetFollowingByOffsetState {}

class FriendsFansGetFollowingByOffsetSuccess extends FriendsFansGetFollowingByOffsetState {
  FriendsFansGetFollowingByOffsetSuccess(this.users);
  final List<Plurdart.User> users;
}

class FriendsFansGetFollowingByOffsetFailed extends FriendsFansGetFollowingByOffsetState {}
