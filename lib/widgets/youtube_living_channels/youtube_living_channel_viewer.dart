import 'package:flutter/material.dart';
import 'package:meat/models/chikkutteh/res/living_channel.dart';
import 'package:meat/widgets/other/empty_logo.dart';
import 'package:meat/widgets/youtube_living_channels/youtube_living_channel_card.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

class YoutubeLivingChannelViewer extends StatefulWidget {
  YoutubeLivingChannelViewer(this.livingChannels);

  final List<LivingChannel> livingChannels;

  @override
  _YoutubeLivingChannelViewerState createState() => _YoutubeLivingChannelViewerState();
}

class _YoutubeLivingChannelViewerState extends State<YoutubeLivingChannelViewer> {

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    Color indicatorColor = Theme.of(context).textTheme.bodyText1.color;

    return SmartRefresher(
      controller: _refreshController,
      header: WaterDropMaterialHeader(
        color: indicatorColor,
      ),
      onRefresh: _onRefresh,
      child: ListView.builder(
        itemBuilder: (context, index) {
          if (widget.livingChannels.length > 0) {
            if (index < widget.livingChannels.length) {
              return YoutubeLivingChannelCard(widget.livingChannels[index]);
            } else if (index == widget.livingChannels.length) {
              return SizedBox(
                height: kMinInteractiveDimension,
              );
            }
          } else {
            if (index == 0) {
              return EmptyLogo();
            }
          }
          return null;
        },
      ),
    );
  }

  //--

  _onRefresh() async {
    await Static.updateLivingChannelsResCache();
    _refreshController.refreshCompleted();
    setState(() {});
  }
}
