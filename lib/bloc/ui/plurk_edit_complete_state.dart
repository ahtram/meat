part of 'plurk_edit_complete_cubit.dart';

@immutable
abstract class PlurkEditCompleteState {}

class PlurkEditCompleteInitial extends PlurkEditCompleteState {}

class PlurkEditCompleteSuccess extends PlurkEditCompleteState {
  PlurkEditCompleteSuccess(this.plurk);
  final Plurdart.Plurk plurk;
}