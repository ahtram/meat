import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'bookmarks_get_bookmarks_state.dart';

class BookmarksGetBookmarksCubit extends Cubit<BookmarksGetBookmarksState> {
  BookmarksGetBookmarksCubit() : super(BookmarksGetBookmarksInitial());

  //tag can be an empty string.
  request(String tag) async {
    List<String> outputTags;
    if (tag != null) {
      outputTags = [tag];
    }
    Plurdart.Bookmarks bookmarks = await Plurdart.bookmarksGetBookmarks(Plurdart.BookmarksGetBookmarks(
      tags: outputTags,
    ));

    if (bookmarks != null) {
      if (!bookmarks.hasError()) {
        emit(BookmarksGetBookmarksSuccess(bookmarks));
      } else {
        emit(BookmarksGetBookmarksFailed(bookmarks.errorText));
      }
    } else {
      emit(BookmarksGetBookmarksFailed('Unknown error'));
    }
  }

}
