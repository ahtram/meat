part of 'theme_bloc.dart';

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();
}

class ThemeChanged extends ThemeEvent {
  final AppTheme appTheme;
  final ThemeMode themeMode;
  final double textScale;
  final CanvasColor canvasColor;

  ThemeChanged(
      {@required this.appTheme,
      @required this.themeMode,
      @required this.textScale,
      @required this.canvasColor});

  @override
  List<Object> get props => [appTheme, themeMode, textScale, canvasColor];
}
