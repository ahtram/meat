part of 'viewer_background_switch_cubit.dart';

@immutable
abstract class ViewerBackgroundSwitchState {}

class ViewerBackgroundSwitchInitial extends ViewerBackgroundSwitchState {}

class ViewerBackgroundSwitchActive extends ViewerBackgroundSwitchState {
  ViewerBackgroundSwitchActive(this.image);
  final Uint8List image;
}
