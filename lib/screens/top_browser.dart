
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/plurk/plurk_cards_viewer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TopBrowser extends StatefulWidget {
  TopBrowser({Key key}) : super(key: key);

  @override
  _TopBrowserState createState() => _TopBrowserState();
}

class _TopBrowserState extends State<TopBrowser> {

  BuildContext scaffoldBodyContext;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: MultiBlocListener(
        listeners: [
          BlocListener<FlushBarCubit, FlushBarState>(
            listener: (context, state) {
              if (state is FlushBarRequest) {
                //Only display FlushBar when I am the current route.
                if (ModalRoute.of(context).isCurrent) {
                  _showFlushBar(scaffoldBodyContext, state.icon, state.title,
                      state.message);
                }
              }
            },
          ),
        ],
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              centerTitle: true,
              brightness: Theme.of(context).brightness,
              backgroundColor: Colors.transparent,
              flexibleSpace: AcrylicContainer(
                Container(),
                backgroundColor: Theme.of(context).primaryColor,
                transparentBackLayer: false,
                mode: AcrylicContainerMode.BackdropFilter,
              ),
              elevation: 0,
              titleSpacing: 0.0,
              leading: AppBarActionButton(Icons.arrow_back, onTap: () {
                // context.read<HomeRefreshCubit>().request();
                Navigator.pop(context);
              }),
              automaticallyImplyLeading: false,
              title: TabBar(
                // controller: _tabController,
                tabs: [
                  Tab(
                    child: Text(
                        FlutterI18n.translate(
                            context, 'topReplurks'),
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'topFavorites'),
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'topResponses'),
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                        fontSize: 13,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            body: Builder(
              builder: (context) {
                scaffoldBodyContext = context;
                return Container(
                  // color: Theme.of(context).canvasColor.withOpacity(0.4),
                  child: Column(
                    children: [
                      Expanded(
                        child: Container(
                          child: TabBarView(
                            children: [

                              //Top Replurks.
                              PlurkCardsViewer(
                                PlurkCardsViewerArgs(
                                  BrowsingType.TopReplurks,
                                  true,
                                  hasAppBar: false,
                                ),
                              ),

                              //Top Favorites.
                              PlurkCardsViewer(
                                PlurkCardsViewerArgs(
                                  BrowsingType.TopFavorites,
                                  true,
                                  hasAppBar: false,
                                ),
                              ),

                              //Top Responses.
                              PlurkCardsViewer(
                                PlurkCardsViewerArgs(
                                  BrowsingType.TopResponses,
                                  true,
                                  hasAppBar: false,
                                ),
                              ),

                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                );
              },
            ),

            ),
      ),
    );
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }
}