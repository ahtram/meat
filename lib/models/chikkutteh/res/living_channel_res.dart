import 'package:meat/models/chikkutteh/res/living_channel.dart';

class LivingChannelsRes {

  Map<String, List<LivingChannel>> channels;

  LivingChannelsRes({this.channels});

  LivingChannelsRes.fromJson(Map<String, dynamic> json) {
    channels = Map<String, List<LivingChannel>>();
    if (json != null) {
      json.forEach((key, value) {
        List<LivingChannel> newList = [];
        value.forEach((livingChannel) {
            if (livingChannel != null) {
              newList.add(LivingChannel.fromJson(livingChannel));
            }
          }
        );
        channels[key] = newList;
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (channels != null) {
      channels.forEach((key, livingChannels) {
        data[key] = [];
        livingChannels.forEach((element) {
          data[key].add(element.toJson());
        });
      });
    }
    return data;
  }

  int totalLivingCount() {
    int returnCount = 0;
    if (channels != null) {
      channels.forEach((key, livingChannels) {
        returnCount += livingChannels.length;
      });
    }
    return returnCount;
  }

  int livingCount(String lang) {
    if (channels != null) {
      if (channels.containsKey(lang)) {
        return channels[lang].length;
      }
    }
    return 0;
  }

  List<LivingChannel> livingChannels(String lang) {
    if (channels != null) {
      if (channels.containsKey(lang)) {
        return channels[lang];
      }
    }
    return [];
  }

}