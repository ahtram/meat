import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'response_edit_complete_state.dart';

class ResponseEditCompleteCubit extends Cubit<ResponseEditCompleteState> {
  ResponseEditCompleteCubit() : super(ResponseEditCompleteInitial());

  success(int plurkId, int responseId, Plurdart.ResponseEditResult responseEditResult) {
    emit(ResponseEditCompleteSuccess(plurkId, responseId, responseEditResult));
  }

  failed(String errorText) {
    emit(ResponseEditCompleteFailed(errorText));
  }
}
