import 'package:flutter/material.dart';
import 'package:meat/screens/about.dart';
import 'package:meat/screens/alert_browser.dart';
import 'package:meat/screens/anonymous_browser.dart';
import 'package:meat/screens/bookmarks.dart';
import 'package:meat/screens/bookmarks_browser.dart';
import 'package:meat/screens/friends_and_fans.dart';
import 'package:meat/screens/hot_links_browser.dart';
import 'package:meat/screens/launch.dart';
import 'package:meat/screens/official_news_browser.dart';
import 'package:meat/screens/qr_viewer.dart';
import 'package:meat/screens/search_browser.dart';
import 'package:meat/screens/top_browser.dart';
import 'package:meat/screens/vt_watch_browser.dart';
import 'package:meat/screens/welcome_login.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/screens/home.dart';
import 'package:meat/screens/settings.dart';
import 'package:meat/screens/theme_playground.dart';

//Reference: https://www.youtube.com/watch?v=nyvwx7o277U
Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return new MaterialPageRoute(
        builder: (context) => Launch(),
        settings: settings,
      );
    case '/welcome_login':
      return new TransparentRoute(
        builder: (context) => WelcomeLogin(),
        settings: settings,
      );
    case '/theme_playground':
      return new TransparentRoute(
        builder: (context) => ThemePlayground(),
        settings: settings,
      );
    case '/home':
      return new MaterialPageRoute(
        builder: (context) => Home(),
        settings: settings,
      );
    case '/home/alert_browser':
      return new TransparentRoute(
        builder: (context) => AlertBrowser(),
        settings: settings,
      );
      //[Dep]: Replace with more dynamic way.
//    case '/home/youtube_fullscreen_viewer':   //For playing fullscreen youtube video.
//      return new MaterialPageRoute(
//        builder: (context) => YoutubeFullscreenViewer(settings.arguments),
//        settings: settings,
//      );
      //[Dep]: Replace with more dynamic way.
//    case '/home/image_links_viewer':   //For image links viewer.
//      return new MaterialPageRoute(
//        builder: (context) => ImageLinksViewer(settings.arguments),
//        settings: settings,
//      );
    case '/home/top_browser':
      return new MaterialPageRoute(
        builder: (context) => TopBrowser(),
        settings: settings,
      );
    case '/home/anonymous_browser':
      return new MaterialPageRoute(
        builder: (context) => AnonymousBrowser(),
        settings: settings,
      );
    case '/home/hot_links_browser':
      return new MaterialPageRoute(
        builder: (context) => HotLinksBrowser(),
        settings: settings,
      );
    case '/home/search':
      return new MaterialPageRoute(
        builder: (context) => SearchBrowser(SearchBrowserArgs('')),
        settings: settings,
      );
    case '/home/official_news':
      return new MaterialPageRoute(
        builder: (context) => OfficialNewsBrowser(),
        settings: settings,
      );
    case '/home/friends_and_fans':
      return new MaterialPageRoute(
        builder: (context) => FriendsAndFans(),
        settings: settings,
      );
    case '/home/bookmarks':
      return new MaterialPageRoute(
        builder: (context) => Bookmarks(),
        settings: settings,
      );
    case '/home/bookmarks_browser':
      return new MaterialPageRoute(
        builder: (context) => BookmarksBrowser(settings.arguments),
        settings: settings,
      );
    // case '/home/cliques':
    //   return new TransparentRoute(
    //     builder: (context) => Cliques(),
    //     settings: settings,
    //   );
    case '/home/vt_watch_browser':
      return new MaterialPageRoute(
        builder: (context) => VtWatchBrowser(),
        settings: settings,
      );
    case '/home/about':
      return new MaterialPageRoute(
        builder: (context) => About(),
        settings: settings,
      );
    case '/home/settings':
      return new TransparentRoute(
        builder: (context) => Settings(),
        settings: settings,
      );
    case '/home/qr_viewer':
      return new MaterialPageRoute(
        builder: (context) => QrViewer(),
        settings: settings,
      );
    //[Dep]: Replace with more dynamic way.
//    case '/home/plurk_viewer':   //For image links viewer.
//      return new MaterialPageRoute(
//        builder: (context) => PlurkViewer(settings.arguments),
//        settings: settings,
//      );
    //[Dep]: Replace with more dynamic way.
//     case '/home/plurk_poster':
//       return new TransparentRoute(
//         builder: (context) => PlurkPoster(),
//         settings: settings,
//       );
    default:
      return _errorRoute();
  }
}

//The default return Route: An error page.
Route<dynamic> _errorRoute() {
  return MaterialPageRoute(
      settings: RouteSettings(name: 'error'),
      builder: (context) => Scaffold(
            appBar: null,
            body: SafeArea(
              child: Center(
                child: Text('Error!'),
              ),
            ),
          ));
}
