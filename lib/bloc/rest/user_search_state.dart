part of 'user_search_cubit.dart';

@immutable
abstract class UserSearchState {}

class UserSearchInitial extends UserSearchState {}

class UserSearchSuccess extends UserSearchState {
  UserSearchSuccess(this.userSearch);
  final Plurdart.UserSearch userSearch;
}

class UserSearchFailed extends UserSearchState {
  UserSearchFailed(this.errorText);
  final String errorText;
}

class UserSearchTrackBackSuccess extends UserSearchState {
  UserSearchTrackBackSuccess(this.userSearch);
  final Plurdart.UserSearch userSearch;
}

class UserSearchTrackBackFailed extends UserSearchState {
  UserSearchTrackBackFailed(this.errorText);
  final String errorText;
}

