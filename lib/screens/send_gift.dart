import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:pretty_json/pretty_json.dart';
import 'package:quantity_input/quantity_input.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/screens/busy.dart' as Busy;
import 'package:fluttertoast/fluttertoast.dart';

void show(BuildContext context, {int userId, int plurkId, int responseId}) {
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _SendGiftDialog(userId: userId, plurkId: plurkId, responseId: responseId,);
  }));
}

class _SendGiftDialog extends StatefulWidget {
  _SendGiftDialog({this.userId, this.plurkId, this.responseId});

  final int userId;
  final int plurkId;
  final int responseId;

  @override
  _SendGiftDialogState createState() => _SendGiftDialogState();
}

class _SendGiftDialogState extends State<_SendGiftDialog> {
  bool _isSending = false;

  TextEditingController _messageController = TextEditingController();

  //The editing quantity.
  int _plurkCoinMin = 0;
  int _plurkCoinMax = 0;
  int _plurkCoinQuantity = 0;

  bool _isAnonymous = false;

  @override
  void initState() {
    //Decide the quantity input.
    if (Static.balance != null) {
      if (Static.balance.bones > 1) {
        _plurkCoinMin = 1;
        _plurkCoinMax = Static.balance.bones - 1;
        _plurkCoinQuantity = 1;
      } else {
        _plurkCoinMin = 0;
        _plurkCoinMax = 0;
        _plurkCoinQuantity = 0;
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async {
        return !_isSending;
      },
      child: GestureDetector(
        child: AcrylicScaffold(
            resizeToAvoidBottomInset: true,
            transparentColor: Theme.of(context).canvasColor.withOpacity(0.4),
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0,
              titleSpacing: 0.0,
              leading: Container(),
              automaticallyImplyLeading: true,
              actions: [
                AppBarActionButton(Icons.close, onTap: () {
                  if (!_isSending) {
                    Navigator.pop(context);
                  }
                }),
                SizedBox(
                  width: 2,
                ),
              ],
            ),
            child: SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  color: Colors.transparent,
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //Send gift UI
                      SizedBox(
                        height: 24,
                      ),

                      Image.asset(
                        'assets/images/plurk_coin.png',
                        scale: 0.9,
                      ),

                      SizedBox(
                        height: 16,
                      ),

                      Text(
                        FlutterI18n.translate(context, 'labelSendPlurkCoin'),
                        style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold),
                      ),

                      SizedBox(
                        height: 24,
                      ),

                      QuantityInput(
                          value: _plurkCoinQuantity,
                          minValue: _plurkCoinMin,
                          maxValue: _plurkCoinMax,
                          onChanged: (value) {
                            setState(() =>
                            _plurkCoinQuantity =
                                int.parse(value.replaceAll(',', '')));
                          }
                      ),

                      SizedBox(
                        height: 24,
                      ),

                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        child: Text(
                          Define.localizedStrReplaceParams(FlutterI18n.translate(context, 'sendPlurkCoinInfo'),
                            paramA: Static.balance.bones.toString(),
                            paramB: _plurkCoinMax.toString(),
                          ),
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),

                      SizedBox(
                        height: 24,
                      ),

                    ],
                  ),
                ),
              ),
            ),
          bottomNavigationBar: BottomAppBar(
            elevation: 0,
            color: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  // Input UIs here...
                  Divider(),

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    child: _settingsViewItem(
                        FlutterI18n.translate(context, 'labelSendGiftAnonymously'), true,
                        trailing: Switch(
                          value: _isAnonymous,
                          onChanged: (value) {
                            _setAnonymous(value);
                          },
                        ), onItemTap: () {
                      _setAnonymous(!_isAnonymous);
                    }),
                  ),

                  //Input field
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    child: TextField(
                      controller: _messageController,
                      keyboardAppearance: Theme.of(context).brightness,
                      readOnly: _isSending,
                      showCursor: true,
                      decoration: InputDecoration(
                        isDense: true,
                        border: InputBorder.none,
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(8, 0, 8, 2),
                        hintText: FlutterI18n.translate(context, 'labelLeaveAMessage'),
                      ),
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                    ),
                  ),

                  //Send button.
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    child: ElevatedButton(
                        child: Text(FlutterI18n.translate(context, 'btnLabelSend')),
                        onPressed: () async {
                          //Send Gift.
                          Busy.show(context);

                          setState(() {
                            _isSending = true;
                            FocusScope.of(context)
                                .unfocus(disposition: UnfocusDisposition.scope);
                          });

                          Plurdart.PremiumSendGift premiumSendGift = Plurdart.PremiumSendGift(
                            userId: widget.userId,
                            plurkId: widget.plurkId,
                            responseId: widget.responseId,
                            quantity: _plurkCoinQuantity,
                            message: _messageController.text,
                            sendAsAnonymous: _isAnonymous,
                          );

                          print(prettyJson(premiumSendGift.toBody()));

                          //We don't rely on Cubit here.
                          Plurdart.PremiumSendGiftRes premiumSendGiftRes = await Plurdart.premiumSendGift(premiumSendGift);

                          // Hide Busy.
                          Navigator.pop(context);

                          setState(() {
                            _isSending = false;
                          });

                          //Check the result.
                          if (premiumSendGiftRes.hasError()) {
                            Fluttertoast.showToast(
                                msg: 'Error: ' + premiumSendGiftRes.errorText,
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Theme.of(context).backgroundColor,
                                textColor: Theme.of(context).textTheme.bodyText1.color,
                                fontSize: Theme.of(context).textTheme.subtitle2.fontSize
                            );
                          } else {
                            Navigator.pop(context);
                            context.read<FlushBarCubit>().request(
                                Icons.check_circle,
                                'Success!',
                                'Gift Sent');
                          }
                        }),
                  ),

                  SizedBox(
                    height: MediaQuery.of(context).viewInsets.bottom,
                  ),
                ],
              ),
            ),
          ),
        ),
        onTap: () {
          // Lose focus when tap background.
          setState(() {
            FocusScope.of(context)
                .unfocus(disposition: UnfocusDisposition.scope);
          });
        },
      ),
    );
  }

  //A customizable settings view item.
  Widget _settingsViewItem(String title, bool titleIsBold,
      {Widget trailing, EdgeInsets padding, Function onItemTap}) {
    List<Widget> rowChildren = [];

    rowChildren.add(Expanded(
      child: Text(
        title,
        style: TextStyle(
            fontWeight: titleIsBold ? FontWeight.bold : FontWeight.normal,
            fontSize: 13),
      ),
    ));

    if (trailing != null) {
      rowChildren.add(trailing);
    }

    return //Main option.
      Container(
        padding: padding == null ? EdgeInsets.all(0) : padding,
        height: 30,
        child: InkWell(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: rowChildren,
          ),
          onTap: onItemTap,
        ),
      );
  }

  _setAnonymous(bool value) {
    setState(() {
      _isAnonymous = value;
    });
  }

}
