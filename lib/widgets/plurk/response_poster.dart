import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import 'package:meat/bloc/rest/responses_response_add_cubit.dart';
import 'package:meat/bloc/rest/responses_response_edit_cubit.dart';
import 'package:meat/bloc/ui/response_edit_complete_cubit.dart';

import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:plurdart/plurdart.dart' as Plurdart;

import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';

import 'base_editor.dart';

enum ResponsePosterMode { NewResponse, EditResponse }

class ResponsePoster extends StatefulWidget {
  ResponsePoster(this.plurkId, this.enableComment,
      {this.mode = ResponsePosterMode.NewResponse, this.responseId, this.initialContent, Key key})
      : super(key: key);

  final int plurkId;
  final bool enableComment;

  final ResponsePosterMode mode;
  final int responseId;
  //The initial text in the TextField.
  final String initialContent;

  @override
  ResponsePosterState createState() => ResponsePosterState();
}

class ResponsePosterState extends State<ResponsePoster>
    with AfterLayoutMixin, WidgetsBindingObserver, BaseEditor {
  ResponsePosterState();

  bool isSendingResponse = false;
  //Showing toolbar?
  bool isInEditingMode = false;

  @override
  void initState() {
    super.initState();

    qualifierMode = QualifierMode.Response;

    WidgetsBinding.instance.addObserver(this);

    // Query
//    print('Keyboard visibility direct query: ${KeyboardVisibility.isVisible}');

    KeyboardVisibilityController().onChange.listen(onKeyboardVisibilityChange);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (widget.initialContent != null) {
      //Set the exist contemt.
      commentController.text = widget.initialContent;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    commentController.dispose();
    hideMentionSuggestionOverlay();
    hideQualifierSelectorOverlayEntry();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    //Fucking crazy hack just for match the keyboard show/hide animation end.
    if (isInEmoticonsMode) {
      if (!isShowingKeyboard()) {
//        print('try show EmoticonView');
        isShowingEmoticonView = true;
      }
    } else {
      if (isShowingKeyboard()) {
//        print('try hide EmoticonView');
        isShowingEmoticonView = false;
      }
    }

    //SO SO stupid we have to delay the measure timing.
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) {
        if (MediaQuery.of(context).viewInsets.bottom >
            measuredSoftKeyboardHeight) {
          //Just remember the highest viewInsets.bottom.
          measuredSoftKeyboardHeight = MediaQuery.of(context).viewInsets.bottom;
//        print('MediaQuery.of(context).viewInsets.bottom: [' + MediaQuery.of(context).viewInsets.bottom.toString() + ']');
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
//    bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);
//    //I m not sure where to put this so... (Nope)'
//    print('isKeyboardVisible: ' + isKeyboardVisible.toString());

//    _onKeyboardVisibilityChange(KeyboardVisibilityNotification().isKeyboardVisible);

//    print('[Editor Build]');

    List<Widget> columnChildren = [];

    columnChildren.add(Divider(
      height: 0.5,
      thickness: 0.5,
    ));

    //When uploadingPicture, display the LinearProgressIndicator.
    if (isUploadingPictures) {
      columnChildren.add(LinearProgressIndicator(
        backgroundColor: Colors.transparent,
        color: Theme.of(context).colorScheme.secondary,
        minHeight: 8,
      ));
    }

    //Comment input field.
    if (!isUploadingPictures) {
      columnChildren.add(buildCommentTextField(
          context,
          isInEditingMode,
          false,
          isInEmoticonsMode,
          widget.enableComment && !isSendingResponse && !isUploadingPictures,
          isSendingResponse,
          minLines: 1,
          maxLines: 6));
    }

    //Toolbar.
    if (isInEditingMode && !isSendingResponse && !isUploadingPictures) {
      columnChildren.add(buildUtilityBar(context,
          isInEmoticonsMode ? (Icons.keyboard) : (Icons.insert_emoticon), () {
        //Hide overlay.
        hideQualifierSelectorOverlayEntry();
        hideMentionSuggestionOverlay();

        Vibrator.vibrateShortNote();

        //Toggle isInEmoticonsMode.
        if (!isInEmoticonsMode) {
          toggleEmoticonMode(true);
        } else {
          toggleEmoticonMode(false);
        }
      }, () {
        if (!isSendingResponse && !isUploadingPictures) {
          //Test (toggle OverlayEntry )
          if (qualifierSelectorOverlayEntry == null) {
            showQualifierSelectorOverlayEntry(context);
          } else {
            hideQualifierSelectorOverlayEntry();
          }
          Vibrator.vibrateShortNote();
        }
      }, () {
        if (!isSendingResponse && !isUploadingPictures) {
          //Check and send the content.
          checkAndSendResponse();
        }
      }));
    }

    //Emoticon view.
    if (isShowingEmoticonView && !isSendingResponse) {
      columnChildren.add(buildEmoticonView());
    }

    return MultiBlocListener(
      listeners: [
        BlocListener<ResponsesResponseAddCubit, ResponsesResponseAddState>(
          listener: (context, state) {
          if (state is ResponsesResponseAddSuccess) {
            //Clear the comment TextField.
            commentController.clear();
            Vibrator.vibrateSuccess();
          }

          //Leave the isSendingResponse state
          setState(() {
            isSendingResponse = false;
          });
        }),
        BlocListener<ResponsesResponseEditCubit, ResponsesResponseEditState>(
          listener: (context, state) {
            if (state is ResponsesResponseEditSuccess) {
              //Clear the comment TextField.
              commentController.clear();
              Vibrator.vibrateSuccess();
              context.read<ResponseEditCompleteCubit>().success(widget.plurkId, widget.responseId, state.responseEditResult);
            } else if (state is ResponsesResponseEditFailed) {
              //Clear the comment TextField.
              commentController.clear();
              Vibrator.vibrateError();
              context.read<ResponseEditCompleteCubit>().failed(state.errorText);
            }

            //Leave the isSendingResponse state
            setState(() {
              isSendingResponse = false;
            });
        }),
      ],
      child: Container(
//        key: _rootKey,
        color: Theme.of(context).cardColor.withOpacity(0.2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: columnChildren,
        ),
      ),
    );
  }

  //Keyboard vis can be manually disable by user
  //and can be auto enable by TextField.
  //so we have to let keyboard vis decide if exit editing mode.
  void onKeyboardVisibilityChange(bool visible) {
//    print('_onKeyboardVisibilityChange: ' + visible.toString());
    if (visible) {
      isInEditingMode = true;
    } else {
      //This means we are leaving editing mode
      if (!isInEmoticonsMode) {
        isInEditingMode = false;
        commentFocus.unfocus(disposition: UnfocusDisposition.scope);
      }

      //Hide overlays
      hideQualifierSelectorOverlayEntry();
      hideMentionSuggestionOverlay();
    }
  }

  //--

  void checkAndSendResponse() async {
    //Check if the TextField has content.
    if (commentController.text.isEmpty != true) {
      //Hide overlay.
      hideQualifierSelectorOverlayEntry();
      hideMentionSuggestionOverlay();

      if (widget.mode == ResponsePosterMode.NewResponse) {
        //New response.
        //Send the content. (by cubit)
        context.read<ResponsesResponseAddCubit>().request(widget.plurkId,
            Static.prefResponseQualifier, commentController.text);
      } else {
        //Edit response.
        // print('send edit response [' + widget.plurkId.toString() + '][' + widget.responseId.toString() + ']');
        context.read<ResponsesResponseEditCubit>().post(Plurdart.ResponsesEdit(
            plurkId: widget.plurkId,
            responseId: widget.responseId,
            content: commentController.text));
      }

      commentFocus.unfocus(disposition: UnfocusDisposition.scope);
      setState(() {
        //[Special case] We should force immediate leave emotion mode
        isInEmoticonsMode = false;
        isShowingEmoticonView = false;
        isInEditingMode = false;
        isSendingResponse = true;
      });

      Vibrator.vibrateShortNote();
    } else {
      Vibrator.vibrateError();
    }
  }

  @override
  void requestSetState(Function fn) {
    setState(fn);
  }

  //For user to focus on open screen.
  requestFocus() {
    commentFocus.requestFocus();
  }
}
