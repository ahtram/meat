import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'add_account_state.dart';

class AddAccountCubit extends Cubit<AddAccountState> {
  AddAccountCubit() : super(AddAccountInitial());

  request() async {
    emit(AddAccountRequest());
  }

}
