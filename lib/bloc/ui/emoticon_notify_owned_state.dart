part of 'emoticon_notify_owned_cubit.dart';

@immutable
abstract class EmoticonNotifyOwnedState {}

class EmoticonNotifyOwnedInitial extends EmoticonNotifyOwnedState {}

class EmoticonNotifyOwnedShow extends EmoticonNotifyOwnedState {
  EmoticonNotifyOwnedShow(this.url);
  final String url;
}
