part of 'hotlinks_get_links_cubit.dart';

@immutable
abstract class HotlinksGetLinksState {}

class HotlinksGetLinksInitial extends HotlinksGetLinksState {}

class HotlinksGetLinksSuccess extends HotlinksGetLinksState {
  HotlinksGetLinksSuccess(this.hotlinks);
  final List<Plurdart.HotLink> hotlinks;
}

class HotlinksGetLinksFailed extends HotlinksGetLinksState { }

class HotlinksTrackBackLinksSuccess extends HotlinksGetLinksState {
  HotlinksTrackBackLinksSuccess(this.hotlinks);
  final List<Plurdart.HotLink> hotlinks;
}

class HotlinksTrackBackLinksFailed extends HotlinksGetLinksState { }