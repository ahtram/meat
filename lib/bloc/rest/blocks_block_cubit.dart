import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'blocks_block_state.dart';

class BlocksBlockCubit extends Cubit<BlocksBlockState> {
  BlocksBlockCubit() : super(BlocksBlockInitial());

  request(int userId) async {
    Plurdart.Base base = await Plurdart.blocksBlock(Plurdart.UserID(userId: userId));

    if (base != null) {
      if (base.hasSuccess()) {
        emit(BlocksBlockSuccess(userId));
      } else {
        emit(BlocksBlockFailed(base.errorText));
      }
    } else {
      emit(BlocksBlockFailed(base.errorText));
    }

  }
}
