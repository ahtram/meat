part of 'users_update_cubit.dart';

@immutable
abstract class UsersUpdateState {}

class UsersUpdateInitial extends UsersUpdateState {}

class UsersUpdatePinPlurkSuccess extends UsersUpdateState {
  UsersUpdatePinPlurkSuccess(this.user);
  final Plurdart.User user;
}

class UsersUpdatePinPlurkFailed extends UsersUpdateState {
  UsersUpdatePinPlurkFailed(this.errorText);
  final String errorText;
}

class UsersUpdateUnpinPlurkSuccess extends UsersUpdateState {
  UsersUpdateUnpinPlurkSuccess(this.user);
  final Plurdart.User user;
}

class UsersUpdateUnpinPlurkFailed extends UsersUpdateState {
  UsersUpdateUnpinPlurkFailed(this.errorText);
  final String errorText;
}
