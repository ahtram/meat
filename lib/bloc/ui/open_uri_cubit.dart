import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'open_uri_state.dart';

class OpenUriCubit extends Cubit<OpenUriState> {
  OpenUriCubit() : super(OpenUriInitial());

  request(Uri uri) {
    emit(OpenUriRequest(uri));
  }
}
