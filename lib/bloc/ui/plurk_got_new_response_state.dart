part of 'plurk_got_new_response_cubit.dart';

@immutable
abstract class PlurkGotNewResponseState {}

class PlurkGotNewResponseInitial extends PlurkGotNewResponseState {}
class PlurkGotNewResponseSuccess extends PlurkGotNewResponseState {
  PlurkGotNewResponseSuccess(this.plurk);
  final Plurdart.Plurk plurk;
}
