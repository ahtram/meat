
import 'package:after_layout/after_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meat/bloc/rest/blocks_block_cubit.dart';
import 'package:meat/bloc/rest/blocks_get_cubit.dart';
import 'package:meat/bloc/rest/blocks_unblock_cubit.dart';
import 'package:meat/bloc/rest/friends_fans_get_fans_by_offset_cubit.dart';
import 'package:meat/bloc/rest/friends_fans_get_following_by_offset_cubit.dart';
import 'package:meat/bloc/rest/friends_fans_get_friends_by_offset_cubit.dart';
import 'package:meat/bloc/rest/user_search_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/models/plurk/users_collection.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:tuple/tuple.dart';
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';
import 'package:meat/screens/user_viewer.dart';

//A customizable user's list viewer for select view and select
//Friends/Fans/Following

class UsersViewerArgs {
  UsersViewerArgs(this.title, this.browsingType,
      {this.userId,
        this.isToggleList = true,
        this.definitelyIncludeSelf = false,
        this.userModals,
        this.searchTerm})
      : plurkerTuples = (userModals == null)
      ? ([])
      : (userModals
      .map((e) => Tuple2<Plurdart.User, bool>(e, false))
      .toList());

  //This is for users_browser title.
  final String title;
  final UsersBrowsingType browsingType;

  //This userId is optional. Getting following list do not need this.
  //For Friends, Fans, Following APIs
  final int userId;
  final bool isToggleList;
  final bool definitelyIncludeSelf;

  //For search mode
  String searchTerm;

  //For Plurkers
  final List<Plurdart.User> userModals;
  final List<Tuple2<Plurdart.User, bool>> plurkerTuples;
}

//Mode
enum UsersBrowsingType {
  Friends,    //Get users from API cubit
  Fans,       //Get users from API cubit
  Following,  //Get users from API cubit
  Blocks,     //Get users from API cubit
  UserModals, //Directly pass in User modals into UsersViewerArgs.plurkers.
  UserIds,    //Pass in UserIds list. For viewing a plurk's replurkers or favorers.
  Search      //Get users from API cubit
}

class UsersViewer extends StatefulWidget {
  UsersViewer(this.args, {Key key}) : super(key: key);

  final UsersViewerArgs args;

  @override
  UsersViewerState createState() => UsersViewerState();
}

class UsersViewerState extends State<UsersViewer> with AfterLayoutMixin {
  BuildContext _blocProviderContext;
  RefreshController _refreshController = RefreshController();

  UsersCollection _usersCollection = UsersCollection();

  bool _updateError = false;
  bool _isRequestingRefresh = false;
  bool _isRequestingLoad = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //This could prevent from assert error or smartRefresher.
    if (_refreshController.position != null) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        if (widget.args.browsingType == UsersBrowsingType.Following ||
            widget.args.browsingType == UsersBrowsingType.Friends ||
            widget.args.browsingType == UsersBrowsingType.Fans ||
            widget.args.browsingType == UsersBrowsingType.Blocks ||
            widget.args.browsingType == UsersBrowsingType.Search) {
          //Request a refresh first.
          _refreshController.requestRefresh();
        }
      });
    }
  }

  bool isRefreshing() {
    return _refreshController.isRefresh;
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<FriendsFansGetFansByOffsetCubit>(
          create: (BuildContext context) => FriendsFansGetFansByOffsetCubit(),
        ),
        BlocProvider<FriendsFansGetFollowingByOffsetCubit>(
          create: (BuildContext context) =>
              FriendsFansGetFollowingByOffsetCubit(),
        ),
        BlocProvider<FriendsFansGetFriendsByOffsetCubit>(
          create: (BuildContext context) =>
              FriendsFansGetFriendsByOffsetCubit(),
        ),
        BlocProvider<UserSearchCubit>(
          create: (BuildContext context) =>
              UserSearchCubit(),
        ),
        BlocProvider<BlocksGetCubit>(
          create: (BuildContext context) =>
              BlocksGetCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<FriendsFansGetFansByOffsetCubit,
              FriendsFansGetFansByOffsetState>(
            listener: (context, state) {
              if (state is FriendsFansGetFansByOffsetSuccess) {
                _onGetUsers(state.users);
              } else if (state is FriendsFansGetFansByOffsetFailed) {
                _onGetUsers(null);
              }
            },
          ),
          BlocListener<FriendsFansGetFollowingByOffsetCubit,
              FriendsFansGetFollowingByOffsetState>(
            listener: (context, state) {
              if (state is FriendsFansGetFollowingByOffsetSuccess) {
                _onGetUsers(state.users);
              } else if (state is FriendsFansGetFollowingByOffsetFailed) {
                _onGetUsers(null);
              }
            },
          ),
          BlocListener<FriendsFansGetFriendsByOffsetCubit,
              FriendsFansGetFriendsByOffsetState>(
            listener: (context, state) {
              if (state is FriendsFansGetFriendsByOffsetSuccess) {
                _onGetUsers(state.users);
              } else if (state is FriendsFansGetFriendsByOffsetFailed) {
                _onGetUsers(null);
              }
            },
          ),
          BlocListener<UserSearchCubit,
              UserSearchState>(
            listener: (context, state) {
              if (state is UserSearchSuccess) {
                _onGetUsers(state.userSearch.users);
              } else if (state is UserSearchFailed) {
                _onGetUsers(null);
              } else if (state is UserSearchTrackBackSuccess) {
                _onGetUsers(state.userSearch.users);
              } else if (state is UserSearchTrackBackFailed) {
                _onGetUsers(null);
              }
            },
          ),
          BlocListener<BlocksGetCubit,
              BlocksGetState>(
            listener: (context, state) {
              if (state is BlocksGetSuccess) {
                _onGetUsers(state.users);
              } else if (state is BlocksGetFailed) {
                _onGetUsers(null);
              }
            },
          ),

          //[Very Tricky]: When success block a user. Do a refresh.
          BlocListener<BlocksBlockCubit,
              BlocksBlockState>(
            listener: (context, state) {
              if (state is BlocksBlockSuccess) {
                _refreshController.requestRefresh();
              }
            },
          ),

          //[Very Tricky]: When success unblock a user. Do a refresh.
          BlocListener<BlocksUnblockCubit,
              BlocksUnblockState>(
            listener: (context, state) {
              if (state is BlocksUnblockSuccess) {
                _refreshController.requestRefresh();
              }
            },
          ),

        ],
        child: Container(
          color: Theme.of(context).canvasColor.withOpacity(0.4),
          //We'll need this smartRefresh thing here...
          child: Builder(builder: (context) {
            _blocProviderContext = context;

            Color indicatorColor = Theme.of(context).textTheme.bodyText1.color;

            return NotificationListener<ScrollNotification>(
              child: SmartRefresher(
                controller: _refreshController,
                header: WaterDropMaterialHeader(
                  color: indicatorColor,
                ),
                onRefresh: _onRefresh,
                child: ListView(
                  controller: PrimaryScrollController.of(context),
                  children: _buildListViewItems(),
                ),
                enablePullUp: true,
                onLoading: _onLoading,
                //[Dep]: Change to ListView
                // child: ListView.builder(
                //   controller: _scrollController,
                //   itemBuilder: _buildUserListViewItem,
                // ),
              ),
              onNotification: (scrollNotification) {
                double triggerFetchMoreSize =
                    0.75 * scrollNotification.metrics.maxScrollExtent;

                if (scrollNotification.metrics.pixels  > 10 &&scrollNotification.metrics.pixels > triggerFetchMoreSize) {
                  // call fetch more method here
                  _loadMoreUsers();
                }

                return true;
              }
            );
          }),
        ),
      ),
    );
  }

  List<Widget> _buildListViewItems() {
    List<Widget> returnList = [];

    //Include self??
    if (widget.args.definitelyIncludeSelf && Static.me != null) {
      returnList.add(
          _listViewItem(Static.me, widget.args.isToggleList, true));
    }

    if (widget.args.browsingType == UsersBrowsingType.UserModals) {
      //User passed in Users list.
      for (int i = 0; i < widget.args.plurkerTuples.length; ++i) {
        returnList.add(_listViewItem(
            widget.args.plurkerTuples[i].item1,
            widget.args.isToggleList,
            widget.args.plurkerTuples[i].item2, onToggleChanged: (value) {
              // print('onToggleChanged:' + value.toString());
          setState(() {
            widget.args.plurkerTuples[i] =
                Tuple2(widget.args.plurkerTuples[i].item1, value);
          });
        }, onTap: (user) {
          //Open user viewer.
          context.read<UserViewerCubit>().open(UserViewerArgs(
              user.nickName
          ));
        }));
      }
    } else {
      //User usersCollection from APIs.
      List<UserToggle> usersToggles =
      _usersCollection.getUsersToggles();

      for (int i = 0; i < usersToggles.length; ++i) {
        returnList.add(_listViewItem(usersToggles[i].user,
            widget.args.isToggleList, usersToggles[i].toggle, onToggleChanged: (value) {
              setState(() {
                usersToggles[i].toggle = value;
              });
            }, onTap: (user) {
              //Open user viewer.
              context.read<UserViewerCubit>().open(UserViewerArgs(
                  user.nickName
              ));
            }));
      }
    }

    // returnList.add(SizedBox(
    //   height: kMinInteractiveDimension,
    // ));

    return returnList;
  }

  //Build one user item.
  // Widget _buildUserListViewItem(BuildContext context, int index) {
  //   if (widget.args.definitelyIncludeSelf && Static.me != null) {
  //     if (index == 0) {
  //       return _listViewItem(
  //           Static.me, widget.args.isToggleList, true, (value) {});
  //     }
  //
  //     //Decrease index by 1 to match the usersToggles index.
  //     index -= 1;
  //   }
  //
  //   if (widget.args.usersListType == UsersListType.Plurkers) {
  //     //User passed in Users list.
  //     //Plurkers
  //     if (index < widget.args.plurkerTuples.length) {
  //       return _listViewItem(
  //           widget.args.plurkerTuples[index].item1,
  //           widget.args.isToggleList,
  //           widget.args.plurkerTuples[index].item2, (value) {
  //         setState(() {
  //           widget.args.plurkerTuples[index] =
  //               Tuple2(widget.args.plurkerTuples[index].item1, value);
  //         });
  //       });
  //     } else {
  //       return null;
  //     }
  //   } else {
  //     //User usersCollection from APIs.
  //     List<Tuple2<Plurdart.User, bool>> usersToggles =
  //         _usersCollection.getUsersToggles();
  //
  //     if (index < usersToggles.length) {
  //       return _listViewItem(usersToggles[index].item1,
  //           widget.args.isToggleList, usersToggles[index].item2, (value) {
  //         setState(() {
  //           usersToggles[index] = Tuple2(usersToggles[index].item1, value);
  //         });
  //       });
  //     } else {
  //       return null;
  //     }
  //   }
  // }

  Widget _listViewItem(Plurdart.User user, bool hasToggle, bool toggleValue,
      {Function(bool) onToggleChanged, Function(Plurdart.User) onTap}) {
    List<Widget> rowChildren = [];

    //Avatar.
    rowChildren.add(SizedBox(
      width: 56,
      height: 56,
      child: PlurkCardPrimeHeaderState.headerAvatar(
        context,
        user,
        margin: EdgeInsets.all(4),
        onTap: (user) {
          if (hasToggle) {
            onToggleChanged?.call(!toggleValue);
          } else {
            onTap?.call(user);
          }
        },
      ),
    ));

    rowChildren.add(SizedBox(
      width: 8,
    ));

    //Display Name.
    rowChildren.add(Expanded(
      child: SizedBox(
        height: 56,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PlurkCardPrimeHeaderState.buildHeaderNickName(context, user, onNickNameTap: () {
              if (hasToggle) {
                onToggleChanged?.call(!toggleValue);
              } else {
                onTap?.call(user);
              }
            },),
            PlurkCardPrimeHeaderState.buildUserDisplayName(
                context, null, user, false, (user) {
              if (hasToggle) {
                onToggleChanged?.call(!toggleValue);
              } else {
                onTap?.call(user);
              }
            },
                fit: FlexFit.tight),
          ],
        ),
      ),
    ));

    // rowChildren.add(PlurkCardPrimeHeader.buildUserDisplayName(
    //     context, user, false, null,
    //     fit: FlexFit.tight));

    rowChildren.add(SizedBox(
      width: 8,
    ));

    //Toggle
    if (hasToggle) {
      rowChildren.add(Switch(
          value: toggleValue,
          onChanged: (value) {
            onToggleChanged?.call(value);
          }));
    }

    return InkWell(
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: rowChildren,
          )),
      onTap: () {
        if (hasToggle) {
          onToggleChanged?.call(!toggleValue);
        } else {
          onTap?.call(user);
        }
      },
    );
  }

  //Get the current selected users.
  List<Plurdart.User> getSelectedUsers() {
    List<Plurdart.User> returnList = [];

    if (widget.args.definitelyIncludeSelf && Static.me != null) {
      returnList.add(Static.me);
    }

    if (widget.args.browsingType == UsersBrowsingType.UserModals) {
      //From Plurkers.
      for (int i = 0; i < widget.args.plurkerTuples.length; ++i) {
        if (widget.args.plurkerTuples[i].item2) {
          returnList.add(widget.args.plurkerTuples[i].item1);
        }
      }
    } else {
      //From API usersCollections.
      List<UserToggle> usersToggles =
      _usersCollection.getUsersToggles();

      for (int i = 0; i < usersToggles.length; ++i) {
        if (usersToggles[i].toggle) {
          returnList.add(usersToggles[i].user);
        }
      }
    }

    return returnList;
  }

  setFilterTerm(String filterTerm) {
    // print('setFilterTerm [' + filterTerm + ']');
    setState(() {
      _usersCollection.setFilterTerm(filterTerm);
    });
  }

  refreshSearchTerm(String searchTerm) {
    // print('refreshSearchTerm [' + searchTerm + ']');
    widget.args.searchTerm = searchTerm;
    _refreshController.requestRefresh(
        needMove: true, duration: Duration(milliseconds: 200));
  }

  _onRefresh() {
    _refreshUsers();
  }

  _onLoading() {
    _loadMoreUsers();
  }

  // The will clear the whole list and start from the beginning.
  _refreshUsers() {
    // print('_refreshUsers');
    if (!_isRequestingRefresh && !_isRequestingLoad) {
      _isRequestingRefresh = true;
      _updateError = false;

      //Which request do we need?
      if (widget.args.browsingType == UsersBrowsingType.Fans) {
        _blocProviderContext.read<FriendsFansGetFansByOffsetCubit>().request(
          widget.args.userId,
          0,
          20,
        );
      } else if (widget.args.browsingType == UsersBrowsingType.Following) {
        _blocProviderContext
            .read<FriendsFansGetFollowingByOffsetCubit>()
            .request(
          0,
          20,
        );
      } else if (widget.args.browsingType == UsersBrowsingType.Friends) {
        _blocProviderContext.read<FriendsFansGetFriendsByOffsetCubit>().request(
          widget.args.userId,
          0,
          20,
        );
      } else if (widget.args.browsingType == UsersBrowsingType.Blocks) {
        _blocProviderContext.read<BlocksGetCubit>().request(0);
      } else if (widget.args.browsingType == UsersBrowsingType.Search) {
        _blocProviderContext.read<UserSearchCubit>().request(widget.args.searchTerm, 0);
      }

      //No offset means get from the oldest!.
    } else {
      _refreshController.refreshCompleted();
    }
  }

  _loadMoreUsers() {
    if (!_isRequestingLoad && !_isRequestingRefresh && !_updateError) {
      //Which request do we need?
      if (widget.args.browsingType == UsersBrowsingType.Fans) {
        _blocProviderContext.read<FriendsFansGetFansByOffsetCubit>().request(
          widget.args.userId,
          _usersCollection.getUsersLength(),
          20,
        );
        _isRequestingLoad = true;
        _blocProviderContext.read<RequestIndicatorCubit>().show();
      } else if (widget.args.browsingType == UsersBrowsingType.Following) {
        _blocProviderContext
            .read<FriendsFansGetFollowingByOffsetCubit>()
            .request(
          _usersCollection.getUsersLength(),
          20,
        );
        _isRequestingLoad = true;
        _blocProviderContext.read<RequestIndicatorCubit>().show();
      } else if (widget.args.browsingType == UsersBrowsingType.Friends) {
        _blocProviderContext.read<FriendsFansGetFriendsByOffsetCubit>().request(
          widget.args.userId,
          _usersCollection.getUsersLength(),
          20,
        );
        _isRequestingLoad = true;
        _blocProviderContext.read<RequestIndicatorCubit>().show();
      } else if (widget.args.browsingType == UsersBrowsingType.Blocks) {
        _blocProviderContext.read<BlocksGetCubit>().request(_usersCollection.getUsersLength());
        _isRequestingLoad = true;
        _blocProviderContext.read<RequestIndicatorCubit>().show();
      }  else if (widget.args.browsingType == UsersBrowsingType.Search) {
        _blocProviderContext.read<UserSearchCubit>().request(widget.args.searchTerm, _usersCollection.getUsersLength());
        _isRequestingLoad = true;
        _blocProviderContext.read<RequestIndicatorCubit>().show();
      }
    }
  }

  _onGetUsers(List<Plurdart.User> users) {
    if (users != null) {
      setState(() {
        // print('_onGetUsers() users.length: ' + users.length.toString());
        _refreshController.refreshCompleted();
        _refreshController.loadComplete();
        _blocProviderContext.read<RequestIndicatorCubit>().hide();

        //This is a refresh so we need to clear stuffs.
        if (_isRequestingRefresh) {
          _usersCollection.clear();
        }

        //The will add the collection length.
        _usersCollection.add(users);
        _updateError = false;
      });
    } else {
//      print('refreshFailed()');
      _refreshController.refreshFailed();
      _refreshController.loadFailed();
      _updateError = true;
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRequestingRefresh = false;
    _isRequestingLoad = false;
  }

}
