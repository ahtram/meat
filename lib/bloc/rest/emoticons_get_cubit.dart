import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'emoticons_get_state.dart';

class EmoticonsGetCubit extends Cubit<EmoticonsGetState> {
  EmoticonsGetCubit() : super(EmoticonsGetInitial());

  Future request() async {
    Plurdart.Emoticons emoticons = await Plurdart.emoticonsGet();

    if (emoticons != null) {
      //Update static data directly.
      if (Static.me != null) {
        Static.emoticonCollection.updateOwnedBasic(emoticons.getUserOwnedEmoticons(Static.me));
      }

      Static.emoticonCollection.updateCustom(emoticons.custom);
      emit(EmoticonsGetSuccess(emoticons));
    } else {
      emit(EmoticonsGetFailed());
    }
  }
}
