part of 'refresh_unread_count_cubit.dart';

@immutable
abstract class RefreshUnreadCountState {}

class RefreshUnreadCountInitial extends RefreshUnreadCountState {}

class RefreshUnreadCountRequest extends RefreshUnreadCountState {}
