part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginSuccess extends LoginState {
  LoginSuccess(this.credentials);
  final Plurdart.Credentials credentials;
}

class LoginFailed extends LoginState {}
