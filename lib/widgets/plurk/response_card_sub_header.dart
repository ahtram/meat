import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:meat/utils/extensions.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:strings/strings.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:enum_to_string/enum_to_string.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

class ResponseCardSubHeader extends StatelessWidget {
  ResponseCardSubHeader(
      this.response, this.user, this.floor, this.withNameShadow,
      {this.onNameTap, this.onAvatarTap});

  final Plurdart.Response response;
  final Plurdart.User user;
  final int floor;

  final bool withNameShadow;
  final Function(Plurdart.Response, Plurdart.User) onNameTap;
  final Function onAvatarTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // Left Avatar
          GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Theme.of(context).hintColor,
                  width: 0.5,
                ),
                shape: BoxShape.circle,
              ),
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                backgroundImage: CachedNetworkImageProvider(user.bigAvatarUrl()),
                // child: Image(
                //   fit: BoxFit.fill,
                //   // placeholder: MemoryImage(kTransparentImage),
                //   image: CachedNetworkImageProvider(user.bigAvatarUrl()),
                // ),
              ),
            ),
            onTap: () {
              onAvatarTap?.call(user);
            },
          ),

          SizedBox(
            width: 8,
          ),

          //Mid
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _buildHeaderFloorDisplayNameAndQualifierRow(
                  context, response, user, floor, withNameShadow, onNameTap),
            ),
          ),

          SizedBox(
            width: 8,
          ),

          // Response time.
          buildHeaderTime(context, response.posted),

          SizedBox(
            width: 8,
          ),

          _buildHeaderFloor(context, floor),

          // Right button
          // Moved to outside.
          // InkWell(
          //   customBorder: CircleBorder(),
          //   child: Container(
          //     width: 40,
          //     height: 40,
          //     child: Icon(
          //       Icons.expand_more,
          //       size: 24,
          //       color: Theme.of(context).iconTheme.color,
          //     ),
          //   ),
          //   onTap: () {},
          // ),
        ],
      ),
    );
  }

  static Widget buildHeaderTime(BuildContext context, String timeString) {
    return Text(
      timeAgoFromPosted(context, timeString),
      style: TextStyle(
        color: Theme.of(context).hintColor,
        fontSize: 13,
      ),
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget _buildHeaderFloor(BuildContext context, int floor) {
    return Text(
      '[' + (floor + 1).toString() + ']',
      style: TextStyle(
        color: Theme.of(context).textTheme.bodyText1.color,
        fontSize: 13,
      ),
      overflow: TextOverflow.ellipsis,
    );
  }

  static Widget buildUserDisplayName(
      BuildContext context,
      Plurdart.Response response,
      Plurdart.User user,
      bool withShadow,
      Function onNameTap) {
    //For the light brightness we'll need white shadow on the colored user name...
    List<Shadow> shadows = [];
    if (withShadow) {
      //    if (Theme.of(context).brightness == Brightness.light) {
      shadows = [
//        Shadow(
//          offset: Offset(3.0, 0.0),
//          blurRadius: 15.0,
//          color: Theme.of(context).canvasColor,
//        ),
        Shadow(
          offset: Offset(0.0, 0.0),
          blurRadius: 14.0,
          color: Theme.of(context).textTheme.bodyText1.color,
        ),
//        Shadow(
//          offset: Offset(-3.0, 0.0),
//          blurRadius: 15.0,
//          color: Theme.of(context).canvasColor,
//        )
      ];
//    } // Dark brightness don't use shadow.
    }

    //Anonymous response could use handle as display name.
    String displayNameStr = (response != null && response.handle != null)
        ? response.handle
        : user.displayName;

    Color displayNameColor = (response != null && response.myAnonymous == true && Static.me != null)
        ? HexColor.fromHex(
            Static.me.nameColor, Theme.of(context).textTheme.bodyText1.color)
        : HexColor.fromHex(
            user.nameColor, Theme.of(context).textTheme.bodyText1.color);

    return Flexible(
      child: GestureDetector(
        child: Text(
          displayNameStr,
          style: TextStyle(
            // Default Color is text body color.
            color: displayNameColor,
            fontWeight: FontWeight.w600,
            fontSize: 14,
            shadows: shadows,
          ),
          overflow: TextOverflow.ellipsis,
        ),
        onTap: onNameTap == null
            ? null
            : () {
                //Let the outside handle the logic about anonymous handle.
                onNameTap?.call(response, user);
              },
      ),
    );
  }

  List<Widget> _buildHeaderFloorDisplayNameAndQualifierRow(
      BuildContext context,
      Plurdart.Response response,
      Plurdart.User user,
      int floor,
      bool withShadow,
      Function onNameTap) {
    List<Widget> returnWidgets = [];

    //DisplayName
    returnWidgets.add(
        buildUserDisplayName(context, response, user, withShadow, onNameTap));

    // print('[Response]: ' + prettyJson(response.toJson()));

    Plurdart.ResponseQualifier qualifier = EnumToString.fromString(
        Plurdart.ResponseQualifier.values, capitalize(response.qualifier));

    if (qualifier != null) {
      //Display the qualifier.
      returnWidgets.add(SizedBox(
        width: 4,
      ));
      returnWidgets.add(Container(
        padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(3),
          color: Color(response.qualifierColorHex()),
        ),
        child: Text(
          FlutterI18n.translate(
              context, EnumToString.convertToString(qualifier)),
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
          overflow: TextOverflow.ellipsis,
        ),
      ));
    }

    return returnWidgets;
  }

  // Return a format time ago string from the posted time.
  static String timeAgoFromPosted(BuildContext context, String postedTime) {
    try {
//      print('postedTime: ' + postedTime);
      DateTime postedDateTime = DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
          .parseUTC(postedTime)
          .toLocal();
      Duration difDuration = DateTime.now().difference(postedDateTime);
      // return timeago.format(DateTime.now().subtract(difDuration),
      //     locale: Localizations.localeOf(context).toString());
      return timeago.format(DateTime.now().subtract(difDuration),
          locale: 'en_short');
    } catch (e) {
      print('Oops! postedTime [' + postedTime + '] cannot be parsed!');
      return '??';
    }
  }
}
