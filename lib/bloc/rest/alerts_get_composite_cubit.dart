import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'alerts_get_composite_state.dart';

class AlertsGetCompositeCubit extends Cubit<AlertsGetCompositeState> {
  AlertsGetCompositeCubit() : super(AlertsGetCompositeInitial());

  void request() async {
    List<Plurdart.Alert> historyAlerts = await Plurdart.alertsGetHistory();
    List<Plurdart.Alert> activeAlerts = await Plurdart.alertsGetActive();

    //Combine the two list and sort them properly...
    List<Plurdart.Alert> alerts = [];

    if (historyAlerts != null) {
      alerts.addAll(historyAlerts);
    }

    if (activeAlerts != null) {
      alerts.addAll(activeAlerts);
    }

    alerts.sort((a, b) {
      DateTime dateTimeA = a.postedDateTime();
      DateTime dateTimeB = b.postedDateTime();
      if (dateTimeA == null || dateTimeB == null) {
        //Cannot compare.
        return 0;
      }
      //Use the DateTime compareTo to do the job.
      return -(a.postedDateTime().compareTo(b.postedDateTime()));
    });

    if (alerts != null) {
      emit(AlertsGetCompositeSuccess(alerts));
    } else {
      emit(AlertsGetCompositeFailed());
    }
  }
}
