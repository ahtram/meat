import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

//For update feed cache compute function.
Future<List<PlurkFeed>> updateFeedCache(PlurksCollection plurksCollection) async {
  plurksCollection.feedsCache.clear();
  for (int i = 0; i < plurksCollection.collection.length; ++i) {
    List<Plurdart.Plurk> plurks = plurksCollection.collection[i].plurks;
    // print('[_collection][' + i.toString() + '] plurks length [' + plurks.length.toString() + ']');
    //Tricky! It is possible the different plurks contain same plurk! due to API nature.
    for (int p = 0; p < plurks.length; ++p) {

      //[Dep]
      //First things first: Check isViewingUnreadOnly
      // if (plurksCollection.isViewingUnreadOnly) {
      //   if (plurks[p].isUnread != 1) {
      //     //1 means this plurk is unread. Skip if not.
      //     continue;
      //   }
      // }

      //check if every plurk is unique. Filter out those already exist.
      PlurkFeed foundFeed = plurksCollection.feedsCache
          .firstWhere((element) => element.plurk.plurkId == plurks[p].plurkId, orElse: () {
        return null;
      });

      if (foundFeed != null) {
        //Skip this one.
        // print('[found duplicated plurk][' +
        //     plurks[p].plurkId.toString() +
        //     '] Skip it.');
        continue;
      }

      //[Tricky!] Check if there's only one limit to.
      if (plurksCollection.myPrivate) {
        List<int> limitTo = plurks[p].getLimitedTo();
        //My private only limit to one ppl.
        if (limitTo != null && limitTo.length != 1) {
          continue;
        }

        //!!!! Note we cannot use Static.meId here !!!!
        //This is a method designed for running by compute()
        if (limitTo[0] != plurksCollection.meId) {
          continue;
        }
      }

      //[Tricky!] Check spy.
      if (plurksCollection.spy) {
        if (plurks[p].anonymous == null ||
            !plurks[p].anonymous ||
            plurks[p].replurkerId != null) {
          continue;
        }
      }

      Plurdart.User plurkOwner = plurksCollection.collection[i].plurkUsers[plurks[p].ownerId];

      if (plurkOwner != null) {
        //We need replurker for check following and friends.
        Plurdart.User plurkReplurker;
        if (plurks[p].replurkerId != null) {
          if (plurksCollection.collection[i].plurkUsers.containsKey(plurks[p].replurkerId)) {
            plurkReplurker = plurksCollection.collection[i].plurkUsers[plurks[p].replurkerId];
            // print('Got plurkReplurker: [' + plurkReplurker.displayName + ']');
          } else {
            // print('Oops! plurkUsers does not contains replurkerId [' + plurks[p].replurkerId.toString() + ']');
          }
        } else {
          // print('replurkerId is null.');
        }

        //Check if use completion to filter only friends plurks...
        if (plurksCollection.completionMap == null ||
            plurksCollection.completionMap.containsKey(plurkOwner.id) ||
            (plurkReplurker != null &&
                plurksCollection.completionMap.containsKey(plurkReplurker.id))) {
          //Either no need to filter or this is my friend's plurk

          //Check if I am following this guy?
          if (plurksCollection.limitUsers == null ||
              plurksCollection.limitUsers.contains(plurkOwner.id) ||
              (plurkReplurker != null &&
                  plurksCollection.limitUsers.contains(plurkReplurker.id))) {
            //Either no need to filter or I am following him.

            // if (plurkOwner == null) {
            //  print('User not exist: [' + plurks[p].userId.toString() + ']');
            // } else {
            //  print('User exist: [' + plurkOwner.nickName + ']');
            // }

            //Cache those been filtered.
            plurksCollection.feedsCache.add(PlurkFeed(
                plurks[p],
                plurkOwner,
                plurkReplurker,
                plurksCollection.collection[i].queryUsers(plurks[p].favorers),
                plurksCollection.collection[i].queryUsers(plurks[p].replurkers)));
          }
        }
      } else {
        print('Oops! PlurkOwner is null!? This should not happen!');
      }
    }
  }

  //TopPlurks
  if (plurksCollection.topPlurks != null) {
    // print('plurksCollection._topPlurks.plurks.length: ' + plurksCollection._topPlurks.plurks.length.toString());
    plurksCollection.topPlurks.plurks.forEach((plurk) {
      // print('add plurk feed [' + plurk.plurkId.toString() + ']');
      plurksCollection.feedsCache.add(PlurkFeed(plurk, plurk.owner, null, null, null));
    });
  }

  //AnonymousPlurks
  if (plurksCollection.anonymousPlurks != null) {
    // print('plurksCollection._anonymousPlurks.plurks.length: ' + plurksCollection._anonymousPlurks.plurks.length.toString());
    plurksCollection.anonymousPlurks.plurks.forEach((key, value) {
      // print('add plurk feed [' + value.plurkId.toString() + ']');
      //Anonymous Plurk does not has an Owner.
      plurksCollection.feedsCache.add(PlurkFeed(value, null, null, null, null));
    });
  }

  //Bookmarks
  if (plurksCollection.bookmarks != null) {
    plurksCollection.bookmarks.plurks.forEach((p) {
      plurksCollection.feedsCache.add(PlurkFeed(
        p,
        plurksCollection.bookmarks.queryUser(p.ownerId),
        null,
        plurksCollection.bookmarks.queryUsers(p.favorers),
        plurksCollection.bookmarks.queryUsers(p.replurkers),
        bookmark: plurksCollection.bookmarks.queryBookmark(p.plurkId),
      ));
    });
  }

  //officialNewsPlurks
  if (plurksCollection.officialNewsPlurks != null) {
    // print('plurksCollection._officialNewsPlurks.plurks.length: ' + plurksCollection._officialNewsPlurks.plurks.length.toString());
    plurksCollection.officialNewsPlurks.forEach((value) {
      // print('add plurk feed [' + value.plurkId.toString() + ']');
      // print('add plurk feed [' + prettyJson(value.toJson()) + ']');
      //officialNews Plurk does not has an Owner.
      //Note the API is crazy that it returns duplicated plurks! We have to prevent this.
      if (!plurksCollection.feedsCache.any((plurkFeed) => plurkFeed.plurk.plurkId == value.plurkId)) {
        plurksCollection.feedsCache.add(PlurkFeed(value, null, null, null, null));
      }
    });
  }

  return plurksCollection.feedsCache;
}

// bool isFeedCacheContainsPlurk(List<PlurkFeed> feedsCache, int plurkId) {
//   PlurkFeed foundFeed = feedsCache
//       .firstWhere((element) => element.plurk.plurkId == plurkId, orElse: () {
//     return null;
//   });
//
//   return (foundFeed != null);
// }

class PlurksCollection {
  PlurksCollection() : meId = Static.meId;

  //[Tricky!]
  //This is a tricky temp self user ID. We need this mainly for running
  // await compute(updateFeedCache, this)!
  int meId;

  // All of the TimelineGetPlurks result goes here.
  List<Plurdart.Plurks> collection = [];
  // Ok. I decide to just separate them all for better maintainability
  Plurdart.TopPlurks topPlurks;
  Plurdart.TopicPlurks anonymousPlurks;
  Plurdart.Bookmarks bookmarks;
  List<Plurdart.Plurk> officialNewsPlurks;

  //<Plurk, Owner, Replurker>
  List<PlurkFeed> feedsCache = [];

  //These are for advance filters made by me.
  //Cache the completionMap for feed filter. (Friends filter)
  Map<int, Plurdart.Completion> completionMap;
  //Filter those I am following.
  List<int> limitUsers;
  //My private is a special case which filter-out plurks only limit to me.
  bool myPrivate = false;

  //Spy is a special case which filter-out plurks only show anonymous!
  bool spy = false;

  //Set by user through Static.isViewingUnreadOnly() if need.
  // bool isViewingUnreadOnly = false;

  // Make sure you call setup before use PlurksCollection!
  // setup(bool isViewingUnreadOnly) {
  //   this.isViewingUnreadOnly = isViewingUnreadOnly;
  // }

  // Add a new TimelineGetPlurks result here. (with options to form _feedCache)
  Future addTop(Plurdart.Plurks plurks,
      {Map<int, Plurdart.Completion> completionMap,
        List<Plurdart.User> limitUsers,
        bool myPrivate = false,
        bool spy = false}) async {
    if (plurks.plurks.length > 0) {
      collection.insert(0, plurks);
    }

    if (completionMap != null) {
      this.completionMap = completionMap;
    }

    if (limitUsers != null) {
      this.limitUsers = limitUsers.map((e) => e.id).toList();
      // print('_followingUsers.length: ' + _followingUsers.length.toString());
    }

    //My private is a special case which filter-out plurks only limit to me.
    this.myPrivate = myPrivate;
    this.spy = spy;

    feedsCache = await compute(updateFeedCache, this);
    // _updateFeedCache();
    // print('feedsCache.length: ' + feedsCache.length.toString());
  }

  // Add a new TimelineGetPlurks result here. (with options to form _feedCache)
  Future add(Plurdart.Plurks plurks,
      {Map<int, Plurdart.Completion> completionMap,
      List<Plurdart.User> limitUsers,
      bool myPrivate = false,
      bool spy = false}) async {
    if (plurks.plurks.length > 0) {
      collection.add(plurks);
    }

    if (completionMap != null) {
      this.completionMap = completionMap;
    }

    if (limitUsers != null) {
      this.limitUsers = limitUsers.map((e) => e.id).toList();
      // print('_followingUsers.length: ' + _followingUsers.length.toString());
    }

    //My private is a special case which filter-out plurks only limit to me.
    this.myPrivate = myPrivate;
    this.spy = spy;

    feedsCache = await compute(updateFeedCache, this);
    // _updateFeedCache();
    // print('feedsCache.length: ' + feedsCache.length.toString());
  }

  //Multiple version
  Future addMultiple(List<Plurdart.Plurks> plurksList,
      {Map<int, Plurdart.Completion> completionMap,
      List<Plurdart.User> limitUsers,
      bool myPrivate = false,
      bool spy = false}) async {
    if (plurksList != null && plurksList.length > 0) {
      for (int i = 0; i < plurksList.length; ++i) {
        if (plurksList[i].plurks.length > 0) {
          collection.add(plurksList[i]);
        }
      }
    }

    if (completionMap != null) {
      this.completionMap = completionMap;
    }

    if (limitUsers != null) {
      this.limitUsers = limitUsers.map((e) => e.id).toList();
      // print('_followingUsers.length: ' + _followingUsers.length.toString());
    }

    //My private is a special case which filter-out plurks only limit to me.
    this.myPrivate = myPrivate;
    this.spy = spy;

    feedsCache = await compute(updateFeedCache, this);
    // print('feedsCache.length: ' + feedsCache.length.toString());
    // _updateFeedCache();
  }

  Future setTopPlurks(
    Plurdart.TopPlurks topPlurks,
  ) async {
    this.topPlurks = topPlurks;

    feedsCache = await compute(updateFeedCache, this);
    // print('feedsCache.length: ' + feedsCache.length.toString());
    // _updateFeedCache();
  }

  Future setAnonymousPlurks(
    Plurdart.TopicPlurks anonymousPlurks,
  ) async {
    this.anonymousPlurks = anonymousPlurks;

    feedsCache = await compute(updateFeedCache, this);
    // print('feedsCache.length: ' + feedsCache.length.toString());
    // _updateFeedCache();
  }

  Future setBookmarksPlurks(
    Plurdart.Bookmarks bookmarks,
  ) async {
    // print('[setBookmarksPlurks]');
    this.bookmarks = bookmarks;

    feedsCache = await compute(updateFeedCache, this);
    // print('feedsCache.length: ' + feedsCache.length.toString());
    // _updateFeedCache();
  }

  Future setOfficialNewsPlurks(
      List<Plurdart.Plurk> officialNewsPlurks,
      ) async {
    this.officialNewsPlurks = officialNewsPlurks;

    feedsCache = await compute(updateFeedCache, this);
    // print('feedsCache.length: ' + feedsCache.length.toString());
    // _updateFeedCache();
  }

  Future overridePlurk(Plurdart.Plurk plurk) async {
    //_collection
    for (int i = 0; i < collection.length; ++i) {
      for (int p = 0; p < collection[i].plurks.length; ++p) {
        if (collection[i].plurks[p].plurkId == plurk.plurkId) {
          //Replace this one.
          collection[i].plurks[p] = plurk;
        }
      }
    }

    //_feedCache
    feedsCache = await compute(updateFeedCache, this);
    // print('feedsCache.length: ' + feedsCache.length.toString());
    // _updateFeedCache();
  }

  //--

  //[Dep]: We cannot merely do this...
  // Future<bool> setIsViewingUnreadOnly(bool b) async {
  //   if (b != isViewingUnreadOnly) {
  //     isViewingUnreadOnly = b;
  //     // print('setIsViewingUnreadOnly: ' + b.toString());
  //     //_feedCache
  //     feedsCache = await compute(updateFeedCache, this);
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  //This is troublesome
  Future marksAllRead(List<int> ids) async {
    for (int i = 0; i < collection.length; ++i) {
      List<Plurdart.Plurk> plurks = collection[i].plurks;
      // print('[_collection][' + i.toString() + '] plurks length [' + plurks.length.toString() + ']');
      //Tricky! It is possible the different plurks contain same plurk! due to API nature.
      for (int p = 0; p < plurks.length; ++p) {
        if (ids.contains(plurks[p].plurkId) && plurks[p].isUnread != 2) {
          plurks[p].isUnread = 0;
        }
      }
    }

    //TopPlurks
    if (topPlurks != null) {
      topPlurks.plurks.forEach((plurk) {
        if (ids.contains(plurk.plurkId) && plurk.isUnread != 2) {
          plurk.isUnread = 0;
        }
      });
    }

    //AnonymousPlurks
    if (anonymousPlurks != null) {
      anonymousPlurks.plurks.forEach((key, value) {
        if (ids.contains(value.plurkId) && value.isUnread != 2) {
          value.isUnread = 0;
        }
      });
    }

    //Bookmarks
    if (bookmarks != null) {
      bookmarks.plurks.forEach((p) {
        if (ids.contains(p.plurkId) && p.isUnread != 2) {
          p.isUnread = 0;
        }
      });
    }

    //officialNewsPlurks
    if (officialNewsPlurks != null) {
      // print('_officialNewsPlurks.plurks.length: ' + _officialNewsPlurks.plurks.length.toString());
      officialNewsPlurks.forEach((value) {
        if (ids.contains(value.plurkId) && value.isUnread != 2) {
          value.isUnread = 0;
        }
      });
    }
  }

  // Add a new Profile plurks result here.
  //[Dep]: This one will not provide enough User data.
  // void addPlurkList(List<Plurdart.Plurk> plurks) {
  //   //Add a dummy Plurks modal.
  //   _collection.add(Plurdart.Plurks(
  //     plurks: plurks,
  //     plurkUsers: Map<int, Plurdart.User>(),
  //   ));
  //   _updateFeedCache();
  // }

  // void _updateFeedCache() {
  //   feedsCache.clear();
  //   for (int i = 0; i < collection.length; ++i) {
  //     List<Plurdart.Plurk> plurks = collection[i].plurks;
  //     // print('[_collection][' + i.toString() + '] plurks length [' + plurks.length.toString() + ']');
  //     //Tricky! It is possible the different plurks contain same plurk! due to API nature.
  //     for (int p = 0; p < plurks.length; ++p) {
  //       //check if every plurk is unique. Filter out those already exist.
  //       if (_isFeedCacheContainsPlurk(plurks[p].plurkId)) {
  //         //Skip this one.
  //         print('[found duplicated plurk][' +
  //             plurks[p].plurkId.toString() +
  //             '] Skip it.');
  //         continue;
  //       }
  //
  //       //[Tricky!] Check if there's only one limit to.
  //       if (this.myPrivate) {
  //         List<int> limitTo = plurks[p].getLimitedTo();
  //         //My private only limit to one ppl.
  //         if (limitTo != null && limitTo.length != 1) {
  //           continue;
  //         }
  //
  //         // print('ony one limit: ' + limitTo[0].toString());
  //         if (limitTo[0] != Static.meId) {
  //           continue;
  //         }
  //       }
  //
  //       //[Tricky!] Check spy.
  //       if (this.spy) {
  //         if (plurks[p].anonymous == null ||
  //             !plurks[p].anonymous ||
  //             plurks[p].replurkerId != null) {
  //           continue;
  //         }
  //       }
  //
  //       Plurdart.User plurkOwner = collection[i].plurkUsers[plurks[p].ownerId];
  //
  //       if (plurkOwner != null) {
  //         //We need replurker for check following and friends.
  //         Plurdart.User plurkReplurker;
  //         if (plurks[p].replurkerId != null) {
  //           if (collection[i].plurkUsers.containsKey(plurks[p].replurkerId)) {
  //             plurkReplurker = collection[i].plurkUsers[plurks[p].replurkerId];
  //             // print('Got plurkReplurker: [' + plurkReplurker.displayName + ']');
  //           } else {
  //             // print('Oops! plurkUsers does not contains replurkerId [' + plurks[p].replurkerId.toString() + ']');
  //           }
  //         } else {
  //           // print('replurkerId is null.');
  //         }
  //
  //         //Check if use completion to filter only friends plurks...
  //         if (this.completionMap == null ||
  //             this.completionMap.containsKey(plurkOwner.id) ||
  //             (plurkReplurker != null &&
  //                 this.completionMap.containsKey(plurkReplurker.id))) {
  //           //Either no need to filter or this is my friend's plurk
  //
  //           //Check if I am following this guy?
  //           if (this.limitUsers == null ||
  //               this.limitUsers.contains(plurkOwner.id) ||
  //               (plurkReplurker != null &&
  //                   this.limitUsers.contains(plurkReplurker.id))) {
  //             //Either no need to filter or I am following him.
  //
  //             // if (plurkOwner == null) {
  //             //  print('User not exist: [' + plurks[p].userId.toString() + ']');
  //             // } else {
  //             //  print('User exist: [' + plurkOwner.nickName + ']');
  //             // }
  //
  //             //Cache those been filtered.
  //             feedsCache.add(PlurkFeed(
  //                 plurks[p],
  //                 plurkOwner,
  //                 plurkReplurker,
  //                 collection[i].queryUsers(plurks[p].favorers),
  //                 collection[i].queryUsers(plurks[p].replurkers)));
  //           }
  //         }
  //       } else {
  //         print('Oops! PlurkOwner is null!? This should not happen!');
  //       }
  //     }
  //   }
  //
  //   //TopPlurks
  //   if (this.topPlurks != null) {
  //     // print('this._topPlurks.plurks.length: ' + this._topPlurks.plurks.length.toString());
  //     this.topPlurks.plurks.forEach((plurk) {
  //       // print('add plurk feed [' + plurk.plurkId.toString() + ']');
  //       feedsCache.add(PlurkFeed(plurk, plurk.owner, null, null, null));
  //     });
  //   }
  //
  //   //AnonymousPlurks
  //   if (this.anonymousPlurks != null) {
  //     // print('this._anonymousPlurks.plurks.length: ' + this._anonymousPlurks.plurks.length.toString());
  //     this.anonymousPlurks.plurks.forEach((key, value) {
  //       // print('add plurk feed [' + value.plurkId.toString() + ']');
  //       //Anonymous Plurk does not has an Owner.
  //       feedsCache.add(PlurkFeed(value, null, null, null, null));
  //     });
  //   }
  //
  //   //Bookmarks
  //   if (this.bookmarks != null) {
  //     this.bookmarks.plurks.forEach((p) {
  //       feedsCache.add(PlurkFeed(
  //           p,
  //           this.bookmarks.queryUser(p.ownerId),
  //           null,
  //           this.bookmarks.queryUsers(p.favorers),
  //           this.bookmarks.queryUsers(p.replurkers),
  //           bookmark: this.bookmarks.queryBookmark(p.plurkId),
  //       ));
  //     });
  //   }
  // }

  // bool _isFeedCacheContainsPlurk(int plurkId) {
  //   PlurkFeed foundFeed = feedsCache
  //       .firstWhere((element) => element.plurk.plurkId == plurkId, orElse: () {
  //     return null;
  //   });
  //
  //   return (foundFeed != null);
  // }

  // Get the current top offset. (time)
  DateTime getTopOffset() {
    if (collection.length > 0) {
      Plurdart.Plurks firstPlurks = collection.first;
      Plurdart.Plurk firstPlurk = firstPlurks.plurks.first;
      if (firstPlurk != null) {
        try {
          return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
              .parse(firstPlurk.posted);
        } catch (e) {
          print('Oops! Cannot parse posted time: ' + firstPlurk.posted);
          return DateTime.now();
        }
      }
    }
    return DateTime.now();
  }

  // Get the current bottom offset. (time)
  DateTime getBottomOffset() {
    if (collection.length > 0) {
      Plurdart.Plurks lastPlurks = collection.last;
      Plurdart.Plurk lastPlurk = lastPlurks.plurks.last;
      if (lastPlurk != null) {
        try {
          return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
              .parse(lastPlurk.posted);
        } catch (e) {
          print('Oops! Cannot parse posted time: ' + lastPlurk.posted);
          return DateTime.now();
        }
      }
    }
    return DateTime.now();
  }

  // Clear all.
  void clear() {
    collection.clear();
    this.topPlurks = null;
    this.anonymousPlurks = null;
    this.officialNewsPlurks = null;
    this.completionMap = null;
    this.limitUsers = null;
    this.myPrivate = false;
    this.spy = false;
    feedsCache.clear();
  }

  void deletePlurk(int plurkId) {
    //_collection
    for (int i = 0; i < collection.length; ++i) {
      collection[i]
          .plurks
          .removeWhere((element) => element.plurkId == plurkId);
    }

    //_feedCache
    feedsCache.removeWhere((element) => element.plurk.plurkId == plurkId);
  }

  // A convenient function for getting the necessary info for display home feed.
  //<Plurk, Owner, Replurker>
  List<PlurkFeed> feeds() {
    // print('_feedCache.length: ' + _feedCache.length.toString());
    return feedsCache;
  }

  // How many PlurkCard do we need?
  int feedsLength() {
    return feedsCache.length;
  }
}

//Represent a PlurkFeed use by a PlurkCard.
class PlurkFeed {
  PlurkFeed(
      this.plurk, this.owner, this.replurker, this.favorers, this.replurkers,
      {this.bookmark});

  final Plurdart.Plurk plurk;
  final Plurdart.User owner;
  final Plurdart.User replurker;

  //Only if this is a bookmark feed
  final Plurdart.Bookmark bookmark;

  final List<Plurdart.User> favorers;
  final List<Plurdart.User> replurkers;
}
