import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:clipboard/clipboard.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:core';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/theme/app_themes.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:share_plus/share_plus.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

void show(BuildContext context, ImageLinksViewerArgs args) {
  //Nope: this will cause a bit glitch.
//  SystemChrome.setEnabledSystemUIOverlays([]);
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _ImageLinksViewer(args);
  }));

  //Nope: this will make Hero unusable.
//  showDialog(
//    useSafeArea: false,
//    barrierColor: Theme.of(context).canvasColor,
//    barrierDismissible: false,
//    routeSettings: RouteSettings(
//      name: 'image_links_viewer',
//    ),
//    context: context,
//    builder: (BuildContext context) {
//      return _ImageLinksViewer(args);
//    },
//  );
}

//The args for this view.
class ImageLinksViewerArgs {
  final List<PlurkCardImageLink> imageLinks;
  final int initialPage;

  ImageLinksViewerArgs(this.imageLinks, this.initialPage);
}

// The image viewer for PlurkCardImageLinks.
class _ImageLinksViewer extends StatefulWidget {
  _ImageLinksViewer(this.args)
      : pageController = PageController(initialPage: args.initialPage);

  final ImageLinksViewerArgs args;
  final PageController pageController;

  @override
  _ImageLinksViewerState createState() => _ImageLinksViewerState();
}

class _ImageLinksViewerState extends State<_ImageLinksViewer>
    with AfterLayoutMixin {
  _ImageLinksViewerState();

  String _displayingFileInfo = '';

  //Allow close image gesture only when the scale state is initial.
  PhotoViewScaleState _currentScaleState = PhotoViewScaleState.initial;

  static const double VIRTUAL_MAX_VERTICAL_DRAG_OFFSET = 150.0;
  static const double GESTURE_SENSITIVITY_TO_CLOSE = 50.0;
  Offset _translateOffset = Offset(0, 0);

  int _frontLayerOpacity = 1;

  Color _indicatorColor = Colors.white;

  int _currentPageIndex = 0;

  @override
  void initState() {
    HapticFeedback.lightImpact();
    super.initState();
    _currentPageIndex = widget.args.initialPage;
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    setState(() {
      _indicatorColor = Theme.of(context).primaryColor;
      if (Static.settingsAppTheme == AppTheme.Pure) {
        //Force the Pure theme indicator color to white color. The black color looks suck.
        _indicatorColor = Theme.of(context).textTheme.bodyText1.color;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: Stack(
            children: [
              //PhotoViewer.
              GestureDetector(
                child: Transform.translate(
                  offset: _translateOffset,
                  child: PhotoViewGallery.builder(
                    backgroundDecoration:
                        BoxDecoration(color: Colors.transparent),
                    scrollPhysics: const BouncingScrollPhysics(),
                    builder: (BuildContext context, int index) {
                      return PhotoViewGalleryPageOptions(
                        //Check if the link has already been downloaded. (has a fullImageRawInfo data)
                        imageProvider: widget
                                    .args.imageLinks[index].fullImageRawInfo ==
                                null
                            ? CachedNetworkImageProvider(
                                widget.args.imageLinks[index].getFullImageUrl())
                            : MemoryImage(widget.args.imageLinks[index]
                                .fullImageRawInfo.uint8list),

                        initialScale: PhotoViewComputedScale.contained,
                        heroAttributes: PhotoViewHeroAttributes(
                            tag: widget.args.imageLinks[index].getHeroTag()),
                        tightMode: true,
                        minScale: PhotoViewComputedScale.contained,
                        maxScale: PhotoViewComputedScale.contained * 10.0,
                      );
                    },
                    itemCount: widget.args.imageLinks.length,
                    loadingBuilder: (context, event) => Center(
                      child: Container(
                        width: 20.0,
                        height: 20.0,
                        child: CircularProgressIndicator(
                          value: event == null
                              ? 0
                              : event.cumulativeBytesLoaded /
                                  event.expectedTotalBytes,
                        ),
                      ),
                    ),
                    pageController: widget.pageController,
                    onPageChanged: _onViewerPageChanged,
                    scaleStateChangedCallback: _onViewerScaleStateChanged,
                  ),
                ),
                onVerticalDragUpdate: _currentScaleState == PhotoViewScaleState.initial ? (details) {
                  //Let's try this...
                  //A magic way to scale the delta by current offset rate to GESTURE_SENSITIVITY_TO_CLOSE
                  double deltaScale = 1 -
                      (_translateOffset.dy.abs() /
                          VIRTUAL_MAX_VERTICAL_DRAG_OFFSET);
                  setState(() {
                    _translateOffset =
                        details.delta * deltaScale + _translateOffset;
                  });
                } : null,
                onVerticalDragEnd: _currentScaleState == PhotoViewScaleState.initial ? (details) {
                  // print('onVerticalDragEnd');

                  //Check the offset to see if we should close the viewer.
                  if (_translateOffset.dy.abs() >=
                      GESTURE_SENSITIVITY_TO_CLOSE) {
                    if (_flushBar != null) {
                      _flushBar.dismiss();
                    }

                    HapticFeedback.lightImpact();

                    Navigator.pop(context);
                  } else {
                    //Transform back to center.
                    setState(() {
                      _translateOffset = Offset(0, 0);
                    });
                  }
                } : null,
                onTap: () {
                  HapticFeedback.lightImpact();
                  setState(() {
                    if (_frontLayerOpacity != 0) {
                      _frontLayerOpacity = 0;
                    } else {
                      _frontLayerOpacity = 1;
                    }
                  });
                },
              ),

              //Front layer to display a back arrow button on the left top.
              AnimatedOpacity(
                duration: Duration(milliseconds: 150),
                opacity: _frontLayerOpacity.toDouble(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    //The upper left arrow bar
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      height: kToolbarHeight,
                      child: Row(
                        children: [
                          //Arrow button
                          AppBarActionButton(Icons.arrow_back,
                              iconColor: Theme.of(context).textTheme.bodyText1.color, onTap: () {
                            HapticFeedback.lightImpact();
                            Navigator.pop(context);
                          }),
                          Expanded(
                              child: Text(
                            _displayingFileInfo,
                            textAlign: TextAlign.center,
                          )),
                          //Options button
                          PopupMenuButton<String>(
                              onSelected: _onPopupSelected,
                              itemBuilder: (context) {
                                return [
                                  menuItem('Share Link', Icons.share, 'Share Link'),
                                  menuItem('Copy Link', Icons.copy, 'Copy Link'),
                                  menuItem('Save', Icons.save_alt, 'Save'),
                                ];
                              }),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    //Hide the indicator if only one link is provided.
                    Visibility(
                      visible: widget.args.imageLinks.length > 1,
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: SmoothPageIndicator(
                          controller: widget.pageController,
                          count: widget.args.imageLinks.length,
                          effect: WormEffect(
                            activeDotColor: _indicatorColor,
                            dotWidth: 8,
                            dotHeight: 8,
                          ),
                          onDotClicked: (index) {
                            //Do nothing.
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: kMinInteractiveDimension,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  PopupMenuItem<String> menuItem(String value, IconData icon, String text) {
    return PopupMenuItem(
      value: value,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: Theme.of(context).iconTheme.color,
          ),
          SizedBox(
            width: 8,
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 5), child: Text(text)),
        ],
      ),
    );
  }

  _onViewerPageChanged(int index) {
    // print('_onViewerPageChanged: ' + index.toString());
    setState(() {
      _currentScaleState = PhotoViewScaleState.initial;
    });

    _currentPageIndex = index;
  }

  _onViewerScaleStateChanged(PhotoViewScaleState state) {
    // print('_onViewerScaleStateChanged: ' + state.toString());
    setState(() {
      _currentScaleState = state;
    });
  }

  _onPopupSelected(String value) async {
    switch (value) {
      case 'Share Link':
        HapticFeedback.lightImpact();
        if (_currentPageIndex >= 0 &&
            _currentPageIndex < widget.args.imageLinks.length) {
          Share.share(
              widget.args.imageLinks[_currentPageIndex].getFullImageUrl());
        }
        break;
      case 'Copy Link':
        HapticFeedback.lightImpact();
        FlutterClipboard.copy(widget.args.imageLinks[_currentPageIndex].getFullImageUrl());
        _showFlushBar(context, Icons.content_copy, 'Link copied.');
        break;
      case 'Save':
        HapticFeedback.lightImpact();
        if (_currentPageIndex >= 0 &&
            _currentPageIndex < widget.args.imageLinks.length) {
          String imageUrl =
              widget.args.imageLinks[_currentPageIndex].getFullImageUrl();

          File cachedFile = await DefaultCacheManager().getSingleFile(imageUrl);
          if (cachedFile != null) {
            // int fileSize = await cachedFile.length();
            // print('Got file size: [' + fileSize.toString() + ']');

            PermissionStatus status = await Permission.storage.status;
            if (!status.isGranted) {
              status = await Permission.storage.request();
            }

            if (status.isGranted) {
              Uri imageUri = Uri.parse(imageUrl);
              Map<dynamic, dynamic> saveResult =
                  await ImageGallerySaver.saveImage(
                      cachedFile.readAsBytesSync(),
                      quality: 100,
                      name: imageUri.pathSegments.last);

              if (saveResult != null && saveResult['isSuccess'] == true) {
                _showFlushBar(context, Icons.save_alt, 'File Saved!');
              }
            } else {
              _showFlushBar(context, Icons.warning, 'Permission denied.');
            }

            // if (status.isGranted) {
            //
            //   if (Platform.isAndroid) {
            //     //Save the file...
            //     String picturesPath = await AndroidPathProvider.picturesPath;
            //     String urlFileName = widget.args.imageLinks[_currentPageIndex].getFullImageFileName();
            //     if (picturesPath != null) {
            //       Directory targetDir = await Directory(picturesPath + '/BahKutTeh').create();
            //       if (targetDir != null) {
            //         File copiedFile = await cachedFile.copy(targetDir.path + '/' + urlFileName);
            //         print('copy file[' + targetDir.path + '/' + urlFileName + ']');
            //         if (copiedFile != null) {
            //           _showFlushBar(context, Icons.save_alt, 'File Saved!', targetDir.path + '/' + urlFileName);
            //         } else {
            //           _showFlushBar(context, Icons.warning, 'Oops!', 'Copy file failed!');
            //         }
            //       } else {
            //         _showFlushBar(context, Icons.warning, 'Oops!', 'Create directory failed!');
            //       }
            //     } else {
            //       _showFlushBar(context, Icons.warning, 'Oops!', 'Cannot get the directory.');
            //     }
            //   } else if (Platform.isIOS) {
            //     //Todo
            //   }
            // } else {
            //   _showFlushBar(context, Icons.warning, 'Oops!', 'Permission denied.');
            // }
          } else {
            // print('Oops! cachedFile is null!');
            _showFlushBar(context, Icons.warning,
                'Waiting for the file. Try again later.');
          }
        }
        break;
    }
  }

  //The showing FlushBar
  Flushbar _flushBar;
  _showFlushBar(BuildContext context, IconData icon, String message) {
    _flushBar = Flushbar(
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      duration: Duration(seconds: 3),
      onStatusChanged: (status) {
        if (status == FlushbarStatus.DISMISSED) {
          //Forget the reference.
          _flushBar = null;
        }
      },
    )..show(context);
  }
}
