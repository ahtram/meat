part of 'plurk_search_cubit.dart';

@immutable
abstract class PlurkSearchState {}

class PlurkSearchInitial extends PlurkSearchState {}

class PlurkSearchSuccess extends PlurkSearchState {
  PlurkSearchSuccess(this.plurkSearch);
  final Plurdart.PlurkSearch plurkSearch;
}

class PlurkSearchFailed extends PlurkSearchState {
  PlurkSearchFailed(this.errorText);
  final String errorText;
}

class PlurkSearchTrackBackSuccess extends PlurkSearchState {
  PlurkSearchTrackBackSuccess(this.plurkSearch);
  final Plurdart.PlurkSearch plurkSearch;
}

class PlurkSearchTrackBackFailed extends PlurkSearchState {
  PlurkSearchTrackBackFailed(this.errorText);
  final String errorText;
}