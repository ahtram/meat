import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/rest/hotlinks_get_links_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/models/plurk/hot_links_collection.dart';
// import 'package:meat/widgets/ad/feed_ad_widget.dart';
import 'package:meat/widgets/hot_links/hot_link_card.dart';
import 'package:meat/widgets/other/empty_logo.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;

class HotLinksViewer extends StatefulWidget {
  HotLinksViewer({Key key}) : super(key: key);

  @override
  _HotLinksViewerState createState() => _HotLinksViewerState();
}

class _HotLinksViewerState extends State<HotLinksViewer> with AfterLayoutMixin {
  BuildContext _blocProviderContext;
  RefreshController _refreshController = RefreshController();

  //We'll need a fancy class to store Plurks.
  HotlinksCollection _hotlinksCollection = HotlinksCollection();
  bool _timelineUpdateError = false;
  bool _isRequestingFeed = false;
  //Is case we need this...
  bool _noHistoryRemained = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //This could prevent from assert error or smartRefresher.
    if (_refreshController.position != null) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _refreshController.requestRefresh();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HotlinksGetLinksCubit>(
          create: (BuildContext context) => HotlinksGetLinksCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<HotlinksGetLinksCubit, HotlinksGetLinksState>(
            listener: (context, state) {
              if (state is HotlinksGetLinksSuccess) {
                _onTimelineRefreshPlurksState(state.hotlinks);
              } else if (state is HotlinksGetLinksFailed) {
                _onTimelineRefreshPlurksState(null);
              } else if (state is HotlinksTrackBackLinksSuccess) {
                _onTimelineTrackBackState(state.hotlinks);
              } else if (state is HotlinksTrackBackLinksFailed) {
                _onTimelineTrackBackState(null);
              }
              // _onHotlinksGetLinksState(state);
            },
          ),
        ],
        child: Builder(builder: (context) {
          _blocProviderContext = context;

          List<Widget> sliverWidgets = [];

          //A lot of cards.
          sliverWidgets.add(SliverList(
            delegate: SliverChildListDelegate(
              _buildLinkCards(),
            ),
          ));

          //Why do we need this?
          sliverWidgets.add(SliverFillRemaining(
            hasScrollBody: false,
            child: Container(
              height: MediaQuery.of(context).viewInsets.bottom,
            ),
          ));

          Color indicatorColor = Theme.of(context).textTheme.bodyText1.color;

          return NotificationListener<ScrollNotification>(
              child: SmartRefresher(
                controller: _refreshController,
                header: WaterDropMaterialHeader(
                  color: indicatorColor,
                ),
                onRefresh: _onRefresh,
                child: CustomScrollView(
                  controller: PrimaryScrollController.of(context),
                  slivers: sliverWidgets,
                ),
              ),
              onNotification: (scrollNotification) {
                double triggerFetchMoreSize =
                    0.75 * scrollNotification.metrics.maxScrollExtent;
                if (scrollNotification.metrics.pixels > 10 &&
                    scrollNotification.metrics.pixels > triggerFetchMoreSize) {
                  // call fetch more method here
                  _trackBackPlurkTimeline();
                }

                return true;
              });
        }),
      ),
    );
  }

  //For SliverChildListDelegate
  List<Widget> _buildLinkCards() {
    List<Plurdart.HotLink> feeds = _hotlinksCollection.getFeeds();

    List<Widget> returnWidgets = [];
    //If the list is empty...
    if (feeds.length == 0) {
      //Display an empty logo.
      returnWidgets.add(EmptyLogo());
    } else {
      //Display ADWidgets??
      bool shouldDisplayADs = Static.shouldDisplayAds();
      int currentFeedCountPerAd = Define.HOTLINK_COUNT_PER_AD;
      int adAppearCountDown = currentFeedCountPerAd;

      for (int i = 0; i < feeds.length; ++i) {
        //All feeds to cards.
        returnWidgets.add(HotLinkCard(
          feeds[i],
        ));

        //Only when not premium and set to show ads.
        if (shouldDisplayADs) {
          //[Dep]
          // adAppearCountDown--;
          // if (adAppearCountDown == 0) {
          //   returnWidgets.add(FeedAdWidget());
          //   if (currentFeedCountPerAd < Define.HOTLINK_COUNT_PER_AD_MAX) {
          //     currentFeedCountPerAd += Define.HOTLINK_COUNT_PER_AD_INC;
          //   }
          //   adAppearCountDown = currentFeedCountPerAd;
          // }
        }
      }
    }

    return returnWidgets;
  }

  _onRefresh() {
    //Active refresh timeline.
    _refreshTimeline();
  }

  _refreshTimeline() {
    if (!_isRequestingFeed) {
      _isRequestingFeed = true;
      _noHistoryRemained = false;
      _timelineUpdateError = false;

      _blocProviderContext.read<HotlinksGetLinksCubit>().get();

      //No offset means get from the newest!.
    }
  }

  _onTimelineRefreshPlurksState(List<Plurdart.HotLink> hotlinks) async {
    //Update the plurks data and setState for the ListView.builder.
    // print('[Cubit response]: TimelineGetPlurksCubit');
    if (hotlinks != null) {
      if (hotlinks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _hotlinksCollection.clear();

      setState(() {
        _hotlinksCollection.add(hotlinks);
        _timelineUpdateError = false;
      });

      _refreshController.refreshCompleted();
    } else {
      _refreshController.refreshFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRequestingFeed = false;
  }

  _trackBackPlurkTimeline() {
    //Only Home mode and trackback
    if (!_isRequestingFeed && !_noHistoryRemained && !_timelineUpdateError) {
      _isRequestingFeed = true;

      _blocProviderContext.read<RequestIndicatorCubit>().show();
      _blocProviderContext
          .read<HotlinksGetLinksCubit>()
          .trackBack(offset: _hotlinksCollection.getFeedsLength());
      //No offset means get from the newest!.
    }
  }

  _onTimelineTrackBackState(List<Plurdart.HotLink> hotlinks) async {
    _blocProviderContext.read<RequestIndicatorCubit>().hide();

    if (hotlinks != null) {
      if (hotlinks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //Built-in filters: filtered by server.
      setState(() {
        _hotlinksCollection.add(hotlinks);
        _timelineUpdateError = false;
      });
    } else {
      _refreshController.refreshFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRequestingFeed = false;
  }
}
