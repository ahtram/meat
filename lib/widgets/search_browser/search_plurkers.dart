import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/widgets/user/users_viewer.dart';

class SearchPlurkers extends StatefulWidget {
  SearchPlurkers({Key key}) : super(key: key);

  @override
  SearchPlurkersState createState() => SearchPlurkersState();
}

class SearchPlurkersState extends State<SearchPlurkers> with AutomaticKeepAliveClientMixin<SearchPlurkers> {

  //The search term we are using.
  String _searchTerm = '';
  GlobalKey<UsersViewerState> _usersViewerGlobalKey = GlobalKey<UsersViewerState>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Builder(
      builder: (context) {

        //Do we have a search term?
        if (_searchTerm.isEmpty != true) {
          //We have a search term.
          //Display the search PlurkViewer.
          return UsersViewer(
            UsersViewerArgs(
              'Search',
              UsersBrowsingType.Search,
              searchTerm: _searchTerm,
              isToggleList: false,
            ),
            key: _usersViewerGlobalKey,
          );
        } else {
          return Container(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    MdiIcons.accountSearchOutline,
                    size: 96,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    'Looking for someone?',
                    style: Theme.of(context).textTheme.caption.copyWith(fontSize: 14),
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  runSearch(String searchTerm) {
    // print('runSearch[' + searchTerm + ']');
    if (_searchTerm.isEmpty == true) {
      // print('_searchTerm = searchTerm');
      //First time. Just setState will do the work
      setState(() {
        _searchTerm = searchTerm;
      });
    } else {
      //Not first time. Will need to refreshSearchTerm.
      _searchTerm = searchTerm;
      if (_usersViewerGlobalKey.currentState != null) {
        // print('refreshSearchTerm[' + _searchTerm + ']');
        _usersViewerGlobalKey.currentState.refreshSearchTerm(_searchTerm);
      }
    }
  }
}