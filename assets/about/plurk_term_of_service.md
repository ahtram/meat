## **Terms of Service**

By using the Plurk.com (the "Service"), a service of Plurk Limited. ("Plurk"), you accept the following terms and conditions (the "Terms of Service"). Please also be aware of the Privacy Policy located on Plurk.com.  Usage of the service is also governed by the privacy policy. In case of inconsistency between the TOS and the privacy policy, these terms of service shall prevail.

### **Main Features of the Service and the Software**

The main features of the Service let you:

* post updates to your plurklog located at https://www.plurk.com/yourusername using the Plurk web service, and/or using mobile apps on mobile phones.
* add Plurk members as your friends on the service and follow their plurklogs; and
browse the plurklogs and plurks of your contacts and other Plurk members who have given sufficient user-access privileges for you to do so

### **General Conditions**

* You must be at least 13 years of age to use the Service.
* You agree to keep your password secure.
* You are solely responsible for any activity that occurs under your screen name and account.
* You must not harass, intimidate, threaten or impersonate other Plurk users.
* You agree not to engage in any illegal or unauthorized conduct and agree to comply with all local laws that may be applicable to you regarding acceptable online conduct, behaviour and content usage.
* You are solely responsible for any data, screen names, graphics, photos, profiles, audio and video clips, links and any other content (the "Content") that you submit, post and display on Plurk.
* We may, but have no obligation to, remove Content and accounts containing Content that we consider in our sole discretion being unlawful, offensive, threatening, obscene or otherwise objectionable or violate any party's intellectual property rights. While Plurk prohibits such Content on the site, you understand and agree that the Content posted on the Plurk Service by its users is not necessarily reviewed by Plurk and Plurk cannot be held responsible for this Content.
* You agree to indemnify, defend and hold Plurk harmless from and against any and all claims, damages, losses and expenses (including but not limited to reasonable legal fees) relating to any acts by you, including but not limited to any Content submitted by you, in connection with using the Service, resulting in claims against us by other users or third parties.
* You agree not to modify, adapt or hack the Plurk Service, transmit any worms or viruses or any code of a destructive nature.
* You agree not to modify another website so as to falsely imply that it is associated with the Plurk Service.
* You agree not to send any unsolicited commercial messages to any Plurk users. (ie "Spam")
* We reserve the right to modify or terminate the Service, and to refuse the Service to anyone, for any reason, with or without notice, at any time.
* You agree that we may reclaim usernames on the Service on behalf of businesses or individuals who hold legal claim or trademark on those usernames.
* These Terms of Use shall be governed by the laws of Cayman Islands and you submit to the exclusive jurisdiction of the Cayman Islands courts.

### **Content Policy**

* Plurk is a platform to discuss and connect in an open environment. The nature of Plurk's content might be funny, serious, offensive or anywhere in between. While Plurk provides a lot of leeway in what content is acceptable, here are some guidelines for content that is not. If you are not sure, remember the rule of thumb: show enough respect to others so that we all may continue to enjoy Plurk for what it is.

### **Copyright**

The Service is protected by copyright, trademark, trade secrets and other intellectual property rights of Plurk. You agree to use the Service pursuant to these Terms of Use, and not to reproduce, modify, translate, distribute, sublicense, or create derivative works based on, the Service in any form unless otherwise expressly provided in these Terms of Service.

We claim no ownership of any intellectual property rights over the Content that you upload to the Service. Any intellectual property rights in the uploaded Content belong solely to you (or your licensors). However, by uploading the Content to the Service, you grant Plurk a non-exclusive, royalty-free, worldwide, perpetual and irrevocable right to use, reproduce, modify, distribute, sublicense, and prepare derivative works of, such Content, in any format or medium now known or later developed, solely on and through the Service, subject to these Terms of Service and our privacy policy. You represent and warrant that you have sufficient rights to grant this license.

We will review all claims of copyright infringement received and remove the Content deemed to have been posted or distributed in violation of any such laws. To make a claim, please provide us with the following:

* A description of the copyrighted work that you claim has been infringed;
* Info sufficient to enable Plurk to locate the allegedly infringing Content on the Service;
* Your address, phone number and email address;
* Statement by you that you have good faith belief that the use of the Content is not authorized;
* Statement by you that the info in your notification is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf; and
* A signature of the person authorized to act on behalf of the owner of the copyright interest.

Claims can be sent to copyrightcomplaints (at) plurk.com or by snail mail to

Plurk.com
P.O. Box 2582
Grand Cayman KY1-1103
Cayman Islands

### **Limitation of Liability**

The Service and the Software are made available to you for your convenience on an "as is" and "as available" basis. PLURK DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS OR WARRANTIES (INCLUDING BUT NOT LIMITED TO MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, SATISFACTORY QUALITY, RELIABILITY OR NON-INFRINGEMENT) REGARDING THE USE OR THE RESULTS OF THE USE OR THE CONTENTS OF THE SERVICE OR THE SOFTWARE. Plurk does not warrant or represent that the Software will be free of all viruses. You understand that the Service and the Software are still alpha/beta versions and may not perform with complete functionality, may be undergoing testing, may be inconsistently available, may have software "bugs" and may have other issues affecting availability and functionality. In addition to all other disclaimers and limitations of liability contained in these Terms of Use, you specifically agree that Plurk is not liable for any of the above issues. Moreover, you understand that Plurk does not warrant or represent that the Service and/or the Software will work on all mobile phones, will be compatible with all mobile phone networks and/or will be available in all geographical areas.

PLURK SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE, WHETHER DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL, ARISING OUT OF THE USE OF OR THE INABILITY TO USE THE SERVICE OR THE SOFTWARE, OR THE RESULTS OR THE CONTENTS THEREOF, TO THE GREATEST EXTENT PERMITTED BY LAW.

If you are dissatisfied with the Service or the Software, or have any other disputes or claims with or against Plurk with respect to the Service or the Software or these Terms of Use, then your sole and exclusive remedy is to discontinue using the Service and/or the Software.

### **Modification of these Terms of Use**

We are continuously developing the features and functionality of Plurk. Consequently, we reserve the right to modify these terms of service at any time at our sole discretion.  You agree to be bound to such alterations when you use Plurk and the alterations shall be effective at the time of posting on Plurk.com.  It is important that you review these Terms of Service regularly on Plurk.com to ensure you are updated on any modifications to these terms.  If you are dissatisfied with or do not agree to abide by any altered Terms of Service, your sole remedy should be to close your account on Plurk and discontinue use of our service.

© 2008-2021 Plurk