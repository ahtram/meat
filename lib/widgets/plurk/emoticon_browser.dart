import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/rest/emoticons_get_cubit.dart';
import 'package:meat/bloc/ui/emoticon_ask_delete_cubit.dart';
import 'package:meat/models/emoticon/emoticon_collection.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:plurdart/plurdart.dart' as Plurdart;

class EmoticonBrowser extends StatefulWidget {
  EmoticonBrowser(this.onEmoticonTap, this.onBackspaceTap, this.onSpaceTap, this.onReturnTap, {Key key}) : super(key: key);

  final Function onEmoticonTap;
  final Function onBackspaceTap;
  final Function onSpaceTap;
  final Function onReturnTap;

  @override
  _EmoticonBrowserState createState() => _EmoticonBrowserState();
}

class _EmoticonBrowserState extends State<EmoticonBrowser> with AfterLayoutMixin {

  //For emoticon page view.
  PageController _emoticonPageController = PageController(initialPage: Static.emoticonTabIndex);

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    if (context != null) {
      context.read<EmoticonsGetCubit>().request();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        return SafeArea(
          child: Column(
            children: [

              //Emoticon Page view.
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    color: Theme.of(context).canvasColor.withOpacity(0.5),
                  ),
                  margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                  child: PageView(
                      controller: _emoticonPageController,
                      children: [
                        _pageContent(EmoticonCat.OwnedBasic, widget.onEmoticonTap, false, false),
                        _pageContent(EmoticonCat.Dice, widget.onEmoticonTap, false, false),
                        _pageContent(EmoticonCat.Custom, widget.onEmoticonTap, true, true),
                      ],
                      onPageChanged: (index) {
                        setState(() => Static.saveEmoticonTabIndex(index));
                      }
                  ),
                ),
              ),

              //Tool bar 1: tab + backspace
              Container(
                padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: BottomNavyBar(
                        animationDuration: Duration(milliseconds: 200),
                        mainAxisAlignment: MainAxisAlignment.start,
                        itemCornerRadius: 5,
                        containerHeight: 40,
                        iconSize: 20,
                        backgroundColor: Colors.transparent,
                        selectedIndex: Static.emoticonTabIndex,
                        showElevation: false, // use this to remove appBar's elevation
                        onItemSelected: (index) => setState(() {
                          _emoticonPageController.animateToPage(index, duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
                          Static.saveEmoticonTabIndex(index);
                        }),
                        items: [
                          _tabItem(Icons.insert_emoticon, 'Basic'),
                          _tabItem(Icons.dialpad, 'Dice'),
                          _tabItem(Icons.favorite, 'Custom'),
                        ],
                      ),
                    ),

//              Flexible(
//                child: Container(),
//              ),

                    //Back space button.
                    SizedBox(
                      width: 56,
                      height: 28,
                      child: InkWell(
                        child: OutlinedButton(
                          child: Icon(
                            Icons.backspace,
                            size: 20,
                            color: Theme.of(context).textTheme.bodyText1.color,
                          ),
                          onPressed: () {
                            widget.onBackspaceTap?.call();
                          },
                        ),
                        onLongPress: () {
                          widget.onBackspaceTap?.call();
                        },
                      ),
                    ),
//                  BaseEditor.utilityButton(context, Icons.backspace, () {
//                    //Hide overlay.
//                    widget.onBackspaceTap?.call();
//                  },
//                  width: 40.0),
                  ],
                ),
              ),

              //Tool bar 2: space + enter
              Container(
                padding: EdgeInsets.fromLTRB(8, 0, 8, 4),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [

                    //Space button.
                    Expanded(
                      child: SizedBox(
                        height: 28,
                        child: OutlinedButton(
                          child: Text('Space',
                            style: TextStyle(
                              fontSize: 13,
                            ),
                          ),
                          onPressed: () {
                            widget.onSpaceTap?.call();
                          },
                        ),
                      ),
                    ),

                    SizedBox(
                      width: 8,
                    ),

                    //Enter button
                    SizedBox(
                      width: 56,
                      height: 28,
                      child: ElevatedButton(
                        child: Icon(
                          Icons.keyboard_return,
                          size: 20,
                        ),
                        onPressed: () {
                          widget.onReturnTap?.call();
                        },
                      ),
                    ),
                  ],
                ),
              ),

            ],
          ),
        );
      }
    );
  }

  BottomNavyBarItem _tabItem(IconData icon, String text) {
    return BottomNavyBarItem(
      icon: Icon(
        icon,
        color: Theme.of(context).textTheme.bodyText1.color,
      ),
      title: Text(text,
        style: TextStyle(
          color: Theme.of(context).textTheme.bodyText1.color,
        ),),
      textAlign: TextAlign.center,
      inactiveColor: Theme.of(context).textTheme.bodyText1.color,
      activeColor: Theme.of(context).primaryColor,
    );
  }

  Widget _pageContent(EmoticonCat cat, Function onEmoticonTap, bool warpTextCodeWithBrackets, bool longPressAskDelete) {
    return BlocBuilder<EmoticonsGetCubit, EmoticonsGetState>(
      builder: (context, state) {
        //Only when showing request indicator.
        if (state is EmoticonsGetInitial) {
          //Busy indicator.
          return Center(
            child: Container(
              child: FlareActor("assets/rive/MeatLoader.flr",
                animation: "Untitled",
              ),
            ),
          );
        } else {
          //Ignore success or failed. Just use anything we have.
          return GridView.count(
            padding: EdgeInsets.symmetric(horizontal: 8),
            crossAxisCount: 8,
            mainAxisSpacing: 8,
            crossAxisSpacing: 8,
            children: _pageChildren(Static.emoticonCollection.getEmoticons(cat), onEmoticonTap, warpTextCodeWithBrackets, longPressAskDelete),
          );
        }
      },
    );
  }

  List<Widget> _pageChildren(List<Plurdart.Emoticon> emoticons, Function onEmoticonTap, bool warpTextCodeWithBrackets, bool longPressAskDelete) {
    List<Widget> returnList = [];

    emoticons.forEach((emoticon) {
      // print('got url ' + emoticon.previewUrl);
      returnList.add(GestureDetector(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).hintColor),
            borderRadius: BorderRadius.circular(3),
            color: Colors.white.withOpacity(0.5),
          ),
          child: Image(
            fit: BoxFit.scaleDown,
            image: CachedNetworkImageProvider(emoticon.previewUrl),
          ),
        ),
        onTap: () {
          String callbackTextCode = emoticon.textCode;
          if (warpTextCodeWithBrackets) {
            callbackTextCode = '[' + callbackTextCode + ']';
          }
          onEmoticonTap?.call(callbackTextCode);
        },
        onLongPress: longPressAskDelete ? () {
          context.read<EmoticonAskDeleteCubit>().show(emoticon.previewUrl);
        } : null,
      ));
    });

    return returnList;
  }

}