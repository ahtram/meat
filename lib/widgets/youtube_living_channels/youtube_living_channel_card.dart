
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:intl/intl.dart';
import 'package:meat/models/chikkutteh/res/living_channel.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:meat/widgets/plurk/plurk_card_action_button.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class YoutubeLivingChannelCard extends StatelessWidget {
  YoutubeLivingChannelCard(this.livingChannel);

  final LivingChannel livingChannel;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.fromLTRB(Define.horizontalPadding,
                  Define.verticalPadding, Define.horizontalPadding, 0),
              child: Column(
                children: [
                  //Top: Channel thumbnail + owner
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        backgroundImage: NetworkImage(livingChannel.channelThumbnailUrl),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Expanded(
                          child: Text(
                        livingChannel.owner,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontWeight: FontWeight.bold),
                      )),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //Video thumbnail
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 0.5,
                        color: Theme.of(context).hintColor,
                      ),
                      color: Theme.of(context).canvasColor.withOpacity(0.33),
                    ),
                    child: Column(
                      children: [
                        //Image
                        AspectRatio(
                          aspectRatio: 16 / 9,
                          child: Image.network(
                            livingChannel.videoThumbnailUrl,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: Define.verticalPadding,
                              horizontal: Define.horizontalPadding),
                          child: Text(
                            livingChannel.title,
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),

          //App bar
          Container(
            padding: EdgeInsets.fromLTRB(Define.horizontalPadding + 4, 8,
                Define.horizontalPadding + 4, 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      MdiIcons.circleSlice8,
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Transform.translate(
                      child: Text(
                          'Watching: ' +
                              NumberFormat.compact()
                                  .format(livingChannel.viewingCount),
                          style: TextStyle(
                            fontSize: 13,
                            color: Theme.of(context).hintColor,
                          )),
                      offset: Offset(0, -1.5),
                    ),
                  ],
                ),

                Expanded(child: SizedBox()),
                //Share button.
                PlurkCardActionButton(Icons.share, true, Colors.blue,
                    text: 'Share', onTap: () {
                  HapticFeedback.lightImpact();
                  Share.share(_videoLinkUrl());
                }),
                //[Dep]: I guess not
                //Open external button.
                // PlurkCardActionButton(Icons.open_in_new, true, Colors.amber,
                //     onTap: () async {
                //   HapticFeedback.lightImpact();
                //   if (await canLaunch(_videoLinkUrl())) {
                //     launch(_videoLinkUrl());
                //   }
                //   // widget.onShareTap?.call();
                // }),
              ],
            ),
          ),

          Divider(height: 0.5, thickness: 0.5),
        ],
      ),
      onTap: () async {
        //Ok, the built-in video player quality sucks.
        //We'll open the link directly on Youtube app instead...
        // context
        //     .read<YoutubeFullscreenCubit>()
        //     .enter(YoutubeFullscreenViewerArgs(
        //       livingChannel.videoID,
        //       0,
        //       true,
        //       null,
        //     )
        // );
        HapticFeedback.lightImpact();
        if (await canLaunch(_videoLinkUrl())) {
          launch(_videoLinkUrl());
        }
      },
    );
  }

  String _videoLinkUrl() {
    return 'https://www.youtube.com/watch?v=${livingChannel.videoID}';
  }
}
