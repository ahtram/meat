class SheetValueRangeMetadata {
  String range;
  String majorDimension;
  List<List<String>> values;

  SheetValueRangeMetadata({this.range, this.majorDimension, this.values});

  SheetValueRangeMetadata.fromJson(Map<String, dynamic> json) {
    range = json['range'];
    majorDimension = json['majorDimension'];
    if (json['values'] != null) {
      values = [];
      json['values'].forEach((v1) {
        List<String> subValues = [];
        v1.forEach((v2) {
          subValues.add(v2);
        });
        values.add(subValues);
      });
    }
  }

}