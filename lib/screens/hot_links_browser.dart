
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/widgets/hot_links/hot_links_viewer.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HotLinksBrowser extends StatefulWidget {
  HotLinksBrowser({Key key}) : super(key: key);

  @override
  _HotLinksBrowserState createState() => _HotLinksBrowserState();
}

class _HotLinksBrowserState extends State<HotLinksBrowser> {

  BuildContext scaffoldBodyContext;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<RequestIndicatorCubit>(
          create: (BuildContext context) => RequestIndicatorCubit(),
        ),
      ],
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            brightness: Theme.of(context).brightness,
            backgroundColor: Colors.transparent,
            flexibleSpace: AcrylicContainer(
              Container(),
              backgroundColor: Theme.of(context).primaryColor,
              transparentBackLayer: false,
              mode: AcrylicContainerMode.BackdropFilter,
            ),
            elevation: 0,
            leading: AppBarActionButton(Icons.arrow_back, onTap: () {
              // context.read<HomeRefreshCubit>().request();
              Navigator.pop(context);
            }),
            title: Text(
              FlutterI18n.translate(
                  context, 'menuHotLinks'),
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
            ),
          ),
          floatingActionButton: BlocBuilder<RequestIndicatorCubit,
              RequestIndicatorState>(
            builder: (context, state) {
              //Check state.
              List<Widget> columnChildren = [];

              //Only when showing request indicator.
              if (state is RequestIndicatorShow) {
                //Busy indicator.
                columnChildren.add(Container(
                  width: 56,
                  height: 56,
                  child: FlareActor(
                    "assets/rive/MeatLoader.flr",
                    animation: "Untitled",
                  ),
                ));
              }

              return Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: columnChildren,
              );
            },
          ),
          body: Builder(
            builder: (context) {
              scaffoldBodyContext = context;
              return Container(
                // color: Theme.of(context).canvasColor.withOpacity(0.4),
                child: Column(
                  children: [
                    Expanded(
                      child: //Anonymous.
                      HotLinksViewer(),
                    )
                  ],
                ),
              );
            },
          )
      ),
    );
  }
}