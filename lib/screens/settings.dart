import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/theme/app_themes.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/bloc/ui/theme_bloc.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/bloc/ui/settings_acrylic_changed_cubit.dart';
import 'package:meat/bloc/ui/settings_download_full_image_changed_cubit.dart';

class Settings extends StatefulWidget {
  Settings();

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  BuildContext scaffoldBodyContext;

  //For settings option display.
  static const Map<bool, String> settingsStrOnOff = {
    true: 'On',
    false: 'Off',
  };

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AcrylicScaffold(
        transparentColor: Theme.of(context).canvasColor.withOpacity(0.4),
        appBar: AppBar(
          centerTitle: true,
          brightness: Theme.of(context).brightness,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: Container(),
          title: Text(
            FlutterI18n.translate(context, 'menuSettings'),
            style: TextStyle(
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
          actions: [
            AppBarActionButton(Icons.close, onTap: () {
              Navigator.pop(context);
            }),
            SizedBox(
              width: 4,
            )
          ],
        ),
        child: SafeArea(
          child: ListView(
            controller: PrimaryScrollController.of(context),
            padding: EdgeInsets.all(16),
            children: (<Widget>[
              SettingsSubTitle(
                  FlutterI18n.translate(context, 'settingsTitleStyle')),
              SettingsItem(
                  FlutterI18n.translate(context, 'settingsLabelColor'),
                  EnumToString.convertToString(Static.settingsAppTheme),
                  _showStyleColorDialog),
              SettingsItem(
                  FlutterI18n.translate(context, 'settingsBackground'),
                  EnumToString.convertToString(Static.settingsCanvasColor),
                  _showCanvasColorDialog),
              SettingsItem(
                  FlutterI18n.translate(context, 'settingsLabelBrightness'),
                  EnumToString.convertToString(Static.settingsThemeMode),
                  _showStyleBrightnessDialog),
              SettingsItem(
                  FlutterI18n.translate(context, 'settingsLabelTextScale'),
                  Static.settingsTextScale.toString(),
                  _showStyleTextScaleDialog),
              Divider(
                thickness: 1,
              ),
              SettingsSubTitle(
                  FlutterI18n.translate(context, 'settingsTitleEffects')),
              SettingsItem(
                  FlutterI18n.translate(context, 'settingsLabelAcrylicEffect'),
                  settingsStrOnOff[Static.settingsAcrylicEffect],
                  _showAcrylicEffectDialog),
              SettingsItem(
                  FlutterI18n.translate(
                      context, 'settingsLabelHighQualityImageGrid'),
                  settingsStrOnOff[Static.settingsDownloadFullImageInPlurkCard],
                  _showDownloadFullImageInPlurkCardDialog),
              Divider(
                thickness: 1,
              ),
              SettingsSubTitle(
                  FlutterI18n.translate(context, 'settingsTitleMisc')),
              SettingsItem(
                  FlutterI18n.translate(
                      context, 'settingsLabelTapToRevealAlultsContent'),
                  settingsStrOnOff[Static.settingsTapToRevealAdultsOnlyContent],
                  _showTapToRevealAdultsOnlyContentDialog),
              SettingsItem(
                  FlutterI18n.translate(
                      context, 'settingsLabelRemoveLinksInPlurkContent'),
                  settingsStrOnOff[Static.settingsRemoveLinksInPlurkContent],
                  _showRemoveLinksInPlurkContentDialog),
              //[Dep]
              // SettingsItem(
              //     FlutterI18n.translate(
              //         context, 'settingsLabelDisplayMuteButtonAtBottomRight'),
              //     settingsStrOnOff[Static.settingsDisplayMuteButtonAtBottomRight],
              //     _showDisplayMuteButtonAtBottomRightDialog),
              SettingsItem(
                  FlutterI18n.translate(context, 'settingsLabelAPILanguage'),
                  Static.apiLanguage,
                  _showAPILanguageDialog),
              SettingsItem(
                  FlutterI18n.translate(
                      context, 'settingsDisplayVTWatchNotifyText'),
                  settingsStrOnOff[Static.settingsDisplayVTWatchNotifyText],
                  _showDisplayVTWatchNotifyTextDialog),
            ]),
          ),
        ));
  }

  _showStyleColorDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
            children: _styleColorDialogChildren(),
          );
        });
  }

  List<Widget> _styleColorDialogChildren() {
    List<Widget> widgets = [];

    Brightness currentBrightness = Theme.of(context).brightness;

    AppTheme.values.forEach((appTheme) {
      widgets.add(ListTile(
        leading: Icon(
          (appTheme == Static.settingsAppTheme)
              ? Icons.radio_button_checked
              : Icons.radio_button_unchecked,
          color: Colors.grey,
        ),
        title: Text(EnumToString.convertToString(appTheme)),
        trailing: Container(
          width: 32,
          height: 32,
          decoration: BoxDecoration(
            color: getThemeData(currentBrightness, appTheme, Static.settingsCanvasColor, Static.settingsTextScale).primaryColor,
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
          if (Static.settingsAppTheme != appTheme) {
            setState(() {
              Static.saveAppTheme(appTheme);
            });
            BlocProvider.of<ThemeBloc>(context).add(ThemeChanged(
                appTheme: Static.settingsAppTheme,
                themeMode: Static.settingsThemeMode,
                textScale: Static.settingsTextScale,
                canvasColor: Static.settingsCanvasColor));
          }
        },
      ));
    });
    return widgets;
  }

  _showCanvasColorDialog() {
    List<Widget> canvasColorListTiles = [];

    CanvasColor.values.forEach((element) {
      canvasColorListTiles.add(ListTile(
        leading: Icon(
          (element == Static.settingsCanvasColor)
              ? Icons.radio_button_checked
              : Icons.radio_button_unchecked,
          color: Colors.grey,
        ),
        title: Text(EnumToString.convertToString(element)),
        onTap: () {
          Navigator.pop(context);
          if (Static.settingsCanvasColor != element) {
            setState(() {
              Static.saveCanvasColor(element);
            });
            BlocProvider.of<ThemeBloc>(context).add(ThemeChanged(
                appTheme: Static.settingsAppTheme,
                themeMode: Static.settingsThemeMode,
                textScale: Static.settingsTextScale,
                canvasColor: Static.settingsCanvasColor));
          }
        },
      ));
    });

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
            children: canvasColorListTiles,
          );
        });
  }

  _showStyleBrightnessDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: _styleBrightnessDialogChildren(),
          );
        });
  }

  List<Widget> _styleBrightnessDialogChildren() {
    List<Widget> widgets = [];

    ThemeMode.values.forEach((themeMode) {
      widgets.add(ListTile(
        leading: Icon(
          (themeMode == Static.settingsThemeMode)
              ? Icons.radio_button_checked
              : Icons.radio_button_unchecked,
          color: Colors.grey,
        ),
        title: Text(EnumToString.convertToString(themeMode)),
        onTap: () {
          Navigator.pop(context);
          if (Static.settingsThemeMode != themeMode) {
            setState(() {
              Static.saveThemeMode(themeMode);
            });
            BlocProvider.of<ThemeBloc>(context).add(ThemeChanged(
                appTheme: Static.settingsAppTheme,
                themeMode: Static.settingsThemeMode,
                textScale: Static.settingsTextScale,
                canvasColor: Static.settingsCanvasColor));
          }

          // We know how to fix this problem!
//          _showSnackBarMessage('Restart the app for the change to take effect to the NavigationBar!');
        },
      ));
    });

    return widgets;
  }

  _showStyleTextScaleDialog() {
    List<double> scales = [0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5];
    List<Widget> textScaleListTiles = [];

    for (int i = 0; i < scales.length; ++i) {
      textScaleListTiles.add(ListTile(
        leading: Icon(
          (scales[i] == Static.settingsTextScale)
              ? Icons.radio_button_checked
              : Icons.radio_button_unchecked,
          color: Colors.grey,
        ),
        title: Text(
            scales[i].toString() + (scales[i] == 1.1 ? '  (Default)' : '')),
        onTap: () {
          Navigator.pop(context);
          if (Static.settingsTextScale != scales[i]) {
            setState(() {
              Static.saveTextScale(scales[i]);
            });
            BlocProvider.of<ThemeBloc>(context).add(ThemeChanged(
                appTheme: Static.settingsAppTheme,
                themeMode: Static.settingsThemeMode,
                textScale: Static.settingsTextScale,
                canvasColor: Static.settingsCanvasColor));
          }
        },
      ));
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: textScaleListTiles,
          );
        });
  }

  _showAcrylicEffectDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: [
              //On
              ListTile(
                leading: Icon(
                  (true == Static.settingsAcrylicEffect)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[true]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsAcrylicEffect != true) {
                    setState(() {
                      Static.saveAcrylicEffect(true);
                    });
                    BlocProvider.of<SettingsAcrylicChangedCubit>(context)
                        .success();
                  }
                },
              ),
              //Off
              ListTile(
                leading: Icon(
                  (false == Static.settingsAcrylicEffect)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[false]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsAcrylicEffect != false) {
                    setState(() {
                      Static.saveAcrylicEffect(false);
                    });
                    BlocProvider.of<SettingsAcrylicChangedCubit>(context)
                        .success();
                  }
                },
              ),
            ],
          );
        });
  }

  _showDownloadFullImageInPlurkCardDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: [
              //On
              ListTile(
                leading: Icon(
                  (true == Static.settingsDownloadFullImageInPlurkCard)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[true]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsDownloadFullImageInPlurkCard != true) {
                    setState(() {
                      Static.saveDownloadFullImageInPlurkCard(true);
                    });
                    BlocProvider.of<SettingsDownloadFullImageChangedCubit>(
                            context)
                        .success();
                  }
                },
              ),

              //Off
              ListTile(
                leading: Icon(
                  (false == Static.settingsDownloadFullImageInPlurkCard)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[false]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsDownloadFullImageInPlurkCard != false) {
                    setState(() {
                      Static.saveDownloadFullImageInPlurkCard(false);
                    });
                    BlocProvider.of<SettingsDownloadFullImageChangedCubit>(
                            context)
                        .success();
                  }
                },
              ),
            ],
          );
        });
  }

  _showTapToRevealAdultsOnlyContentDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: [
              //On
              ListTile(
                leading: Icon(
                  (true == Static.settingsTapToRevealAdultsOnlyContent)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[true]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsTapToRevealAdultsOnlyContent != true) {
                    setState(() {
                      Static.saveTapToRevealAdultsOnlyContent(true);
                    });
                  }
                },
              ),

              //Off
              ListTile(
                leading: Icon(
                  (false == Static.settingsTapToRevealAdultsOnlyContent)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[false]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsTapToRevealAdultsOnlyContent != false) {
                    setState(() {
                      Static.saveTapToRevealAdultsOnlyContent(false);
                    });
                  }
                },
              ),
            ],
          );
        });
  }

  _showRemoveLinksInPlurkContentDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: [
              //On
              ListTile(
                leading: Icon(
                  (true == Static.settingsRemoveLinksInPlurkContent)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[true]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsRemoveLinksInPlurkContent != true) {
                    setState(() {
                      Static.saveRemoveLinksInPlurkContent(true);
                    });
                  }
                },
              ),

              //Off
              ListTile(
                leading: Icon(
                  (false == Static.settingsRemoveLinksInPlurkContent)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[false]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsRemoveLinksInPlurkContent != false) {
                    setState(() {
                      Static.saveRemoveLinksInPlurkContent(false);
                    });
                  }
                },
              ),
            ],
          );
        });
  }

  // _showDisplayMuteButtonAtBottomRightDialog() {
  //   showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return SimpleDialog(
  //           contentPadding: EdgeInsets.all(16),
  //           children: [
  //             //On
  //             ListTile(
  //               leading: Icon(
  //                 (true == Static.settingsDisplayMuteButtonAtBottomRight)
  //                     ? Icons.radio_button_checked
  //                     : Icons.radio_button_unchecked,
  //                 color: Colors.grey,
  //               ),
  //               title: Text(settingsStrOnOff[true]),
  //               onTap: () {
  //                 Navigator.pop(context);
  //                 if (Static.settingsDisplayMuteButtonAtBottomRight != true) {
  //                   setState(() {
  //                     Static.saveDisplayMuteButtonAtBottomRight(true);
  //                   });
  //                 }
  //               },
  //             ),
  //
  //             //Off
  //             ListTile(
  //               leading: Icon(
  //                 (false == Static.settingsDisplayMuteButtonAtBottomRight)
  //                     ? Icons.radio_button_checked
  //                     : Icons.radio_button_unchecked,
  //                 color: Colors.grey,
  //               ),
  //               title: Text(settingsStrOnOff[false]),
  //               onTap: () {
  //                 Navigator.pop(context);
  //                 if (Static.settingsDisplayMuteButtonAtBottomRight != false) {
  //                   setState(() {
  //                     Static.saveDisplayMuteButtonAtBottomRight(false);
  //                   });
  //                 }
  //               },
  //             ),
  //           ],
  //         );
  //       });
  // }

  _showAPILanguageDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: _apiLanguageDialogChildren(),
          );
        });
  }

  static const List<String> API_LANGUAGE_DISPLAY = [
    '中文',
    'English'
  ];

  List<Widget> _apiLanguageDialogChildren() {
    List<Widget> widgets = [];
    for (int i = 0 ; i < Define.API_LANGUAGES.length ; ++i) {
      widgets.add(ListTile(
        leading: Icon(
          (Define.API_LANGUAGES[i] == Static.apiLanguage)
              ? Icons.radio_button_checked
              : Icons.radio_button_unchecked,
          color: Colors.grey,
        ),
        title: Text(Define.API_LANGUAGES[i] + ' (' + API_LANGUAGE_DISPLAY[i] + ')'),
        onTap: () {
          Navigator.pop(context);
          if (Static.apiLanguage != Define.API_LANGUAGES[i]) {
            setState(() {
              Static.saveAPILanguage(Define.API_LANGUAGES[i]);
            });
          }
        },
      ));
    }

    return widgets;
  }

  _showDisplayVTWatchNotifyTextDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(16),
            children: [
              //On
              ListTile(
                leading: Icon(
                  (true == Static.settingsDisplayVTWatchNotifyText)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[true]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsDisplayVTWatchNotifyText != true) {
                    setState(() {
                      Static.saveDisplayVTWatchNotifyText(true);
                    });
                  }
                },
              ),

              //Off
              ListTile(
                leading: Icon(
                  (false == Static.settingsDisplayVTWatchNotifyText)
                      ? Icons.radio_button_checked
                      : Icons.radio_button_unchecked,
                  color: Colors.grey,
                ),
                title: Text(settingsStrOnOff[false]),
                onTap: () {
                  Navigator.pop(context);
                  if (Static.settingsDisplayVTWatchNotifyText != false) {
                    setState(() {
                      Static.saveDisplayVTWatchNotifyText(false);
                    });
                  }
                },
              ),
            ],
          );
        });
  }

}

class SettingsSubTitle extends StatelessWidget {
  SettingsSubTitle(this.title);

  final String title;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold),
      ),
      dense: true,
    );
  }
}

class SettingsItem extends StatelessWidget {
  SettingsItem(this.title, this.subTitle, this.onTap);

  final String title;
  final String subTitle;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: Theme.of(context).textTheme.subtitle2,
      ),
      subtitle: Text(
        subTitle,
        style: Theme.of(context).textTheme.caption,
      ),
      dense: true,
      onTap: onTap,
    );
  }
}
