part of 'blocks_block_cubit.dart';

@immutable
abstract class BlocksBlockState {}

class BlocksBlockInitial extends BlocksBlockState {}

class BlocksBlockSuccess extends BlocksBlockState {
  BlocksBlockSuccess(this.userId);
  final int userId;
}

class BlocksBlockFailed extends BlocksBlockState {
  BlocksBlockFailed(this.errorText);
  final String errorText;
}