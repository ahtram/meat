import 'package:after_layout/after_layout.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:meat/bloc/rest/timeline_plurk_add_cubit.dart';
import 'package:meat/bloc/rest/timeline_plurk_edit_cubit.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/bloc/ui/plurk_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/plurk_post_complete_cubit.dart';
import 'package:meat/bloc/ui/users_selected_cubit.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/plurk/base_editor.dart';
import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/screens/users_browser.dart' as UsersBrowser;
import 'package:meat/widgets/user/users_viewer.dart' as UsersViewer;
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';

void show(BuildContext context, PlurkPosterArgs args) {
  Navigator.push(context,
      new MaterialPageRoute(builder: (BuildContext context) {
    return _PlurkPoster(args);
  }));
}

//New Plurk or edit Plurk?
enum PlurkPosterMode {
  New, //Compose a new Plurk
  Edit, //Edit an existing Plurk
  Private //Sending a private Plurk
}

class PlurkPosterArgs {
  PlurkPosterArgs(this.mode,
      {this.editingPlurk, this.privatePlurkUser, this.initialContent});
  final PlurkPosterMode mode;
  final Plurdart.Plurk editingPlurk;
  final Plurdart.User privatePlurkUser;
  //Set the initial content for your share intent content.
  final String initialContent;
}

class _PlurkPoster extends StatefulWidget {
  _PlurkPoster(this.args, {Key key}) : super(key: key);

  final PlurkPosterArgs args;

  @override
  _PlurkPosterState createState() => _PlurkPosterState();
}

enum WhoCanSeeThis {
  TheWholeWorld,
  Friends,
  LimitTo,
  // Except,
}

class _PlurkPosterState extends State<_PlurkPoster>
    with AfterLayoutMixin, WidgetsBindingObserver, BaseEditor {
  bool _isSendingPlurk = false;

  //Is seeing settings view?
  bool _settingsViewToggled = false;
  bool _isShowingSettingsView = false;

  BuildContext _blocProviderContext;

  //Pre declare the modal we gonna edit / send.
  Plurdart.TimelinePlurkAdd _modalTimelinePlurkAdd = Plurdart.TimelinePlurkAdd(
    lang: Static.apiLanguage,
    content: '',
    qualifier: Plurdart.PlurkQualifier.None,
  );

  Plurdart.TimelinePlurkEdit _modalTimelinePlurkEdit =
      Plurdart.TimelinePlurkEdit(
    plurkId: 0,
    content: '',
  );

  WhoCanSeeThis whoCanSeeValue = WhoCanSeeThis.TheWholeWorld;

  List<int> selectedLimitTo = [];
  // List<Plurdart.User> selectedExcept = List<Plurdart.User>();

  bool isAnonymous = false;
  bool anonymousToFollowers = false;
  bool publishToAnonymous = false;
  //Open/Close/Friends
  List<bool> commentableTabIsSelected = [true, false, false];
  bool replurkable = true;
  bool adultOnly = false;

  @override
  void initState() {
    super.initState();

    qualifierMode = QualifierMode.Plurk;

    WidgetsBinding.instance.addObserver(this);

    // Query
//    print('Keyboard visibility direct query: ${KeyboardVisibility.isVisible}');

    KeyboardVisibilityController().onChange.listen(onKeyboardVisibilityChange);

    //Try focus on comment.
    commentFocus.requestFocus();

    //This is just for display the replurk icon correctly.
    if (widget.args.mode == PlurkPosterMode.Private) {
      replurkable = false;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    commentController.dispose();
    hideMentionSuggestionOverlay();
    hideQualifierSelectorOverlayEntry();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    //Fucking crazy hack just for match the keyboard show/hide animation end.
    if (isInEmoticonsMode) {
      if (!isShowingKeyboard()) {
//        print('try show EmoticonView');
        isShowingEmoticonView = true;
      }
    } else {
      if (isShowingKeyboard()) {
//        print('try hide EmoticonView');
        isShowingEmoticonView = false;
      }
    }

    //Fucking crazy hack just for match the keyboard show/hide animation end.
    if (_settingsViewToggled) {
      if (!isShowingKeyboard()) {
        _isShowingSettingsView = true;
      }
    } else {
      if (isShowingKeyboard()) {
        _isShowingSettingsView = false;
      }
    }

    //SO SO stupid we have to delay the measure timing.
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (MediaQuery.of(context).viewInsets.bottom >
          measuredSoftKeyboardHeight) {
        //Just remember the highest viewInsets.bottom.
        measuredSoftKeyboardHeight = MediaQuery.of(context).viewInsets.bottom;
//        print('MediaQuery.of(context).viewInsets.bottom: [' + MediaQuery.of(context).viewInsets.bottom.toString() + ']');
      }
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (!mounted) return;

    //Edit mode will need to initialize some params.
    if (widget.args.mode == PlurkPosterMode.Edit &&
        widget.args.editingPlurk != null) {
      setState(() {
        //Copy the values from editing modal.
        _modalTimelinePlurkEdit.plurkId = widget.args.editingPlurk.plurkId;

        switch (widget.args.editingPlurk.noComments) {
          case Plurdart.Comment.Allow:
            commentableTabIsSelected = [true, false, false];
            break;
          case Plurdart.Comment.DisableComment:
            commentableTabIsSelected = [false, true, false];
            break;
          case Plurdart.Comment.FriendsOnly:
            commentableTabIsSelected = [false, false, true];
            break;
        }

        _modalTimelinePlurkEdit.limitedTo =
            List<int>.from(widget.args.editingPlurk.getLimitedTo());
        _modalTimelinePlurkEdit.excluded =
            List<int>.from(widget.args.editingPlurk.getExcluded());
        _modalTimelinePlurkEdit.replurkable =
            widget.args.editingPlurk.replurkable ? 1 : 0;
        _modalTimelinePlurkEdit.porn = widget.args.editingPlurk.porn ? 1 : 0;

        // print('editing limitedTo: [' + _modalTimelinePlurkEdit.limitedTo.join(',') + ']');

        if (_modalTimelinePlurkEdit.limitedTo == null ||
            _modalTimelinePlurkEdit.limitedTo.length == 0) {
          whoCanSeeValue = WhoCanSeeThis.TheWholeWorld;
        } else if (_modalTimelinePlurkEdit.limitedTo.length == 1 &&
            _modalTimelinePlurkEdit.limitedTo[0] == 0) {
          whoCanSeeValue = WhoCanSeeThis.Friends;
        } else {
          whoCanSeeValue = WhoCanSeeThis.LimitTo;
        }

        selectedLimitTo = widget.args.editingPlurk.getLimitedTo();

        commentController.text = widget.args.editingPlurk.contentRaw;

        if (widget.args.editingPlurk.anonymous != null) {
          isAnonymous = widget.args.editingPlurk.anonymous;
        }

        if (widget.args.editingPlurk.publishToFollowers != null) {
          anonymousToFollowers = widget.args.editingPlurk.publishToFollowers;
        }
      });
    } else if (widget.args.mode == PlurkPosterMode.New) {
      //New Plurk mode
      if (widget.args.initialContent != null &&
          widget.args.initialContent.isNotEmpty) {
        //Set the initial content.
        setState(() {
          commentController.text = widget.args.initialContent;
        });
      } else {
        //Try to read existing draft.
        setState(() {
          commentController.text = Static.newPlurkDraft;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnChildren = [];

    //Comment TextField.
    columnChildren.add(Flexible(
      flex: 2,
      child: buildCommentTextField(
          context,
          !_isSendingPlurk,
          true,
          (isInEmoticonsMode || _settingsViewToggled),
          !_isSendingPlurk && !isUploadingPictures,
          _isSendingPlurk,
          minLines: null,
          maxLines: null, onTap: () {
        //Leave settingsView. (Better UX)
        _toggleSettingsView(false);
      }),
    ));

    //Summary toggle button: can toggle PlurkSetting view.
    columnChildren.add(_buildSummaryToggleButton());

    //Toolbar.
    if (!_isSendingPlurk && !isUploadingPictures) {
      columnChildren.add(buildUtilityBar(context,
          isInEmoticonsMode ? (Icons.keyboard) : (Icons.insert_emoticon), () {
        //Hide overlay.
        hideQualifierSelectorOverlayEntry();
        hideMentionSuggestionOverlay();

        Vibrator.vibrateShortNote();

        //Toggle isInEmoticonsMode.
        if (!isInEmoticonsMode) {
          toggleEmoticonMode(true);

          //When open emoticon mode also close Settings view instantly.
          if (_settingsViewToggled) {
            _toggleSettingsView(false);
            _isShowingSettingsView = false;

            isShowingEmoticonView = true;
          }
        } else {
          toggleEmoticonMode(false);
        }
      }, () {
        if (!_isSendingPlurk && !isUploadingPictures) {
          //Toggle qualifier overlay.
          if (qualifierSelectorOverlayEntry == null) {
            showQualifierSelectorOverlayEntry(context);
          } else {
            hideQualifierSelectorOverlayEntry();
          }
          Vibrator.vibrateShortNote();
        }
      }, () {
        if (!_isSendingPlurk && !isUploadingPictures) {
          //Hide overlay.
          hideQualifierSelectorOverlayEntry();
          hideMentionSuggestionOverlay();

          //[Special case] We should force immediate leave emotion mode
          isInEmoticonsMode = false;
          isShowingEmoticonView = false;
          _settingsViewToggled = false;
          _isShowingSettingsView = false;

          //Check and send the content.
          if (widget.args.mode == PlurkPosterMode.New ||
              widget.args.mode == PlurkPosterMode.Private) {
            checkAndSendPlurkAdd();
          } else {
            checkAndSendPlurkEdit();
          }
        }
      },
          forcePlurkQualifier:
              isAnonymous ? Plurdart.PlurkQualifier.Whispers : null));
    }

    //Emoticon view.
    if (isShowingEmoticonView && !_isSendingPlurk) {
      columnChildren.add(buildEmoticonView());
    }

    //Settings View.
    if (_isShowingSettingsView && !_isSendingPlurk) {
      columnChildren.add(Expanded(flex: 3, child: _buildSettingsView()));
    }

    //When uploadingPicture, display the LinearProgressIndicator.
    if (isUploadingPictures) {
      columnChildren.add(LinearProgressIndicator(
        backgroundColor: Colors.transparent,
        color: Theme.of(context).colorScheme.secondary,
        minHeight: 8,
      ));
    }

    return WillPopScope(
      onWillPop: () async {
        //Check if we need to exit emoticon insert mode.
        if (isInEmoticonsMode) {
          //Exit emoticon mode.
          toggleEmoticonMode(false);
          return false;
        } else if (_settingsViewToggled) {
          _toggleSettingsView(false);
          return false;
        } else {
//          urlTapSubscription?.cancel();

          //For new Plurk mode we'll want to save draft on exit.
          if (widget.args.mode == PlurkPosterMode.New &&
              widget.args.initialContent == null) {
            Static.saveNewPlurkDraft(commentController.text);
          }

          return true;
        }
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider<TimelinePlurkAddCubit>(
            create: (BuildContext context) => TimelinePlurkAddCubit(),
          ),
          BlocProvider<TimelinePlurkEditCubit>(
            create: (BuildContext context) => TimelinePlurkEditCubit(),
          ),
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<TimelinePlurkAddCubit, TimelinePlurkAddState>(
                listener: (context, state) async {
              if (state is TimelinePlurkAddSuccess) {
                //Success

                //Maybe not?
                setState(() {
                  _isSendingPlurk = false;
                });

                //Pop to return to home.
                Navigator.pop(context);

                //Try send plurk complete to Home.
                _blocProviderContext
                    .read<PlurkPostCompleteCubit>()
                    .success(state.resultPlurk);

                //Clear the newPlurkDraft.
                Static.clearNewPlurkDraft();
              } else if (state is TimelinePlurkAddFailed) {
                //Failed

                setState(() {
                  _isSendingPlurk = false;
                });

                //Show error on the flush bar.
                _showFlushBar(
                    context,
                    Icons.warning,
                    FlutterI18n.translate(context, 'dialogSendPlurkFailed'),
                    state.errorText);
              }
            }),
            BlocListener<TimelinePlurkEditCubit, TimelinePlurkEditState>(
                listener: (context, state) async {
              if (state is TimelinePlurkEditSuccess) {
                //Success

                //Maybe not?
                setState(() {
                  _isSendingPlurk = false;
                });

                //Pop to return to home.
                Navigator.pop(context);

                //Try send plurk complete to Home.
                _blocProviderContext
                    .read<PlurkEditCompleteCubit>()
                    .success(state.resultPlurk);
              } else if (state is TimelinePlurkEditFailed) {
                //Failed

                setState(() {
                  _isSendingPlurk = false;
                });

                //Show error on the flush bar.
                _showFlushBar(
                    context,
                    Icons.warning,
                    FlutterI18n.translate(context, 'dialogSendPlurkFailed'),
                    state.errorText);
              }
            }),
            BlocListener<UsersSelectedCubit, UsersSelectedState>(
              listener: (context, state) {
                if (state is UsersSelectedComplete) {
                  //Should fill the limitTo list or except list.
                  setState(() {
                    selectedLimitTo = state.users.map((e) => e.id).toList();
                  });
                }
              },
            ),
            BlocListener<FlushBarCubit, FlushBarState>(
              listener: (context, state) {
                if (state is FlushBarRequest) {
                  //Only display FlushBar when I am the current route.
                  if (ModalRoute.of(context).isCurrent) {
                    _showFlushBar(_blocProviderContext, state.icon, state.title,
                        state.message);
                  }
                }
              },
            ),
          ],
          child: Builder(
            builder: (context) {
              _blocProviderContext = context;
              return Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  brightness: Theme.of(context).brightness,
                  backgroundColor:
                      Theme.of(context).canvasColor.withOpacity(0.4),
                  elevation: 0,
                  leading: AppBarActionButton(Icons.arrow_back, onTap: () {
                    //Check if we need to hide keyboard or leave emoticon view first.
                    //Check if we need to exit emoticon insert mode.
                    if (isInEmoticonsMode) {
                      //Exit emoticon mode.
                      toggleEmoticonMode(false);
                      Vibrator.vibrateShortNote();
                    } else if (_settingsViewToggled) {
                      _toggleSettingsView(false);
                      Vibrator.vibrateShortNote();
                    } else if (isShowingKeyboard()) {
                      tryUnfocusCommentTextField();
                      Vibrator.vibrateShortNote();
                    } else {
                      //For new Plurk mode we'll want to save draft on exit.
                      if (widget.args.mode == PlurkPosterMode.New &&
                          widget.args.initialContent == null) {
                        Static.saveNewPlurkDraft(commentController.text);
                      }

                      Navigator.pop(context);
                    }
                  }),
                  title: Text(
                    widget.args.mode == PlurkPosterMode.New
                        ? FlutterI18n.translate(context, 'titleNewPlurk')
                        : FlutterI18n.translate(context, 'titleEditPlurk'),
                    style: TextStyle(
                      color: Theme.of(context).textTheme.bodyText1.color,
                    ),
                  ),
                ),
                body: SafeArea(
                  child: Container(
                    color: Theme.of(context).canvasColor.withOpacity(0.4),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: columnChildren,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  //Keyboard vis can be manually disable by user
  //and can be auto enable by TextField.
  //so we have to let keyboard vis decide if exit editing mode.
  void onKeyboardVisibilityChange(bool visible) {
//    print('_onKeyboardVisibilityChange: ' + visible.toString());
    if (visible) {
//      isInEditingMode = true;
    } else {
      //This means we are leaving editing mode
      if (!isInEmoticonsMode && !_settingsViewToggled) {
//        isInEditingMode = false;
        commentFocus.unfocus(disposition: UnfocusDisposition.scope);
      }

      //Hide overlays
      hideQualifierSelectorOverlayEntry();
      hideMentionSuggestionOverlay();
    }
  }

  Widget _buildSummaryToggleButton() {
    //Decide icons, colors and display Strings by settings.
    List<Widget> rowChildren = [];

    //Commentable icon.
    Color commentIconColor = Colors.grey.withOpacity(0.4);
    if (commentableTabIsSelected[0] == true) {
      //Open
      commentIconColor = Colors.amber;
    } else if (commentableTabIsSelected[1] == true) {
      //Close
      commentIconColor = Colors.grey.withOpacity(0.4);
    } else if (commentableTabIsSelected[2] == true) {
      //Friends
      commentIconColor = Colors.blue;
    }
    rowChildren.add(_summaryToggleIcon(Icons.message, commentIconColor));

    //Replurkable icon.
    Color rebplurkableIconColor =
        replurkable ? Colors.green : Colors.grey.withOpacity(0.4);
    rowChildren.add(_summaryToggleIcon(Icons.cached, rebplurkableIconColor));

    //Who can see?
    String whoCanSeeText = '';
    switch (whoCanSeeValue) {
      case WhoCanSeeThis.TheWholeWorld:
        whoCanSeeText = FlutterI18n.translate(context, 'labelTheWholeWorld');
        break;
      case WhoCanSeeThis.Friends:
        whoCanSeeText = FlutterI18n.translate(context, 'labelFriends');
        break;
      case WhoCanSeeThis.LimitTo:
        whoCanSeeText = FlutterI18n.translate(context, 'labelLimitTo') +
            (selectedLimitTo.length > 0
                ? ' (' + selectedLimitTo.length.toString() + ')'
                : '');
        break;
    }

    rowChildren.add(Expanded(
        child: Center(
            child: Text(
      whoCanSeeText,
      style: TextStyle(
        fontSize: 13,
      ),
    ))));

    //Anonymous icon.
    Color anonymousIconColor =
        isAnonymous ? Colors.deepPurple.shade400 : Colors.grey.withOpacity(0.4);
    rowChildren.add(_summaryToggleIcon(
        MdiIcons.incognito, anonymousIconColor));

    //Adult icon.
    Color adultIconColor =
        adultOnly ? Colors.red : Colors.grey.withOpacity(0.4);
    rowChildren
        .add(_summaryToggleIcon(MdiIcons.alert, adultIconColor));

    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).hintColor.withOpacity(0.3),
          width: 0.5,
        ),
        color: _settingsViewToggled
            ? Theme.of(context).colorScheme.secondary.withOpacity(0.3)
            : Colors.transparent,
        borderRadius: BorderRadius.circular(3),
      ),
      height: 28,
      margin: EdgeInsets.all(8),
      child: InkWell(
        onTap: () {
          //Disable when sending plurk.
          if (!_isSendingPlurk) {
            setState(() {
              _toggleSettingsView(!_settingsViewToggled);
            });
          }
          Vibrator.vibrateShortNote();
        },
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: rowChildren,
        ),
      ),
    );
  }

  _summaryToggleIcon(IconData icon, Color color) {
    return SizedBox(
      width: 40,
      child: Center(
        child: Icon(
          icon,
          size: 20,
          color: color,
        ),
      ),
    );
  }

  _toggleSettingsView(bool b) {
    if (_settingsViewToggled != b) {
      if (b) {
        //Toggle on
        setState(() {
          _settingsViewToggled = b;
        });

        if (isInEmoticonsMode) {
          toggleEmoticonMode(false);
          isShowingEmoticonView = false;
          //This is complicate...
          _isShowingSettingsView = true;
        }

        if (!commentFocus.hasFocus && !isShowingKeyboard()) {
          _isShowingSettingsView = true;
        }
      } else {
        //Toggle off
        setState(() {
          _settingsViewToggled = b;
        });

        if (!commentFocus.hasFocus && !isShowingKeyboard()) {
          _isShowingSettingsView = false;
        }
      }
    }
  }

  //-- Settings View

  Widget _buildSettingsView() {
    //Decide which item should be displayed.
    List<Widget> listViewChildren = [];

    //The can be edit for premium user. (Who can see)
    //Normal user cannot use this in edit mode.
    if (widget.args.mode == PlurkPosterMode.New ||
        (Static.me != null && Static.me.premium)) {
      //dropdownChildren
      List<DropdownMenuItem> dropdownChildren = [];
      dropdownChildren.add(_whoCanSeeDropdownMenuItem(
          FlutterI18n.translate(context, 'labelTheWholeWorld'),
          WhoCanSeeThis.TheWholeWorld));
      dropdownChildren.add(_whoCanSeeDropdownMenuItem(
          FlutterI18n.translate(context, 'labelFriends'),
          WhoCanSeeThis.Friends));
      dropdownChildren.add(_whoCanSeeDropdownMenuItem(
          FlutterI18n.translate(context, 'labelLimitTo') +
              (selectedLimitTo.length > 0
                  ? ' (' + selectedLimitTo.length.toString() + ')'
                  : ''),
          WhoCanSeeThis.LimitTo));

      // if (Static.me.premium) {}
      // //Todo: Move this up there.
      // dropdownChildren
      //     .add(_whoCanSeeDropdownMenuItem('Except' + (selectedExcept.length > 0 ? ' (' + selectedExcept.length.toString() + ')' : ''), WhoCanSeeThis.Except));

      //Dropdown for who can see.
      listViewChildren.add(_settingsViewItem(
          FlutterI18n.translate(context, 'titleWhoCanSeeThis'), true,
          trailing: widget.args.mode == PlurkPosterMode.Private
              //Private plurk mode should just display the user name.
              ? PlurkCardPrimeHeaderState.buildUserDisplayName(
                  context, null, widget.args.privatePlurkUser, false, null,
                  fontSize: 13)
              : Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: DropdownButton(
                      value: whoCanSeeValue,
                      underline: Container(),
                      items: dropdownChildren,
                      disabledHint: _whoCanSeeDropdownMenuItem(
                          FlutterI18n.translate(context, 'labelTheWholeWorld'),
                          WhoCanSeeThis.TheWholeWorld),
                      onChanged: isAnonymous
                          ? null
                          : (value) {
                              setState(() {
                                //Clear selected Users.
                                selectedLimitTo.clear();
                                // selectedExcept.clear();

                                if (value == WhoCanSeeThis.Friends) {
                                  replurkable = false;
                                } else if (value == WhoCanSeeThis.LimitTo) {
                                  replurkable = false;
                                  //Open users browser to select from friend.
                                  UsersBrowser.show(
                                    context,
                                    UsersViewer.UsersViewerArgs(
                                      FlutterI18n.translate(
                                          context, 'labelLimitTo'),
                                      UsersViewer.UsersBrowsingType.Friends,
                                      userId: Static.meId,
                                      definitelyIncludeSelf: true,
                                    ),
                                  );
                                }
                                // else if (value == WhoCanSeeThis.Except) {
                                //   //Open users browser to select from friend.
                                //   UsersBrowser.show(
                                //     context,
                                //     UsersBrowser.UsersBrowserArgs(
                                //         'Except', UsersBrowser.UsersListType.Friends,
                                //         userId: Static.me.id
                                //     ),
                                //   );
                                // }

                                // print('DropdownButton onChanged: ' + value.toString());
                                whoCanSeeValue = value;
                              });
                            }),
                )));

      //Divider
      listViewChildren.add(Divider(height: 12));
    }

    //Private plurk doesn't need these options.
    if (widget.args.mode != PlurkPosterMode.Private) {
      //Other options.
      listViewChildren.add(_settingsViewItem(
          FlutterI18n.translate(context, 'titleOtherOption'), true));

      //Commentable options
      listViewChildren.add(_settingsViewItem(
        FlutterI18n.translate(context, 'labelResponse'),
        false,
        padding: EdgeInsets.only(left: 8),
        trailing: ToggleButtons(
            constraints: BoxConstraints(
              maxHeight: 30,
            ),
            children: [
              _commentableToggleItem(
                  FlutterI18n.translate(context, 'labelResponseOpen')),
              _commentableToggleItem(
                  FlutterI18n.translate(context, 'labelResponseClose')),
              _commentableToggleItem(
                  FlutterI18n.translate(context, 'labelResponseFriends')),
            ],
            isSelected: commentableTabIsSelected,
            onPressed: (index) {
              if (!isAnonymous) {
                setState(() {
                  for (int i = 0; i < commentableTabIsSelected.length; ++i) {
                    commentableTabIsSelected[i] = (i == index);
                  }
                });
              }
            }),
      ));

      //Replurkable
      listViewChildren.add(_settingsViewItem(
          FlutterI18n.translate(context, 'labelReplurkable'), false,
          padding: EdgeInsets.only(left: 8),
          trailing: Switch(
            value: replurkable,
            onChanged: whoCanSeeValue == WhoCanSeeThis.TheWholeWorld
                ? (value) {
                    setState(() {
                      replurkable = value;
                    });
                  }
                : null,
          ),
          onItemTap: whoCanSeeValue == WhoCanSeeThis.TheWholeWorld
              ? () {
                  setState(() {
                    replurkable = !replurkable;
                  });
                }
              : null));

      //Adult only
      listViewChildren.add(_settingsViewItem(
          FlutterI18n.translate(context, 'lableAdultOnly'), false,
          padding: EdgeInsets.only(left: 8),
          trailing: Switch(
            value: adultOnly,
            onChanged: (value) {
              setState(() {
                adultOnly = value;
              });
            },
          ), onItemTap: () {
        setState(() {
          adultOnly = !adultOnly;
        });
      }));

      //Divider
      listViewChildren.add(Divider(height: 12));

      if (widget.args.mode == PlurkPosterMode.New) {
        //Anonymous Settings
        listViewChildren.add(_settingsViewItem(
            FlutterI18n.translate(context, 'titleAnonymousPost'), true,
            trailing: Switch(
              value: isAnonymous,
              onChanged: (value) {
                _setAnonymous(value);
              },
            ), onItemTap: () {
          _setAnonymous(!isAnonymous);
        }));

        if (isAnonymous) {
          //Sub title.
          listViewChildren.add(_settingsViewItem(
              FlutterI18n.translate(context, 'labelPublishTo'), false));

          //Sub setting 1
          listViewChildren.add(_settingsViewItem(
              FlutterI18n.translate(context, 'labelMyFollowers'), false,
              padding: EdgeInsets.only(left: 8),
              trailing: Switch(
                value: anonymousToFollowers,
                onChanged: (value) {
                  _setAnonymousToFollowers(value);
                },
              ), onItemTap: () {
            _setAnonymousToFollowers(!anonymousToFollowers);
          }));

          //Sub setting 2
          listViewChildren.add(_settingsViewItem(
              FlutterI18n.translate(context, 'labelPublicForum'), false,
              padding: EdgeInsets.only(left: 8),
              trailing: Switch(
                value: publishToAnonymous,
                onChanged: (value) {
                  _setAnonymousToPublicForum(value);
                },
              ), onItemTap: () {
            _setAnonymousToPublicForum(!publishToAnonymous);
          }));
        }
      }
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      //The height is actually measured in runtime.. my beautiful hack.
      height: measuredSoftKeyboardHeight - kMinInteractiveDimension,
      child: ListView(
        children: listViewChildren,
      ),
    );
  }

  _setAnonymous(bool value) {
    setState(() {
      isAnonymous = value;
      if (!value) {
        anonymousToFollowers = false;
        publishToAnonymous = false;
      } else {
        //Clear selected Users.
        selectedLimitTo.clear();
        // selectedExcept.clear();

        whoCanSeeValue = WhoCanSeeThis.TheWholeWorld;
        commentableTabIsSelected = [true, false, false];

        anonymousToFollowers = true;
        publishToAnonymous = false;
      }
    });
  }

  _setAnonymousToFollowers(bool value) {
    setState(() {
      anonymousToFollowers = value;
      // if (!value) {
      //   publishToAnonymous = true;
      // }
    });
  }

  _setAnonymousToPublicForum(bool value) {
    setState(() {
      publishToAnonymous = value;
      // if (!value) {
      //   anonymousToFollowers = true;
      // }
    });
  }

  Widget _commentableToggleItem(String text) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }

  //A customizable settings view item.
  Widget _settingsViewItem(String title, bool titleIsBold,
      {Widget trailing, EdgeInsets padding, Function onItemTap}) {
    List<Widget> rowChildren = [];

    rowChildren.add(Expanded(
      child: Text(
        title,
        style: TextStyle(
            fontWeight: titleIsBold ? FontWeight.bold : FontWeight.normal,
            fontSize: 13),
      ),
    ));

    if (trailing != null) {
      rowChildren.add(trailing);
    }

    return //Main option.
        Container(
      padding: padding == null ? EdgeInsets.all(0) : padding,
      height: 30,
      child: InkWell(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: rowChildren,
        ),
        onTap: onItemTap,
      ),
    );
  }

  DropdownMenuItem _whoCanSeeDropdownMenuItem(
      String title, WhoCanSeeThis whoCanSeeThis) {
    return DropdownMenuItem(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 5),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 13,
          ),
        ),
      ),
      value: whoCanSeeThis,
    );
  }

  //--

  void checkAndSendPlurkAdd() async {
    //Check if the TextField has content.
    if (commentController.text.isEmpty != true) {
      //Disable all editor interface.
      //Exit emoticon mode.
      toggleEmoticonMode(false);

      _toggleSettingsView(false);

      commentFocus.unfocus(disposition: UnfocusDisposition.scope);

      setState(() {
        _isSendingPlurk = true;
      });

      //Lang (???)
      _modalTimelinePlurkAdd.lang = 'en';

      //Prepare our plurk data.
      _modalTimelinePlurkAdd.content = commentController.text;
      _modalTimelinePlurkAdd.qualifier = isAnonymous
          ? Plurdart.PlurkQualifier.Whispers
          : Static.prefPlurkQualifier;

      //Private plurk?
      if (isAnonymous) {
        //Anonymous plurk? remove limitedTo and exclude.
        _modalTimelinePlurkAdd.limitedTo = null;
        _modalTimelinePlurkAdd.excluded = null;
        _modalTimelinePlurkAdd.noComment = Plurdart.Comment.Allow;

        //Anonymous to my followers.
        _modalTimelinePlurkAdd.publishToFollowers =
            anonymousToFollowers ? 1 : 0;
        //Anonymous to public forum.
        _modalTimelinePlurkAdd.publishToAnonymous = publishToAnonymous ? 1 : 0;

        //Replurkable
        _modalTimelinePlurkAdd.replurkable = 1;

        //Adult only
        _modalTimelinePlurkAdd.porn = adultOnly ? 1 : 0;

        _modalTimelinePlurkAdd.anonymous = 1;

        _modalTimelinePlurkAdd.uid = Static.me.id;
      } else if (widget.args.mode == PlurkPosterMode.Private) {
        //Private plurk only need limit_to
        _modalTimelinePlurkAdd.limitedTo = [widget.args.privatePlurkUser.id];

        _modalTimelinePlurkAdd.uid = Static.me.id;
      } else {
        //Non-private mode.
        //Set limit_to
        if (whoCanSeeValue == WhoCanSeeThis.TheWholeWorld) {
          _modalTimelinePlurkAdd.limitedTo = null;
        } else if (whoCanSeeValue == WhoCanSeeThis.Friends) {
          _modalTimelinePlurkAdd.limitedTo = [0];
        } else if (whoCanSeeValue == WhoCanSeeThis.LimitTo) {
          //From the selectedLimitTo list.
          _modalTimelinePlurkAdd.limitedTo =
              selectedLimitTo.map((e) => e).toList();
        }

        //Comment?
        if (commentableTabIsSelected[0]) {
          //Open
          _modalTimelinePlurkAdd.noComment = Plurdart.Comment.Allow;
        } else if (commentableTabIsSelected[1]) {
          //Close
          _modalTimelinePlurkAdd.noComment = Plurdart.Comment.DisableComment;
        } else if (commentableTabIsSelected[2]) {
          //Friends
          _modalTimelinePlurkAdd.noComment = Plurdart.Comment.FriendsOnly;
        }

        //Replurkable
        _modalTimelinePlurkAdd.replurkable = replurkable ? 1 : 0;

        //Adult only
        _modalTimelinePlurkAdd.porn = adultOnly ? 1 : 0;

        //Anonymous. (???)
        _modalTimelinePlurkAdd.anonymous = null;

        //Anonymous to my followers.
        _modalTimelinePlurkAdd.publishToFollowers = null;

        //Anonymous to public forum.
        _modalTimelinePlurkAdd.publishToAnonymous = null;

        //Not sure if we need this. (???)
        _modalTimelinePlurkAdd.uid = Static.me.id;
      }

      // print(prettyJson(_modalTimelinePlurkAdd.toBody()));

      //Send the content. (by cubit)
      _blocProviderContext
          .read<TimelinePlurkAddCubit>()
          .post(_modalTimelinePlurkAdd);

      Vibrator.vibrateShortNote();
    } else {
      Vibrator.vibrateError();
    }
  }

  void checkAndSendPlurkEdit() async {
    //Check if the TextField has content.
    if (commentController.text.isEmpty != true) {
      //Disable all editor interface.
      //Exit emoticon mode.
      toggleEmoticonMode(false);

      _toggleSettingsView(false);

      commentFocus.unfocus(disposition: UnfocusDisposition.scope);

      setState(() {
        _isSendingPlurk = true;
      });

      //Prepare our plurk data.
      _modalTimelinePlurkEdit.content = commentController.text;

      //Set limit_to
      if (whoCanSeeValue == WhoCanSeeThis.TheWholeWorld) {
        _modalTimelinePlurkEdit.limitedTo = null;
      } else if (whoCanSeeValue == WhoCanSeeThis.Friends) {
        _modalTimelinePlurkEdit.limitedTo = [0];
      } else if (whoCanSeeValue == WhoCanSeeThis.LimitTo) {
        //From the selectedLimitTo list.
        _modalTimelinePlurkEdit.limitedTo =
            selectedLimitTo.map((e) => e).toList();
      }

      //Comment?
      if (commentableTabIsSelected[0]) {
        //Open
        _modalTimelinePlurkEdit.noComment = Plurdart.Comment.Allow;
      } else if (commentableTabIsSelected[1]) {
        //Close
        _modalTimelinePlurkEdit.noComment = Plurdart.Comment.DisableComment;
      } else if (commentableTabIsSelected[2]) {
        //Friends
        _modalTimelinePlurkEdit.noComment = Plurdart.Comment.FriendsOnly;
      }

      //Replurkable
      _modalTimelinePlurkEdit.replurkable = replurkable ? 1 : 0;

      //Adult only
      _modalTimelinePlurkEdit.porn = adultOnly ? 1 : 0;

      //Lang (???)
      // _modalTimelinePlurkEdit.lang = 'en';

      //Not sure if we need this. (???)
      // _modalTimelinePlurkEdit.uid = Static.me.id;

      // print(prettyJson(_modalTimelinePlurkEdit.toBody()));

      //Send the content. (by cubit)
      _blocProviderContext
          .read<TimelinePlurkEditCubit>()
          .post(_modalTimelinePlurkEdit);

      Vibrator.vibrateShortNote();
    } else {
      Vibrator.vibrateError();
    }
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  @override
  void requestSetState(Function fn) {
    setState(fn);
  }

  @override
  bool onWantChangePlurkQualifier(Plurdart.PlurkQualifier plurkQualifier) {
    if (plurkQualifier == Plurdart.PlurkQualifier.Whispers) {
      _setAnonymous(true);
      return false;
    }
    return true;
  }
}
