
import "package:path/path.dart" show dirname;
import 'dart:io' show File, Platform;
import 'gdata_helper.dart' as GDataHelper;
import 'package:http/http.dart' as Http;
import 'package:pretty_json/pretty_json.dart';

// Data sheet:
// https://docs.google.com/spreadsheets/d/1QW10H_dUxa43Ymzss8xPD5GSlvFK88Iik6KHp6sZ_4A/edit?usp=drive_web&ouid=115643654859392513077
final String spreadsheetID = '1QW10H_dUxa43Ymzss8xPD5GSlvFK88Iik6KHp6sZ_4A';
final String apiKey = 'AIzaSyAZPHWMU4HCk6FsapayDrG5hZGcfMoiYgI';

void main() {
  print('Hello sync_sheet!');
  Future((_runSync));
}

void _runSync() async {
//   Http.Response workSheetJsonFeedResponse = await Http.get(Uri.parse(GDataHelper.workSheetJsonFeedUrl(workSheetID)));
//   List<String> cellFeedUrls = GDataHelper.workSheetFeedToJsonCellFeedURLs(workSheetJsonFeedResponse.body);
//
//   //[Note]: The foreach won't await for every loop.
// //  cellFeedUrls.forEach((cellFeedUrl) async {
// //    // Await to make sure the response is in correct order.
// //    Http.Response cellFeedResponse = await Http.get(cellFeedUrl);
// //    GDataHelper.WorkSheetData workSheetData = GDataHelper.cellFeedJsonToWorkSheetData(cellFeedResponse.body);
// //    print('[' + workSheetData.title +']');
// //  });
//
//   List<GDataHelper.WorkSheetData> workSheetDataList = [];
//   for (int i = 0 ; i < cellFeedUrls.length ; ++i) {
//     // Await to make sure the response is in correct order.
//     Http.Response cellFeedResponse = await Http.get(Uri.parse(cellFeedUrls[i]));
//     GDataHelper.WorkSheetData workSheetData = GDataHelper.cellFeedJsonToWorkSheetData(cellFeedResponse.body);
//     workSheetDataList.add(workSheetData);
//   }

  List<GDataHelper.WorkSheetData> workSheetDataList = await GDataHelper.fetchGSheetData(spreadsheetID, apiKey);

  // Scan first WorkSheetData's first row to decide how many json file and their names we need...
  List<String> fileNames = [];
  if (workSheetDataList.length > 0) {
    if (workSheetDataList[0].stringTable.length > 0) {
      // Read the first row.
      for (int i = 1 ; i < workSheetDataList[0].stringTable[0].length ; ++i) {
        String cellContent = workSheetDataList[0].stringTable[0][i];
        if(cellContent.isEmpty == false) {
          fileNames.add(workSheetDataList[0].stringTable[0][i]);
        }
      }
    }
  }

  //Windows need to remove first '/'
  String localeSavePath;
  if (Platform.isWindows) {
    localeSavePath = dirname(Platform.script.path).replaceFirst('/', '') + '/../assets/locales/';
  } else {
    localeSavePath = dirname(Platform.script.path) + '/../assets/locales/';
  }

//  print(dirname(Platform.script.path));
  // The file name we need (start from column 1)
//  print('filenames ' + fileNames.toString());

  for (int i = 0 ; i < fileNames.length ; ++i) {
    Map<String, String> newLocaleMap = Map<String, String>();
    //Scan all WorkSheetDatas and map col[0] with col[i + 1]
    for (int w = 0 ; w < workSheetDataList.length ; ++w) {
      for (int r = 1 ; r < workSheetDataList[w].stringTable.length ; ++r) {
        if (workSheetDataList[w].stringTable[r].length > i + 1) {
          String key = workSheetDataList[w].stringTable[r][0];
          String value = workSheetDataList[w].stringTable[r][i + 1];
          if (key.isEmpty == false) {
            newLocaleMap[key] = value;
          }
        }
      }
    }

    // Write the map object as a json file.
    File newFile = File(localeSavePath + fileNames[i] + '.json');
    newFile.writeAsString(prettyJson(newLocaleMap));
  }
}