part of 'response_edit_complete_cubit.dart';

@immutable
abstract class ResponseEditCompleteState {}

class ResponseEditCompleteInitial extends ResponseEditCompleteState {}

class ResponseEditCompleteSuccess extends ResponseEditCompleteState {
  ResponseEditCompleteSuccess(this.plurkId, this.responseId, this.responseEditResult);
  final int plurkId;
  final int responseId;
  final Plurdart.ResponseEditResult responseEditResult;
}

class ResponseEditCompleteFailed extends ResponseEditCompleteState {
  ResponseEditCompleteFailed(this.errorText);
  final String errorText;
}