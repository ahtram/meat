
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/users_selected_cubit.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/widgets/other/bottom_search_bar.dart';
import 'package:meat/widgets/user/users_viewer.dart';

//A customizable user's list browser for select view and select
//Friends/Fans/Following

void show(BuildContext context, UsersViewerArgs args) {
  // print('[_UsersBrowser] show()');
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _UsersBrowser(args);
  }));
}

class _UsersBrowser extends StatefulWidget {
  _UsersBrowser(this.args, {Key key}) : super(key: key);

  final UsersViewerArgs args;

  @override
  _UsersBrowserState createState() => _UsersBrowserState();
}

class _UsersBrowserState extends State<_UsersBrowser> {
  BuildContext _blocProviderContext;
  GlobalKey<UsersViewerState> _viewerKey = GlobalKey<UsersViewerState>();

  GlobalKey<BottomSearchBarState> _bottomInputBarKey = GlobalKey<BottomSearchBarState>();

  @override
  void initState() {
    super.initState();
    // print('[UsersBrowser]: models:' + widget.args.userModals.length.toString());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        //Emit the selected users.
        if (widget.args.isToggleList && _viewerKey.currentState != null) {
          _blocProviderContext
              .read<UsersSelectedCubit>()
              .complete(_viewerKey.currentState.getSelectedUsers());
        }
        return true;
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider<RequestIndicatorCubit>(
            create: (BuildContext context) => RequestIndicatorCubit(),
          ),
        ],
        child: AcrylicScaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            centerTitle: true,
            brightness: Theme.of(context).brightness,
            backgroundColor:
            Theme.of(context).canvasColor.withOpacity(0.4),
            elevation: 0,
            leading: AppBarActionButton(Icons.arrow_back, onTap: () {
              Navigator.pop(context);
              //Emit the selected users.
              if (widget.args.isToggleList && _viewerKey.currentState != null) {
                _blocProviderContext
                    .read<UsersSelectedCubit>()
                    .complete(_viewerKey.currentState.getSelectedUsers());
              }
            }),
            title: Text(
              widget.args.title,
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
            ),
          ),
          floatingActionButton: BlocBuilder<RequestIndicatorCubit,
              RequestIndicatorState>(
            builder: (context, state) {
              //Check state.
              List<Widget> columnChildren = [];

              //Only when showing request indicator.
              if (state is RequestIndicatorShow) {
                //Busy indicator.
                columnChildren.add(Container(
                  width: 56,
                  height: 56,
                  child: FlareActor(
                    "assets/rive/MeatLoader.flr",
                    animation: "Untitled",
                  ),
                ));
              }

              return Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: columnChildren,
              );
            },
          ),
          child: Builder(
            builder: (context) {
              _blocProviderContext = context;
              return UsersViewer(
                widget.args,
                key: _viewerKey,
              );
            },
          ),
          bottomNavigationBar: BottomSearchBar(
            onSearchChanged: _onSearchTermChanged,
            withSearchButton: false,
            key: _bottomInputBarKey,
          ),
        ),
      ),
    );
  }

  _onSearchTermChanged(String term) {
    //Search User
    _viewerKey.currentState.setFilterTerm(term);
  }

}
