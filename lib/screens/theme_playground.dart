import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meat/models/chikkutteh/res/living_channel_res.dart';
import 'dart:ui' as ui;
import 'package:meat/theme/app_themes.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/ui/theme_bloc.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:pretty_json/pretty_json.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:meat/widgets/youtube_video_player/youtube_video_player.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:meat/chikkutteh/chikkutteh.dart' as ChikKutTeh;
import 'package:meat/system/static_stuffs.dart' as Static;

class ThemePlayground extends StatefulWidget {
  ThemePlayground();

  @override
  _ThemePlaygroundState createState() => _ThemePlaygroundState();
}

enum ImageOrientation {
  Square,
  Landscape,
  Portrait,
}

//A structure to store image info
class ImageRawInfo {
  ImageRawInfo(this.source, this.byteData, this.uiImage);

  final String source;
  final ByteData byteData;
  final ui.Image uiImage;

  double getImageSlope() {
    return uiImage.height / uiImage.width;
  }

  //For Column child flex
  int getImageSlopeAsFlexRef() {
    return (getImageSlope() * 100.0).toInt();
  }

  //For row child flex
  int getReverseImageSlopeAsFlexRef() {
    return ((1.0 / getImageSlope()) * 100.0).toInt();
  }

  ImageOrientation getImageOrientation() {
    if (uiImage.height == uiImage.width) {
      return ImageOrientation.Square;
    } else {
      double slope = getImageSlope();
      if (slope > 1.0) {
        return ImageOrientation.Portrait;
      } else {
        return ImageOrientation.Landscape;
      }
    }
  }

  //A helper for calculate average slope.
  static double calculateAverageSlope(List<ImageRawInfo> infos) {
    if (infos.length >= 1) {
      double total = 0.0;
      infos.forEach((element) {
        total += element.getImageSlope();
      });
      return total / infos.length;
    }
    return 1.0;
  }
}

class _ThemePlaygroundState extends State<ThemePlayground> {
  _ThemePlaygroundState();

  AppTheme usingAppTheme = AppTheme.DeepOrange;
  ThemeMode usingThemeMode = ThemeMode.system;

  // For showing a SnackBar.
  final _acrylicScaffoldKey = GlobalKey<AcrylicScaffoldState>();

  // For test toggle buttons.
  List<bool> togglesIsSelected = [false, false, false];
  bool switchValue = false;
  bool cupertinoSwitchValue = false;
  bool checkBoxValueA = false;
  bool checkBoxValueB = true;
  bool checkBoxValueC;

  double sliderValue = 50.0;

  //Test collect all image byteData...
  List<ImageRawInfo> _imageRawInfoList = [];

  String _youtubeSearchChannelId = 'UCwVQIkAtyZzQSA-OY1rsGig';

  @override
  void initState() {
    super.initState();
    _imageRawInfoList.clear();

    //Test the youtube search stuffs.
    // _testYoutubeSearchCall();

    _testChikKutTehAPIs();
  }

  _testChikKutTehAPIs() async {
    LivingChannelsRes res = await ChikKutTeh.getLivingChannels();
    if (res != null) {
      print('Got LivingChannelsRes: [' + prettyJson(res.toJson()) + ']');
    } else {
      print('OOps! _testChikKutTehAPIs() res is null?!');
    }
  }

  // _testYoutubeSearchCall() async {
  //   //[Fuck]: Youtube explore broken. Cannot even work.
  //   // YoutubeExplode ye = YoutubeExplode();
  //   // ChannelUploadsList channelUploadsList = await ye.channels.getUploadsFromPage(_youtubeSearchChannelId);
  //   // if (channelUploadsList.length > 0) {
  //   //   print('_testYoutubeSearchCall got first video channel[' +
  //   //       channelUploadsList.first.channelId.value +
  //   //       '] length[' +
  //   //       channelUploadsList.length.toString() +
  //   //       '] isLive[' +
  //   //       channelUploadsList.first.isLive.toString() +
  //   //       ']');
  //   // } else {
  //   //   print('Oops! _testYoutubeSearchCall() no video found...');
  //   // }
  //
  //   //[Nope]: The is too limit and cannot search the target channel.
  //   // SearchList sl = await ye.search
  //   //     .getVideos(_youtubeSearchChannelId, filter: filters.types.channel);
  //   // if (sl.length > 0) {
  //   //   print('_testYoutubeSearchCall got first video channel[' +
  //   //       sl.first.channelId.value +
  //   //       '] length[' +
  //   //       sl.length.toString() +
  //   //       '] isLive[' +
  //   //       sl.first.isLive.toString() +
  //   //       ']');
  //   // } else {
  //   //   print('Oops! _testYoutubeSearchCall() no video found...');
  //   // }
  // }

  Future loadImageByteData(String assetPath) async {
    ByteData byteData = await rootBundle.load(assetPath);

    //Also depend on dart.ui to find the images width/height.
    ui.Image image = await decodeImageFromList(byteData.buffer.asUint8List());

    //Store this.
    _imageRawInfoList.add(ImageRawInfo(assetPath, byteData, image));
  }

  Row _buildImageCollectionRow(List<ImageRawInfo> infos) {
    List<Widget> rowChildren = [];
    infos.forEach((element) {
      rowChildren.add(Expanded(
        child: Image(
          image: MemoryImage(element.byteData.buffer.asUint8List()),
          fit: BoxFit.cover,
        ),
        flex: element.getReverseImageSlopeAsFlexRef(),
      ));
    });

    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: rowChildren,
    );
  }

  Column _buildImageCollectionColumn(List<ImageRawInfo> infos) {
    List<Widget> columnChildren = [];
    infos.forEach((element) {
      columnChildren.add(Expanded(
        child: Image(
          image: MemoryImage(element.byteData.buffer.asUint8List()),
          fit: BoxFit.cover,
        ),
        flex: element.getImageSlopeAsFlexRef(),
      ));
    });

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: columnChildren,
    );
  }

  @override
  Widget build(BuildContext context) {
    PlurkCardYoutubeVideoLink videoLinkInfo = PlurkCardYoutubeVideoLink(
        '【対決】ドラえもんマッシュアップメドレー -DORAEMON Mash Up Medley Battle...',
        '3J7EV8gPNe0',
        'https://i.ytimg.com/vi/3J7EV8gPNe0/default.jpg',
        480,
        270);

    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: AcrylicScaffold(
        key: _acrylicScaffoldKey,
        appBar: AppBar(
          title: Text('Theme Playground'),
          elevation: 0,
          automaticallyImplyLeading: true,
          backgroundColor: Theme.of(context).primaryColor.withOpacity(0.5),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {},
        ),
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            // A dropdown for switching theme.
            DropdownButton<AppTheme>(
              value: usingAppTheme,
              items:
                  AppTheme.values.map<DropdownMenuItem<AppTheme>>((appTheme) {
                return DropdownMenuItem<AppTheme>(
                  value: appTheme,
                  child: Text(EnumToString.convertToString(appTheme)),
                );
              }).toList(),
              onChanged: (newAppTheme) async {
                usingAppTheme = newAppTheme;

                //Store the theme into Preferences.
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setString(Define.appThemePreferencesKey,
                    EnumToString.convertToString(newAppTheme));

                BlocProvider.of<ThemeBloc>(context).add(ThemeChanged(
                    appTheme: usingAppTheme, themeMode: usingThemeMode, textScale: Static.settingsTextScale,
                    canvasColor: Static.settingsCanvasColor));
              },
            ),

            DropdownButton<ThemeMode>(
              value: usingThemeMode,
              items: ThemeMode.values
                  .map<DropdownMenuItem<ThemeMode>>((themeMode) {
                return DropdownMenuItem<ThemeMode>(
                  value: themeMode,
                  child: Text(EnumToString.convertToString(themeMode)),
                );
              }).toList(),
              onChanged: (newThemeMode) async {
                usingThemeMode = newThemeMode;

                //Store the theme into Preferences.
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setString(Define.themeModePreferencesKey,
                    EnumToString.convertToString(newThemeMode));

                BlocProvider.of<ThemeBloc>(context).add(ThemeChanged(
                    appTheme: usingAppTheme, themeMode: usingThemeMode, textScale: Static.settingsTextScale,
                    canvasColor: Static.settingsCanvasColor));
              },
            ),

            Divider(),

            //Test Youtube Player. (Shiba inu)
            YoutubeVideoPlayer(videoLinkInfo.youtubeVideoID),

            Divider(),

            Container(
              decoration: BoxDecoration(
                border: Border.all(),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                  image: AssetImage('assets/images/BKTTokenLogo.png'),
                  width: 64,
                  height: 64,
                ),
              ),
            ),

            const Divider(
              height: 25,
            ),

            Text(
              'Material Widgets',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),

            // Transparent card.
            Card(
              color: Colors.transparent,
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    TextField(
                      decoration: const InputDecoration(
                        filled: true,
                        icon: Icon(Icons.portrait, size: 36),
                        hintText: 'TextField',
                      ),
                    ),
                    ElevatedButton.icon(
                      icon: const Icon(Icons.alarm, size: 18),
                      label: Text('Material RaisedButton'),
                      onPressed: () {},
                    ),
                    TextButton.icon(
                        icon: Icon(Icons.settings, size: 18),
                        label: Text('FlatButton'),
                        onPressed: () {}),
                  ],
                ),
              ),
            ),

            ElevatedButton.icon(
              icon: const Icon(Icons.alarm, size: 18),
              label: Text('Material RaisedButton'),
              onPressed: () {},
            ),

            Slider(
              value: sliderValue,
              min: 0,
              max: 100,
              onChanged: (value) {
                setState(() {
                  sliderValue = value;
                });
              },
            ),

            TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                filled: true,
                icon: Icon(Icons.mail, size: 36),
                helperText: 'helperText',
                labelText: 'labelText',
              ),
            ),
            TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                filled: true,
                icon: Icon(Icons.mail, size: 36),
                helperText: 'helperText',
                labelText: 'labelText',
              ),
            ),
            TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                filled: true,
                icon: Icon(Icons.mail, size: 36),
                helperText: 'helperText',
                labelText: 'labelText',
              ),
            ),

            OutlinedButton.icon(
                icon: Icon(Icons.add_a_photo, size: 18),
                label: Text('OutlinedButton'),
                onPressed: () {}),

            SizedBox(
              height: 10,
            ),

            Center(
              child: CircularProgressIndicator(),
            ),

            SizedBox(
              height: 10,
            ),

            Center(
              child: LinearProgressIndicator(),
            ),

            SizedBox(
              height: 10,
            ),

            ToggleButtons(
              children: <Widget>[
                Icon(Icons.camera_enhance),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text('Toggle Buttons'),
                ),
                Icon(Icons.open_in_new),
              ],
              isSelected: togglesIsSelected,
              onPressed: (index) {
                setState(() {
                  togglesIsSelected[index] = !togglesIsSelected[index];
                });
              },
            ),

            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                Switch(
                  value: switchValue,
                  onChanged: (value) {
                    setState(() {
                      switchValue = value;
                    });
                  },
                ),
                CupertinoSwitch(
                  value: cupertinoSwitchValue,
                  onChanged: (value) {
                    setState(() {
                      cupertinoSwitchValue = value;
                    });
                  },
                ),
                Checkbox(
                  value: checkBoxValueA,
                  onChanged: (value) {
                    setState(() {
                      checkBoxValueA = value;
                    });
                  },
                ),
                Checkbox(
                  value: checkBoxValueB,
                  onChanged: (value) {
                    setState(() {
                      checkBoxValueB = value;
                    });
                  },
                ),
                Checkbox(
                  value: checkBoxValueC,
                  tristate: true,
                  onChanged: (value) {
                    setState(() {
                      checkBoxValueC = value;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
