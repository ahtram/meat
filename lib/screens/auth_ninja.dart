import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:io';

void show(BuildContext context, AuthNinjaArgs args) {
  //Nope: this will cause a bit glitch.
  //SystemChrome.setEnabledSystemUIOverlays([]);
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _AuthNinja(args);
  }));
}

class AuthNinjaArgs {
  final String initUri;
  final Function onVerifierReceived;
  final Function onLoginError;
  final Function onLoginCancel;

  final String loginUsername;
  final String loginPassword;

  AuthNinjaArgs(this.initUri, this.onVerifierReceived, this.onLoginError,
      this.onLoginCancel, this.loginUsername, this.loginPassword);
}

class _AuthNinja extends StatefulWidget {
  _AuthNinja(this.args);
  final AuthNinjaArgs args;

  @override
  _AuthNinjaState createState() => _AuthNinjaState();
}

class _AuthNinjaState extends State<_AuthNinja> {
  _AuthNinjaState();

  // Use for control the webView to evaluate JS.
  WebViewController _controller = WebViewController();

  bool _loginSucceed = false;

  final String _getVerifierJS =
      'document.getElementById("oauth_verifier").innerHTML;';
  final String _mobileGrantAccessJS =
      'if (typeof document.authorize !== "undefined") { document.authorize.accept.value="1"; document.authorize.submit(); }';
  final String _grantAccessJS =
      'if (typeof document.authorize !== "undefined") { document.authorize.accept.value="1"; document.authorize.submit(); }';

  final String _fillUserNameInputJS =
      'document.getElementsByName("nick_name")[0].value = "%u";';
  final String _fillPasswordInputJS =
      'document.getElementsByName("password")[0].value = "%p";';

  final String _submitFormJS =
      'document.getElementById("login_form").submit();';

  //This works on iOS. Not sure if works on Android. (OMFG NO!!)
  final String _tryGetOTPInputJSiOS = 'String(document.getElementById("otp"));';
  final String _tryGetOTPInputJSAndroid = 'document.getElementById("otp");';

  final String _fillOTPInputJS = 'document.getElementById("otp").value = "%a";';
  final String _submitOTPFormJS =
      'document.getElementById("login_2fa").submit();';

  final String _plurkLoginPath = '/login';
  final String _plurkLogin2FAPath = '/login/2fa';
  final String _plurkLoginMobileAuthUrlPath = '/m/authorize';
  final String _plurkLoginAuthUrlPath = '/OAuth/authorize';
  final String _plurkLoginAuthDoneUrlPath = '/OAuth/authorizeDone';

  @override
  void initState() {
    super.initState();
    WebViewCookieManager().clearCookies();
    _controller.clearCache();

    _controller.setJavaScriptMode(JavaScriptMode.unrestricted);

    _controller.setNavigationDelegate(
      NavigationDelegate(
        onProgress: (int progress) {
          // Update loading bar.
        },
        onPageStarted: (url) {
          // print('onPageStarted: ' + url);
        },
        onPageFinished: (url) async {
          Uri uri = Uri.parse(url);

          // print('onPageFinished: [' +
          //     url +
          //     '] Path: [' +
          //     uri.path +
          //     ']');

          if (_loginSucceed) {
            // print('[Auth flow has completed][Ignore this nonsense]');
            return;
          }

          if (uri.path == _plurkLoginPath) {
            // print('[We are in login path]: Try see if there\'s an error');
            // Try test if login error...by getting the error class.
            if (uri.queryParameters.containsKey('error')) {
              //We got an login error.
              // print('[We got an AuthNinja login error]: ' +
              //     uri.queryParameters['error']);
              // Leave this page.
              WebViewCookieManager().clearCookies();
              _controller.clearCache().then((value) {
                widget.args.onLoginError();
              });
            } else {
              //No error.
              // print('No error found... Try get the OTP input element...');

              //[Very Tricky!]: iOS WebView treat JS different from Android WebView!
              String tryGetOTPInputJS = '';
              if (Platform.isIOS) {
                tryGetOTPInputJS = _tryGetOTPInputJSiOS;
              } else {
                tryGetOTPInputJS = _tryGetOTPInputJSAndroid;
              }

              //Check if this is a 2FA input screen.
              String getOTPResult = await _controller
                  .runJavaScriptReturningResult(tryGetOTPInputJS);
              // print('[getOTPResult][' +
              //     getOTPResult +
              //     ']');
              //'<null>' for iOS webview behavior
              //'null' for Android webview behavior
              if (getOTPResult == null ||
                  getOTPResult == 'null' ||
                  getOTPResult == '<null>') {
                //This is true login page.
                // Auto fill input field.
                // print('[Starting to fill the form]');
                _controller
                    .runJavaScript(_fillUserNameInputJS.replaceAll(
                        '%u', widget.args.loginUsername))
                    .then((value) {
                  _controller
                      .runJavaScript(_fillPasswordInputJS.replaceAll(
                          '%p', widget.args.loginPassword))
                      .then((value) {
                    _controller.runJavaScript(_submitFormJS);
                  });
                });
              } else {
                // print('Got an OTPResult so pop 2FA dialog.');
                //This is OTP input page.
                _pop2FADialog();
              }
            }
          } else if (uri.path == _plurkLogin2FAPath) {
            // print('Got an _plurkLogin2FAPath so pop 2FA dialog. (again)');
            //This means 2FA code entering error... pop the dialog again.
            _pop2FADialog();
          } else if (uri.path == _plurkLoginAuthDoneUrlPath) {
            // print('evaluating verifier... [' +
            //     _getVerifierJS +
            //     ']');
            //Auth done , we should try read the verifier.
            _controller
                .runJavaScriptReturningResult(_getVerifierJS)
                .then((result) {
//                          print('evaluate verifier result: ' + result);

              //This is for iOS's stupid reload after clearCookies()
              //Got to prevent unnecessary logic
              _loginSucceed = true;

              WebViewCookieManager().clearCookies();
              _controller.clearCache().then((value) {
                widget.args.onVerifierReceived(
                    (result as String).replaceAll(RegExp('"'), ''));
              });
            });
          } else if (uri.path == _plurkLoginMobileAuthUrlPath) {
            //Press grant access.
            // print('auto grant access...[' + url + ']');
            // This will run two times. I don't know why. Maybe it's a redirect?
            // I'll just solve it by adding a undefine check in the JS code.
            _controller.runJavaScript(_mobileGrantAccessJS);
          } else if (uri.path == _plurkLoginAuthUrlPath) {
            // print('auto grant access...[' + url + ']');
            // This will run two times. I don't know why. Maybe it's a redirect?
            // I'll just solve it by adding a undefine check in the JS code.
            _controller.runJavaScript(_grantAccessJS);
          } else {
            //??? This is not a legal url...
            //Treat this as a login error.
            // print('This is not a legal url... onPageFinished: ' +
            //     url);
            widget.args.onLoginError();
          }
        },
        onWebResourceError: (WebResourceError error) {},
        onNavigationRequest: (NavigationRequest request) {
          if (request.url.startsWith('https://www.youtube.com/')) {
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ),
    );

    _controller.loadRequest(Uri.parse(widget.args.initUri));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: IgnorePointer(
        child: Material(
          color: Colors.transparent,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Opacity(
                    opacity: 0.0,
                    child: WebViewWidget(
                      controller: _controller,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _pop2FADialog() {
    TextEditingController keywordEditorController = TextEditingController();
    AwesomeDialog(
        context: context,
        dialogType: DialogType.info,
        headerAnimationLoop: false,
        animType: AnimType.scale,
        dismissOnTouchOutside: false,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 8,
              ),
              Text(
                'Two-factor authentication',
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              SizedBox(
                width: 200,
                child: TextField(
                  controller: keywordEditorController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(6),
                    border: OutlineInputBorder(),
                    hintText: 'Authentication Code',
                    hintStyle: TextStyle(
                      color: Theme.of(context).hintColor,
                      fontSize: 14,
                    ),
                    isDense: true,
                  ),
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  keyboardAppearance: Theme.of(context).brightness,
                ),
              ),
            ],
          ),
        ),
        btnOkText: 'Confirm',
        btnCancelText: 'Cancel',
        btnOkOnPress: () async {
          //Try send the authentication code.
          _controller
              .runJavaScript(_fillOTPInputJS.replaceAll(
                  '%a', keywordEditorController.text))
              .then((value) {
            _controller.runJavaScript(_submitOTPFormJS);
          });
        },
        btnCancelOnPress: () {
          //Cancel login authentication.
          widget.args.onLoginCancel();
        })
      ..show();
  }
}
