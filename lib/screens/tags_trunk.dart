import 'package:after_layout/after_layout.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/bookmark/tags_trunk_item.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/screens/busy.dart' as Busy;

void show(BuildContext context, TagsTrunkArgs args) {
  Navigator.push(context, TransparentRoute(
      builder: (BuildContext context) {
    return TagsTrunk(args);
  },));
}

// enum TagsTrunkMode {
//   Selection, //For quick assign tags to bookmark.
//   Index, //For viewing all bookmarks in bookmark browser.
// }

class TagsTrunkArgs {
  TagsTrunkArgs({this.plurkID, this.onTagsDecided});
  final int plurkID;
  // final TagsTrunkMode mode;
  final Function onTagsDecided;
}

class TagsTrunk extends StatefulWidget {
  TagsTrunk(this.args, {Key key}) : super(key: key);

  final TagsTrunkArgs args;

  @override
  _TagsTrunkState createState() => _TagsTrunkState();
}

class _TagsTrunkState extends State<TagsTrunk> with AfterLayoutMixin {
  //The bookmark we are editing tag: possible null!
  Plurdart.Bookmark _editingBookmark;

  TextEditingController _tagEditorController;
  FocusNode _tagEditorFocusNode;
  String _newTagName = '';

  List<String> _selectedTags;
  List<String> _tags = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //Refresh tags data
    setState(() {
      //Use the cache directly
      _tags = Static.getTags();
      // print('[all tags cache]: [' + _tags.join(', ') + ']');
    });

    await Static.updateTagsCache();

    if (mounted) {
      setState(() {
        //Update the cache for good
        _tags = Static.getTags();
        // print('[all tags cache]: [' + _tags.join(', ') + ']');
      });
    }

    if (mounted) {
      //Refresh bookmark data
      _editingBookmark = await Plurdart.bookmarksGetBookmark(
          Plurdart.APlurkID(plurkID: widget.args.plurkID));
    }

    if (mounted) {
      if (_editingBookmark != null) {
        setState(() {
          _selectedTags = _editingBookmark.tags;
        });
        // print('[bookmark tags]: [' + _editingBookmark.tags.join(', ') + ']');
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //Add all tags item.
    List<Widget> listChildren = [];
    if (_tags != null) {
      for (int i = 0 ; i < _tags.length ; ++i) {
        listChildren.add(TagsTrunkItem(
            _tags[i],
           selected: _selectedTags == null ? false : _selectedTags.contains(_tags[i]),
           onTap: (tag) {
              if (mounted && _selectedTags != null) {
                setState(() {
                  if (_selectedTags.contains(tag)) {
                    _selectedTags.remove(tag);
                  } else {
                    _selectedTags.add(tag);
                  }
                });
              }
           },
        ));
      }
    }

    List<Widget> columnChildren = [];
    columnChildren.add(Expanded(
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: 8),
        children: listChildren,
      ),
    ));

    columnChildren.add(ElevatedButton.icon(
      icon: Icon(MdiIcons.tagPlus),
      label: Text('New'),
      onPressed: () async {
        _showTagEditor();
        //Show text editing dialog.
      },
    ));

    return WillPopScope(
      onWillPop: () async {
        if (_editingBookmark != null && _selectedTags != null) {
          widget.args.onTagsDecided?.call(_editingBookmark.id, _selectedTags);
        }
        return true;
      },
      child: AcrylicScaffold(
          transparentColor: Theme.of(context).canvasColor.withOpacity(0.4),
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            centerTitle: true,
            brightness: Theme.of(context).brightness,
            backgroundColor: Colors.transparent,
            elevation: 0,
            titleSpacing: 0.0,
            leading: Container(),
            automaticallyImplyLeading: true,
            actions: [
              AppBarActionButton(Icons.close, onTap: () {
                if (_editingBookmark != null && _selectedTags != null) {
                  widget.args.onTagsDecided?.call(_editingBookmark.id, _selectedTags);
                }
                Navigator.pop(context);
              }),
              SizedBox(
                width: 4,
              )
            ],
            title: Text(
              FlutterI18n.translate(context, 'titleTags'),
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
            ),
          ),
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: columnChildren,
              ),
            ),
          )
      ),
    );
  }

  _showTagEditor() {
    _newTagName = '';
    AwesomeDialog(
      context: context,
      dialogType: DialogType.NO_HEADER,
      animType: AnimType.SCALE,
      headerAnimationLoop: false,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 8,
            ),
            Text(
              FlutterI18n.translate(context, 'labelAddTagDialogTitle'),
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              width: 200,
              child: TextField(
                autofocus: true,
                controller: _tagEditorController,
                focusNode: _tagEditorFocusNode,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(6),
                  border: OutlineInputBorder(),
                  hintText: 'Tag Name',
                  hintStyle: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 14,
                  ),
                  isDense: true,
                ),
                textAlign: TextAlign.center,
                keyboardType: TextInputType.name,
                keyboardAppearance: Theme.of(context).brightness,
                onChanged: _onTagEditorChanges,
              ),
            ),
          ],
        ),
      ),
      btnOkText: FlutterI18n.translate(context, 'dialogAdd'),
      btnCancelText: FlutterI18n.translate(context, 'dialogCancel'),
      dismissOnTouchOutside: false,
      btnOkOnPress: () async {
        _tryAddNewTag(_newTagName);
      },
      btnCancelOnPress: () { },
    )..show();
  }

  _onTagEditorChanges(String newTagName) {
    _newTagName = newTagName;
  }

  Future<bool> _tryAddNewTag(String newTag) async {
    if (!_tags.contains(newTag) && newTag.isNotEmpty) {
      Busy.show(context);
      Plurdart.Status status = await Plurdart.bookmarksCreateTag(Plurdart.ATag(tag: newTag));
      Navigator.pop(context);
      if (!status.hasError()) {
        //Success.
        setState(() {
          _tags.add(newTag);
        });
        return true;
      } else {
        //Failed?
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.SCALE,
          headerAnimationLoop: false,
          title: FlutterI18n.translate(context, 'dialogError'),
          desc: status.errorText,
          btnOkText: FlutterI18n.translate(context, 'dialogConfirm'),
          btnOkOnPress: () {}
        )..show();
        return false;
      }
    }
    return false;
  }

}
