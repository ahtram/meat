import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/models/chikkutteh/res/living_channel_res.dart';
import 'package:meat/theme/app_themes.dart';
import 'package:meat/widgets/other/themed_badge.dart';
import 'home_drawer_item.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

class HomeDrawerBody extends StatelessWidget {
  HomeDrawerBody(
      {this.onFriendAndFansTap,
      this.onBookmarksTap,
      this.onTopTap,
      this.onAnonymousTap,
      this.onHotLinksTap,
      this.onCliquesTap,
      this.onSearchTap,
      this.onOfficialNewsTap,
      this.onVTWatchTap,
      this.onAboutTap,
      this.onLogoutTap,
      this.onPlaygroundTap});

  final Function onFriendAndFansTap;
  final Function onBookmarksTap;
  final Function onTopTap;
  final Function onAnonymousTap;
  final Function onHotLinksTap;
  final Function onCliquesTap;
  final Function onSearchTap;
  final Function onOfficialNewsTap;
  final Function onVTWatchTap;
  final Function onAboutTap;
  final Function onLogoutTap;
  final Function onPlaygroundTap;

  @override
  Widget build(BuildContext context) {
    List<Widget> listChildren = [];

    listChildren.add(HomeDrawerItem(
      MdiIcons.chartAreaspline,
      FlutterI18n.translate(context, 'menuTopPlurks'),
      onTap: onTopTap,
    ));

    listChildren.add(HomeDrawerItem(
      MdiIcons.incognito,
      FlutterI18n.translate(context, 'menuAnonymousPlurks'),
      onTap: onAnonymousTap,
    ));

    listChildren.add(HomeDrawerItem(
      Icons.whatshot,
      FlutterI18n.translate(context, 'menuHotLinks'),
      onTap: onHotLinksTap,
    ));

    listChildren.add(HomeDrawerItem(
      Icons.search,
      FlutterI18n.translate(context, 'menuSearch'),
      onTap: onSearchTap,
    ));

    listChildren.add(HomeDrawerItem(
      MdiIcons.newspaper,
      FlutterI18n.translate(context, 'menuOfficialNews'),
      onTap: onOfficialNewsTap,
    ));

    listChildren.add(HomeDrawerItem(
      MdiIcons.accountMultiple,
      FlutterI18n.translate(context, 'menuMyFriends'),
      onTap: onFriendAndFansTap,
    ));

    if (Static.meIsPremium()) {
      listChildren.add(HomeDrawerItem(
        Icons.bookmarks,
        FlutterI18n.translate(context, 'menuBookmarks'),
        onTap: onBookmarksTap,
      ));
    }

    Widget liveCountWidget = Container();
    LivingChannelsRes livingChannelsRes = Static.getLivingChannelsRes();
    if (livingChannelsRes != null && Static.settingsDisplayVTWatchNotifyText) {
      int livingCount = livingChannelsRes.totalLivingCount();
      if (livingCount > 0) {
        Color dotColor = Theme.of(context).lowContrastPrimaryColor;
        liveCountWidget = ThemedBadge(dotColor, livingCount.toString());
      }
    }

    listChildren.add(HomeDrawerItem(
      Icons.live_tv,
      FlutterI18n.translate(context, 'menuVTWatch'),
      trailing: liveCountWidget,
      onTap: onVTWatchTap,
    ));

    listChildren.add(HomeDrawerItem(
      Icons.info_outline,
      FlutterI18n.translate(context, 'menuAbout'),
      onTap: onAboutTap,
    ));

    //For debug page.
    // listChildren.add(HomeDrawerItem(
    //   MdiIcons.ladybug,
    //   'Playground',
    //   onTap: onPlaygroundTap,
    // ));

    return Container(
      color: Theme.of(context).canvasColor.withOpacity(0.5),
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        children: listChildren,
      ),
    );
  }
}
