part of 'stats_top_replurks_cubit.dart';

@immutable
abstract class StatsTopReplurksState {}

class StatsTopReplurksInitial extends StatsTopReplurksState {}

class StatsTopReplurksSuccess extends StatsTopReplurksState {
  StatsTopReplurksSuccess(this.topPlurks);
  final Plurdart.TopPlurks topPlurks;
}

class StatsTopReplurksFailed extends StatsTopReplurksState { }