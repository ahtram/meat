import 'package:flutter/material.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';

class Cliques extends StatefulWidget {
  Cliques({Key key}) : super(key: key);

  @override
  _CliquesState createState() => _CliquesState();
}

class _CliquesState extends State<Cliques> {

  BuildContext scaffoldBodyContext;

  @override
  Widget build(BuildContext context) {
    return AcrylicScaffold(
        appBar: AppBar(
          centerTitle: true,
          brightness: Theme.of(context).brightness,
          backgroundColor:
          Theme.of(context).canvasColor.withOpacity(0.4),
          elevation: 0,
          leading: AppBarActionButton(Icons.arrow_back, onTap: () {
            Navigator.pop(context);
          }),
          title: Text(
            'Cliques',
            style: TextStyle(
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
        child: Container(
          color: Theme.of(context).canvasColor.withOpacity(0.4),
          child: Center(
            child: Text('Working in progress...'),
          ),
        ));
  }
}