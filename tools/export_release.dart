import 'dart:io';
import "package:path/path.dart";
import 'package:pubspec_parse/pubspec_parse.dart';

void main() async {
  String scriptPath = dirname(Platform.script.toFilePath(windows: Platform.isWindows));
  Directory scriptDir = Directory(scriptPath);
  String projectPath = dirname(scriptDir.path);

  //Read pubspec file.
  File pubspecFile = File(projectPath + '/pubspec.yaml');
  //Copy the file to desktop.
  Pubspec pubspec = Pubspec.parse(await pubspecFile.readAsString());

  String home = "";
  Map<String, String> envVars = Platform.environment;
  if (Platform.isMacOS) {
    home = envVars['HOME'];
  } else if (Platform.isLinux) {
    home = envVars['HOME'];
  } else if (Platform.isWindows) {
    home = envVars['UserProfile'];
  }

  //Try copy apk file.
  File apkFile = File(projectPath + '/build/app/outputs/flutter-apk/app-release.apk');
  if (apkFile != null && apkFile.existsSync()) {
    await apkFile.copy(home + '/Desktop/app-release-' + pubspec.version.major.toString() + '_' + pubspec.version.minor.toString() + '_' + pubspec.version.patch.toString() + '.apk');
    print('Export APK version [' + pubspec.version.toString() + '] to [' + home + '/Desktop/app-release-' + pubspec.version.major.toString() + '_' + pubspec.version.minor.toString() + '_' + pubspec.version.patch.toString() + '.apk' + ']');
  } else {
    print('Oops! release apk not exist! [' + projectPath + '/build/app/outputs/flutter-apk/app-release.apk' + ']');
  }

  //Try copy aab file.
  File aabFile = File(projectPath + '/build/app/outputs/bundle/release/app-release.aab');
  if (aabFile != null && aabFile.existsSync()) {
    await aabFile.copy(home + '/Desktop/app-release-' + pubspec.version.major.toString() + '_' + pubspec.version.minor.toString() + '_' + pubspec.version.patch.toString() + '.aab');
    print('Export AAB version [' + pubspec.version.toString() + '] to [' + home + '/Desktop/app-release-' + pubspec.version.major.toString() + '_' + pubspec.version.minor.toString() + '_' + pubspec.version.patch.toString() + '.aab' + ']');
  } else {
    print('Oops! release apk not exist! [' + projectPath + '/build/app/outputs/bundle/release/app-release.aab' + ']');
  }

}