import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'official_news_state.dart';

class OfficialNewsCubit extends Cubit<OfficialNewsState> {
  OfficialNewsCubit() : super(OfficialNewsInitial());

  request() async {
    // print('request with Static.apiLanguage: ' + Static.apiLanguage);
    List<Plurdart.Plurk> officialNewsPlurks = await Plurdart.plurkTopFetchOfficialPlurks(Plurdart.LangQry(lang: Static.apiLanguage));
    if (officialNewsPlurks != null) {
      emit(OfficialNewsSuccess(officialNewsPlurks));
    } else {
      emit(OfficialNewsFailed());
    }
  }
}
