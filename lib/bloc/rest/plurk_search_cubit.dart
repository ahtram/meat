import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'plurk_search_state.dart';

class PlurkSearchCubit extends Cubit<PlurkSearchState> {
  PlurkSearchCubit() : super(PlurkSearchInitial());

  request(String query, int offset) async {
    Plurdart.PlurkSearch plurkSearch = await Plurdart.plurkSearchSearch(Plurdart.Search(
      query: query,
      offset: offset,
    ));

    if (plurkSearch != null) {
      if (!plurkSearch.hasError()) {
        if (plurkSearch.error != null && plurkSearch.error.isEmpty == false) {
          //Has error?
          emit(PlurkSearchFailed(plurkSearch.error));
        } else {
          emit(PlurkSearchSuccess(plurkSearch));
        }
      } else {
        emit(PlurkSearchFailed(plurkSearch.errorText));
      }
    } else {
      emit(PlurkSearchFailed('Unknown error'));
    }
  }

  trackBack(String query, int offset) async {
    Plurdart.PlurkSearch plurkSearch = await Plurdart.plurkSearchSearch(Plurdart.Search(
      query: query,
      offset: offset,
    ));

    if (plurkSearch != null) {
      if (!plurkSearch.hasError()) {
        if (plurkSearch.error != null && plurkSearch.error.isEmpty == false) {
          //Has error?
          emit(PlurkSearchTrackBackFailed(plurkSearch.error));
        } else {
          emit(PlurkSearchTrackBackSuccess(plurkSearch));
        }
      } else {
        emit(PlurkSearchTrackBackFailed(plurkSearch.errorText));
      }
    } else {
      emit(PlurkSearchTrackBackFailed('Unknown error'));
    }
  }

}
