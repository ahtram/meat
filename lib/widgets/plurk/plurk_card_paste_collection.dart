import 'package:flutter/material.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:meat/widgets/plurk/plurk_paste_view.dart';

// enum PasteCollectionType {
//   All,
//   Pages,
// }

class PlurkCardPasteCollection extends StatefulWidget {
  const PlurkCardPasteCollection(this.plurkCardPasteLinks, this.type, this.onLinkOpen, {Key key}) : super(key: key);

  final List<PlurkCardPasteLink> plurkCardPasteLinks;
  final PlurkPasteViewType type;
  // final PasteCollectionType pasteCollectionType;
  final Function(Uri) onLinkOpen;

  @override
  _PlurkCardPasteCollectionState createState() =>
      _PlurkCardPasteCollectionState();
}

class _PlurkCardPasteCollectionState extends State<PlurkCardPasteCollection> {

  @override
  Widget build(BuildContext context) {
    List<Widget> columnChildren = [];

    //Just add all PlurkCardPasteLinks.
    for (int i = 0; i < widget.plurkCardPasteLinks.length; ++i) {
      columnChildren.add(PlurkPasteView(widget.plurkCardPasteLinks[i], widget.type, widget.onLinkOpen));
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: columnChildren,
    );
  }

}
