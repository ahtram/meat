import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'responses_response_add_state.dart';

class ResponsesResponseAddCubit extends Cubit<ResponsesResponseAddState> {
  ResponsesResponseAddCubit() : super(ResponsesResponseAddInitial());

  void request(int plurkId, Plurdart.ResponseQualifier qualifier, String content) async {
    Plurdart.Response response = await Plurdart.responsesResponseAdd(Plurdart.ResponsesResponseAdd(
      plurkId: plurkId,
      qualifier: qualifier,
      content: content,
    ));

    if (response != null) {
      emit(ResponsesResponseAddSuccess(response));
    } else {
      emit(ResponsesResponseAddFailed());
    }

  }
}
