part of 'add_account_cubit.dart';

@immutable
abstract class AddAccountState {}

class AddAccountInitial extends AddAccountState {}
class AddAccountRequest extends AddAccountState {}