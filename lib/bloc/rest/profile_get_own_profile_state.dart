part of 'profile_get_own_profile_cubit.dart';

@immutable
abstract class ProfileGetOwnProfileState {}

class ProfileGetOwnProfileInitial extends ProfileGetOwnProfileState {}

class ProfileGetOwnProfileSuccess extends ProfileGetOwnProfileState {
  ProfileGetOwnProfileSuccess(this.profile);
  final Plurdart.Profile profile;
}

class ProfileGetOwnProfileFailed extends ProfileGetOwnProfileState {
  ProfileGetOwnProfileFailed(this.error);
  final String error;
}
