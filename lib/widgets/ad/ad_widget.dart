// import 'package:flutter/material.dart';
// import 'package:flutter_icons/flutter_icons.dart';
// import 'package:flutter_native_admob/flutter_native_admob.dart';
// import 'package:flutter_native_admob/native_admob_controller.dart';
// import 'package:flutter_native_admob/native_admob_options.dart';
// import 'package:admob_flutter/admob_flutter.dart';
//
// class AdWidget extends StatefulWidget {
//   AdWidget({Key key, this.type = NativeAdmobType.full, this.onADLoadCompleted, @required this.adUnitID}) : super(key: key);
//
//   final NativeAdmobType type;
//   final Function onADLoadCompleted;
//   final String adUnitID;
//
//   @override
//   _AdWidgetState createState() => _AdWidgetState();
// }
//
// class _AdWidgetState extends State<AdWidget> {
//
//   NativeAdmobController _adMobController = NativeAdmobController();
//
//   // static const String FEEDS_NATIVE_AD_UNIT_ID = 'ca-app-pub-8147450582749445/3256624707';
//   // static const String FEEDS_BANNER_AD_UNIT_ID = 'ca-app-pub-8147450582749445/3633502376';
//
//   @override
//   void initState() {
//     super.initState();
//
//     // print('AdWidget: ad unit ID [' + widget.adUnitID + ']');
//
//     _adMobController.stateChanged.listen((state) {
//       if (state == AdLoadState.loadCompleted) {
//         widget.onADLoadCompleted?.call();
//       }
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return LayoutBuilder(
//       builder: (context, constraints) {
//
//         //Set height by NativeAdmobType.
//         double containerHeightRate = 1.0;
//         Widget adWidget;
//         if (widget.type == NativeAdmobType.full) {
//           //full
//           containerHeightRate = 1;
//           adWidget = NativeAdmob(
//               adUnitID: widget.adUnitID,
//               controller: _adMobController,
//               loading: Container(child: Center(
//                 child: CircularProgressIndicator(),
//               ),),
//               error: Center(child: Icon(
//                 MdiIcons.alert_circle,
//               )),
//               type: widget.type,
//               options: NativeAdmobOptions(
//                 ratingColor: Theme.of(context).primaryColor,
//                 adLabelTextStyle: NativeTextStyle(
//                   fontSize: 12,
//                   color: Colors.white,
//                   backgroundColor: Colors.amber,
//                 ),
//                 headlineTextStyle: NativeTextStyle(
//                   fontSize: 14,
//                   color: Theme.of(context).textTheme.bodyText1.color,
//                 ),
//                 advertiserTextStyle: NativeTextStyle(
//                   fontSize: 12,
//                   color: Theme.of(context).textTheme.bodyText1.color,
//                 ),
//                 bodyTextStyle: NativeTextStyle(
//                   fontSize: 12,
//                   color: Theme.of(context).textTheme.caption.color,
//                 ),
//                 storeTextStyle: NativeTextStyle(
//                   fontSize: 12,
//                   color: Theme.of(context).textTheme.bodyText1.color,
//                 ),
//                 priceTextStyle: NativeTextStyle(
//                   fontSize: 12,
//                   color: Theme.of(context).textTheme.bodyText1.color,
//                 ),
//                 callToActionStyle: NativeTextStyle(
//                   fontSize: 15,
//                   color: Theme.of(context).textTheme.bodyText1.color,
//                   backgroundColor: Theme.of(context).primaryColor.withOpacity(0.5),
//                   isVisible: false,
//                 ),
//               )
//           );
//         } else {
//           //banner
//           containerHeightRate = 0.15;
//           adWidget = AdmobBanner(
//               adUnitId: widget.adUnitID,
//               adSize: AdmobBannerSize.ADAPTIVE_BANNER(width: constraints.maxWidth.toInt()),
//               listener: (AdmobAdEvent event, Map<String, dynamic> args) {
//                 switch (event) {
//                   case AdmobAdEvent.loaded:
//                     // print('Admob banner loaded!');
//                     widget.onADLoadCompleted?.call();
//                     break;
//
//                   case AdmobAdEvent.opened:
//                     // print('Admob banner opened!');
//                     break;
//
//                   case AdmobAdEvent.closed:
//                     // print('Admob banner closed!');
//                     break;
//
//                   case AdmobAdEvent.failedToLoad:
//                     // print('Admob banner failed to load. Error code: ${args['errorCode']}');
//                     break;
//                   default:
//                     break;
//                 }
//               }
//           );
//         }
//
//         return Container(
//           height: constraints.maxWidth * containerHeightRate,
//           child: Center(child: adWidget),
//         );
//       }
//     );
//   }
// }