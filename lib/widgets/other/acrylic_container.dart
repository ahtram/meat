import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/ui/acrylic_background_switch_cubit.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

enum AcrylicContainerMode {
  ImageFiltered, //Faster but work on background image only.
  BackdropFilter, //Slower but work on anything in the back including background image.
  None, //No effect, just a container with background.
}

// A general themed Scaffold use across the project.
class AcrylicContainer extends StatefulWidget {
  AcrylicContainer(this.child,
      {this.initialImage,
      this.transparentBackLayer = false,
      this.width,
      this.height,
      this.backgroundColor,
      this.mode = AcrylicContainerMode.ImageFiltered,
      this.backgroundOpacity = 0.5,
      Key key})
      : super(key: key);

  final Widget child;

  //The initial image could be null and then provide by AcrylicBackgroundSwitchCubit.
  final ImageProvider initialImage;
  final transparentBackLayer;
  final double width;
  final double height;
  final Color backgroundColor;
  final AcrylicContainerMode mode;
  final double backgroundOpacity; // from 0-1.0

  @override
  AcrylicContainerState createState() => AcrylicContainerState();
}

class AcrylicContainerState extends State<AcrylicContainer> {
  // For blur image filter.
  final double _sigmaX = 20.0; // from 0-10
  final double _sigmaY = 20.0; // from 0-10

  BuildContext blocProviderContext;

  Uint8List _cubitSetImageFirst;
  Uint8List _cubitSetImageSecond;
  CrossFadeState _crossFadeState = CrossFadeState.showFirst;

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: BlocProvider<AcrylicBackgroundSwitchCubit>(
        create: (BuildContext context) => AcrylicBackgroundSwitchCubit(),
        child: MultiBlocListener(
          listeners: [
            BlocListener<AcrylicBackgroundSwitchCubit,
                AcrylicBackgroundSwitchState>(
              listener: (listenerContext, state) {
                if (state is AcrylicBackgroundSwitchActive) {
                  if (_crossFadeState == CrossFadeState.showFirst) {
                    //Switch to second.
                    setState(() {
                      if (_cubitSetImageSecond != state.image) {
                        _cubitSetImageSecond = state.image;
                      }
                      _crossFadeState = CrossFadeState.showSecond;
                    });
                  } else {
                    //Switch to first.
                    setState(() {
                      if (_cubitSetImageFirst != state.image) {
                        _cubitSetImageFirst = state.image;
                      }
                      _crossFadeState = CrossFadeState.showFirst;
                    });
                  }
                }
              },
            ),
          ],
          child: Builder(
            builder: (context) {
              blocProviderContext = context;
              //Decide background color.
              Color backgroundColor = widget.backgroundColor == null
                  ? Theme.of(context).canvasColor
                  : widget.backgroundColor;

              //Check which background image should we use.
              ImageProvider imageProvider1;
              if (_cubitSetImageFirst != null) {
                imageProvider1 = MemoryImage(_cubitSetImageFirst);
              } else if (widget.initialImage != null) {
                imageProvider1 = widget.initialImage;
              } else {
                imageProvider1 = MemoryImage(kTransparentImage);
              }

              ImageProvider imageProvider2;
              if (_cubitSetImageSecond != null) {
                imageProvider2 = MemoryImage(_cubitSetImageSecond);
              } else if (widget.initialImage != null) {
                imageProvider2 = widget.initialImage;
              } else {
                imageProvider2 = MemoryImage(kTransparentImage);
              }

              if (Static.settingsAcrylicEffect) {
                //1. Background.
                Widget background = Container();
                if (widget.mode == AcrylicContainerMode.ImageFiltered) {
                  background = Positioned.fill(
                    child: ImageFiltered(
                      imageFilter: ImageFilter.blur(
                          sigmaX: _sigmaX,
                          sigmaY: _sigmaY,
                          tileMode: TileMode.mirror),
                      child: AnimatedCrossFade(
                        duration: Duration(seconds: 1),
                        crossFadeState: _crossFadeState,
                        firstChild: Image(
                          fit: BoxFit.cover,
                          // placeholder: MemoryImage(kTransparentImage),
                          image: imageProvider1,
                        ),
                        secondChild: Image(
                          fit: BoxFit.cover,
                          // placeholder: MemoryImage(kTransparentImage),
                          image: imageProvider2,
                        ),
                        layoutBuilder: (Widget topChild, Key topChildKey,
                            Widget bottomChild, Key bottomChildKey) {
                          return Stack(
                            clipBehavior: Clip.hardEdge,
                            fit: StackFit.expand,
                            children: <Widget>[
                              Positioned.fill(
                                key: bottomChildKey,
                                child: bottomChild,
                              ),
                              Positioned.fill(
                                key: topChildKey,
                                child: topChild,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  );
                } else if (widget.mode == AcrylicContainerMode.BackdropFilter ||
                    widget.mode == AcrylicContainerMode.None) {
                  background = Positioned.fill(
                    child: AnimatedCrossFade(
                      duration: Duration(seconds: 2),
                      crossFadeState: _crossFadeState,
                      firstChild: Image(
                        fit: BoxFit.cover,
                        // placeholder: MemoryImage(kTransparentImage),
                        image: imageProvider1,
                      ),
                      secondChild: Image(
                        fit: BoxFit.cover,
                        // placeholder: MemoryImage(kTransparentImage),
                        image: imageProvider2,
                      ),
                    ),
                  );
                }

                //2. Child.
                Widget child = Container();
                if (widget.mode == AcrylicContainerMode.ImageFiltered ||
                    widget.mode == AcrylicContainerMode.None) {
                  child = Material(
                    color: Colors.transparent,
                    child: Container(
                      color: widget.transparentBackLayer
                          ? Colors.transparent
                          : backgroundColor.withOpacity(widget.backgroundOpacity),
                      child: widget.child,
                    ),
                  );
                } else {
                  child = BackdropFilter(
                    filter: ImageFilter.blur(
                        sigmaX: _sigmaX,
                        sigmaY: _sigmaY,
                        tileMode: TileMode.mirror),
                    child: Material(
                      color: Colors.transparent,
                      child: Container(
                        color: widget.transparentBackLayer
                            ? Colors.transparent
                            : backgroundColor.withOpacity(widget.backgroundOpacity),
                        child: widget.child,
                      ),
                    ),
                  );
                }

                //With acrylic
                return Container(
                  width: widget.width,
                  height: widget.height,
                  child: Stack(
                    children: [
                      //Background image.
                      background,
                      //Child.
                      child,
                    ],
                  ),
                );
              } else {
                //Non acrylic (Setting off)
                return Container(
                  color: backgroundColor,
                  child: widget.child,
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
