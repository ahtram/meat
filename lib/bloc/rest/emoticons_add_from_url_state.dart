part of 'emoticons_add_from_url_cubit.dart';

@immutable
abstract class EmoticonsAddFromUrlState {}

class EmoticonsAddFromUrlInitial extends EmoticonsAddFromUrlState {}

class EmoticonsAddFromUrlSuccess extends EmoticonsAddFromUrlState {
  EmoticonsAddFromUrlSuccess(this.keyword);
  final String keyword;
}

class EmoticonsAddFromUrlFailed extends EmoticonsAddFromUrlState {
  EmoticonsAddFromUrlFailed(this.errorText);
  final String errorText;
}
