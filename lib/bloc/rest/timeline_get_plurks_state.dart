part of 'timeline_get_plurks_cubit.dart';

@immutable
abstract class TimelineGetPlurksState {}

class TimelineGetPlurksInitial extends TimelineGetPlurksState {}

class TimelineGetPlurksSuccess extends TimelineGetPlurksState {
  TimelineGetPlurksSuccess(this.plurksList);
  final List<Plurdart.Plurks> plurksList;
}

class TimelineGetNewPlurksSuccess extends TimelineGetPlurksState {
  TimelineGetNewPlurksSuccess(this.plurks);
  final Plurdart.Plurks plurks;
}