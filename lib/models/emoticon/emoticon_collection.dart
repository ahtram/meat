import 'dart:convert';

import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:flutter/services.dart';

enum EmoticonCat {
  OwnedBasic,
  Dice,
  Custom,
}

//This will be a static data cache used by emoticon browser.
class EmoticonCollection {

  //These will be directly access by Emoticon browser.
  List<Plurdart.Emoticon> ownedBasic = [];
  List<Plurdart.Emoticon> dice = [];
  List<Plurdart.Emoticon> custom = [];

  //This will load dice set from the json resource file.
  loadDice() async {
    String jsonStr = await rootBundle.loadString('assets/emoticon/emoticon.json');
    dynamic json = jsonDecode(jsonStr);

    //The json should contains defined emoticon data.
    List<dynamic> diceOuterList = json['dice'];
    if (diceOuterList != null) {
      dice = [];
      diceOuterList.forEach((element) {
        List<dynamic> diceInnerList = element;
        if (diceInnerList.length > 1) {
          dice.add(Plurdart.Emoticon(diceInnerList[0], diceInnerList[1]));
        }
      });
    }
  }

  //Update the owned basic set. This should be called by cubit.
  updateOwnedBasic(List<Plurdart.Emoticon> list) {
    ownedBasic.clear();
    ownedBasic.addAll(list);

    //Special case and stupid hard-coded logic:
    //Remove dice emoticons from list.
    ownedBasic.removeWhere((ownedBasicEmoticon) {
      //Is this basicEmoticon exist in dice?
      Plurdart.Emoticon foundEmoticon = dice.firstWhere((diceEmoticon) {
        return diceEmoticon.textCode == ownedBasicEmoticon.textCode;
      },
      orElse: () {
        return null;
      });

      if (foundEmoticon != null) {
        return true;
      } else {
        return false;
      }
    });
  }

  //Update the custom set. This should be called by cubit.
  updateCustom(List<Plurdart.Emoticon> list) {
    custom.clear();
    custom.addAll(list);
  }

  List<Plurdart.Emoticon> getEmoticons(EmoticonCat cat) {
    switch(cat) {
      case EmoticonCat.OwnedBasic:
        return ownedBasic;
      case EmoticonCat.Dice:
        return dice;
      case EmoticonCat.Custom:
        return custom;
    }
    return null;
  }

  //Check if we already have a emoticon.
  bool hasCustomEmoticonByUrl(String url) {
    Plurdart.Emoticon foundEmoticon = custom.firstWhere((emoticon) => emoticon.previewUrl == url, orElse: () {
      return null;
    });

    return foundEmoticon != null;
  }

}