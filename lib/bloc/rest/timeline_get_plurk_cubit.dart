import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'timeline_get_plurk_state.dart';

class TimelineGetPlurkCubit extends Cubit<TimelineGetPlurkState> {
  TimelineGetPlurkCubit() : super(TimelineGetPlurkState(null));

  void request(Plurdart.TimelineGetPlurk timelineGetPlurk) async {
    Plurdart.PlurkWithUser plurkWithUser = await Plurdart.timelineGetPlurk(timelineGetPlurk);

    //Collect Users to global User Cache.
    if (plurkWithUser != null) {
      plurkWithUser.plurkUsers.forEach((key, value) {
        Static.addToUserCache(value);
      });
    }

    emit(TimelineGetPlurkState(plurkWithUser));
  }
}
