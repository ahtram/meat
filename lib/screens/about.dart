import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:package_info/package_info.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:url_launcher/url_launcher.dart';
import 'package:meat/screens/md_viewer.dart' as MDViewer;

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> with AfterLayoutMixin {
  BuildContext scaffoldBodyContext;

  String appName = '';
  String packageName = '';
  String version = '';
  String buildNumber = '';

  //Read the about content...
  String aboutMDContent = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //Read the version number...
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    if (!mounted) return;

    //Read the about content...
    String aboutMDString = await rootBundle.loadString('assets/about/about.md');

    if (!mounted) return;

    setState(() {
      appName = packageInfo.appName;
      packageName = packageInfo.packageName;
      version = packageInfo.version + ' - ' + packageInfo.buildNumber +
          ((kDebugMode) ? ' - Debug' : '') +
          ((kProfileMode) ? ' - Profile' : '') +
          ((kReleaseMode) ? ' - Release' : '');
      buildNumber = packageInfo.buildNumber;

      aboutMDContent = aboutMDString;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        flexibleSpace: AcrylicContainer(
          Container(),
          backgroundColor: Theme.of(context).primaryColor,
          transparentBackLayer: false,
          mode: AcrylicContainerMode.BackdropFilter,
        ),
        elevation: 0,
        leading: AppBarActionButton(Icons.arrow_back, onTap: () {
          Navigator.pop(context);
        }),
        title: Text(
          FlutterI18n.translate(context, 'menuAbout'),
          style: TextStyle(
            color: Theme.of(context).textTheme.bodyText1.color,
          ),
        ),
      ),
      body: AcrylicContainer(
        ListView(
          controller: PrimaryScrollController.of(context),
          padding: EdgeInsets.all(16),
          children: [
            SizedBox(
              height: 16,
            ),

            Container(
              child: Center(
                child: Container(
                  width: 240,
                  height: 240,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: GestureDetector(
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage:
                          AssetImage('assets/images/BKTTokenLogo.png'),
                      // child: Image(
                      //   fit: BoxFit.cover,
                      //   image: AssetImage('assets/images/BKTTokenLogo.png'),
                      // ),
                    ),
                    onTap: () async {
                      //Launch the Telegram channel link.
                      if (await canLaunch(Define.bktOfficialTelegramLink)) {
                        launch(Define.bktOfficialTelegramLink);
                      }
                    },
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 16,
            ),

            Text(
              (appName == null) ? ('null') : (appName),
              style: Theme.of(context).textTheme.headline5,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              version,
              textAlign: TextAlign.center,
            ),

            SizedBox(
              height: 16,
            ),

            OutlinedButton.icon(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                ),
                onPressed: () async {
                  if (Platform.isIOS) {
                    if (await canLaunch(Define.bktAppleAppStoreLink)) {
                      launch(Define.bktAppleAppStoreLink);
                    }
                  } else if (Platform.isAndroid) {
                    if (await canLaunch(Define.bktGooglePlayStoreLink)) {
                      launch(Define.bktGooglePlayStoreLink);
                    }
                  } else {
                    if (await canLaunch(Define.bktOfficialPlurkLink)) {
                      launch(Define.bktOfficialPlurkLink);
                    }
                  }
                },
                icon: Icon(
                  MdiIcons.star,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ),
                label: Text(
                  'Rate Us!',
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1,
                )),

            OutlinedButton.icon(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                ),
                onPressed: () async {
                  BlocProvider.of<OpenUriCubit>(context)
                      .request(Uri.tryParse(Define.bktOfficialPlurkLink));
                  // if (await canLaunch(Define.bktOfficialPlurkLink)) {
                  //   launch(Define.bktOfficialPlurkLink);
                  // }
                },
                icon: Icon(
                  MdiIcons.at,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ),
                label: Text(
                  'BahKutTeh Plurk Page',
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1,
                )),

            OutlinedButton.icon(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                ),
                onPressed: () async {
                  //Term of service.
                  String tosMDString = await rootBundle
                      .loadString('assets/about/plurk_term_of_service.md');
                  MDViewer.show(context, 'Plurk Term Of Service', tosMDString);
                },
                icon: Icon(
                  MdiIcons.scriptText,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ),
                label: Text(
                  'Term Of Service',
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1,
                )),

            OutlinedButton.icon(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                ),
                onPressed: () async {
                  //Open privacy policy.
                  String ppMDString = await rootBundle
                      .loadString('assets/about/plurk_privacy_policy.md');
                  MDViewer.show(context, 'Plurk Privacy Policy', ppMDString);
                },
                icon: Icon(
                  MdiIcons.noteText,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ),
                label: Text(
                  'Privacy Policy',
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1,
                )),

            //Test AdWidget
            // Divider(),
            // AdWidget(type: NativeAdmobType.full,),
            // Divider(),
            // AdWidget(type: NativeAdmobType.banner,),
            // Divider(),

            // Container(
            //   padding: EdgeInsets.all(16),
            //   child: MarkdownBody(
            //     data: aboutMDContent,
            //   ),
            // ),

            //Bottom margin.
            SizedBox(
              height: MediaQuery.of(context).viewInsets.bottom,
            ),
          ],
        ),
        backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
        initialImage: AssetImage('assets/images/background001.jpg'),
      ),
    );
  }
}
