part of 'responses_response_edit_cubit.dart';

@immutable
abstract class ResponsesResponseEditState {}

class ResponsesResponseEditInitial extends ResponsesResponseEditState {}

class ResponsesResponseEditSuccess extends ResponsesResponseEditState {
  ResponsesResponseEditSuccess(this.responseEditResult);
  final Plurdart.ResponseEditResult responseEditResult;
}

class ResponsesResponseEditFailed extends ResponsesResponseEditState {
  ResponsesResponseEditFailed(this.errorText);
  final String errorText;
}