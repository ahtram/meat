import 'package:after_layout/after_layout.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/widgets/bookmark/tags_trunk_item.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/screens/busy.dart' as Busy;

class Bookmarks extends StatefulWidget {
  Bookmarks({Key key}) : super(key: key);

  @override
  _BookmarksState createState() => _BookmarksState();
}

class _BookmarksState extends State<Bookmarks> with AfterLayoutMixin {

  //Are we in editing mode to edit all tags?
  bool _isInEditingMode = false;

  TextEditingController _tagEditorController;
  FocusNode _tagEditorFocusNode;
  String _newTagName = '';
  List<String> _tags = [];

  bool _isRequesting = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //Refresh tags data
    if (mounted) {
      setState(() {
        //Use the cache directly
        _tags = Static.getTags();
        // print('[all tags cache]: [' + _tags.join(', ') + ']');
      });
    }

    await Static.updateTagsCache();

    if (mounted) {
      setState(() {
        //Update the cache for good
        _tags = Static.getTags();
        // print('[all tags cache]: [' + _tags.join(', ') + ']');
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //Add all tags item.
    List<Widget> listChildren = [];

    //The all bookmarks item
    listChildren.add(TagsTrunkItem(
      'All Bookmarks',
      selected: false,
      onTap: (tag) {
        //Open bookmark viewer...
        _toggleEditModeOff();
        //Open with a null string tag means browse all bookmarks.
        Navigator.pushNamed(context, '/home/bookmarks_browser', arguments: null);
      },
    ));

    //Other user item.
    if (_tags != null) {
      for (int i = 0 ; i < _tags.length ; ++i) {
        listChildren.add(TagsTrunkItem(
          _tags[i],
          selected: false,
          showDeleteButton: _isInEditingMode,
          onTap: (tag) {
            //Open bookmark viewer...
            _toggleEditModeOff();
            //Open with a null string tag means browse all bookmarks.
            Navigator.pushNamed(context, '/home/bookmarks_browser', arguments: tag);
          },
          onDeleteTap: (tag) {
            _tryRemoveTag(tag);
          },
        ));
      }
    }

    List<Widget> columnChildren = [];
    columnChildren.add(Expanded(
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: 8),
        children: listChildren,
      ),
    ));

    if (_isInEditingMode) {
      columnChildren.add(ElevatedButton.icon(
        icon: Icon(MdiIcons.tagPlus),
        label: Text('New'),
        onPressed: () async {
          _showTagEditor();
          //Show text editing dialog.
        },
      ));
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        brightness: Theme.of(context).brightness,
        backgroundColor: Colors.transparent,
        flexibleSpace: AcrylicContainer(
          Container(),
          backgroundColor: Theme.of(context).primaryColor,
          transparentBackLayer: false,
          mode: AcrylicContainerMode.BackdropFilter,
        ),
        elevation: 0,
        leading: AppBarActionButton(Icons.arrow_back, onTap: () {
          Navigator.pop(context);
        }),
        title: Text(
          FlutterI18n.translate(context, 'menuBookmarks'),
          style: TextStyle(
            color: Theme.of(context).textTheme.bodyText1.color,
          ),
        ),
        actions: [
          AppBarActionButton((_isInEditingMode) ? (Icons.check) : (Icons.edit_outlined), onTap: () {
            _toggleEditMode();
          }),
          SizedBox(
            width: 4,
          )
        ],
      ),
      body: AcrylicContainer(
        SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: columnChildren,
            ),
          ),
        ),
        backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
        initialImage: AssetImage('assets/images/background001.jpg'),
      ),
    );
  }

  _toggleEditMode() {
    setState(() {
      _isInEditingMode = !_isInEditingMode;
    });
  }

  _toggleEditModeOff() {
    setState(() {
      _isInEditingMode = false;
    });
  }

  _showTagEditor() {
    _newTagName = '';
    AwesomeDialog(
      context: context,
      dialogType: DialogType.NO_HEADER,
      animType: AnimType.SCALE,
      headerAnimationLoop: false,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 8,
            ),
            Text(
              FlutterI18n.translate(context, 'labelAddTagDialogTitle'),
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              width: 200,
              child: TextField(
                autofocus: true,
                controller: _tagEditorController,
                focusNode: _tagEditorFocusNode,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(6),
                  border: OutlineInputBorder(),
                  hintText: 'Tag Name',
                  hintStyle: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 14,
                  ),
                  isDense: true,
                ),
                textAlign: TextAlign.center,
                keyboardType: TextInputType.name,
                keyboardAppearance: Theme.of(context).brightness,
                onChanged: _onTagEditorChanges,
              ),
            ),
          ],
        ),
      ),
      btnOkText: FlutterI18n.translate(context, 'dialogAdd'),
      btnCancelText: FlutterI18n.translate(context, 'dialogCancel'),
      dismissOnTouchOutside: false,
      btnOkOnPress: () async {
        _tryAddNewTag(_newTagName);
      },
      btnCancelOnPress: () { },
    )..show();
  }

  _onTagEditorChanges(String newTagName) {
    _newTagName = newTagName;
  }

  Future<bool> _tryAddNewTag(String newTag) async {
    if (!_tags.contains(newTag) && newTag.isNotEmpty && !_isRequesting) {
      _isRequesting = true;
      Busy.show(context);
      Plurdart.Status status = await Plurdart.bookmarksCreateTag(Plurdart.ATag(tag: newTag));
      Navigator.pop(context);
      _isRequesting = false;
      if (!status.hasError()) {
        //Success.
        setState(() {
          _tags.add(newTag);
        });
        return true;
      } else {
        //Failed?
        AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.SCALE,
            headerAnimationLoop: false,
            title: FlutterI18n.translate(context, 'dialogError'),
            desc: status.errorText,
            btnOkText: FlutterI18n.translate(context, 'dialogConfirm'),
            btnOkOnPress: () {}
        )..show();
        return false;
      }
    }
    return false;
  }

  _tryRemoveTag(String tagName) async {
    if (_tags.contains(tagName) && tagName.isNotEmpty && !_isRequesting) {
      _isRequesting = true;
      Plurdart.Status status = await Plurdart.bookmarksRemoveTag(Plurdart.ATag(tag: tagName));
      _isRequesting = false;
      if (!status.hasError()) {
        //Success.
        setState(() {
          _tags.remove(tagName);
        });
        return true;
      } else {
        //Failed?
        AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.SCALE,
            headerAnimationLoop: false,
            title: FlutterI18n.translate(context, 'dialogError'),
            desc: status.errorText,
            btnOkText: FlutterI18n.translate(context, 'dialogConfirm'),
            btnOkOnPress: () {}
        )..show();
        return false;
      }
    }
    return false;
  }

}
