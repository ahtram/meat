import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'refresh_unread_count_state.dart';

class RefreshUnreadCountCubit extends Cubit<RefreshUnreadCountState> {
  RefreshUnreadCountCubit() : super(RefreshUnreadCountInitial());

  request() async {
    emit(RefreshUnreadCountRequest());
  }
}
