
import 'dart:io';
import 'dart:ui';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:meat/models/notification/notification_payload.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

//[Dep]: Replaced by AccountKeep/AccountStore.
// const String accessTokenStorageKey = 'accessToken';
//[Dep]: Replaced by AccountKeep/AccountStore.
// const String tokenSecretStorageKey = 'tokenSecret';
//[Dep]: Replaced by AccountKeep/AccountStore.
const String meIdPreferencesKey = 'meId';

const String accountStoreStorageKey = 'accountStore';

const String appThemePreferencesKey = 'appTheme';
const String themeModePreferencesKey = 'themeMode';
const String canvasColorPreferencesKey = 'canvasColor';
const String textScalePreferencesKey = 'textScale';
const String acrylicEffectPreferencesKey = 'acrylicEffect';
const String downloadFullImageInPlurkCardPreferencesKey = 'downloadFullImageInPlurkCard';
const String tapToRevealAdultsOnlyContentPreferencesKey = 'tapToRevealAdultsOnlyContent';
const String removeLinksInPlurkContentPreferencesKey = 'removeLinksInPlurkContent';
const String displayMuteButtonAtBottomRightPreferencesKey = 'displayMuteButtonAtBottomRight';
const String displayVTWatchNotifyTextPreferencesKey = 'displayVTWatchNotifyText';
const String apiLanguagePreferencesKey = 'apiLanguage';
const String systemLocaleNamePreferenceKey = 'systemLocalName';

const String plurksMainFilterPreferencesKey = 'plurksMainFilter';
const String plurkQualifierPreferencesKey = 'plurkQualifier';
const String responseQualifierPreferencesKey = 'responseQualifier';
const String emoticonTabIndexPreferencesKey = 'emoticonTabIndex';

const String newPlurkDraftPreferencesKey = 'newPlurkDraft';
const String isViewingUnreadOnlyPreferencesKey = 'isViewingUnreadOnly';

const String bktHashTagScheme = 'bktHashTag';
const String bktScheme = 'bahkutteh';

const String youtubeDesktopAuthority = 'www.youtube.com';
const String youtubeMobileAuthority = 'm.youtube.com';
const String youtubeShortenAuthority = 'youtu.be';

const String twitterMediaAuthority = 'pbs.twimg.com';
const String twitterMediaImagePathSegment = 'media';
//Todo: we'll need a new player widget for this thing...
const String twitterMediaVideoPathSegment = 'tweet_video';

const String plurkImageServiceAuthority = 'images.plurk.com';
const String plurkEmoticonServiceAuthority = 'emos.plurk.com';
const String plurkPasteAuthority = 'paste.plurk.com';

const String plurkAuthority = 'www.plurk.com';
const String plurkSite = 'https://www.plurk.com';
const String plurkPostPath = plurkSite + '/p/';
const String plurkUserPath = plurkSite + '/u/';
const String plurkMobileUserPath = plurkSite + '/m/';

const String bktOfficialTelegramLink = 'https://t.me/bahkutteh';
const String bktOfficialPlurkLink = 'https://www.plurk.com/devel1337';
const String bktAppleAppStoreLink = 'https://apps.apple.com/app/bahkutteh/id1538166300';
const String bktGooglePlayStoreLink = 'https://play.google.com/store/apps/details?id=uni.team.appBKT';

const double horizontalPadding = 16.0;
const double verticalPadding = 16.0;

const int PLURK_MAX_CHARACTER_COUNT = 360;
const int PLURK_IMAGE_LINK_TREATED_CHARACTER_COUNT = 31;
const int PLURK_IMAGE_LINK_ACTUAL_CHARACTER_COUNT = 51;

const int PLURK_FEED_COUNT_PER_AD = 5;
const int PLURK_FEED_COUNT_PER_AD_MAX = 15;
const int PLURK_FEED_COUNT_PER_AD_INC = 5;

const int HOTLINK_COUNT_PER_AD = 10;
const int HOTLINK_COUNT_PER_AD_MAX = 15;
const int HOTLINK_COUNT_PER_AD_INC = 5;

const int COMMENT_COUNT_PER_AD = 15;
const int COMMENT_COUNT_PER_AD_MAX = 40;
const int COMMENT_COUNT_PER_AD_INC= 5;

//I don't know if Plurk has define their own api languages.
const List<String> API_LANGUAGES = [
  'zh',
  'en'
];

//For feed native ads
String feedADUnitID() {
  // return 'ca-app-pub-3940256099942544/2247696110';
  if (Platform.isIOS) {
    return 'ca-app-pub-8147450582749445/1324223179';
  } else {
    return 'ca-app-pub-8147450582749445/3256624707';
  }
}

//For comment banner ads
String commentADUnitID() {
  //Test Ad by Google.
  // return 'ca-app-pub-3940256099942544/6300978111';
  if (Platform.isIOS) {
    return 'ca-app-pub-8147450582749445/8404462142';
  } else {
    return 'ca-app-pub-8147450582749445/3633502376';
  }
}

// These url end will be treat as a image link in PlurkCard.
// https://api.flutter.dev/flutter/widgets/Image-class.html
const List<String> imageUrlExtensions = [
  '.png',
  '.gif',
  '.jpg',
  '.jpeg',
  '.bmp',
  '.jpe',
  '.jif',
  '.jfif',
  '.jfi',
//  '.webp',  //This has issue for cachedNetworkImage
  '.dib',
];

//Check if a url is end with any image extensions.
bool urlEndWithSupportImageExtensions(String url) {
  if (url != null) {
    String lowerCaseUrl = url.toLowerCase();
    for (int i = 0 ; i < imageUrlExtensions.length ; ++i) {
      if (lowerCaseUrl.endsWith(imageUrlExtensions[i])) {
        return true;
      }
    }
  }
  return false;
}

//A convenient function that get Plurk Qualifier color.
Color getPlurkQualifierColor(Plurdart.PlurkQualifier qualifier) {
  if (qualifier == Plurdart.PlurkQualifier.None) {
    return Color(Plurdart.qualifierColorMap[':']);
  } else {
    String qualifierStr = EnumToString.convertToString(qualifier).toLowerCase();
    return Color(Plurdart.qualifierColorMap[qualifierStr]);
  }
}

//A convenient function that get Response Qualifier color.
Color getResponseQualifierColor(Plurdart.ResponseQualifier qualifier) {
  if (qualifier == Plurdart.ResponseQualifier.None) {
    return Color(Plurdart.qualifierColorMap[':']);
  } else {
    String qualifierStr = EnumToString.convertToString(qualifier).toLowerCase();
    return Color(Plurdart.qualifierColorMap[qualifierStr]);
  }
}

//A very mysterious way to transform an int Id to String Id...
String getPlurkPostLink(int plurkId) {
  //Tricky: they use base 36 as post url path
  return plurkPostPath + plurkId.toRadixString(36).toLowerCase();
}

//A convenient function for getting the proper i18n key for alert type and numOthers.
String getI8nKeyForAlert(Plurdart.Alert alert) {
  switch (alert.alertType()) {
    case Plurdart.AlertType.FriendshipRequest:
      return 'notificationFriendshipRequest';
    case Plurdart.AlertType.FriendshipPending:
      return 'notificationFriendshipPending';
    case Plurdart.AlertType.NewFan:
      return 'notificationNewFan';
    case Plurdart.AlertType.FriendshipAccepted:
      return 'notificationFriendshipAccepted';
    case Plurdart.AlertType.NewFriend:
      return 'notificationNewFriend';
    case Plurdart.AlertType.PrivatePlurk:
      return 'notificationPrivatePlurk';
    case Plurdart.AlertType.PlurkLiked:
      if (alert.numOthers != null && alert.numOthers > 0) {
        return 'notificationPlurkLikedNumOthers';
      }
      return 'notificationPlurkLiked';
    case Plurdart.AlertType.PlurkReplurked:
      if (alert.numOthers != null && alert.numOthers > 0) {
        return 'notificationPlurkReplurkedNumOthers';
      }
      return 'notificationPlurkReplurked';
    case Plurdart.AlertType.Mentioned:
      if (alert.numOthers != null && alert.numOthers > 0) {
        return 'notificationMentionedNumOthers';
      }
      return 'notificationMentioned';
    case Plurdart.AlertType.MyResponded:
      if (alert.numOthers != null && alert.numOthers > 0) {
        return 'notificationMyRespondedNumOthers';
      }
      return 'notificationMyResponded';
    default:
      return '';
  }
}

bool shouldPlaySoundForAlert(Plurdart.Alert alert) {
  switch (alert.alertType()) {
    case Plurdart.AlertType.FriendshipRequest:
      return true;
    case Plurdart.AlertType.FriendshipPending:
      return false;
    case Plurdart.AlertType.NewFan:
      return false;
    case Plurdart.AlertType.FriendshipAccepted:
      return true;
    case Plurdart.AlertType.NewFriend:
      return false;
    case Plurdart.AlertType.PrivatePlurk:
      return true;
    case Plurdart.AlertType.PlurkLiked:
      return false;
    case Plurdart.AlertType.PlurkReplurked:
      return false;
    case Plurdart.AlertType.Mentioned:
      return true;
    case Plurdart.AlertType.MyResponded:
      return false;
    default:
      return false;
  }
}

NotificationPayload generateDeepLinkPayloadForAlert(Plurdart.Alert alert) {
  switch (alert.alertType()) {
    case Plurdart.AlertType.FriendshipRequest:
    case Plurdart.AlertType.FriendshipPending:
    case Plurdart.AlertType.NewFan:
    case Plurdart.AlertType.FriendshipAccepted:
    case Plurdart.AlertType.NewFriend:
      return NotificationPayload(
          alert.alertType(),
          queryParameters: {
            'userNickName': alert.getUser().nickName
          }
      );
    case Plurdart.AlertType.PrivatePlurk:
    case Plurdart.AlertType.PlurkLiked:
    case Plurdart.AlertType.PlurkReplurked:
      return NotificationPayload(
          alert.alertType(),
          queryParameters: {
            'userNickName': alert.getUser().nickName,
            'plurkId': alert.plurkId.toString()
          }
      );
    case Plurdart.AlertType.Mentioned:
    case Plurdart.AlertType.MyResponded:
      return NotificationPayload(
          alert.alertType(),
          queryParameters: {
            'userNickName': alert.getUser().nickName,
            'plurkId': alert.plurkId.toString(),
            'responseId': alert.responseId.toString(),
          }
      );
    default:
      return NotificationPayload(
        Plurdart.AlertType.Unknown
      );
  }
}

bool shouldEnableVibrationForAlert(Plurdart.Alert alert) {
  switch (alert.alertType()) {
    case Plurdart.AlertType.FriendshipRequest:
      return true;
    case Plurdart.AlertType.FriendshipPending:
      return false;
    case Plurdart.AlertType.NewFan:
      return true;
    case Plurdart.AlertType.FriendshipAccepted:
      return true;
    case Plurdart.AlertType.NewFriend:
      return false;
    case Plurdart.AlertType.PrivatePlurk:
      return true;
    case Plurdart.AlertType.PlurkLiked:
      return true;
    case Plurdart.AlertType.PlurkReplurked:
      return true;
    case Plurdart.AlertType.Mentioned:
      return true;
    case Plurdart.AlertType.MyResponded:
      return true;
    default:
      return false;
  }
}

String localizedStrReplaceAlertParams(Plurdart.Alert alert, String localizedString) {
  if (localizedString != null && alert != null) {
    Plurdart.User user = alert.getUser();
    if (user != null) {
      localizedString = localizedString.replaceAll('%a', user.displayName);
    }

    if (alert.numOthers != null) {
      localizedString = localizedString.replaceAll('%b', alert.numOthers.toString());
    }
    return localizedString;
  }
  return '';
}

String localizedStrReplaceParams(String localizedString, {String paramA, String paramB}) {
  if (paramA != null) {
    localizedString = localizedString.replaceAll('%a', paramA);
  }
  if (paramB != null) {
    localizedString = localizedString.replaceAll('%b', paramB);
  }
  return localizedString;
}
