part of 'report_abuse_dialog_cubit.dart';

@immutable
abstract class ReportAbuseDialogState {}

class ReportAbuseDialogInitial extends ReportAbuseDialogState {}

class ReportAbuseDialogOpenPlurk extends ReportAbuseDialogState {
  ReportAbuseDialogOpenPlurk(this.plurkId);
  final plurkId;
}

class ReportAbuseDialogOpenResponse extends ReportAbuseDialogState {
  ReportAbuseDialogOpenResponse(this.responseId);
  final responseId;
}