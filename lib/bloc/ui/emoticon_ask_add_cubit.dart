import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'emoticon_ask_add_state.dart';

class EmoticonAskAddCubit extends Cubit<EmoticonAskAddState> {
  EmoticonAskAddCubit() : super(EmoticonAskAddInitial());

  show(String url) async {
    emit(EmoticonAskAddShow(url));
  }
}
