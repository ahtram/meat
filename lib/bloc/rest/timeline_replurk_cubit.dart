import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_replurk_state.dart';

//[Dep]: We don't need this anymore.
class TimelineReplurkCubit extends Cubit<TimelineReplurkState> {
  TimelineReplurkCubit() : super(TimelineReplurkInitial());

  void replurk(int plurkID) async {
    emit(TimelineReplurkPressed());
    Plurdart.RePlurk responseRePlurk = await Plurdart.timelineReplurk(Plurdart.PlurkIDs(ids: [plurkID]));
    if (responseRePlurk != null) {
      if(responseRePlurk.success) {
        emit(TimelineReplurkSuccess());
      } else {
        emit(TimelineReplurkFailed());
      }
    } else {
      emit(TimelineReplurkFailed());
    }
  }

  void unReplurk(int plurkID) async {
    emit(TimelineUnReplurkPressed());
    Plurdart.RePlurk responseRePlurk = await Plurdart.timelineUnreplurk(Plurdart.PlurkIDs(ids: [plurkID]));
    if (responseRePlurk != null) {
      if(responseRePlurk.success) {
        emit(TimelineUnReplurkSuccess());
      } else {
        emit(TimelineUnReplurkFailed());
      }
    } else {
      emit(TimelineUnReplurkFailed());
    }
  }
}
