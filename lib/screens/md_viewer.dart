import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';

void show(BuildContext context, String title, String mdContent) {
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _MDViewer(title, mdContent);
  }));
}

class _MDViewer extends StatelessWidget {
  _MDViewer(this._title, this._mdContent);

  final String _title;
  final String _mdContent;

  @override
  Widget build(BuildContext context) {
    return AcrylicScaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
          elevation: 0,
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          actions: [
            AppBarActionButton(Icons.close, onTap: () {
              Navigator.pop(context);
            }),
            SizedBox(
              width: 2,
            )
          ],
          title: Center(
            child: Text(
              _title,
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
            ),
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            color: Theme.of(context).canvasColor.withOpacity(0.4),
            padding: EdgeInsets.fromLTRB(16, 8, 16, kMinInteractiveDimension + 8),
            child: MarkdownBody(
              data: _mdContent,
            ),
          ),
        ));
  }
}
