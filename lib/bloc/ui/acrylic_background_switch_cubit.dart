import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'acrylic_background_switch_state.dart';

class AcrylicBackgroundSwitchCubit extends Cubit<AcrylicBackgroundSwitchState> {
  AcrylicBackgroundSwitchCubit() : super(AcrylicBackgroundSwitchInitial());

  void active(Uint8List image) {
    emit(AcrylicBackgroundSwitchActive(image));
  }
}
