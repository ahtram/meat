
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

class PlurkCardTextContent extends StatefulWidget {
  PlurkCardTextContent(this.textContent);

  final String textContent;

  @override
  PlurkCardTextContentState createState() => PlurkCardTextContentState();
}

class PlurkCardTextContentState extends State<PlurkCardTextContent> {
  PlurkCardTextContentState();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    context.dependOnInheritedWidgetOfExactType();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          child: HtmlWidget(
            widget.textContent,
            enableCaching: false,
            buildAsync: false,
            textStyle: Theme.of(context).textTheme.bodyText1,

            //[Dep]: A tag use theme accent color by default now.
            // hyperlinkColor: Theme.of(context).highContrastPrimaryColor,

            //Damn! This is too hard!
            //We've managed to alter the img element to a link.
//        customWidgetBuilder: (element) {
//
//          //Custom emoticon widget to get proper gesture detector.
//          if (element.localName == 'img') {
//            //Is the class name right?
//            if (element.className == 'emoticon' || element.className == 'emoticon_my') {
//              String sourceUrl = element.attributes['src'];
//              if (sourceUrl != null) {
////                print(element.outerHtml);
//
//              //Damn! This is too hard!
////                return ImageLayout(
////                  CachedNetworkImageProvider(sourceUrl,),
////                );
////                GestureDetector(
////                  child: ImageLayout(
////                    CachedNetworkImageProvider(sourceUrl,),
////                    text: element.attributes['alt'],
////                  ),
////                  onTap: () {
////                    print('on emoticon tap: [' + sourceUrl + ']');
////                  },
////                );
//                //Ok. We can give this emoticon a custom container...
////                return Image(
////                  image: CachedNetworkImageProvider(sourceUrl),
////                );
//              }
//            }
//          }
//
//          return null;
//        },
            onTapUrl: (url) {
              //!!!!!!!!!! Warning!!! This thing is very tricky! It only store the first closure !!!!!!!!!!
              //!!!!!!!!!! You can consider this thing static! We'll try make a static StreamController for this!!!!!!!!
              //https://stackoverflow.com/questions/15717681/dart-how-to-create-listen-and-emits-custom-event

              // print('Static onTapUrl: ' + url);
              Static.htmlWidgetUrlTapController.add(url);
              return true;
            },
          ),
        )
      ],
    );
  }

}