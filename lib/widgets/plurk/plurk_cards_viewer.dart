import 'package:after_layout/after_layout.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meat/bloc/rest/bookmarks_get_bookmarks_cubit.dart';
import 'package:meat/bloc/rest/official_news_cubit.dart';
import 'package:meat/bloc/rest/plurk_search_cubit.dart';
import 'package:meat/bloc/rest/stats_get_anonymous_plurks_cubit.dart';
import 'package:meat/bloc/rest/stats_top_favorites_cubit.dart';
import 'package:meat/bloc/rest/stats_top_replurks_cubit.dart';
import 'package:meat/bloc/rest/stats_top_responded_cubit.dart';
import 'package:meat/bloc/rest/timeline_get_plurks_cubit.dart';
import 'package:meat/bloc/rest/timeline_mark_as_read_cubit.dart';
import 'package:meat/bloc/rest/timeline_plurk_delete_cubit.dart';
import 'package:meat/bloc/rest/timeline_track_back_cubit.dart';
import 'package:meat/bloc/ui/home_refresh_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/viewer_background_switch_cubit.dart';
import 'package:meat/models/plurk/plurks_collection.dart';
// import 'package:meat/widgets/ad/feed_ad_widget.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/empty_logo.dart';
import 'package:meat/widgets/plurk/plurk_card.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/screens/plurk_poster.dart' as PlurkPoster;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/theme/app_themes.dart';

//A customizable user's list viewer for select view and select
//Friends/Fans/Following

class PlurkCardsViewerArgs {
  PlurkCardsViewerArgs(
    this.browsingType,
    this.openViewerById, {
    this.hasAppBar = true,
    this.leading,
    this.title,
    this.actions,
    this.searchTerm,
    this.bookmarkTag,
    this.onLeaveTop,
    this.onReturnTop,
  });

  final BrowsingType browsingType;
  //Should we open the viewer by cache data or by Id?
  //For Top and Anonymous Plurks we'll want this be true.
  final bool openViewerById;

  final bool hasAppBar;
  final Widget leading;
  final Widget title;
  final List<Widget> actions;

  //For search mode
  String searchTerm;

  //For bookmarks mode
  String bookmarkTag;

  final Function onLeaveTop;
  final Function onReturnTop;
}

//Mode
enum BrowsingType {
  Home, //timelineGetPlurks
  Anonymous, //statsGetAnonymousPlurks
  TopReplurks, //statsTopReplurks
  TopFavorites, //statsTopFavorites
  TopResponses, //statsTopResponded
  Search, //PlurkSearchSearch
  OfficialNews, //plurkTopFetchOfficialPlurks
  Bookmarks, //Browsing bookmarks with a bookmark tag
}

class PlurkCardsViewer extends StatefulWidget {
  PlurkCardsViewer(this.args, {Key key}) : super(key: key);

  final PlurkCardsViewerArgs args;

  @override
  PlurkCardsViewerState createState() => PlurkCardsViewerState();
}

class PlurkCardsViewerState extends State<PlurkCardsViewer>
    with AfterLayoutMixin {
  BuildContext _blocProviderContext;

  //This thing is getting me trouble.
  // RefreshController _refreshController = RefreshController();

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();

  // ScrollController _scrollController = ScrollController();

  //We'll need a fancy class to store Plurks.
  PlurksCollection _plurksCollection = PlurksCollection();
  bool _timelineUpdateError = false;
  bool _isGettingNewPlurk = false;
  bool _isRefreshingFeed = false;
  bool _isLoadingFeed = false;
  //Is case we need this...
  bool _noHistoryRemained = false;

  double _lastScrollPosition = 0.0;

  // bool _emptyLogoVisible = false;

  @override
  void initState() {
    super.initState();

    //[Dep]
    //Init _plurksCollection with isViewingUnreadOnly pref if using Home mode.
    // if (widget.args.browsingType == BrowsingType.Home) {
    //   _plurksCollection.setup(Static.isViewingUnreadOnly());
    // } else {
    //   _plurksCollection.setup(false);
    // }
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //This could prevent from assert error or smartRefresher.
    // if (_refreshController.position != null) {
    //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    //     // print('[Refresh for the first time]');
    //     _refreshController.requestRefresh();
    //   });
    // }

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      // print('[Refresh for the first time]');
      _refreshIndicatorKey.currentState.show();
    });
  }

  @override
  void didUpdateWidget(PlurkCardsViewer oldWidget) {
    //This is for prevent SmartRefresh from getting same _refreshController error after some rebuild situations.
    //Just try get a new one!
    // _refreshController = RefreshController();
    // if (_refreshController.position != null) {
    //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    //     // print('[Refresh for the first time]');
    //     _refreshController.requestRefresh();
    //   });
    // }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<TimelineTrackBackCubit>(
          create: (BuildContext context) => TimelineTrackBackCubit(),
        ),
        BlocProvider<StatsGetAnonymousPlurksCubit>(
          create: (BuildContext context) => StatsGetAnonymousPlurksCubit(),
        ),
        BlocProvider<StatsTopFavoritesCubit>(
          create: (BuildContext context) => StatsTopFavoritesCubit(),
        ),
        BlocProvider<StatsTopReplurksCubit>(
          create: (BuildContext context) => StatsTopReplurksCubit(),
        ),
        BlocProvider<StatsTopRespondedCubit>(
          create: (BuildContext context) => StatsTopRespondedCubit(),
        ),
        BlocProvider<PlurkSearchCubit>(
          create: (BuildContext context) => PlurkSearchCubit(),
        ),
        BlocProvider<OfficialNewsCubit>(
          create: (BuildContext context) => OfficialNewsCubit(),
        ),
        BlocProvider<BookmarksGetBookmarksCubit>(
          create: (BuildContext context) => BookmarksGetBookmarksCubit(),
        ),
        //For PlurkCard switch PlurkViewer background.
        //This is useless. But PlurkCard seems need it...
        BlocProvider<ViewerBackgroundSwitchCubit>(
          create: (BuildContext context) => ViewerBackgroundSwitchCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<TimelineGetPlurksCubit, TimelineGetPlurksState>(
            listener: (context, state) {
              if (state is TimelineGetPlurksSuccess) {
                //Update the plurks data and setState for the ListView.builder.
                _onTimelineRefreshPlurksSuccess(state);
              } else if (state is TimelineGetNewPlurksSuccess) {
                _onTimelineGetNewPlurksSuccess(state);
              }
            },
          ),
          BlocListener<TimelineTrackBackCubit, TimelineTrackBackState>(
            listener: (context, state) {
              //Update the plurks data and setState for the ListView.builder.
              _onTimelineTrackBackState(state);
            },
          ),
          BlocListener<StatsGetAnonymousPlurksCubit,
              StatsGetAnonymousPlurksState>(
            listener: (context, state) {
              if (state is StatsGetAnonymousPlurksSuccess) {
                _onAnonymousPlurks(state.topicPlurks);
              } else if (state is StatsGetAnonymousPlurksFailed) {
                _onAnonymousPlurks(null);
              }
            },
          ),
          BlocListener<StatsTopFavoritesCubit, StatsTopFavoritesState>(
            listener: (context, state) {
              if (state is StatsTopFavoritesSuccess) {
                _onTopPlurks(state.topPlurks);
              } else if (state is StatsTopFavoritesFailed) {
                _onTopPlurks(null);
              }
            },
          ),
          BlocListener<StatsTopReplurksCubit, StatsTopReplurksState>(
            listener: (context, state) {
              if (state is StatsTopReplurksSuccess) {
                _onTopPlurks(state.topPlurks);
              } else if (state is StatsTopReplurksFailed) {
                _onTopPlurks(null);
              }
            },
          ),
          BlocListener<StatsTopRespondedCubit, StatsTopRespondedState>(
            listener: (context, state) {
              if (state is StatsTopRespondedSuccess) {
                _onTopPlurks(state.topPlurks);
              } else if (state is StatsTopRespondedFailed) {
                _onTopPlurks(null);
              }
            },
          ),
          BlocListener<PlurkSearchCubit, PlurkSearchState>(
            listener: (context, state) {
              if (state is PlurkSearchSuccess) {
                _onPlurkSearch(state.plurkSearch);
              } else if (state is PlurkSearchFailed) {
                _onPlurkSearch(null);
              } else if (state is PlurkSearchTrackBackSuccess) {
                _onPlurkSearchTrackBackState(state.plurkSearch);
              } else if (state is PlurkSearchTrackBackFailed) {
                _onPlurkSearchTrackBackState(null);
              }
            },
          ),
          BlocListener<OfficialNewsCubit, OfficialNewsState>(
            listener: (context, state) {
              if (state is OfficialNewsSuccess) {
                _onOfficialNews(state.officialNewsPlurks);
              } else if (state is OfficialNewsFailed) {
                _onOfficialNews(null);
              }
            },
          ),
          BlocListener<BookmarksGetBookmarksCubit, BookmarksGetBookmarksState>(
            listener: (context, state) {
              if (state is BookmarksGetBookmarksSuccess) {
                _onGetBookmarks(state.bookmarks);
              } else if (state is BookmarksGetBookmarksFailed) {
                _onGetBookmarks(null);
              }
            },
          ),
          BlocListener<TimelineMarkAsReadCubit, TimelineMarkAsReadState>(
            listener: (listenerContext, state) {
              if (state is TimelineMarkAsReadSuccess) {
                _plurksCollection.marksAllRead(state.plurkIDs);
              } //Ignore the null situation.
            },
          ),
        ],
        child: AcrylicContainer(
          Builder(builder: (context) {
            _blocProviderContext = context;

            List<Widget> sliverWidgets = [];

            //AppBar
            if (widget.args.hasAppBar) {
              sliverWidgets.add(SliverAppBar(
                elevation: 0,
                titleSpacing: 0,
                //Todo: support this.
                // systemOverlayStyle: Sy,
                // brightness: Theme.of(context).brightness,
                backgroundColor: Colors.transparent,
                floating: true,
                flexibleSpace: AcrylicContainer(
                  Container(),
                  backgroundColor: Theme.of(context).lowContrastPrimaryColor,
                  transparentBackLayer: false,
                  mode: AcrylicContainerMode.BackdropFilter,
                ),
                centerTitle: true,
                leading: widget.args.leading,
                title: Container(
                  child: Center(
                    child: widget.args.title,
                  ),
                ),
                actions: widget.args.actions,
              ));
            }

            //[Dep]: A lot of cards.
            // sliverWidgets.add(SliverList(
            //   delegate: SliverChildListDelegate(
            //     _buildPlurkCards(),
            //   ),
            // ));

            //This maybe faster?
            sliverWidgets.add(SliverList(
              delegate: SliverChildBuilderDelegate(
                _buildPlurkCard,
                addAutomaticKeepAlives: false,
              ),
            ));

            //Why do we need this?
            sliverWidgets.add(SliverFillRemaining(
              hasScrollBody: false,
              child: Container(
                height: MediaQuery.of(context).viewInsets.bottom,
                color: Theme.of(context).primaryColor.withOpacity(0.4),
              ),
            ));

            Color indicatorColor = Theme.of(context).textTheme.bodyText1.color;

            return NotificationListener<ScrollNotification>(
              child: RefreshIndicator(
                key: _refreshIndicatorKey,
                displacement: 100,
                color: indicatorColor,
                backgroundColor: Theme.of(context).primaryColor,
                // controller: _refreshController,
                // header: WaterDropMaterialHeader(
                //   color: indicatorColor,
                // ),
                onRefresh: _onRefresh,
                child: CustomScrollView(
                  // controller: PrimaryScrollController.of(context),
                  slivers: sliverWidgets,
                  primary: true,
                ),
                // enablePullUp: true,
                // onLoading: _onLoading,
              ),
              onNotification: (scrollNotification) {
                if (widget.args.browsingType == BrowsingType.Home) {
                  //Only Home will need track back.
                  double triggerFetchMoreSize =
                      0.75 * scrollNotification.metrics.maxScrollExtent;
                  if (scrollNotification.metrics.pixels > 10 &&
                      scrollNotification.metrics.pixels > triggerFetchMoreSize) {
                    // call fetch more method here
                    _trackBackPlurkTimeline();
                  }

                  //Leave / return to top.
                  if (scrollNotification.metrics.pixels <= 10.0) {
                    if (_lastScrollPosition > 10.0 &&
                        widget.args.onReturnTop != null) {
                      widget.args.onReturnTop();
                    }
                  } else {
                    if (_lastScrollPosition <= 10.0 &&
                        widget.args.onLeaveTop != null) {
                      widget.args.onLeaveTop();
                    }
                  }

                  _lastScrollPosition = scrollNotification.metrics.pixels;
                }
                return true;
              },
            );
          }),
          initialImage: AssetImage('assets/images/background001.jpg'),
          transparentBackLayer: false,
        ),
      ),
    );
  }

  // //For SliverChildListDelegate
  // //considerPinPlurk: Only when Home -> mine filter
  // List<Widget> _buildPlurkCards() {
  //   List<PlurkFeed> feeds = _plurksCollection.getHomeFeeds();
  //
  //   //The refresher will sort out feed for us. But we'll need to process to pin icon.
  //   int pinnedPlurkId = (Static.me != null && Static.me.pinnedPlurkId != null)
  //       ? (Static.me.pinnedPlurkId)
  //       : (0);
  //
  //   //We need a filter for Friends and Fans PlurksMainFilter...
  //   //Friends: we need getCompletion for check my whole friend ids.
  //   //Following: we need filter out those Owner which following = true (means I am following him)
  //
  //   List<Widget> returnWidgets = [];
  //   //If the list is empty...
  //   if (feeds.length == 0) {
  //     //Display an empty logo.
  //     returnWidgets.add(_fadeInEmptyLogo());
  //   } else {
  //     //Display ADWidgets??
  //     bool shouldDisplayADs = Static.shouldDisplayAds();
  //     int currentFeedCountPerAd = Define.PLURK_FEED_COUNT_PER_AD;
  //     int adAppearCountDown = currentFeedCountPerAd;
  //     for (int i = 0; i < feeds.length; ++i) {
  //       returnWidgets.add(PlurkCard(
  //           feeds[i].plurk,
  //           feeds[i].owner,
  //           feeds[i].replurker,
  //           feeds[i].favorers,
  //           feeds[i].replurkers,
  //           PlurkCardMode.FeedView,
  //           true,
  //           widget.args.openViewerById,
  //           bookmark: feeds[i].bookmark,
  //           hintPorn: Static.settingsTapToRevealAdultsOnlyContent,
  //           showPinIcon: pinnedPlurkId == feeds[i].plurk.plurkId,
  //           //[Dep] Keep the first N card alive for better scroll back effect.
  //           // needKeepAlive: i < 13 ? true : false,
  //           onWantEditPlurk: (plurk) {
  //         // print('Home onWantEditPlurk.');
  //         //Open Plurk Poster for edit post.
  //         PlurkPoster.show(
  //             context,
  //             PlurkPoster.PlurkPosterArgs(
  //               PlurkPoster.PlurkPosterMode.Edit,
  //               editingPlurk: plurk,
  //             ));
  //       }, onWantDeletePlurk: (plurk) {
  //         //onWantDeletePlurk
  //         // print('Home onWantDeletePlurk');
  //         _blocProviderContext
  //             .read<TimelinePlurkDeleteCubit>()
  //             .request(plurk.plurkId);
  //       }));
  //
  //       //Only when not premium and set to show ads.
  //       if (shouldDisplayADs) {
  //         adAppearCountDown--;
  //         if (adAppearCountDown == 0) {
  //           returnWidgets.add(FeedAdWidget(
  //             type: NativeAdmobType.full,
  //             padding: EdgeInsets.symmetric(
  //                 horizontal: Define.horizontalPadding,
  //                 vertical: Define.verticalPadding),
  //           ));
  //           if (currentFeedCountPerAd < Define.PLURK_FEED_COUNT_PER_AD_MAX) {
  //             currentFeedCountPerAd += Define.PLURK_FEED_COUNT_PER_AD_INC;
  //           }
  //           adAppearCountDown = currentFeedCountPerAd;
  //         }
  //       }
  //     }
  //
  //     //Add a large chunk of footer to make sure the user can scroll something to backtrack.
  //     // returnWidgets.add(Container(
  //     //   height: MediaQuery.of(context).size.height,
  //     //   child: Center(
  //     //     child: Container(
  //     //       width: 256,
  //     //       height: 256,
  //     //       child: FlareActor(
  //     //         "assets/rive/MeatLoader.flr",
  //     //         animation: "Untitled",
  //     //       ),
  //     //     ),
  //     //   ),
  //     // ));
  //
  //   }
  //
  //   return returnWidgets;
  // }

  //For SliverChildBuilderDelegate
  Widget _buildPlurkCard(BuildContext context, int index) {
    //This is tricky: We need to return the correct widget for the correct index.

    List<PlurkFeed> feeds = _plurksCollection.feeds();

    //The refresher will sort out feed for us. But we'll need to process to pin icon.
    int pinnedPlurkId = (Static.me != null && Static.me.pinnedPlurkId != null)
        ? (Static.me.pinnedPlurkId)
        : (0);

    //We need a filter for Friends and Fans PlurksMainFilter...
    //Friends: we need getCompletion for check my whole friend ids.
    //Following: we need filter out those Owner which following = true (means I am following him)
    //If the list is empty...
    if (feeds.length == 0) {
      if (index == 0) {
        //Display an empty logo.
        return EmptyLogo();
      } else {
        return null;
      }
    } else {
      //We got content.
      if (Static.shouldDisplayAds()) {
        //[Dep]: This will be always false for now...
        //With ADs
        // int useFeedIndex = transferADListIndexToFeedIndex(index);
        // if (useFeedIndex < feeds.length) {
        //   if (isAnADIndex(index)) {
        //     return FeedAdWidget(
        //       padding: EdgeInsets.symmetric(
        //           horizontal: Define.horizontalPadding,
        //           vertical: Define.verticalPadding),
        //     );
        //   } else {
        //     return _plurkCard(pinnedPlurkId, feeds[useFeedIndex]);
        //   }
        // } else if (useFeedIndex == feeds.length) {
        //     //Make a footer.
        //     return Container(
        //       color: Theme.of(context).canvasColor.withOpacity(0.5),
        //       height: 124,
        //       child: Container(),
        //     );
        // } else {
        //   return null;
        // }
      } else {
        //All
        if (index < feeds.length) {
          return _plurkCard(pinnedPlurkId, feeds[index]);
        } else if (index == feeds.length) {
          //Make a footer.
          return Container(
            color: Theme.of(context).canvasColor.withOpacity(0.5),
            height: 124,
            child: Container(),
          );
        } else {
          return null;
        }
      }
    }
  }

  //100 elements of AD placement index.
  List<int> placementOfADs;
  _tryInitPlacementsOfADs() {
    if (placementOfADs == null) {
      placementOfADs = [];
      int currentIncrement = Define.PLURK_FEED_COUNT_PER_AD;
      int pointingIndex = Define.PLURK_FEED_COUNT_PER_AD;
      while(placementOfADs.length < 100) {
        placementOfADs.add(pointingIndex);
        pointingIndex += currentIncrement;
        //Continue on checking
        if (currentIncrement < Define.PLURK_FEED_COUNT_PER_AD_MAX) {
          currentIncrement += Define.PLURK_FEED_COUNT_PER_AD_INC;
        }
      }

      // print('[init placementOfADs complete]: [' + placementOfADs.join(', ') + ']');
    }
  }

  //Is this suppose to be an AD index?
  bool isAnADIndex(int index) {
    _tryInitPlacementsOfADs();
    return placementOfADs.contains(index);
  }

  int transferADListIndexToFeedIndex(int adListIndex) {
    _tryInitPlacementsOfADs();
    int feedIndex = -1;

    for (int i = 0 ; i <= adListIndex ; ++i) {
      if (placementOfADs.contains(i)) {
        //Do nothing
      } else {
        feedIndex += 1;
      }
    }
    return feedIndex;
  }

  // int transferFeedIndexToADListIndex(int feedIndex) {
  //   _tryInitPlacementsOfADs();
  //   int offsetByADs = 0;
  //   for (int i = 0 ; i <= feedIndex ; ++i) {
  //     if (placementOfADs.contains(i)) {
  //       offsetByADs += 1;
  //     }
  //   }
  //   return feedIndex + offsetByADs;
  // }

  Widget _plurkCard(int pinnedPlurkId, PlurkFeed plurkFeed) {
    return PlurkCard(
        plurkFeed.plurk,
        plurkFeed.owner,
        plurkFeed.replurker,
        plurkFeed.favorers,
        plurkFeed.replurkers,
        PlurkCardMode.FeedView,
        true,
        widget.args.openViewerById,
        bookmark: plurkFeed.bookmark,
        hintPorn: Static.settingsTapToRevealAdultsOnlyContent,
        showPinIcon: pinnedPlurkId == plurkFeed.plurk.plurkId,
        //[Dep] Keep the first N card alive for better scroll back effect.
        // needKeepAlive: i < 13 ? true : false,
        onWantEditPlurk: (plurk) {
          // print('Home onWantEditPlurk.');
          //Open Plurk Poster for edit post.
          PlurkPoster.show(
              context,
              PlurkPoster.PlurkPosterArgs(
                PlurkPoster.PlurkPosterMode.Edit,
                editingPlurk: plurk,
              ));
        }, onWantDeletePlurk: (plurk) {
      //onWantDeletePlurk
      // print('Home onWantDeletePlurk');
      _blocProviderContext
          .read<TimelinePlurkDeleteCubit>()
          .request(plurk.plurkId);
    });
  }

  // Widget _emptyLogo() {
  //   return AnimatedOpacity(
  //     opacity: _emptyLogoVisible ? 1.0 : 0.0,
  //     duration: Duration(milliseconds: 500),
  //     child: EmptyLogo(),
  //   );
  // }

  overridePlurk(Plurdart.Plurk plurk) async {
    await _plurksCollection.overridePlurk(plurk);
    setState(() {});
  }

  deletePlurk(int plurkId) {
    setState(() {
      _plurksCollection.deletePlurk(plurkId);
    });
  }

  setIsViewingUnreadOnly(bool b) async {
    //[Dep]: We cannot merely do this...
    // bool hasSet = await _plurksCollection.setIsViewingUnreadOnly(b);
    // if (hasSet) {
    //   //Scroll to top.
    //   setState(() {});
    // }

    //When changed we need to use different API for getting plurks.
    if (widget.args.browsingType == BrowsingType.Home) {
      refresh();
    }
  }

  clear() {
    setState(() {
      //This could clear all cards.
      _plurksCollection.clear();
      PrimaryScrollController.of(context).jumpTo(0.0);
    });
  }

  returnToTop() async {
    //Check the current position to see if instant set or animateTo
    if (PrimaryScrollController.of(context).position.pixels > 5000) {
      //Too far! Just set pos to 0.0
      PrimaryScrollController.of(context).position.jumpTo(5000.0);
      await PrimaryScrollController.of(context).position.animateTo(0.0,
          duration: Duration(
              milliseconds: (PrimaryScrollController.of(context).position.pixels * 0.1).toInt()),
          curve: Curves.linear);
    } else {
      //AnimateTo 0.0
      await PrimaryScrollController.of(context).position.animateTo(0.0,
          duration: Duration(
              milliseconds: (PrimaryScrollController.of(context).position.pixels * 0.1).toInt()),
          curve: Curves.linear);
    }
  }

  getNewPlurks(int count) async {
    if (!_isRefreshingFeed &&
        !_isGettingNewPlurk &&
        !_isLoadingFeed) {
      HapticFeedback.lightImpact();

      _isGettingNewPlurk = true;

      _blocProviderContext.read<RequestIndicatorCubit>().show();
      _blocProviderContext.read<TimelineGetPlurksCubit>().getNewPlurks(
          Plurdart.PollingGetPlurks(
            offset: _plurksCollection.getTopOffset(),
            limit: count,
            favorersDetail: true,
            replurkersDetail: true,
            limitedDetail: true)
      );
    }
  }

  _onTimelineGetNewPlurksSuccess(TimelineGetNewPlurksSuccess state) async {
    _blocProviderContext.read<RequestIndicatorCubit>().hide();

    if (state.plurks != null) {
      await _plurksCollection.addTop(state.plurks);
      setState(() {
        _timelineUpdateError = false;
      });
    }

    // _refreshController.loadComplete();
    _isGettingNewPlurk = false;

    returnToTop();
  }

  //Start a refresh.
  refresh() {
    _refreshIndicatorKey.currentState.show();
    // _refreshController.requestRefresh(
    //     needMove: true, duration: Duration(milliseconds: 200));
  }

  refreshSearchTerm(String searchTerm) {
    widget.args.searchTerm = searchTerm;
    _refreshIndicatorKey.currentState.show();
    // _refreshController.requestRefresh(
    //     needMove: true, duration: Duration(milliseconds: 200));
  }

  //Are we requesting feed?
  bool isRefreshingFeed() {
    return _isRefreshingFeed;
  }

  Future _onRefresh() async {
    //Active refresh timeline.
    await _refreshPlurkTimeline();
  }

  // _onLoading() {
  //   // call fetch more method here
  //   _trackBackPlurkTimeline();
  // }

  Future _refreshPlurkTimeline() async {
    if (!_isRefreshingFeed &&
        !_isGettingNewPlurk &&
        !_isLoadingFeed) {

      HapticFeedback.lightImpact();

      _isRefreshingFeed = true;
      _noHistoryRemained = false;
      _timelineUpdateError = false;

      // print('[_refreshPlurkTimeline]: browsingType[' + widget.args.browsingType.toString() + '] Static.usingPlurkFilter()[' + Static.usingPlurkFilter().toString() + ']');

      switch (widget.args.browsingType) {
        case BrowsingType.Home:
          int repeatTimes = 1;
          //[Kinda Hacky]: Some special filter better request more at one time
          if (Static.prefPlurksMainFilter() ==
              Static.PlurksMainFilter.MyPrivate) {
            repeatTimes = 5;
          } else if (Static.prefPlurksMainFilter() ==
              Static.PlurksMainFilter.Spy) {
            repeatTimes = 3;
          }

          await _blocProviderContext.read<TimelineGetPlurksCubit>().getPlurks(
              Plurdart.TimelineGetPlurks(
                  filter: Static.usingPlurkFilter(),
                  favorersDetail: true,
                  replurkersDetail: true,
                  limitedDetail: true,
                  limit: 15),
              repeatTimes: repeatTimes,
              isViewingUnreadOnly: Static.isViewingUnreadOnly()
          );
          break;
        case BrowsingType.TopReplurks:
          await _blocProviderContext.read<StatsTopReplurksCubit>().request();
          break;
        case BrowsingType.TopFavorites:
          await _blocProviderContext.read<StatsTopFavoritesCubit>().request();
          break;
        case BrowsingType.TopResponses:
          await _blocProviderContext.read<StatsTopRespondedCubit>().request();
          break;
        case BrowsingType.Anonymous:
          await _blocProviderContext.read<StatsGetAnonymousPlurksCubit>().request();
          break;
        case BrowsingType.Search:
          await _blocProviderContext
              .read<PlurkSearchCubit>()
              .request(widget.args.searchTerm, 0);
          break;
        case BrowsingType.OfficialNews:
          await _blocProviderContext.read<OfficialNewsCubit>().request();
          break;
        case BrowsingType.Bookmarks:
          await _blocProviderContext
              .read<BookmarksGetBookmarksCubit>()
              .request(widget.args.bookmarkTag);
          break;
      }

      //No offset means get from the newest!.
    } else {
      // _refreshController.refreshCompleted();
    }

    return;
  }

  _onTimelineRefreshPlurksSuccess(TimelineGetPlurksSuccess state) async {
    if (widget.args.browsingType == BrowsingType.Home) {
      BlocProvider.of<HomeRefreshCubit>(context).complete();
    }

    //Update the plurks data and setState for the ListView.builder.
    // print('[Cubit response]: TimelineGetPlurksCubit state.plurksList.length[' + state.plurksList.length.toString() + ']');
    if (state.plurksList != null) {
      if (state.plurksList.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();

      //Special: MyPlurks try to read pinnedPlurk and put in on the top.
      if (Static.prefPlurksMainFilter() == Static.PlurksMainFilter.MyPlurks) {
        int pinnedPlurkId =
            (Static.me != null && Static.me.pinnedPlurkId != null)
                ? (Static.me.pinnedPlurkId)
                : (0);
        if (pinnedPlurkId != 0) {
          Plurdart.PlurkWithUser plurkWithUser =
              await Plurdart.timelineGetPlurk(Plurdart.TimelineGetPlurk(
                  plurkId: pinnedPlurkId,
                  favorersDetail: true,
                  limitedDetail: true,
                  replurkersDetail: true));

          //The pinned Plurk on top
          if (plurkWithUser != null && !plurkWithUser.hasError()) {
            await _plurksCollection.add(Plurdart.Plurks(
                plurks: [plurkWithUser.plurk],
                plurkUsers: plurkWithUser.plurkUsers));
          }

          //Also remove the pinned plurk in state. (kinda hacky)
          List<Plurdart.Plurks> removePinnedPlurksList = state.plurksList;
          removePinnedPlurksList.forEach((plurks) {
            plurks.plurks
                .removeWhere((element) => (element.plurkId == pinnedPlurkId));
          });
        }

        //This should be done no matter if there's pinned Plurk.
        await _plurksCollection.addMultiple(state.plurksList);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() ==
          Static.PlurksMainFilter.Friends) {
        //Extra filter: Friends
        //We need completion data for filter friend.
        Map<int, Plurdart.Completion> completionMap =
            await Plurdart.friendsFansGetCompletion();
        await _plurksCollection.addMultiple(state.plurksList,
            completionMap: completionMap);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() ==
          Static.PlurksMainFilter.Following) {
        //Extra filter: Following

        //This is stupid. But works correctly.
        List<Plurdart.User> followingUsers = [];
        while (true) {
          List<Plurdart.User> users =
              await Plurdart.friendsFansGetFollowingByOffset(
                  Plurdart.FriendsFansGetFollowingByOffset(
            offset: followingUsers.length,
            limit: 100,
          ));

          if (users.length > 0) {
            followingUsers.addAll(users);
          } else {
            //End of the list.
            break;
          }
        }

        //Give all my following users to _plurksCollection.
        await _plurksCollection.addMultiple(state.plurksList,
            limitUsers: followingUsers);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() ==
          Static.PlurksMainFilter.MyPrivate) {
        //Extra filter: My Private
        await _plurksCollection.addMultiple(state.plurksList, myPrivate: true);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() == Static.PlurksMainFilter.Spy) {
        //Extra filter: Spy
        await _plurksCollection.addMultiple(state.plurksList, spy: true);
        setState(() {
          _timelineUpdateError = false;
        });
      } else {
        //Built-in filters: filtered by server.
        // print('addMultiple');
        await _plurksCollection.addMultiple(state.plurksList);
        setState(() {
          _timelineUpdateError = false;
        });
      }

      // _refreshController.refreshCompleted();
      // _refreshController.loadComplete();

      // _emptyLogoVisible = true;
      _isRefreshingFeed = false;

      //[Special case]: Spy filter repeat
      if (Static.prefPlurksMainFilter() == Static.PlurksMainFilter.Spy) {
        _trackBackPlurkTimeline();
      }
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();

      // _emptyLogoVisible = true;
      _isRefreshingFeed = false;
      print('Oops! Something goes wrong!!! Display an error logo.');
    }
  }

  _onTopPlurks(Plurdart.TopPlurks topPlurks) async {
    if (topPlurks != null) {
      if (topPlurks.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();
      await _plurksCollection.setTopPlurks(topPlurks);

      //Built-in filters: filtered by server.
      setState(() {
        _timelineUpdateError = false;
      });

      // _refreshController.refreshCompleted();
      // _refreshController.loadComplete();
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRefreshingFeed = false;
  }

  _onAnonymousPlurks(Plurdart.TopicPlurks topicPlurks) async {
    if (topicPlurks != null) {
      if (topicPlurks.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();
      await _plurksCollection.setAnonymousPlurks(topicPlurks);

      //Built-in filters: filtered by server.
      setState(() {
        _timelineUpdateError = false;
      });

      // _refreshController.refreshCompleted();
      // _refreshController.loadComplete();
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRefreshingFeed = false;
  }

  _onPlurkSearch(Plurdart.PlurkSearch plurkSearch) async {
    // print('[Cubit response]: _onPlurkSearch');
    if (plurkSearch != null && plurkSearch.plurks != null) {
      if (plurkSearch.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();

      await _plurksCollection.add(Plurdart.Plurks(
          plurks: plurkSearch.plurks, plurkUsers: plurkSearch.users));

      setState(() {
        _timelineUpdateError = false;
      });

      // _refreshController.refreshCompleted();
      // _refreshController.loadComplete();

      // _emptyLogoVisible = true;
      _isRefreshingFeed = false;
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();

      // _emptyLogoVisible = true;
      _isRefreshingFeed = false;
      print('Oops! Something goes wrong!!! Display an error logo.');
    }
  }

  _onGetBookmarks(Plurdart.Bookmarks bookmarks) async {
    if (bookmarks != null) {
      if (bookmarks.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();
      await _plurksCollection.setBookmarksPlurks(bookmarks);

      //Built-in filters: filtered by server.
      setState(() {
        _timelineUpdateError = false;
      });

      // _refreshController.refreshCompleted();
      // _refreshController.loadComplete();
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRefreshingFeed = false;
  }

  _onOfficialNews(List<Plurdart.Plurk> officialNewsPlurks) async {
    if (officialNewsPlurks != null) {
      if (officialNewsPlurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();
      await _plurksCollection.setOfficialNewsPlurks(officialNewsPlurks);

      //Built-in filters: filtered by server.
      setState(() {
        _timelineUpdateError = false;
      });

      // _refreshController.refreshCompleted();
      // _refreshController.loadComplete();
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRefreshingFeed = false;
  }

  _trackBackPlurkTimeline() {
    //Only Home mode and track back
    if (widget.args.browsingType == BrowsingType.Home) {
      if (!_isRefreshingFeed &&
          !_isGettingNewPlurk &&
          !_isLoadingFeed &&
          !_noHistoryRemained &&
          !_timelineUpdateError) {

        //Someone says this is not good.
        // HapticFeedback.lightImpact();

        _isLoadingFeed = true;

        int repeatTimes = 1;
        //[Kinda Hacky]: Some filter better request more at one time
        if (Static.prefPlurksMainFilter() ==
            Static.PlurksMainFilter.MyPrivate) {
          repeatTimes = 5;
        } else if (Static.prefPlurksMainFilter() ==
            Static.PlurksMainFilter.Spy) {
          repeatTimes = 3;
        }

        _blocProviderContext.read<RequestIndicatorCubit>().show();
        _blocProviderContext.read<TimelineTrackBackCubit>().timelineTrackBack(
            Plurdart.TimelineGetPlurks(
                offset: _plurksCollection.getBottomOffset(),
                filter: Static.usingPlurkFilter(),
                favorersDetail: true,
                replurkersDetail: true,
                limitedDetail: true,
                limit: 15),
            repeatTimes: repeatTimes,
            isViewingUnreadOnly: Static.isViewingUnreadOnly(),
        );
        //No offset means get from the newest!.
      } else {
        // _refreshController.loadComplete();
      }
    } else if (widget.args.browsingType == BrowsingType.Search) {
      if (!_isRefreshingFeed &&
          !_isLoadingFeed &&
          !_noHistoryRemained &&
          !_timelineUpdateError) {
        _isLoadingFeed = true;

        _blocProviderContext.read<RequestIndicatorCubit>().show();
        _blocProviderContext.read<PlurkSearchCubit>().trackBack(
            widget.args.searchTerm, _plurksCollection.feedsLength());
      } else {
        // _refreshController.loadComplete();
      }
    } else {
      // _refreshController.loadComplete();
    }
    //Other modes doesn't need track-back stuffs.
  }

  _onTimelineTrackBackState(TimelineTrackBackState state) async {
    _blocProviderContext.read<RequestIndicatorCubit>().hide();

    //Update the plurks data and setState for the ListView.builder.
    // print('[Cubit response]: TimelineTrackBackState');
    if (state.plurksList != null) {
      if (state.plurksList.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      if (Static.prefPlurksMainFilter() == Static.PlurksMainFilter.Friends) {
        //Extra filter: Friends
        //We need completion data for filter friend.
        Map<int, Plurdart.Completion> completionMap =
            await Plurdart.friendsFansGetCompletion();
        await _plurksCollection.addMultiple(state.plurksList,
            completionMap: completionMap);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() ==
          Static.PlurksMainFilter.Following) {
        //Extra filter: Following

        //This is stupid. But works correctly.
        List<Plurdart.User> followingUsers = [];
        while (true) {
          List<Plurdart.User> users =
              await Plurdart.friendsFansGetFollowingByOffset(
                  Plurdart.FriendsFansGetFollowingByOffset(
            offset: followingUsers.length,
            limit: 100,
          ));

          if (users.length > 0) {
            followingUsers.addAll(users);
          } else {
            //End of the list.
            break;
          }
        }

        //Give all my following users to _plurksCollection.
        await _plurksCollection.addMultiple(state.plurksList,
            limitUsers: followingUsers);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() ==
          Static.PlurksMainFilter.MyPrivate) {
        //Extra filter: My Private

        await _plurksCollection.addMultiple(state.plurksList, myPrivate: true);
        setState(() {
          _timelineUpdateError = false;
        });
      } else if (Static.prefPlurksMainFilter() == Static.PlurksMainFilter.Spy) {
        //Extra filter: Spy

        await _plurksCollection.addMultiple(state.plurksList, spy: true);
        setState(() {
          _timelineUpdateError = false;
        });
      } else {
        //Built-in filters: filtered by server.
        await _plurksCollection.addMultiple(state.plurksList);
        setState(() {
          _timelineUpdateError = false;
        });
      }

      // _refreshController.loadComplete();

      _isLoadingFeed = false;

      //[Special case]: Spy filter repeat
      if (Static.prefPlurksMainFilter() == Static.PlurksMainFilter.Spy) {
        _trackBackPlurkTimeline();
      }
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();

      _isLoadingFeed = false;
      print('Oops! Something goes wrong!!! Display an error logo.');
    }
  }

  _onPlurkSearchTrackBackState(Plurdart.PlurkSearch plurkSearch) async {
    _blocProviderContext.read<RequestIndicatorCubit>().hide();

    //Update the plurks data and setState for the ListView.builder.
    // print('[Cubit response]: _onPlurkSearchTrackBackState');
    if (plurkSearch != null && plurkSearch.plurks != null) {
      if (plurkSearch.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      await _plurksCollection.add(Plurdart.Plurks(
          plurks: plurkSearch.plurks, plurkUsers: plurkSearch.users));
      setState(() {
        _timelineUpdateError = false;
      });

      // _refreshController.loadComplete();

      _isLoadingFeed = false;
    } else {
      // _refreshController.refreshFailed();
      // _refreshController.loadFailed();

      _isLoadingFeed = false;
      print('Oops! Something goes wrong!!! Display an error logo.');
    }
  }

  //Get the current offset.
  DateTime getCurrentOffset() {
    return _plurksCollection.getBottomOffset();
  }
}
