import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/theme/app_themes.dart';

enum LinkStackType {
  All,
  Pages,
}

class PlurkCardLinkStack extends StatelessWidget {
  PlurkCardLinkStack(this.urlLinks, this.linkStackType, this.onLinkOpen);

  final List<PlurkCardUrlLink> urlLinks;
  final LinkStackType linkStackType;
  final PageController _linkPageIndicatorController = PageController();
  final Function(Uri) onLinkOpen;

  @override
  Widget build(BuildContext context) {
    if (linkStackType == LinkStackType.All) {
      return _buildLinkTagsAll(context, urlLinks);
    } else {
      return _buildLinkTagsPages(
          context, urlLinks, _linkPageIndicatorController);
    }
  }

  // Build one link tag.
  Widget _buildLinkTag(PlurkCardUrlLink urlLink) {
    return GestureDetector(
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
            decoration: BoxDecoration(
              border: Border.all(
                width: 0.5,
                color: Theme.of(context).hintColor,
              ),
              color: Theme.of(context).canvasColor.withOpacity(0.33),
            ),
            child: Builder(
              builder: (context) {
                List<Widget> rowChildren = [];

                //Do we have a thumbnail url??
                if (urlLink.previewImageUrl?.isEmpty ?? true) {
                  //No preview image.

                  //Right column is a must have. (Flexible)
                  rowChildren.add(Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        //Upper row
                        Container(
                          padding: EdgeInsets.fromLTRB(12, 8, 8, 2),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              //Favicon (shit, it's throwing shit like crazy)
                              CachedNetworkImage(
                                imageUrl: urlLink.getFaviconUrlFromGoogle(),
                                width: 16,
                                height: 16,
                                fit: BoxFit.cover,
                                placeholder: (context, url) {
                                  return Icon(Icons.link, size: 16,);
                                },
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              //Authority text.
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: 2),
                                  child: Text(
                                    urlLink.getAuthority(),
                                    style: Theme.of(context).textTheme.caption,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),

                        //Bottom link text.
                        Container(
                            padding: EdgeInsets.fromLTRB(16, 2, 16, 8),
                            child: Text(
                              urlLink.title,
                              overflow: TextOverflow.fade,
                              style: TextStyle(
                                fontSize: 13.6,
                              ),
                            )),
                      ],
                    ),
                  ));
                } else {
                  //Yes, we have a preview image.
                  rowChildren.add(LimitedBox(
                    maxHeight: 110,
                    maxWidth: 110,
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: Image(
                        fit: BoxFit.cover,
                        image: NetworkImage(urlLink.previewImageUrl),
                      ),
                    ),
                  ));

                  //Right column is a must have. (Limit height)
                  rowChildren.add(Flexible(
                    child: Container(
                      height: 110,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          //Upper row
                          Container(
                            // color: Colors.yellow,
                            padding: EdgeInsets.fromLTRB(12, 8, 12, 2),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                //Favicon (shit, it's throwing shit like crazy)
                                CachedNetworkImage(
                                  imageUrl: urlLink.getFaviconUrlFromGoogle(),
                                  width: 16,
                                  height: 16,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) {
                                    return Icon(Icons.link, size: 16,);
                                  },
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                //Authority text.
                                Flexible(
                                  child: Padding(
                                    padding: EdgeInsets.only(bottom: 2),
                                    child: Text(
                                      urlLink.getAuthority(),
                                      style: Theme.of(context).textTheme.caption,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          //Bottom link text.
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.fromLTRB(16, 2, 16, 8),
                                child: Text(
                                  urlLink.title,
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(
                                    fontSize: 13.6,
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ));
                }

                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: rowChildren,
                );
              },
            ),
          );
        },
      ),
      onTap: () async {
        //User tap this tag. Open the url for him.
        onLinkOpen?.call(urlLink.uri);

        // //[Huh]: I forget why I did this...
        // // String bktUrl = urlLink.getUrl().replaceFirst('https', Define.bktScheme);
        // String url = urlLink.getUrl();
        //
        // //bkt version.
        // if (await canLaunch(url)) {
        //   Vibration.vibrateShortNote();
        //   print('[LinkStack] Launch: [' + url + ']');
        //   await launch(url);
        // } else {
        //   print('[LinkStack] canLaunch is false for [' + url + ']');
        // }

        //https version
        // if (await canLaunch(urlLink.getUrl())) {
        //   Vibration.vibrateShortNote();
        //   // print('[LinkStack] Launch: [' + urlLink.getUrl() + ']');
        //   await launch(urlLink.getUrl());
        // } else {
        //   print('[LinkStack] canLaunch is false for [' + urlLink.getUrl() + ']');
        // }

      },
    );
  }

  //Build all link tags stack by a column.
  Widget _buildLinkTagsAll(
      BuildContext context, List<PlurkCardUrlLink> urlLinks) {
    List<Widget> columnChildren = [];
    for (int i = 0; i < urlLinks.length; ++i) {
      columnChildren.add(_buildLinkTag(urlLinks[i]));
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: columnChildren,
    );
  }

  Widget _buildLinkTagsPages(
      BuildContext context,
      List<PlurkCardUrlLink> urlLinks,
      PageController linkPageIndicatorController) {

    Color indicatorColor = Theme.of(context).primaryColor;
    if (Static.settingsAppTheme == AppTheme.Pure) {
      //Force the Pure theme indicator color to white color. The black color looks suck.
      indicatorColor = Theme.of(context).textTheme.bodyText1.color;
    }

    return Container(
      decoration: BoxDecoration(
          border: Border.all(
        width: 0.5,
        color: Theme.of(context).hintColor,
      )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // PageView constraints.
          SizedBox(
            // Calculate the container height from youtube video aspect ratio.
            height: 110,
            child: PageView.builder(
              controller: linkPageIndicatorController,
              itemCount: urlLinks.length,
              // This will prevent from pre allocate YoutubePlayerBuilder.
              itemBuilder: (BuildContext context, int index) {
                return _buildLinkTag(urlLinks[index]);
              },
            ),
          ),

          // Smooth Page Indicator.
          Container(
            padding: EdgeInsets.all(8),
            child: SmoothPageIndicator(
              controller: linkPageIndicatorController,
              count: urlLinks.length,
              effect: WormEffect(
                activeDotColor: indicatorColor,
              ),
              onDotClicked: (index) {
                if (linkPageIndicatorController.hasClients) {
                  linkPageIndicatorController.jumpToPage(index);
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
