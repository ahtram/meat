part of 'youtube_fullscreen_cubit.dart';

@immutable
abstract class YoutubeFullscreenState {}

class YoutubeFullscreenInitial extends YoutubeFullscreenState {}

class YoutubeFullscreenEnterState extends YoutubeFullscreenState {
  YoutubeFullscreenEnterState(this.args);
  final YoutubeFullscreenViewerArgs args;
}

class YoutubeFullscreenLeaveState extends YoutubeFullscreenState {}