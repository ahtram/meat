import 'package:flutter/material.dart';

class PlurkCoinSign extends StatelessWidget {
  const PlurkCoinSign(this.coinNum, {Key key}) : super(key: key);

  final int coinNum;

  @override
  Widget build(BuildContext context) {
    int limitedCoinNum = coinNum > 99 ? 99 : coinNum;
    String coinTextStr = limitedCoinNum.toString();

    return Container(
      width: 22,
      height: 22,
      child: Stack(
        children: [
          Positioned.fill(
            child: Align(
              alignment: Alignment.center,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18.0),
                  border: Border.all(
                    color: Colors.white,
                    width: 2,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Transform.translate(
              offset: Offset(0, -1.5),
              child: Text(
                coinTextStr,
                textScaleFactor: 0.85,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1,
                overflow: TextOverflow.visible,
              ),
            ),
          )
        ],
      ),
    );
  }
}
