import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meat/theme/app_themes.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc()
      : super(ThemeState(
            appTheme: Static.settingsAppTheme,
            themeMode: Static.settingsThemeMode,
            textScale: Static.settingsTextScale,
            canvasColor: Static.settingsCanvasColor)) {
    on<ThemeChanged>((event, emit) => emit(ThemeState(
        appTheme: event.appTheme,
        themeMode: event.themeMode,
        textScale: event.textScale,
        canvasColor: event.canvasColor)));
  }
}
