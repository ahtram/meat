part of 'timeline_plurk_edit_cubit.dart';

@immutable
abstract class TimelinePlurkEditState {}

class TimelinePlurkEditInitial extends TimelinePlurkEditState {}

class TimelinePlurkEditSuccess extends TimelinePlurkEditState {
  TimelinePlurkEditSuccess(this.resultPlurk);
  final Plurdart.Plurk resultPlurk;
}

class TimelinePlurkEditFailed extends TimelinePlurkEditState {
  TimelinePlurkEditFailed(this.errorText);
  final String errorText;
}