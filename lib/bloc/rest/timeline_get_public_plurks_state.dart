part of 'timeline_get_public_plurks_cubit.dart';

@immutable
abstract class TimelineGetPublicPlurksState {}

class TimelineGetPublicPlurksInitial extends TimelineGetPublicPlurksState {}

class TimelineGetPublicPlurksSuccess extends TimelineGetPublicPlurksState {
  TimelineGetPublicPlurksSuccess(this.plurks);
  final Plurdart.Plurks plurks;
}

class TimelineGetPublicTrackBackSuccess extends TimelineGetPublicPlurksState {
  TimelineGetPublicTrackBackSuccess(this.plurks);
  final Plurdart.Plurks plurks;
}