class SpreadsheetsMetadata {
  String spreadsheetId;
  SpreadsheetProperties properties;
  List<Sheet> sheets;
  String spreadsheetUrl;

  SpreadsheetsMetadata({this.spreadsheetId, this.properties, this.sheets, this.spreadsheetUrl});

  SpreadsheetsMetadata.fromJson(Map<String, dynamic> json) {
    spreadsheetId = json['spreadsheetId'];
    properties = json['properties'] != null ? new SpreadsheetProperties.fromJson(json['properties']) : null;
    if (json['sheets'] != null) {
      sheets = [];
      json['sheets'].forEach((v) { sheets.add(new Sheet.fromJson(v)); });
    }
    spreadsheetUrl = json['spreadsheetUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['spreadsheetId'] = this.spreadsheetId;
    if (this.properties != null) {
      data['properties'] = this.properties.toJson();
    }
    if (this.sheets != null) {
      data['sheets'] = this.sheets.map((v) => v.toJson()).toList();
    }
    data['spreadsheetUrl'] = this.spreadsheetUrl;
    return data;
  }

}

class SpreadsheetProperties {
  String title;
  String locale;
  String autoRecalc;
  String timeZone;

  SpreadsheetProperties({this.title, this.locale, this.autoRecalc, this.timeZone});

  SpreadsheetProperties.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    locale = json['locale'];
    autoRecalc = json['autoRecalc'];
    timeZone = json['timeZone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['locale'] = this.locale;
    data['autoRecalc'] = this.autoRecalc;
    data['timeZone'] = this.timeZone;
    return data;
  }
}

class Sheet {
  SheetProperties properties;

  Sheet({this.properties});

  Sheet.fromJson(Map<String, dynamic> json) {
    properties = json['properties'] != null ? new SheetProperties.fromJson(json['properties']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.properties != null) {
      data['properties'] = this.properties.toJson();
    }
    return data;
  }
}

class SheetProperties {
  int sheetId;
  String title;
  int index;
  String sheetType;
  GridProperties gridProperties;

  SheetProperties({this.sheetId, this.title, this.index, this.sheetType, this.gridProperties});

  SheetProperties.fromJson(Map<String, dynamic> json) {
    sheetId = json['sheetId'];
    title = json['title'];
    index = json['index'];
    sheetType = json['sheetType'];
    gridProperties = json['gridProperties'] != null ? new GridProperties.fromJson(json['gridProperties']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sheetId'] = this.sheetId;
    data['title'] = this.title;
    data['index'] = this.index;
    data['sheetType'] = this.sheetType;
    if (this.gridProperties != null) {
      data['gridProperties'] = this.gridProperties.toJson();
    }
    return data;
  }
}

class GridProperties {
  int rowCount;
  int columnCount;

  GridProperties({this.rowCount, this.columnCount});

  GridProperties.fromJson(Map<String, dynamic> json) {
    rowCount = json['rowCount'];
    columnCount = json['columnCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rowCount'] = this.rowCount;
    data['columnCount'] = this.columnCount;
    return data;
  }
}