import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TagsTrunkItem extends StatefulWidget {
  TagsTrunkItem(this.tag,
      {this.selected,
      this.showDeleteButton = false,
      this.onTap,
      this.onDeleteTap,
      Key key})
      : super(key: key);

  final String tag;
  final bool selected;
  final bool showDeleteButton;
  final Function onTap;
  final Function onDeleteTap;

  @override
  _TagsTrunkItemState createState() => _TagsTrunkItemState();
}

class _TagsTrunkItemState extends State<TagsTrunkItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: _leading(),
      title: Transform.translate(
        child: _title(),
        offset: const Offset(-16.0, 0.0),
      ),
      trailing: _trailing(),
      selected: widget.selected,
      onTap: () {
        widget.onTap?.call(widget.tag);
      },
    );
  }

  Widget _leading() {
    Color borderColor = widget.selected
        ? Theme.of(context).primaryColor
        : Theme.of(context).textTheme.bodyText1.color;
    return Container(
      padding: EdgeInsets.all(2),
      width: 34,
      height: 34,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        border: Border.all(color: borderColor, width: 3.0),
      ),
      child: Center(
        child: Transform.translate(
          child: Text(
            widget.tag.substring(0, 1),
            style: Theme.of(context)
                .textTheme
                .subtitle1
                .copyWith(fontWeight: FontWeight.bold, color: borderColor),
            textAlign: TextAlign.center,
          ),
          offset: const Offset(0.0, -2.5),
        ),
      ),
    );
  }

  Widget _title() {
    return Text(widget.tag.substring(1));
  }

  Widget _trailing() {
    if (widget.showDeleteButton) {
      return Container(
        width: 40,
        height: 40,
        child: InkWell(
          child: Icon(MdiIcons.minusBoxOutline),
          onTap: () { widget.onDeleteTap?.call(widget.tag); },
        ),
      );
    } else {
      //Nothing
      return null;
    }
  }
}
