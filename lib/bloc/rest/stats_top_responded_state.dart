part of 'stats_top_responded_cubit.dart';

@immutable
abstract class StatsTopRespondedState {}

class StatsTopRespondedInitial extends StatsTopRespondedState {}

class StatsTopRespondedSuccess extends StatsTopRespondedState {
  StatsTopRespondedSuccess(this.topPlurks);
  final Plurdart.TopPlurks topPlurks;
}

class StatsTopRespondedFailed extends StatsTopRespondedState { }