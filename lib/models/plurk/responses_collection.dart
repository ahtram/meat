
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:tuple/tuple.dart';

Future<List<Tuple2<Plurdart.Response, Plurdart.User>>> updateFeedCache(ResponsesCollection responsesCollection) async {
  responsesCollection.feedCache.clear();
  responsesCollection.responseMap.clear();
  for (int i = 0 ; i < responsesCollection.collection.length ; ++i) {
    List<Plurdart.Response> responses = responsesCollection.collection[i].responses;
    //Tricky! It is possible the different plurks contain same plurk! due to API nature.
    //Todo: check if every responses is unique. Filter out those already exist.
    for (int p = 0 ; p < responses.length ; ++p) {

      if (!responsesCollection.responseMap.containsKey(responses[p].id)) {
        //Not exist yet! Add this

        //Map
        responsesCollection.responseMap[responses[p].id] = responses[p];

        //FeedCache
        Plurdart.User responseOwner = responsesCollection.collection[i].friends[responses[p].userId];

        responsesCollection.feedCache.add(Tuple2(responses[p], responseOwner));
      }//Already exist! Ignore this one.
    }
  }
  return responsesCollection.feedCache;
}

//[Dep]: Replaced by ResponsesCollector.
class ResponsesCollection {
  ResponsesCollection();

  // All of the results goes here.

  //The raw collection of responses...note that these may contains same response.
  List<Plurdart.Responses> collection = [];

  //A Map which index by response's Id. This could be use to check any duplicate response.
  //This is mainly for handle Plurk's crazy responseGet API which give weird behavior.
  Map<int, Plurdart.Response> responseMap = Map<int, Plurdart.Response>();

  //The actual displayable data.
  List<Tuple2<Plurdart.Response, Plurdart.User>> feedCache = [];

  // Add a new result here.
  Future add(Plurdart.Responses responses) async {
    collection.add(responses);
    feedCache = await compute(updateFeedCache, this);
    // _updateFeedCache();
  }

  Future update(int plurkId, int responseId, Plurdart.ResponseEditResult responseEditResult) async {
    //Search for the plurkId and responseId
    collection.forEach((responses) {
      responses.responses.forEach((response) {
        if (response.id == responseId && response.plurkId == plurkId) {
          //Update the content.
          response.content = responseEditResult.content;
          response.contentRaw = responseEditResult.contentRaw;
          response.lastEdited = responseEditResult.lastEdited;
          response.editability = responseEditResult.editability;
        }
      });
    });

    feedCache = await compute(updateFeedCache, this);
    // _updateFeedCache();
  }

  // void _updateFeedCache() {
  //   feedCache.clear();
  //   responseMap.clear();
  //   for (int i = 0 ; i < collection.length ; ++i) {
  //     List<Plurdart.Response> responses = collection[i].responses;
  //     //Tricky! It is possible the different plurks contain same plurk! due to API nature.
  //     //Todo: check if every responses is unique. Filter out those already exist.
  //     for (int p = 0 ; p < responses.length ; ++p) {
  //
  //       if (!responseMap.containsKey(responses[p].id)) {
  //         //Not exist yet! Add this
  //
  //         //Map
  //         responseMap[responses[p].id] = responses[p];
  //
  //         //FeedCache
  //         Plurdart.User responseOwner = collection[i].friends[responses[p].userId];
  //
  //         feedCache.add(Tuple2(responses[p], responseOwner));
  //       }//Already exist! Ignore this one.
  //     }
  //   }
  // }

  // Get the current bottom offset. (time)
  DateTime getCurrentOffset() {
    if (feedCache.length > 0) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv').parse(feedCache.last.item1.posted);
      } catch (e) {
        print('Oops! Cannot parse posted time: ' + feedCache.last.item1.posted);
        return DateTime.now();
      }
    }
    return DateTime.now();
  }

  // Clear all.
  void clear() {
    collection.clear();
    responseMap.clear();
    feedCache.clear();
  }

  //When user delete his own response.
  deleteResponse(int responseId) {
    //_collection
    for (int i = 0 ; i < collection.length ; ++i) {
      collection[i].responses.removeWhere((element) => element.id == responseId);
    }

    //_responseMap
    responseMap.removeWhere((key, value) => value.id == responseId);

    //_feedCache
    feedCache.removeWhere((element) => element.item1.id == responseId);
  }

  // A convenient function for getting the necessary info for display home feed.
  List<Tuple2<Plurdart.Response, Plurdart.User>> getFeed() {
    return feedCache;
  }

  // How many Responses do we need?
  int getResponsesLength() {
    return feedCache.length;
  }

}