part of 'plurk_viewer_modal_prep_cubit.dart';

@immutable
abstract class PlurkViewerModalPrepState {}

class PlurkViewerModalPrepInitial extends PlurkViewerModalPrepState {}

class PlurkViewerModalPrepComplete extends PlurkViewerModalPrepState {}

class PlurkViewerModalPrepFailed extends PlurkViewerModalPrepState {
  PlurkViewerModalPrepFailed(this.errorText);
  final String errorText;
}