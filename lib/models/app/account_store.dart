import 'dart:core';

//A modal class we use the store not only all logged in account info but also the
//current active(using) account index.
//AccountKeep (the manager APIs) should be provide APIs for ease of use.
//Note this modal class doesn't response to any save/load logic.
class AccountStore {

  //Store the logged-in accounts.
  List<AccountTrunk> _accounts;
  //Store the currently activated account index.
  int _activatedAccountIndex;

  AccountStore() : _accounts = [], _activatedAccountIndex = 0;

  AccountStore.fromJson(Map<String, dynamic> json) {
    _activatedAccountIndex = json['activatedAccountIndex'];

    if (json['accounts'] != null) {
      _accounts = [];
      List<dynamic> dataList = json['accounts'];
      if (dataList != null) {
        dataList.forEach((element) {
          _accounts.add(AccountTrunk.fromJson(element));
        });
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['activatedAccountIndex'] = this._activatedAccountIndex;

    List<dynamic> trunkList = [];
    if (this._accounts != null) {
      this._accounts.forEach((at) {
        trunkList.add(at.toJson());
      });

      data['accounts'] = _accounts;
    }
    return data;
  }

  //Try add an account.
  void addAccount(String nickName, int userId, String accessToken, String tokenSecret, { bool andSetActive = true } ) {
    //If an account already exist. We'll just modify it.
    for (int i = 0 ; i < _accounts.length ; ++i) {
      if (_accounts[i].nickName == nickName) {
        _accounts[i].accessToken = accessToken;
        _accounts[i].tokenSecret = tokenSecret;
        return;
      }
    }

    //No exist account found. Add a new one.
    _accounts.add(AccountTrunk(nickName, userId, accessToken, tokenSecret));

    //Also set it as active?
    if (andSetActive) {
      setAccountActive(nickName);
    }
  }

  //Try to set an account as active.
  bool setAccountActive(String nickName) {
    for (int i = 0 ; i < _accounts.length ; ++i) {
      if (_accounts[i].nickName == nickName) {
        _activatedAccountIndex = i;
        return true;
      }
    }
    return false;
  }

  //Set an account active.
  bool setIndexActive(int index) {
    if (index >= 0 && index < _accounts.length) {
      _activatedAccountIndex = index;
      return true;
    }
    return false;
  }

  //Is an account with the nickName already exist?
  bool isAccountExist(String nickName) {
    for (int i = 0 ; i < _accounts.length ; ++i) {
      if (_accounts[i].nickName == nickName) {
        return true;
      }
    }
    return false;
  }

  //Try remove an account.
  bool removeAccount(String nickName) {
    if (isAccountExist(nickName)) {
      _accounts.removeWhere((at) => at.nickName == nickName);
      return true;
    }
    return false;
  }

  //Try remove to activated account.
  bool removeActivatedAccount() {
    if (_activatedAccountIndex >= 0 && _activatedAccountIndex < _accounts.length) {
      _accounts.removeAt(_activatedAccountIndex);
      if (_activatedAccountIndex >= _accounts.length && _activatedAccountIndex > 0) {
        _activatedAccountIndex -= 1;
      }
      return true;
    }
    return false;
  }

  //Get the activated account index.
  int getActivatedAccountIndex() {
    return _activatedAccountIndex;
  }

  //Get the activated account or any usable account if the active one is out of index. (possible return null)
  AccountTrunk getActivatedOrAnUsableAccount() {
    if (_activatedAccountIndex >= 0 && _activatedAccountIndex < _accounts.length) {
      //Make a copy to prevent from stupid monkey modified behavior.
      AccountTrunk at = _accounts[_activatedAccountIndex];
      return AccountTrunk(at.nickName, at.userId, at.accessToken, at.tokenSecret);
    } else if (_accounts.length > 0) {
      //Just use the last one.
      _activatedAccountIndex= _accounts.length - 1;
      AccountTrunk at = _accounts[_activatedAccountIndex];
      return AccountTrunk(at.nickName, at.userId, at.accessToken, at.tokenSecret);
    }
    return null;
  }

  //Get the full nick name list of the logged in accounts.
  List<String> getAccountNickNameList() {
    return _accounts.map((at) => at.nickName).toList();
  }

}

//Represent one use account has logged in success.
class AccountTrunk {
  String nickName;
  int userId;
  String accessToken;
  String tokenSecret;

  AccountTrunk(this.nickName, this.userId, this.accessToken, this.tokenSecret);

  AccountTrunk.fromJson(Map<String, dynamic> json) {
    nickName = json['nickName'];
    userId = json['userId'];
    accessToken = json['accessToken'];
    tokenSecret = json['tokenSecret'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nickName'] = this.nickName;
    data['userId'] = this.userId;
    data['accessToken'] = this.accessToken;
    data['tokenSecret'] = this.tokenSecret;
    return data;
  }
}
