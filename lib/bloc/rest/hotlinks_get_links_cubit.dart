import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'hotlinks_get_links_state.dart';

class HotlinksGetLinksCubit extends Cubit<HotlinksGetLinksState> {
  HotlinksGetLinksCubit() : super(HotlinksGetLinksInitial());
  
  get() async {
    List<Plurdart.HotLink> hotlinks = await Plurdart.hotlinksGetLinks(Plurdart.HotLinksGetLinks());
    if (hotlinks != null) {
      emit(HotlinksGetLinksSuccess(hotlinks));
    } else {
      emit(HotlinksGetLinksFailed());
    }
  }

  trackBack({int offset}) async {
    List<Plurdart.HotLink> hotlinks = await Plurdart.hotlinksGetLinks(Plurdart.HotLinksGetLinks(offset: offset));
    if (hotlinks != null) {
      emit(HotlinksTrackBackLinksSuccess(hotlinks));
    } else {
      emit(HotlinksTrackBackLinksFailed());
    }
  }

}
