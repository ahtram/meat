import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'emoticons_delete_state.dart';

class EmoticonsDeleteCubit extends Cubit<EmoticonsDeleteState> {
  EmoticonsDeleteCubit() : super(EmoticonsDeleteInitial());

  request(String url) async {
    Plurdart.Base base = await Plurdart.emoticonsDelete(Plurdart.EmoticonsDelete(
      url: url,
    ));

    if (base != null) {
      if (!base.hasError()) {
        emit(EmoticonsDeleteSuccess());
      } else {
        emit(EmoticonsDeleteFailed(base.errorText));
      }
    } else {
      emit(EmoticonsDeleteFailed('Unknown error.'));
    }

  }
}
