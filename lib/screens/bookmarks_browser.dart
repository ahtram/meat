import 'package:flutter/material.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/widgets/bookmark/tags_trunk_item.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/plurk/plurk_cards_viewer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookmarksBrowser extends StatefulWidget {
  BookmarksBrowser(this.browsingTag, {Key key}) : super(key: key);

  final String browsingTag;

  @override
  _BookmarksBrowserState createState() => _BookmarksBrowserState();
}

class _BookmarksBrowserState extends State<BookmarksBrowser> {

  BuildContext scaffoldBodyContext;

  @override
  Widget build(BuildContext context) {

    Widget titleWidget = Container();
    if (widget.browsingTag == null || widget.browsingTag.isEmpty) {
      //All bookmarks
      titleWidget = TagsTrunkItem(
        'All Bookmarks',
        selected: false,
      );
    } else {
      //A Tag name
      titleWidget = TagsTrunkItem(
        widget.browsingTag,
        selected: false,
      );
    }

    return  MultiBlocListener(
      listeners: [
        BlocListener<FlushBarCubit, FlushBarState>(
          listener: (context, state) {
            if (state is FlushBarRequest) {
              //Only display FlushBar when I am the current route.
              if (ModalRoute.of(context).isCurrent) {
                _showFlushBar(scaffoldBodyContext, state.icon, state.title,
                    state.message);
              }
            }
          },
        ),
      ],
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          brightness: Theme.of(context).brightness,
          backgroundColor: Colors.transparent,
          flexibleSpace: AcrylicContainer(
            Container(),
            backgroundColor: Theme.of(context).primaryColor,
            transparentBackLayer: false,
            mode: AcrylicContainerMode.BackdropFilter,
          ),
          elevation: 0,
          titleSpacing: 0.0,
          leading: AppBarActionButton(Icons.arrow_back, onTap: () {
            Navigator.pop(context);
          }),
          automaticallyImplyLeading: false,
          title: titleWidget,
        ),
        body: Builder(
          builder: (context) {
            scaffoldBodyContext = context;
            return Container(
              // color: Theme.of(context).canvasColor.withOpacity(0.4),
              child: Column(
                children: [
                  Expanded(
                    child: //Anonymous.
                    PlurkCardsViewer(
                      PlurkCardsViewerArgs(
                        BrowsingType.Bookmarks,
                        true,
                        hasAppBar: false,
                        bookmarkTag: widget.browsingTag,
                      ),
                    ),
                  )
                ],
              ),
            );
          },
        ),

      ),
    );
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }

}