
class App {
  String key;
  String secret;

  App({this.key, this.secret});

  App.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    secret = json['secret'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['secret'] = this.secret;
    return data;
  }
}