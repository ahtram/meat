import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/rest/alerts_get_active_cubit.dart';
import 'package:meat/bloc/rest/alerts_get_composite_cubit.dart';
import 'package:meat/bloc/rest/alerts_get_history_cubit.dart';
import 'package:meat/bloc/ui/alert_number_changed_cubit.dart';
import 'package:meat/bloc/ui/plurk_viewer_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/screens/plurk_viewer.dart';
import 'package:meat/screens/user_viewer.dart';
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';
import 'package:meat/widgets/plurk/response_card_sub_header.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

enum AlertMode {
  Requests,
  Notifications,
  Composite,
}

class AlertViewer extends StatefulWidget {
  AlertViewer(this.mode, {Key key}) : super(key: key);

  final AlertMode mode;

  @override
  _AlertViewerState createState() => _AlertViewerState();
}

class _AlertViewerState extends State<AlertViewer> with AfterLayoutMixin {
  BuildContext _blocProviderContext;
  RefreshController _refreshController = RefreshController();

  List<Plurdart.Alert> _alerts = [];

  bool _isRequestingRefresh = false;

  //This is a temp var for display one time highlight. (update after refreshed)
  int _highlightCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    if (mounted) {
      //Request a refresh first.
      _refreshController.requestRefresh();
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<RequestIndicatorCubit>(
          create: (BuildContext context) => RequestIndicatorCubit(),
        ),
        BlocProvider<AlertsGetActiveCubit>(
          create: (BuildContext context) => AlertsGetActiveCubit(),
        ),
        BlocProvider<AlertsGetHistoryCubit>(
          create: (BuildContext context) => AlertsGetHistoryCubit(),
        ),
        BlocProvider<AlertsGetCompositeCubit>(
          create: (BuildContext context) => AlertsGetCompositeCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<AlertsGetActiveCubit,
              AlertsGetActiveState>(
            listener: (context, state) {
              if (state is AlertsGetActiveSuccess) {
                _onGetAlerts(state.alerts);
              } else if (state is AlertsGetActiveFailed) {
                _onGetAlerts(null);
              }
            },
          ),
          BlocListener<AlertsGetHistoryCubit,
              AlertsGetHistoryState>(
            listener: (context, state) {
              if (state is AlertsGetHistorySuccess) {
                _onGetAlerts(state.alerts);
              } else if (state is AlertsGetHistoryFailed) {
                _onGetAlerts(null);
              }
            },
          ),
          BlocListener<AlertsGetCompositeCubit,
              AlertsGetCompositeState>(
            listener: (context, state) {
              if (state is AlertsGetCompositeSuccess) {
                _onGetAlerts(state.alerts);
              } else if (state is AlertsGetCompositeFailed) {
                _onGetAlerts(null);
              }
            },
          ),
        ],
        child: Container(
          //We'll need this smartRefresh thing here...
          child: Builder(builder: (context) {
            _blocProviderContext = context;

            Color indicatorColor = Theme.of(context).textTheme.bodyText1.color;

            return SmartRefresher(
              controller: _refreshController,
              header: WaterDropMaterialHeader(
                color: indicatorColor,
              ),
              onRefresh: _onRefresh,
              child: ListView(
                controller: PrimaryScrollController.of(context),
                children: _buildListViewItems(),
              ),
              //[Dep]: Change to ListView
              // child: ListView.builder(
              //   controller: _scrollController,
              //   itemBuilder: _buildUserListViewItem,
              // ),
            );
          }),
        ),
      ),
    );
  }

  List<Widget> _buildListViewItems() {
    List<Widget> returnList = [];

    for(int i = 0 ; i < _alerts.length ; ++i) {
      bool highlight = i < _highlightCount ? true : false;
      returnList.add(_listViewItem(_alerts[i], highlight));
    }

    returnList.add(SizedBox(
      height: kMinInteractiveDimension,
    ),);

    return returnList;
  }

  Widget _listViewItem(Plurdart.Alert alert, bool highlight) {
    List<Widget> rowChildren = [];

    //Avatar.
    rowChildren.add(SizedBox(
      width: 56,
      height: 56,
      child: PlurkCardPrimeHeaderState.headerAvatar(
        context,
        alert.getUser(),
        margin: EdgeInsets.all(4),
        onTap: (user) {
          _onAlertTap(alert);
        },
      ),
    ));

    rowChildren.add(SizedBox(
      width: 8,
    ));

    //Display Name.
    rowChildren.add(Expanded(
      child: SizedBox(
        height: 48,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PlurkCardPrimeHeaderState.buildUserDisplayName(context, null, alert.getUser(), false, (user) {
              _onAlertTap(alert);
            },),

            //Alert description.
            Text(
              getAlertDesc(context, alert.alertType()),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    ));

    rowChildren.add(SizedBox(
      width: 8,
    ));

    //Posted time
    rowChildren.add(ResponseCardSubHeader.buildHeaderTime(context, alert.posted));

    return InkWell(
      child: Container(
          color: highlight ? Theme.of(context).colorScheme.secondary.withOpacity(0.2) : Colors.transparent,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: rowChildren,
          )),
      onTap: () {
        _onAlertTap(alert);
      },
    );
  }

  //When an alert item is tapped.
  _onAlertTap(Plurdart.Alert alert) {
    // Open user viewer or plurk viewer by different type.
    switch (alert.alertType()) {
      case Plurdart.AlertType.FriendshipRequest:
      case Plurdart.AlertType.FriendshipPending:
      case Plurdart.AlertType.NewFan:
      case Plurdart.AlertType.FriendshipAccepted:
      case Plurdart.AlertType.NewFriend:
        //Open user viewer.
        Plurdart.User user = alert.getUser();
        if (user != null) {
          _blocProviderContext.read<UserViewerCubit>().open(UserViewerArgs(
            user.nickName,
          ));
        }
        break;

      case Plurdart.AlertType.PrivatePlurk:
      case Plurdart.AlertType.PlurkLiked:
      case Plurdart.AlertType.PlurkReplurked:
      case Plurdart.AlertType.Mentioned:
      case Plurdart.AlertType.MyResponded:
        //Open plurk viewer.
        if(alert.plurkId != null) {
          _blocProviderContext.read<PlurkViewerCubit>().open(PlurkViewerArgs(
            PlurkViewerMode.FromPlurkId,
            plurkId: alert.plurkId,
          ));
        }
        break;

      case Plurdart.AlertType.Unknown:
        //Do nothing...
        break;
    }
  }

  // Get an alert's display description by type.
  static String getAlertDesc(BuildContext context, Plurdart.AlertType alertType) {
    switch (alertType) {
      case Plurdart.AlertType.FriendshipRequest:
        return FlutterI18n.translate(
                context, 'alertDescFriendshipRequest');
      case Plurdart.AlertType.FriendshipPending:
        return FlutterI18n.translate(
                context, 'alertDescFriendshipPending');
      case Plurdart.AlertType.NewFan:
        return FlutterI18n.translate(
                context, 'alertDescNewFan');
      case Plurdart.AlertType.FriendshipAccepted:
        return FlutterI18n.translate(
                context, 'alertDescFriendshipAccepted');
      case Plurdart.AlertType.NewFriend:
        return FlutterI18n.translate(
                context, 'alertDescNewFriend');
      case Plurdart.AlertType.PrivatePlurk:
        return FlutterI18n.translate(
                context, 'alertDescPrivatePlurk');
      case Plurdart.AlertType.PlurkLiked:
        return FlutterI18n.translate(
                context, 'alertDescPlurkLiked');
      case Plurdart.AlertType.PlurkReplurked:
        return FlutterI18n.translate(
                context, 'alertDescPlurkReplurked');
      case Plurdart.AlertType.Mentioned:
        return FlutterI18n.translate(
                context, 'alertDescMentioned');
      case Plurdart.AlertType.MyResponded:
        return FlutterI18n.translate(
                context, 'alertDescMyResponded');
      case Plurdart.AlertType.Unknown:
        return FlutterI18n.translate(
                context, 'alertDescUnknown');
    }

    return '???';
  }

  _onRefresh() {
    _refreshAlerts();
  }

  // The will clear the whole list and start from the beginning.
  _refreshAlerts() {
    if (!_isRequestingRefresh) {
      _isRequestingRefresh = true;

      //Which request do we need?
      if (widget.mode == AlertMode.Requests) {
        _blocProviderContext.read<AlertsGetActiveCubit>().request();
      } else if (widget.mode == AlertMode.Notifications) {
        _blocProviderContext.read<AlertsGetHistoryCubit>().request();
      } else if (widget.mode == AlertMode.Composite) {
        _blocProviderContext.read<AlertsGetCompositeCubit>().request();
      }

      //No offset means get from the oldest!.
    } else {
      _refreshController.refreshCompleted();
    }
  }

  _onGetAlerts(List<Plurdart.Alert> alerts) {
    if (alerts != null) {
      setState(() {
        // print('_onGetAlerts() alerts.length: ' + alerts.length.toString());
        _refreshController.refreshCompleted();
        _blocProviderContext.read<RequestIndicatorCubit>().hide();

        _alerts.clear();

        //The will add the collection length.
        _alerts.addAll(alerts);

        //Check one time display highlight.
        if (widget.mode == AlertMode.Requests) {
          if (Static.req > 0) {
            _highlightCount = Static.req;
            //Auto mark as read request.
            _blocProviderContext.read<AlertNumberChangedCubit>().request(req: 0);
          } else {
            _highlightCount = 0;
          }
        } else if (widget.mode == AlertMode.Notifications) {
          if (Static.noti > 0) {
            _highlightCount = Static.noti;
            //Auto mark as read notifications.
            _blocProviderContext.read<AlertNumberChangedCubit>().request(noti: 0);
          } else {
            _highlightCount = 0;
          }
        } else if (widget.mode == AlertMode.Composite) {
          _highlightCount = 0;
          if (Static.req > 0) {
            _highlightCount += Static.req;
          }
          if (Static.noti > 0) {
            _highlightCount += Static.noti;
          }

          if (Static.req > 0 || Static.noti > 0) {
            _blocProviderContext.read<AlertNumberChangedCubit>().request(noti: 0, req: 0);
          }
        }

      });
    } else {
//      print('refreshFailed()');
      _refreshController.refreshFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }

    _isRequestingRefresh = false;
  }

}