import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/rest/blocks_block_cubit.dart';
import 'package:meat/bloc/rest/blocks_unblock_cubit.dart';
import 'package:meat/bloc/rest/profile_get_own_profile_cubit.dart';
import 'package:meat/bloc/rest/profile_get_public_profile_cubit.dart';
import 'package:meat/bloc/rest/timeline_get_public_plurks_cubit.dart';
import 'package:meat/bloc/rest/timeline_plurk_delete_cubit.dart';
import 'package:meat/bloc/rest/users_update_cubit.dart';
import 'package:meat/bloc/ui/plurk_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/viewer_background_switch_cubit.dart';
import 'package:meat/models/plurk/plurks_collection.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/plurk/plurk_card.dart';
import 'package:meat/widgets/plurk/user_card.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:meat/screens/plurk_poster.dart' as PlurkPoster;
import 'package:meat/system/vibrator.dart' as Vibrator;

void show(BuildContext context, UserViewerArgs args) {
  Navigator.push(context,
      new MaterialPageRoute(builder: (BuildContext context) {
    return _UserViewer(args);
  }));
}

class UserViewerArgs {
  //FromUserNickName
  final String userNickName;

  UserViewerArgs(this.userNickName);
}

class _UserViewer extends StatefulWidget {
  _UserViewer(this.args, {Key key}) : super(key: key);

  final UserViewerArgs args;

  @override
  _UserViewerState createState() => _UserViewerState();
}

class _UserViewerState extends State<_UserViewer> with AfterLayoutMixin {
  GlobalKey<AcrylicContainerState> _backgroundAcrylicContainerKey =
      GlobalKey<AcrylicContainerState>();

  BuildContext _blocProviderContext;

  RefreshController _refreshController = RefreshController();

  //We'll need a fancy class to store Plurks.
  PlurksCollection _plurksCollection = PlurksCollection();
  //This could be null if the user doesn't has a pinned plurk.
  PlurkFeed _pinnedPlurk;

  bool _timelineUpdateError = false;
  bool _isRequestingFeed = false;
  //Is case we need this...
  bool _noHistoryRemained = false;

  bool _showNonReplurkOnly = false;

  Plurdart.Profile profile;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    if (_blocProviderContext != null) {
      if (Static.me != null && widget.args.userNickName == Static.me.nickName) {
        //Get own profile.
        _blocProviderContext.read<ProfileGetOwnProfileCubit>().request();
      } else {
        //Get public profile.
        _blocProviderContext
            .read<ProfileGetPublicProfileCubit>()
            .request(widget.args.userNickName);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<RequestIndicatorCubit>(
          create: (BuildContext context) => RequestIndicatorCubit(),
        ),
        //For PlurkCard switch PlurkViewer background.
        BlocProvider<ViewerBackgroundSwitchCubit>(
          create: (BuildContext context) => ViewerBackgroundSwitchCubit(),
        ),
        BlocProvider<ProfileGetOwnProfileCubit>(
          create: (BuildContext context) => ProfileGetOwnProfileCubit(),
        ),
        BlocProvider<ProfileGetPublicProfileCubit>(
          create: (BuildContext context) => ProfileGetPublicProfileCubit(),
        ),
        BlocProvider<TimelineGetPublicPlurksCubit>(
          create: (BuildContext context) => TimelineGetPublicPlurksCubit(),
        ),
        BlocProvider<UsersUpdateCubit>(
          create: (BuildContext context) => UsersUpdateCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<ProfileGetOwnProfileCubit, ProfileGetOwnProfileState>(
            listener: (context, state) {
              if (state is ProfileGetOwnProfileSuccess) {
                setState(() {
                  profile = state.profile;
                  //This will give us the first plurk list of this user.
                  //[Dep]: This one will not provide enough User data.
                  // _plurksCollection.addPlurkList(profile.plurks);

                  //Self profile does not need to display plurks. Because the home feed actually better.
                  //Start the first time refresh.
                  _refreshController.requestRefresh(
                      needMove: false, duration: Duration(milliseconds: 200));
                });
              }
            },
          ),
          BlocListener<ProfileGetPublicProfileCubit,
              ProfileGetPublicProfileState>(
            listener: (context, state) {
              if (state is ProfileGetPublicProfileSuccess) {
                setState(() {
                  profile = state.profile;
                  //Start the first time refresh.
                  _refreshController.requestRefresh(
                      needMove: false, duration: Duration(milliseconds: 200));
                });
              }
            },
          ),
          BlocListener<TimelineGetPublicPlurksCubit,
              TimelineGetPublicPlurksState>(
            listener: (context, state) {
              if (state is TimelineGetPublicPlurksSuccess) {
                //Update the plurks data and setState for the ListView.builder.
                _onTimelineGetPlurksState(state);
              } else if (state is TimelineGetPublicTrackBackSuccess) {
                //Update the plurks data and setState for the ListView.builder.
                _onTimelineTrackBackState(state);
              }
            },
          ),
          BlocListener<TimelinePlurkDeleteCubit, TimelinePlurkDeleteState>(
            listener: (context, state) {
              if (state is TimelinePlurkDeleteSuccess) {
                //Try remove the plurk
                if (_plurksCollection != null) {
                  _plurksCollection.deletePlurk(state.plurkId);
                }
                if (_pinnedPlurk != null &&
                    _pinnedPlurk.plurk.plurkId == state.plurkId) {
                  _pinnedPlurk = null;
                }
                setState(() {});
              }
            },
          ),
          BlocListener<UsersUpdateCubit, UsersUpdateState>(
            listener: (context, state) {
              if (state is UsersUpdatePinPlurkSuccess) {
                Static.me.pinnedPlurkId = state.user.pinnedPlurkId;
                _refreshPinnedPlurk();
              } else if (state is UsersUpdateUnpinPlurkSuccess) {
                Static.me.pinnedPlurkId = 0;
                _pinnedPlurk = null;
                setState(() {});
              }
            },
          ),
          BlocListener<PlurkEditCompleteCubit, PlurkEditCompleteState>(
            listener: (context, state) {
              if (state is PlurkEditCompleteSuccess) {
                _onPlurkEditCompleteSuccess(state);
              }
            },
          ),
          BlocListener<BlocksBlockCubit, BlocksBlockState>(
            listener: (context, state) {
              if (state is BlocksBlockSuccess) {
                setState(() {
                  //This will hide/un-hide feed for blocked user
                });
              }
            },
          ),
          BlocListener<BlocksUnblockCubit, BlocksUnblockState>(
            listener: (context, state) {
              if (state is BlocksUnblockSuccess) {
                setState(() {
                  //This will hide/un-hide feed for blocked user
                });
              }
            },
          ),
        ],
        child: Builder(
          builder: (context) {
            //Try get the background image of Profile.
            //This is for CardStateMode only. (This could be null in FromPlurkId mode)
            ImageProvider backgroundProvider;

            if (profile != null && profile.userInfo.hasBackground()) {
              // print('[UserViewer Builder] background [' + profile.userInfo.backgroundUrl() + ']');
              backgroundProvider =
                  CachedNetworkImageProvider(profile.userInfo.backgroundUrl());
            } else {
              // print('[UserViewer Builder] no background');
            }

            return Scaffold(
              backgroundColor: Theme.of(context).canvasColor,
              resizeToAvoidBottomInset: false,
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.miniEndFloat,
              //Display load indicator on the floating action button place.
              floatingActionButton:
                  BlocBuilder<RequestIndicatorCubit, RequestIndicatorState>(
                builder: (context, state) {
                  //Check state.
                  List<Widget> columnChildren = [];

                  //Only when showing request indicator.
                  if (state is RequestIndicatorShow) {
                    //Busy indicator.
                    columnChildren.add(Container(
                      width: 56,
                      height: 56,
                      child: FlareActor(
                        "assets/rive/MeatLoader.flr",
                        animation: "Untitled",
                      ),
                    ));
                  }

                  //Bottom margin.
                  columnChildren.add(SizedBox(
                    height: MediaQuery.of(context).viewInsets.bottom,
                  ));

                  return Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: columnChildren,
                  );
                },
              ),

              body: AcrylicContainer(
                Builder(
                  builder: (context) {
                    _blocProviderContext = context;

                    Color indicatorColor =
                        Theme.of(context).textTheme.bodyText1.color;

                    return NotificationListener<ScrollNotification>(
                        child: SmartRefresher(
                          controller: _refreshController,
                          header: WaterDropMaterialHeader(
                            color: indicatorColor,
                          ),
                          //The pull up footer could cause crash when rebuild (something about layout error) so we decide not to use it.
//                        footer: ClassicFooter(
//                          height: 40,
//                          outerBuilder: (Widget child) {
//                            return Container(
//                              height: 40,
//                              child: Center(child: child),
//                            );
//                          },
//                        ),
//                        enablePullUp: false,
//                        onLoading: _onLoading,

                          onRefresh: _onRefresh,
                          child: CustomScrollView(
                            controller: PrimaryScrollController.of(context),
                            slivers: _customScrollViewSliverWidgets(),
                          ),
                        ),
                        onNotification: (scrollNotification) {
                          double triggerFetchMoreSize =
                              0.75 * scrollNotification.metrics.maxScrollExtent;

                          if (scrollNotification.metrics.pixels >
                              triggerFetchMoreSize) {
                            // call fetch more method here
                            _trackBackPlurkTimeline();
                          }

                          return true;
                        });
                  },
                ),
                //The background image provider could be null.
                initialImage: backgroundProvider,
                transparentBackLayer: false,
                key: _backgroundAcrylicContainerKey,
              ),
            );
          },
        ),
      ),
    );
  }

  //The sliverWidgets for main custom view
  List<Widget> _customScrollViewSliverWidgets() {
    List<Widget> returnWidgets = [];

    //SliverAppBar. (User nickname)
    returnWidgets.add(SliverAppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      floating: true,
      flexibleSpace: AcrylicContainer(
        Container(),
        backgroundColor: Theme.of(context).primaryColor,
        transparentBackLayer: false,
        mode: AcrylicContainerMode.BackdropFilter,
      ),
      leading: AppBarActionButton(Icons.arrow_back, onTap: () {
        Navigator.pop(context);
      }),
      titleSpacing: 0.0,
      centerTitle: true,
      title: Text(
        '@' + widget.args.userNickName,
        style: TextStyle(
            fontSize: 16, color: Theme.of(context).textTheme.bodyText1.color),
      ),
    ));

    //UserCard + PlurkCards
    //Check if user modal is ready.
    if (profile != null) {
      returnWidgets.add(SliverList(
          delegate: SliverChildListDelegate(
        _buildUserCardAndPlurkCards(),
      )));
    } else {
      //Empty view
      returnWidgets.add(SliverList(
        delegate: SliverChildListDelegate([
          //Empty list.
        ]),
      ));
    }

    returnWidgets.add(SliverToBoxAdapter(
      child: Container(
        height: MediaQuery.of(context).viewInsets.bottom,
      ),
    ));

    return returnWidgets;
  }

  List<Widget> _buildUserCardAndPlurkCards() {
    List<Widget> returnWidgets = [];

    //UserCard.
    if (profile != null) {
      returnWidgets.add(UserCard(profile));
    }

    if (profile.blockedByMe != null && profile.blockedByMe) {
      //Hide the feed if this one is blocked by me.
    } else {
      //PlurkCards.
      List<PlurkFeed> feeds = _plurksCollection.feeds();

      //If the list is empty...
      if (feeds.length == 0) {
        //Display an empty logo?
        returnWidgets.add(Container(
            //Empty container.
            ));
      } else {
        //Add the switch mode button for all/nonReplurkOnly.
        String displayStr = '';
        if (_showNonReplurkOnly) {
          displayStr += '@' + profile.userInfo.nickName;
        } else {
          displayStr += 'All';
        }
        returnWidgets.add(_switchModeBar(displayStr, () {
          //Switch mode.
          setState(() {
            _showNonReplurkOnly = !_showNonReplurkOnly;
          });
          Vibrator.vibrateShortNote();
        }));

        //Show pinned plurk card if we have the data.
        if (_pinnedPlurk != null) {
          returnWidgets.add(PlurkCard(
              _pinnedPlurk.plurk,
              _pinnedPlurk.owner,
              _pinnedPlurk.replurker,
              _pinnedPlurk.favorers,
              _pinnedPlurk.replurkers,
              PlurkCardMode.FeedView,
              true,
              false,
              showPinIcon: true,
              hintPorn: Static.settingsTapToRevealAdultsOnlyContent,
              onWantEditPlurk: (plurk) {
            // print('Home onWantEditPlurk.');
            //Open Plurk Poster for edit post.
            PlurkPoster.show(
                context,
                PlurkPoster.PlurkPosterArgs(
                  PlurkPoster.PlurkPosterMode.Edit,
                  editingPlurk: plurk,
                ));
          }, onWantDeletePlurk: (plurk) {
            //onWantDeletePlurk
            // print('Home onWantDeletePlurk');
            _blocProviderContext
                .read<TimelinePlurkDeleteCubit>()
                .request(plurk.plurkId);
          }, onWantPinPlurk: (plurk) {
            _blocProviderContext
                .read<UsersUpdateCubit>()
                .pinPlurk(plurk.plurkId);
          }, onWantUnpinPlurk: (plurk) {
            _blocProviderContext
                .read<UsersUpdateCubit>()
                .unpinPlurk();
          }));
        }

        //Add feed PlurkCards
        feeds.forEach((feed) {
          bool showThisCard = false;
          //Filter out non replurk card if _showNonReplurkOnly
          if (!_showNonReplurkOnly || feed.replurker == null) {
            //Filtered out pinned card.
            if (profile.userInfo.pinnedPlurkId != feed.plurk.plurkId) {
              showThisCard = true;
            }
          }

          if (showThisCard) {
            returnWidgets.add(PlurkCard(
                feed.plurk,
                feed.owner,
                feed.replurker,
                feed.favorers,
                feed.replurkers,
                PlurkCardMode.FeedView,
                true,
                false,
                hintPorn: Static.settingsTapToRevealAdultsOnlyContent,
                onWantEditPlurk: (plurk) {
              // print('Home onWantEditPlurk.');
              //Open Plurk Poster for edit post.
              PlurkPoster.show(
                  context,
                  PlurkPoster.PlurkPosterArgs(
                    PlurkPoster.PlurkPosterMode.Edit,
                    editingPlurk: plurk,
                  ));
            }, onWantDeletePlurk: (plurk) {
              //onWantDeletePlurk
              // print('Home onWantDeletePlurk');
              _blocProviderContext
                  .read<TimelinePlurkDeleteCubit>()
                  .request(plurk.plurkId);
            }, onWantPinPlurk: (plurk) {
              _blocProviderContext
                  .read<UsersUpdateCubit>()
                  .pinPlurk(plurk.plurkId);
            }, onWantUnpinPlurk: (plurk) {
              _blocProviderContext
                  .read<UsersUpdateCubit>()
                  .unpinPlurk();
            }));
          }
        });
      }
    }

    return returnWidgets;
  }

  //Display response count widget.
  Widget _switchModeBar(String displayStr, Function onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        color: Theme.of(context).primaryColor.withOpacity(0.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 8,
            ),
            Text(
              displayStr,
              style: TextStyle(
                  color: Theme.of(context).hintColor,
                  fontSize: 13,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(height: 1, thickness: 1),
          ],
        ),
      ),
    );
  }

  //Refresh the PlurkCards.
  _onRefresh() async {
    //Try get the pinned plurk.
    await _refreshPinnedPlurk();

    _refreshPlurkTimeline();
  }

  _refreshPlurkTimeline() {
    if (profile != null && !_isRequestingFeed) {
      _isRequestingFeed = true;
      _noHistoryRemained = false;
      _timelineUpdateError = false;

      _blocProviderContext
          .read<TimelineGetPublicPlurksCubit>()
          .request(profile.userInfo.id);
      //No offset means get from the newest!.
    }
  }

  _refreshPinnedPlurk() async {
    if (profile != null &&
        profile.userInfo != null &&
        profile.userInfo.pinnedPlurkId != null &&
        profile.userInfo.pinnedPlurkId != 0) {
      Plurdart.PlurkWithUser plurkWithUser = await Plurdart.timelineGetPlurk(
          Plurdart.TimelineGetPlurk(
              plurkId: profile.userInfo.pinnedPlurkId,
              favorersDetail: true,
              limitedDetail: true,
              replurkersDetail: true));

      if (plurkWithUser != null && !plurkWithUser.hasError()) {
        setState(() {
          _pinnedPlurk = PlurkFeed(plurkWithUser.plurk, plurkWithUser.user,
              null, plurkWithUser.getFavorers(), plurkWithUser.getReplurkers());
        });
      }
    }
  }

  _trackBackPlurkTimeline() {
    if (profile != null &&
        !_isRequestingFeed &&
        !_noHistoryRemained &&
        !_timelineUpdateError) {
      _isRequestingFeed = true;

      _blocProviderContext.read<RequestIndicatorCubit>().show();
      _blocProviderContext.read<TimelineGetPublicPlurksCubit>().trackback(
            profile.userInfo.id,
            _plurksCollection.getBottomOffset(),
          );
      //No offset means get from the newest!.
    }
  }

  _onTimelineGetPlurksState(TimelineGetPublicPlurksSuccess state) async {
    _isRequestingFeed = false;

    //Update the plurks data and setState for the ListView.builder.
    // print('[Cubit response]: TimelineGetPlurksCubit');
    if (state.plurks != null) {
      if (state.plurks.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      //This is a refresh so we need to clear stuffs.
      _plurksCollection.clear();
      await _plurksCollection.add(state.plurks);
      setState(() {
        _timelineUpdateError = false;
      });
      _refreshController.refreshCompleted();
    } else {
      _refreshController.refreshFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }
  }

  _onTimelineTrackBackState(TimelineGetPublicTrackBackSuccess state) async {
    _blocProviderContext.read<RequestIndicatorCubit>().hide();
    _isRequestingFeed = false;

    //Update the plurks data and setState for the ListView.builder.
    // print('[Cubit response]: TimelineTrackBackState');
    if (state.plurks != null) {
      if (state.plurks.plurks.length == 0) {
        //Hmm... no history remained..
        _noHistoryRemained = true;
      }

      await _plurksCollection.add(state.plurks);
      setState(() {
        _timelineUpdateError = false;
      });
    } else {
      _refreshController.refreshFailed();
      print('Oops! Something goes wrong!!! Display an error logo.');
    }
  }

  _onPlurkEditCompleteSuccess(PlurkEditCompleteSuccess state) async {
    if (_pinnedPlurk != null &&
        _pinnedPlurk.plurk.plurkId == state.plurk.plurkId) {
      _pinnedPlurk.plurk.update(state.plurk);
    }
    await _plurksCollection.overridePlurk(state.plurk);
    setState(() {});
  }
}
