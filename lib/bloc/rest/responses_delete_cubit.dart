import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'responses_delete_state.dart';

//[Dep]: Replaced by ResponsesCollector.
class ResponsesDeleteCubit extends Cubit<ResponsesDeleteState> {
  ResponsesDeleteCubit() : super(ResponsesDeleteInitial());

  request(Plurdart.Response response) async {
    Plurdart.Base base = await Plurdart.responsesResponseDelete(Plurdart.ResponsesResponseDelete(
      plurkId: response.plurkId,
      responseId: response.id,
    ));

    if (base != null) {
      if (!base.hasError()) {
        emit(ResponsesDeleteSuccess(response.id));
      } else {
        emit(ResponsesDeleteFailed(base.errorText));
      }
    } else {
      emit(ResponsesDeleteFailed('Unknown error'));
    }

  }
}
