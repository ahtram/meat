import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/alert_browser/alert_viewer.dart';

class AlertBrowser extends StatefulWidget {
  AlertBrowser(/* this.initialTabIndex, */ {Key key,}) : super(key: key);

  //[Dep]: We don't need this anymore.
  // final int initialTabIndex;

  @override
  _AlertBrowserState createState() => _AlertBrowserState();
}

class _AlertBrowserState extends State<AlertBrowser> {

  @override
  Widget build(BuildContext context) {
    return AcrylicScaffold(
        transparentColor: Theme.of(context).canvasColor.withOpacity(0.4),
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          titleSpacing: 0.0,
          leading: Container(),
          automaticallyImplyLeading: true,
          actions: [
            AppBarActionButton(Icons.close, onTap: () {
              Navigator.pop(context);
            }),
            SizedBox(
              width: 4,
            )
          ],
          title: Text(
            FlutterI18n.translate(
                context, 'notifications'),
            style: TextStyle(
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
        child: AlertViewer(AlertMode.Composite)
    );
  }

  //[Dep]: 2 tabs version.
  // @override
  // Widget build(BuildContext context) {
  //   return DefaultTabController(
  //     length: 2,
  //     initialIndex: widget.initialTabIndex,
  //     child: AcrylicScaffold(
  //         appBar: AppBar(
  //           centerTitle: true,
  //           brightness: Theme.of(context).brightness,
  //           backgroundColor:
  //           Theme.of(context).canvasColor.withOpacity(0.4),
  //           elevation: 0,
  //           titleSpacing: 0.0,
  //           leading: null,
  //           automaticallyImplyLeading: false,
  //           actions: [
  //             AppBarActionButton(Icons.close, onTap: () {
  //               Navigator.pop(context);
  //             }),
  //             SizedBox(
  //               width: 2,
  //             )
  //           ],
  //           title: TabBar(
  //             tabs: [
  //               Tab(
  //                 text: 'Requests',
  //               ),
  //               Tab(
  //                 text: 'Notifications',
  //               ),
  //             ],
  //           ),
  //         ),
  //         child: Container(
  //           color: Theme.of(context).canvasColor.withOpacity(0.4),
  //           child: Column(
  //             children: [
  //               Expanded(
  //                 child: Container(
  //                   child: TabBarView(
  //                     children: [
  //                       //Requests viewer
  //                       AlertViewer(AlertMode.Requests),
  //                       //Notifications viewer
  //                       AlertViewer(AlertMode.Notifications),
  //                     ],
  //                   ),
  //                 ),
  //               )
  //             ],
  //           ),
  //         )),
  //   );
  // }
}