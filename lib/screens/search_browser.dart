
import 'package:after_layout/after_layout.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/other/bottom_search_bar.dart';
import 'package:meat/widgets/search_browser/search_plurkers.dart';
import 'package:meat/widgets/search_browser/search_plurks.dart';

void show(BuildContext context, SearchBrowserArgs args) {
  Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (BuildContext context) {
          return SearchBrowser(args);
        },
        settings: RouteSettings(
          name: 'search_browser',
          arguments: args,
        ),
      )
  );
}

class SearchBrowserArgs {
  SearchBrowserArgs(this.initialPlurksSearchTerm);
  final String initialPlurksSearchTerm;
}

class SearchBrowser extends StatefulWidget {
  SearchBrowser(this.args, {Key key}) : super(key: key);

  final SearchBrowserArgs args;

  @override
  _SearchBrowserState createState() => _SearchBrowserState();
}

class _SearchBrowserState extends State<SearchBrowser> with AfterLayoutMixin, SingleTickerProviderStateMixin {

  BuildContext _scaffoldBodyContext;
  TabController _tabController;

  GlobalKey<SearchPlurksState> _searchPlurksKey = GlobalKey<SearchPlurksState>();
  GlobalKey<SearchPlurkersState> _searchPlurkersKey = GlobalKey<SearchPlurkersState>();

  GlobalKey<BottomSearchBarState> _bottomInputBarKey = GlobalKey<BottomSearchBarState>();

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    _tabController.addListener((){
      //When ever tab changed
      if (_scaffoldBodyContext != null) {
        _scaffoldBodyContext.read<RequestIndicatorCubit>().hide();
      }
    });

    //Check initial search term and use it
    if (widget.args.initialPlurksSearchTerm != null && widget.args.initialPlurksSearchTerm.isEmpty == false) {
      _bottomInputBarKey.currentState.setTerm(widget.args.initialPlurksSearchTerm);
      _onSearchTermEnter(widget.args.initialPlurksSearchTerm);
    } else {
      _bottomInputBarKey.currentState.focus();
    }
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: MultiBlocProvider(
        providers: [
          BlocProvider<RequestIndicatorCubit>(
            create: (BuildContext context) => RequestIndicatorCubit(),
          ),
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<FlushBarCubit, FlushBarState>(
              listener: (context, state) {
                if (state is FlushBarRequest) {
                  //Only display FlushBar when I am the current route.
                  if (ModalRoute.of(context).isCurrent) {
                    _showFlushBar(_scaffoldBodyContext, state.icon, state.title,
                        state.message);
                  }
                }
              },
            ),
          ],
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            floatingActionButtonLocation:
            FloatingActionButtonLocation.miniEndFloat,
            //Display load indicator on the floating action button place.
            floatingActionButton: BlocBuilder<RequestIndicatorCubit,
                RequestIndicatorState>(
              builder: (context, state) {
                //Check state.
                List<Widget> columnChildren = [];

                //Only when showing request indicator.
                if (state is RequestIndicatorShow) {
                  //Busy indicator.
                  columnChildren.add(Container(
                    width: 56,
                    height: 56,
                    child: FlareActor(
                      "assets/rive/MeatLoader.flr",
                      animation: "Untitled",
                    ),
                  ));
                }

                return Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: columnChildren,
                );
              },
            ),
            appBar: AppBar(
              centerTitle: true,
              brightness: Theme.of(context).brightness,
              backgroundColor: Colors.transparent,
              flexibleSpace: AcrylicContainer(
                Container(),
                backgroundColor: Theme.of(context).primaryColor,
                transparentBackLayer: false,
                mode: AcrylicContainerMode.BackdropFilter,
              ),
              elevation: 0,
              titleSpacing: 0.0,
              leading: AppBarActionButton(Icons.arrow_back, onTap: () {
                // context.read<HomeRefreshCubit>().request();
                Navigator.pop(context);
              }),
              automaticallyImplyLeading: false,
              title: TabBar(
                controller: _tabController,
                tabs: [
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'searchPlurks'),
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'searchPlurkers'),
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                ],
              ),
            ),
            body: Builder(
              builder: (context) {
                _scaffoldBodyContext = context;
                return Container(
                  // color: Theme.of(context).canvasColor.withOpacity(0.4),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Container(
                          child: TabBarView(
                            controller: _tabController,
                            children: [
                              //Search Plurks
                              SearchPlurks(
                                key: _searchPlurksKey,
                              ),
                              //Search Plurkers
                              SearchPlurkers(
                                key: _searchPlurkersKey,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
            bottomNavigationBar: BottomSearchBar(
              onSearchEnter: _onSearchTermEnter,
              key: _bottomInputBarKey,
            ),
          ),
        ),
      ),
    );
  }

  _onSearchTermEnter(String term) {
    // print('Got _onSearchTermEnter [' + term + '] at index [' + _tabController.index.toString() + ']');
    //See which tab index we are in.
    if (_tabController.index == 0) {
      //Search plurks
      _searchPlurksKey.currentState.runSearch(term);
    } else if (_tabController.index == 1) {
      //Search plurkers
      _searchPlurkersKey.currentState.runSearch(term);
    }
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }
}