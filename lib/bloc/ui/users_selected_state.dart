part of 'users_selected_cubit.dart';

@immutable
abstract class UsersSelectedState {}

class UsersSelectedInitial extends UsersSelectedState {}

class UsersSelectedComplete extends UsersSelectedState {
  UsersSelectedComplete(this.users);
  final List<Plurdart.User> users;
}
