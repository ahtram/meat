part of 'home_refresh_cubit.dart';

@immutable
abstract class HomeRefreshState {}

class HomeRefreshInitial extends HomeRefreshState {}

class HomeRefreshRequest extends HomeRefreshState {
  HomeRefreshRequest();
}

class HomeRefreshComplete extends HomeRefreshState {
  HomeRefreshComplete();
}
