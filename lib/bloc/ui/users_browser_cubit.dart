import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:meat/widgets/user/users_viewer.dart';

part 'users_browser_state.dart';

class UsersBrowserCubit extends Cubit<UsersBrowserState> {
  UsersBrowserCubit() : super(UsersBrowserInitial());

  void open(UsersViewerArgs args) async {
    emit(UsersBrowserOpen(args));
  }
}
