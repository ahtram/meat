part of 'users_browser_cubit.dart';

@immutable
abstract class UsersBrowserState {}

class UsersBrowserInitial extends UsersBrowserState {}

class UsersBrowserOpen extends UsersBrowserState {
  UsersBrowserOpen(this.args);
  final UsersViewerArgs args;
}
