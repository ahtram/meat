part of 'timeline_plurk_delete_cubit.dart';

@immutable
abstract class TimelinePlurkDeleteState {}

class TimelinePlurkDeleteInitial extends TimelinePlurkDeleteState {}

class TimelinePlurkDeleteSuccess extends TimelinePlurkDeleteState {
  TimelinePlurkDeleteSuccess(this.plurkId);
  final int plurkId;
}
class TimelinePlurkDeleteFailed extends TimelinePlurkDeleteState {}