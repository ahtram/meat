import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'report_abuse_dialog_state.dart';

class ReportAbuseDialogCubit extends Cubit<ReportAbuseDialogState> {
  ReportAbuseDialogCubit() : super(ReportAbuseDialogInitial());

  reportPlurk(int plurkId) {
    emit(ReportAbuseDialogOpenPlurk(plurkId));
  }

  reportResponse(int responseId) {
    emit(ReportAbuseDialogOpenResponse(responseId));
  }
}
