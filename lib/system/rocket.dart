import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:meat/models/app/account_store.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/models/notification/notification_payload.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:pretty_json/pretty_json.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:workmanager/workmanager.dart';
import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'dart:isolate';
import 'package:meat/system/account_keeper.dart' as AccountKeeper;
import 'package:meat/models/app/app.dart';
import 'package:meat/system/assets_locale.dart' as AssetsLocale;
// import 'package:http/http.dart' as http;
// import 'package:path_provider/path_provider.dart';
// import 'package:flutter_isolate/flutter_isolate.dart';

//This is my Android local notification system. Implement on AlarmManager plugin.
//Note this may only work on Android.

//Local notification in isolate.
final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

//Out goal is to long polling from the Plurk Comet server.
// const String bahKutTehCometUrlPreferencesKeyPrefix = 'bahKutTehCometUrl_';
const String bahKutTehCometOffsetPreferencesKey = 'bahKutTehCometOffset';
const String bahKutTehCometNotiCountPreferencesKeyPrefix =
    'bahKutTehCometNotiCount_';
const String bahKutTehCometReqCountPreferencesKeyPrefix =
    'bahKutTehCometNotiCount_';

// //Should be called when each time Comet got url updated.
// void launch(int userId) async {
//   //Start the isolate using AlarmManager.
//   await AndroidAlarmManager.initialize();
//
//   //This will start the
//   await AndroidAlarmManager.oneShot(Duration(seconds: 0), userId, launchCometIsolate,
//       allowWhileIdle: true,
//       rescheduleOnReboot: true,
//   );
// }

// //Should be called when log out.
// void land(int userId) async {
//   //Cancel the isolate using AlarmManager.
//   await AndroidAlarmManager.cancel(userId);
// }

//====

//Test Workmanager.
// void workmanagerCometBackgroundTask() {
//   Workmanager().executeTask((task, inputData) async {
//     // print('Native called background task: $task | userID[' + inputData['userID'] + '] cometServer[' + inputData['cometServer'] + ']');
//     //Make sure this run for 30 seconds..
//     backgroundTaskStartTime = DateTime.now();
//
//     // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
//     AndroidInitializationSettings initializationSettingsAndroid =
//     AndroidInitializationSettings('icon');
//     IOSInitializationSettings initializationSettingsIOS =
//     IOSInitializationSettings(
//         onDidReceiveLocalNotification: _onDidReceiveIOSLocalNotification);
//     InitializationSettings initializationSettings = InitializationSettings(
//         android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
//     await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
//         onSelectNotification: _selectNotification);
//
//     await _getCometTask(inputData['userID'], inputData['cometServer']);
//     return Future.value(true);
//   });
// }

const String BTK_COMET_SEND_PORT_NAME = 'bktCometSendPort';

//Remember the time when task start...
DateTime backgroundTaskStartTime;

//Launched?
bool _isInTheAir = false;

//Inform the app something new happened.
Function(int, int) _onAlertNumberChanged;
Function(Plurdart.Plurk) _onNewPlurk;
Function(Plurdart.Plurk) _onNewResponse;

ReceivePort _receivePortOfMain = ReceivePort();
StreamSubscription _receivePortStreamSubscription;

//This is the isolate starting point.
Isolate _iOSCometIsolate;
Timer _iOSCometTimer;
takeOff(int userID,
    {Function(int, int) onAlertNumberChanged,
    Function onNewPlurk,
    Function onNewResponse}) async {
  print('[Rocket Launch]');

  _isInTheAir = true;
  _onAlertNumberChanged = onAlertNumberChanged;
  _onNewPlurk = onNewPlurk;
  _onNewResponse = onNewResponse;

  AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('icon');
  DarwinInitializationSettings initializationSettingsDarwin =
      DarwinInitializationSettings(
          onDidReceiveLocalNotification: _onDidReceiveLocalNotification);
  InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsDarwin);
  await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onDidReceiveNotificationResponse: _onDidReceiveNotificationResponse,
      onDidReceiveBackgroundNotificationResponse: notificationTapBackground,);

  // //Workmanager background task.
  // await Workmanager().initialize(
  //     workmanagerCometBackgroundTask, // The top level function, aka callbackDispatcher
  //     isInDebugMode:
  //     true // If enabled it will post a notification whenever the task is running. Handy for debugging tasks
  // );
  // await Workmanager().registerPeriodicTask('bktComet', 'getComet',
  //     frequency: Duration(minutes: 15),
  //     inputData: {
  //       'userID': userID,
  //       'cometServer': cometServerUrl,
  //     });

  //[Dep]: Get channel when every task start (It's possible Comet will sleep if we don't do this.)
  //Save the server url in pref
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // if (prefs != null) {
  //   prefs.setString(bahKutTehCometUrlPreferencesKeyPrefix + userID.toString(),
  //       cometServerUrl);
  // }

  //Save the system locale name in pref for localized notification.
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs != null) {
    //The isolate task will need this work around to make localized notification work.
    prefs.setString(Define.systemLocaleNamePreferenceKey, Platform.localeName);
  }

  //Register each time we takeOff
  //A bit tricky you have to remove the old one manually.
  if (_receivePortStreamSubscription != null) {
    _receivePortStreamSubscription.cancel();
  }
  _receivePortOfMain.close();
  IsolateNameServer.removePortNameMapping(BTK_COMET_SEND_PORT_NAME);

  await Future.delayed(Duration(milliseconds: 100));
  if (IsolateNameServer.registerPortWithName(
      _receivePortOfMain.sendPort, BTK_COMET_SEND_PORT_NAME)) {
    try {
      //Not sure why. This keeps getting exception even if I closed it...
      _receivePortStreamSubscription = _receivePortOfMain.listen(_onMainIsolateReceiveTaskComet);
    } catch (e) {
      print(e.toString());
    }
  }

  if (Platform.isAndroid) {
    await AndroidAlarmManager.periodic(
        const Duration(minutes: 1),
        userID,
        _startCometTaskAlarm,
        wakeup: true,
        exact: true,
        rescheduleOnReboot: true,
        allowWhileIdle: true,
        startAt: DateTime.now());
  } else if (Platform.isIOS) {
    //iOS cannot use AndroidAlarmManager and WorkManager only fire task each 15 mins for 30 seconds
    //For now we have only one solution: use a standard isolate to run Comet checking on iOS...
    if (_iOSCometTimer == null) {

      //This is ugly.. but we don't actually know if the isolate has finished it's job.
      if (_iOSCometIsolate != null) {
        _iOSCometIsolate.kill();
        _iOSCometIsolate = null;
      }
      print('[spawn isolate]');
      _iOSCometIsolate = await Isolate.spawn(_startCometTaskAlarm, userID);

      //Schedule timer
      _iOSCometTimer = Timer. periodic(Duration(minutes: 1), (t) async {
        //This is ugly.. but we don't actually know if the isolate has finished it's job.
        if (_iOSCometIsolate != null) {
          _iOSCometIsolate.kill();
          _iOSCometIsolate = null;
        }
        print('[spawn isolate]');
        _iOSCometIsolate = await Isolate.spawn(_startCometTaskAlarm, userID);
      });
    }

  }

  //Do the first time get comet. This will actually start the polling loop.
  //Temp comment
  // _getCometLoop(userID, cometServerUrl);
}

//Main isolate receive the task message.
_onMainIsolateReceiveTaskComet(dynamic cometJson) {
  // print('[_onMainIsolateReceiveTaskComet]');
  //Here we evaluate Comet and do proper callback as the old design.
  if (cometJson != null) {
    Plurdart.Comet comet = Plurdart.Comet.fromJson(cometJson);
    if (comet != null && comet is Plurdart.Comet) {
      if (comet.data != null) {
        // print('_onMainIsolateReceiveTaskComet newOffset[' + comet.newOffset.toString() + ']');
        for (int i = 0; i < comet.data.length; ++i) {
          if (comet.data[i] is Plurdart.CometNewPlurk) {
            _onNewPlurk?.call(comet.data[i]);
          } else if (comet.data[i] is Plurdart.CometNewResponse) {
            _onNewResponse?.call(comet.data[i].plurk);
          } else if (comet.data[i] is Plurdart.CometUpdateNotification) {
            Plurdart.CometUpdateNotification cometData = comet.data[i];
            _onAlertNumberChanged?.call(
                cometData.counts.noti, cometData.counts.req);
          } else {
            print('[' + i.toString() + '] is Unknown type?');
          }
        }
      }
    }
  }
}

//!!!!!! This is where the stand alone isolate start running !!!!!!!
_startCometTaskAlarm(int userID) async {
  //[Guess not]: The _selectNotification() won't work this way. I wonder why.
  // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  // AndroidInitializationSettings initializationSettingsAndroid =
  // AndroidInitializationSettings('icon');
  // IOSInitializationSettings initializationSettingsIOS =
  // IOSInitializationSettings(
  //     onDidReceiveLocalNotification: _onDidReceiveIOSLocalNotification);
  // InitializationSettings initializationSettings = InitializationSettings(
  //     android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
  // await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
  //     onSelectNotification: _selectNotification);

  //Try setup Plurdart for this isolate.
  //Read and decode the app stuff from asset file...
  App app;
  try {
    String jsonStr = await rootBundle.loadString('assets/app/app.json');
    dynamic json = jsonDecode(jsonStr);
    app = App.fromJson(json);
  } catch (e) {
    print(e.toString());
  }

  //Read account store.
  await AccountKeeper.initialize();
  //This will get our saved user credentials.
  AccountTrunk at = AccountKeeper.getActivatedOrAnUsableAccount();
  // print('[Rocket _startCometTaskAlarm()][getActivatedOrAnUsableAccount]');
  // Check these credentials by API checkToken().
  bool plurdartSetupRestoreSuccess = false;
  if ((app != null) &&
      (at != null) &&
      !(at.accessToken?.isEmpty ?? true) &&
      !(at.tokenSecret?.isEmpty ?? true)) {
    // print('[passed check][setup client]');
    // setup client.
    Plurdart.setupClient(app.key, app.secret, at.accessToken, at.tokenSecret);
    plurdartSetupRestoreSuccess = true;

    //I don't think we need this...
//      // Test an API call to see if the client still work...
//      Plurdart.CheckedExpiredToken checkedExpiredToken = await Plurdart.checkToken();
//      if (checkedExpiredToken != null) {
////        _showSnackBarMessage('[Credential restored. Plurdart has been setup]');
//        print('[Credential restored. Plurdart has been setup]');
//        plurdartSetupRestoreSuccess = true;
//      } else {
////        _showSnackBarMessage('[Credential expired. Re-Auth is required.]');
//        print('[Credential expired. Re-Auth is required.]');
//        plurdartSetupRestoreSuccess = false;
//      }

  } else {
    print('[Cannot restore credential. Cannot do _startCometTaskAlarm().]');
    plurdartSetupRestoreSuccess = false;
  }

  //Only if Plurdart is restored so we can start the task.
  if (plurdartSetupRestoreSuccess) {
    //Prevent http calls exceptions. Which may break the Alarm
    try {
      Plurdart.UserChannel userChannel =
          await Plurdart.realtimeGetUserChannel();
      if (userChannel != null) {
        //Initialize AssetsLocale.
        if (!AssetsLocale.isInitialized()) {
          //[Work around]: We cannot use Platform.localeName due to Flutter bug!
          SharedPreferences prefs = await SharedPreferences.getInstance();
          String localeName;
          if (prefs != null) {
            //The isolate task will need this work around to make localized notification work.
            localeName = prefs.getString(Define.systemLocaleNamePreferenceKey);
          }

          await AssetsLocale.initialize(localeName: localeName);
        }

        if (backgroundTaskStartTime == null) {
          //No task is running
          // print('[_startCometTaskAlarm] No task is running. Starting new.');
          backgroundTaskStartTime = DateTime.now();
          await _getCometTask(userID, userChannel.cometServer);
        } else {
          //A task is running! And still waiting for Commet! We cannot end it directly
          //Instead of starting a new task we just extend the task's life by set new backgroundTaskStartTime!
          // print('[_startCometTaskAlarm] A task is running! Try extend it!');
          backgroundTaskStartTime = DateTime.now();
        }
      } else {
        // print('[Cannot get UserChannel. Cannot do _startCometTaskAlarm().]');
        //Force end the existing task.
        backgroundTaskStartTime = null;
      }
    } catch (e) {
      print(e.toString());
      //Force end the existing task.
      backgroundTaskStartTime = null;
    }
  } else {
    //Force end the existing task.
    // print('[plurdartSetupRestore failed] Force end the existing task!');
    backgroundTaskStartTime = null;
  }
}

land() async {
  print('[Rocket Land]');
  // await Workmanager().cancelAll();
  if (_receivePortStreamSubscription != null) {
    _receivePortStreamSubscription.cancel();
  }
  _receivePortOfMain.close();
  IsolateNameServer.removePortNameMapping(BTK_COMET_SEND_PORT_NAME);

  backgroundTaskStartTime = null;

  if (Platform.isIOS) {
    if (_iOSCometIsolate != null) {
      _iOSCometIsolate.kill();
      _iOSCometIsolate = null;
    }

    if (_iOSCometTimer != null) {
      _iOSCometTimer.cancel();
      _iOSCometTimer = null;
    }
  }

  _isInTheAir = false;
}

//[Dep]: Maybe we just relay on Alert task.
// The loop body when the app is alive.
// _getCometLoop(String userID) async {
//   if (!_isInTheAir) {
//     //Stop the loop.
//     return;
//   }
//
//   // print('[_getComet]');
//   // Read url from the preference.
//   SharedPreferences prefs = await SharedPreferences.getInstance();
//   if (prefs != null) {
//     int offset = 0;
//     if (prefs.containsKey(bahKutTehCometOffsetPreferencesKey)) {
//       offset = prefs.getInt(bahKutTehCometOffsetPreferencesKey);
//     }
//
//     String cometServerUrl;
//     if (prefs.containsKey(
//         bahKutTehCometUrlPreferencesKeyPrefix + userID.toString())) {
//       cometServerUrl = prefs
//           .getString(bahKutTehCometUrlPreferencesKeyPrefix + userID.toString());
//     }
//
//     if (cometServerUrl != null) {
//       //We got an url! User it to poll from comet. This could wait for several seconds.
//       // print('_getCometLoop [' + cometServerUrl + '] offset[' + offset.toString() + ']...');
//       Plurdart.Comet comet =
//           await Plurdart.getCometByUrl(cometServerUrl, offset: offset);
//
//       if (comet != null) {
//         //Save the prefix.
//         prefs.setInt(bahKutTehCometOffsetPreferencesKey, comet.newOffset);
//         //Got new comet success! Check for notification!
//         _processComet(comet);
//
//         //This will start the
//         //And do it again.
//         _getCometLoop(userID);
//       } else {
//         print('Oops! _getComet got null comet?!');
//         await Future.delayed(Duration(seconds: 30));
//         _getCometLoop(userID);
//       }
//     } else {
//       print('Oops! _getComet got null cometServerUrl?!');
//       await Future.delayed(Duration(seconds: 30));
//       _getCometLoop(userID);
//     }
//   } else {
//     print('Oops! _getComet got null prefs?!');
//     await Future.delayed(Duration(seconds: 30));
//     _getCometLoop(userID);
//   }
// }

_getCometTask(int userID, String cometServerUrl) async {
  // print('[_getCometOnce] userID[' + userID.toString() + '] cometServerUrl[' + cometServerUrl + ']');
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs != null) {
    int offset = 0;
    if (prefs.containsKey(bahKutTehCometOffsetPreferencesKey)) {
      offset = prefs.getInt(bahKutTehCometOffsetPreferencesKey);
    }

    //[Dep]: Passed in after get is from server when task start.
    // String cometServerUrl;
    // if (prefs.containsKey(
    //     bahKutTehCometUrlPreferencesKeyPrefix + userID.toString())) {
    //   cometServerUrl = prefs
    //       .getString(bahKutTehCometUrlPreferencesKeyPrefix + userID.toString());
    // }

    if (cometServerUrl != null) {
      //We got an url! User it to poll from comet. This could wait for several seconds.
      // print('_getCometTask offset[' + offset.toString() + ']...');
      Plurdart.Comet comet =
          await Plurdart.getCometByUrl(cometServerUrl, offset: offset);
      //This could wait for 30 seconds to 1 minute... depend on the server response time...
      if (comet != null) {
        //Save the prefix.
        prefs.setInt(bahKutTehCometOffsetPreferencesKey, comet.newOffset);
        //Got new comet success! Check for notification! (No need to await this)
        _processComet(userID, comet, prefs);

        //Check the task's life time.
        if (backgroundTaskStartTime != null &&
            DateTime.now().difference(backgroundTaskStartTime).inMinutes < 1) {
          await _getCometTask(userID, cometServerUrl);
        } else {
          //Else the task ended...
          backgroundTaskStartTime = null;
          // print('[_getCometTask ended]');
        }

        //Test: Do it inifinitly
        // await _getCometTask(userID, cometServerUrl);
      } else {
        print('Oops! _getCometTask got null comet?!');
        //Force end the existing task.
        backgroundTaskStartTime = null;
      }
    } else {
      print('Oops! _getCometTask got null cometServerUrl?!');
      //Force end the existing task.
      backgroundTaskStartTime = null;
    }
  } else {
    print('Oops! _getCometTask got null prefs?!');
    //Force end the existing task.
    backgroundTaskStartTime = null;
  }
}

// //This will start the comet monitor flow.
// Future _startCometMonitor() async {
//   Plurdart.UserChannel userChannel = await Plurdart.realtimeGetUserChannel();
//   if (userChannel != null) {
//     print('[Got userChannel success. Start the comet monitor]');
//     //Success on getting user channel. We can start the getComet from Cubit.
//     _blocProviderContext.read<GetCometCubit>().request();
//   } else {
//     print(
//         'Huh? Cannot get userChannel? We cannot start the comet monitor...');
//   }
// }_processComet

//Process the comet state.
_processComet(int userID, Plurdart.Comet comet, SharedPreferences prefs) async {
  // print('_processComet: ' + prettyJson(comet.toJson()));
  //Note that data is possible null!
  if (comet.data != null) {
    // print('_processComet got a newOffset[' + comet.newOffset.toString() + ']');
    for (int i = 0; i < comet.data.length; ++i) {
      if (comet.data[i] is Plurdart.CometNewPlurk) {
        //Todo: Not sure if we need to notify user new Plurk!
        //Todo: We may update the new plurk count!
        // print('[' + i.toString() + '] is CometNewPlurk');
        // _sendLocalNotification('BahKutTeh', 'CometNewPlurk [' + comet.data[i].plurkId.toString() + ']', 'plurkId:' + comet.data[i].plurkId.toString());
        //[Dep]: Replaced by SendPort to main isolate.
        // _onNewPlurk?.call(comet.data[i]);

        //Check if this one is a private plurk sent to me!
        //Notification to user if it is!
      } else if (comet.data[i] is Plurdart.CometNewResponse) {
        //Not sure if we need to notify user new Response
        //We may update the new response count!
        // print('[' + i.toString() + '] is CometNewResponse');
        // _sendLocalNotification('BahKutTeh', 'CometNewResponse [' + comet.data[i].plurkId.toString() + ']', 'plurkId:' + comet.data[i].plurkId.toString());
        //[Dep]: Replaced by SendPort to main isolate.
        // _onNewResponse?.call(comet.data[i].plurk);
      } else if (comet.data[i] is Plurdart.CometUpdateNotification) {
        //Debug log.
        Plurdart.CometUpdateNotification cometData = comet.data[i];
        print('[' +
            i.toString() +
            '] is CometUpdateNotification: ' +
            prettyJson(cometData.toJson()));

        int takeCount = cometData.counts.noti + cometData.counts.req;
        if (takeCount > 0) {
          List<Plurdart.Alert> historyAlerts =
              await Plurdart.alertsGetHistory();
          List<Plurdart.Alert> activeAlerts = await Plurdart.alertsGetActive();

          //Combine the two list and sort them properly...
          List<Plurdart.Alert> alerts = [];

          if (historyAlerts != null) {
            alerts.addAll(historyAlerts);
          }

          if (activeAlerts != null) {
            alerts.addAll(activeAlerts);
          }

          alerts.sort(alertsCompare);

          alerts = alerts.take(takeCount).toList();
          for (int i = alerts.length - 1; i >= 0; --i) {
            // print('alerts[' +
            //     i.toString() +
            //     '] [' +
            //     alerts[i].type +
            //     ']');
            _sendNotificationAlert(comet.newOffset, alerts[i],
                Define.generateDeepLinkPayloadForAlert(alerts[i]));
          }
        }
      } else {
        print('[' + i.toString() + '] is Unknown type?');
      }
    }
  }

  //Notify the main isolate!
  //Here we look-up THE send port by pre-defined port name.
  SendPort sendPort =
      IsolateNameServer.lookupPortByName(BTK_COMET_SEND_PORT_NAME);
  if (sendPort != null) {
    sendPort.send(comet.toJson());
  }
}

//For sort alerts list.
int alertsCompare(Plurdart.Alert a, Plurdart.Alert b) {
  DateTime dateTimeA = a.postedDateTime();
  DateTime dateTimeB = b.postedDateTime();
  if (dateTimeA == null || dateTimeB == null) {
//Cannot compare.
    return 0;
  }
//Use the DateTime compareTo to do the job.
  return -(a.postedDateTime().compareTo(b.postedDateTime()));
}

//Test alarm manager isolate.
// static void printHello() {
//   final DateTime now = DateTime.now();
//   final int isolateId = Isolate.current.hashCode;
//   print("[$now] Hello, world! isolate=$isolateId function='$printHello'");
//   //And do it again.
//   final int helloAlarmID = 0;
//   AndroidAlarmManager.oneShot(Duration(seconds: 5), helloAlarmID, printHello);
// }

//-- Local notifications --

//This will just get the proper alert body localized string for us!
String getAssetsLocaleStringForAlert(Plurdart.Alert alert) {
  return Define.localizedStrReplaceAlertParams(
      alert, AssetsLocale.translate(Define.getI8nKeyForAlert(alert)));
}

//Display local notification for an alert!
Future _sendNotificationAlert(int id, Plurdart.Alert alert,
    NotificationPayload notificationPayload) async {
  if (alert != null) {
    //Try download the user's avatar and use as large icon
    String largeIconPath;

    //[Forget about this]: Not good enough.
    // Plurdart.User user = alert.getUser();
    // if (user != null) {
    //   try {
    //     largeIconPath = await _downloadAndSaveFile(user.mediumAvatarUrl(), 'mediumAvatar_' + user.id.toString());
    //   } catch (e) {
    //     //Fuxk this they give me a monkey url.
    //   }
    // }

    _sendLocalNotification(
      'bkt' + alert.type,
      id,
      getAssetsLocaleStringForAlert(alert),
      null,
      notificationPayload.toString(),
      playSound: Define.shouldPlaySoundForAlert(alert),
      enableVibration: Define.shouldEnableVibrationForAlert(alert),
      largeIconPath: largeIconPath,
    );
  }
}

// IOS only?
void _onDidReceiveLocalNotification(
    int id, String title, String body, String payload) async {}

// IOS only?
@pragma('vm:entry-point')
Future notificationTapBackground(NotificationResponse notificationResponse) async {
  // handle action
  //When user click the notification.
  // print('[_selectNotification] payload: ' + payload);
  if (await canLaunchUrl(Uri.parse((notificationResponse.payload)))) {
    await launchUrl(Uri.parse((notificationResponse.payload)));
  }
}

Future _onDidReceiveNotificationResponse(NotificationResponse notificationResponse) async {
  //When user click the notification.
  // print('[_selectNotification] payload: ' + payload);
  if (await canLaunchUrl(Uri.parse((notificationResponse.payload)))) {
    await launchUrl(Uri.parse((notificationResponse.payload)));
  }
}

//Borrowed code from example. For download user avatar and display in the notification.
// Future<String> _downloadAndSaveFile(String url, String fileName) async {
//   final Directory directory = await getApplicationDocumentsDirectory();
//   final String filePath = '${directory.path}/$fileName';
//   final http.Response response = await http.get(Uri.parse(url));
//   final File file = File(filePath);
//   await file.writeAsBytes(response.bodyBytes);
//   return filePath;
// }

_sendLocalNotification(
    String channelID, int id, String title, String body, String payload,
    {bool playSound = false,
    bool enableVibration = false,
    String largeIconPath,
    String subText}) async {
  AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails(
    channelID,
    'BahKutTeh Notifications',
    channelDescription: 'BahKutTeh local push notifications',
    icon: 'icon',
    largeIcon:
        largeIconPath != null ? FilePathAndroidBitmap(largeIconPath) : null,
    importance: Importance.high,
    priority: Priority.high,
    playSound: playSound,
    enableVibration: enableVibration,
    ticker: 'ticker',
    subText: subText,
  );
  DarwinNotificationDetails darwinNotificationDetails = DarwinNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: darwinNotificationDetails);
  await _flutterLocalNotificationsPlugin
      .show(id, title, body, platformChannelSpecifics, payload: payload);
}
