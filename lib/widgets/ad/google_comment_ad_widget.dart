// import 'package:after_layout/after_layout.dart';
// import 'package:flutter/material.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:meat/system/define.dart' as Define;
//
// //This is a back: google_mobile_ads currently sucks!
// class GoogleCommentAdWidget extends StatefulWidget {
//   const GoogleCommentAdWidget({Key key}) : super(key: key);
//
//   @override
//   _GoogleCommentAdWidgetState createState() => _GoogleCommentAdWidgetState();
// }
//
// class _GoogleCommentAdWidgetState extends State<GoogleCommentAdWidget> with AfterLayoutMixin {
//   BannerAd _inlineAdaptiveAd;
//   AdSize _adSize;
//
//   double get _adWidth => MediaQuery.of(context).size.width;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   void afterFirstLayout(BuildContext context) async {
//     await _loadAd();
//   }
//
//   Future<void> _loadAd() async {
//     await _inlineAdaptiveAd?.dispose();
//     setState(() {
//       _inlineAdaptiveAd = null;
//     });
//
//     // Get an inline adaptive size for the current orientation.
//     AdSize size = await AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
//         _adWidth.truncate());
//
//     if (size == null) {
//       print('Unable to get height of anchored banner.');
//       return;
//     }
//
//     //Load banner.
//     if (mounted) {
//       _inlineAdaptiveAd = BannerAd(
//         adUnitId: Define.commentADUnitID(),
//         size: size,
//         request: AdRequest(),
//         listener: BannerAdListener(
//           onAdLoaded: (Ad ad) async {
//             print('Inline adaptive banner loaded: ${ad.responseInfo}');
//
//             // After the ad is loaded, get the platform ad size and use it to
//             // update the height of the container. This is necessary because the
//             // height can change after the ad is loaded.
//             BannerAd bannerAd = (ad as BannerAd);
//             final AdSize size = await bannerAd.getPlatformAdSize();
//             if (size == null) {
//               print('Error: getPlatformAdSize() returned null for $bannerAd');
//               return;
//             }
//
//             setState(() {
//               _inlineAdaptiveAd = bannerAd;
//               _adSize = size;
//             });
//           },
//           onAdFailedToLoad: (Ad ad, LoadAdError error) {
//             print('Inline adaptive banner failedToLoad: $error');
//             ad.dispose();
//           },
//         ),
//       );
//       await _inlineAdaptiveAd.load();
//     }
//   }
//
//   @override
//   void dispose() {
//     //Dispose banner.
//     _inlineAdaptiveAd?.dispose();
//     _inlineAdaptiveAd = null;
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Widget ad;
//     if (_inlineAdaptiveAd != null) {
//       ad = AdWidget(ad: _inlineAdaptiveAd);
//     } else {
//       ad = Container();
//     }
//
//     double adHeight = 0;
//     if (_adSize != null) {
//       adHeight = _adSize.height.toDouble();
//     }
//
//     return Column(
//       mainAxisSize: MainAxisSize.min,
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         Container(
//           height: adHeight,
//           child: ad,
//         ),
//         Divider(height: 0.5, thickness: 0.5),
//       ],
//     );
//   }
// }
