//Ok... Sometimes you just need to resolve things quickly.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:meat/models/chikkutteh/res/living_channel_res.dart';
import 'package:meat/models/emoticon/emoticon_collection.dart';
import 'package:meat/theme/app_themes.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:enum_to_string/enum_to_string.dart';
import 'dart:io';
import 'package:meat/chikkutteh/chikkutteh.dart' as ChikKutTeh;

// Cache the ThemeMode we are using.
ThemeMode rootMaterialAppThemeMode = ThemeMode.system;

// Cache the User's personal data. (Done by home when app start)
// This global data can be use by anyone.
Plurdart.User me;

//Are we a premium user?
bool meIsPremium() {
  if (me != null) {
    return me.premium;
  }
  return false;
}

String meDefaultLanguage() {
  if (me != null) {
    return me.defaultLang;
  }
  return '';
}

//Should we display ads for the current user?
bool shouldDisplayAds() {
  // if (me != null) {
  //   if (me.premium && !me.showAds) {
  //     return false;
  //   }
  // }
  // return true;

  //[I screwed this]
  return false;
}

//The plurk coin info cache for the current login user.
Plurdart.PremiumBalance balance;

//Is the current user has more then one PlurkCoin?
bool canSendGift() {
  if (balance != null) {
    return balance.bones > 1;
  }
  return false;
}

//Very stupid notification number cache.
//These should only be updated by Home cubit response of AlertsGetUnreadCountCubit and GetCometCubit
//Also alert viewer should directly reset this when finish a getActive or getHistory request.
int noti = 0;
int req = 0;

//This will store all User data from all requests we start the app.
Map<String, Plurdart.User> _nickNameUserCache = Map<String, Plurdart.User>();
Map<int, Plurdart.User> _userIdUserCache = Map<int, Plurdart.User>();
const int NICKNAME_CACHE_COUNT = 50;
List<String> _nickNamesCache = [];

//A a cache record.
void addToUserCache(Plurdart.User user) {
  if (user != null) {
    if (user.id != null) {
      _userIdUserCache[user.id] = user;
    }

    if (user.nickName != null) {
      _nickNameUserCache[user.nickName] = user;
    }

    if (!_nickNamesCache.contains(user.nickName)) {
      _nickNamesCache.add(user.nickName);
    }

    //Limit the _nickNamesCache size.
    if (_nickNamesCache.length > NICKNAME_CACHE_COUNT) {
      _nickNamesCache.removeRange(0, _nickNamesCache.length - NICKNAME_CACHE_COUNT);
    }
  }
}

//Try get a user data from cache.
Plurdart.User getUserByNickName(String nickName) {
  if (_nickNameUserCache.containsKey(nickName)) {
    return _nickNameUserCache[nickName];
  }
  return null;
}

//Try get a user data from cache.
Plurdart.User getUserByUserId(int userId) {
  if (_userIdUserCache.containsKey(userId)) {
    return _userIdUserCache[userId];
  }
  return null;
}

//Try get a users data from cache.
List<Plurdart.User> getUsersByUserIds(List<int> userIds) {
  List<Plurdart.User> returnUsers = [];

  if (userIds != null) {
    userIds.forEach((userId) {
      if (_userIdUserCache.containsKey(userId)) {
        returnUsers.add(_userIdUserCache[userId]);
      } else {
        print('getUsersByUserIds: user not exist [' + userId.toString() + ']');
      }
    });
  }

  return returnUsers;
}

//Get a suggestion nickName list.
List<Plurdart.User> querySuggestionsByNickNameTerm(String mentionTerm) {
  if (mentionTerm.length > 0) {
    String lowerCaseMentionTerm = mentionTerm.toLowerCase();
    return _nickNamesCache
        .where((element) => (element.toLowerCase().contains(lowerCaseMentionTerm) &&
        _nickNameUserCache.containsKey(element)))
        .map((e) => _nickNameUserCache[e])
        .toList();
  } else {
    //Return all
    return _nickNamesCache
        .where((element) => _nickNameUserCache.containsKey(element))
        .map((e) => _nickNameUserCache[e])
        .toList();
  }
}

//I doubt we'll need this.
//Yes, we should do this when switch account.
void clearUserCache() {
  _userIdUserCache.clear();
  _nickNameUserCache.clear();
  _nickNamesCache.clear();
}

//Tags
List<String> _tagsCache = [];

List<String> getTags() {
  return _tagsCache;
}

Future updateTagsCache() async {
  _tagsCache = await Plurdart.bookmarksGetTags();
}

void clearTagsCache() {
  _tagsCache.clear();
}

//------ ChikKutTeh watching Youtube living channels -------

LivingChannelsRes _livingChannelsResCache;

//Get the living channels cache.
LivingChannelsRes getLivingChannelsRes() {
  return _livingChannelsResCache;
}

//This should be used by someone.
Future updateLivingChannelsResCache() async {
  // print('[updateLivingChannelsResCache]');
  _lastLivingChannelsUpdateDateTime = DateTime.now();
  _livingChannelsResCache = await ChikKutTeh.getLivingChannels();
}

void clearLivingChannelsResCache() {
  _livingChannelsResCache = null;
}

//A simple DateTime base update checking loop.
DateTime _lastLivingChannelsUpdateDateTime;
Timer _updateLivingChannelsTimer;

//This will start automatic update LivingChannelsRes
void startLivingChannelsUpdateTimer() async {
  if (_updateLivingChannelsTimer == null) {
    //Update for the first time.
    await updateLivingChannelsResCache();
    _updateLivingChannelsTimer = Timer.periodic(Duration(seconds: 3), (timer) async {
      //Check if we need an update
      if (_lastLivingChannelsUpdateDateTime != null) {
        Duration diffDuration = DateTime.now().difference(_lastLivingChannelsUpdateDateTime);
        //This duration should exactly match the server's update rate.
        if (diffDuration.inMinutes >= 10) {
          await updateLivingChannelsResCache();
        }
      } else {
        await updateLivingChannelsResCache();
      }
    });
  }
}

void stopLivingChannelsUpdateTimer() {
  if (_updateLivingChannelsTimer != null) {
    _updateLivingChannelsTimer.cancel();
    _updateLivingChannelsTimer = null;
  }
}

//------ non-settings pref ------

// The logged in user's id. (This will be faster than userMe when start up)
int meId;
saveMeId(int userId) {
  meId = userId;
  _prefs.setInt(
      Define.meIdPreferencesKey, meId);
}

//Remember to call this when logout.
removeMeId() {
  meId = null;
  _prefs.remove(Define.meIdPreferencesKey);
}

enum PlurksMainFilter {
  AllPlurks,
  MyPlurks,
  Private,
  Responded,
  Replurked,
  Liked,
  Mentioned,
  Friends,          //Client mod
  Following,        //Client mod
  MyPrivate,        //Client mod
  Spy,              //Client mod
  MarkAllAsRead,    //This is not a filter but a dummy for title dropdown.
}

//Can be null: means non filter (Show All).
PlurksMainFilter _prefPlurksMainFilter = PlurksMainFilter.AllPlurks;
//This will check premium state.
PlurksMainFilter prefPlurksMainFilter() {
  //Check me to see if we should filter out mention pref.
  if (me != null) {
    if(!me.premium && _prefPlurksMainFilter == PlurksMainFilter.Mentioned) {
      //Reset to all.
      savePlurksMainFilter(PlurksMainFilter.AllPlurks);
    }
  }

  return _prefPlurksMainFilter;
}
savePlurksMainFilter(PlurksMainFilter plurksMainFilter) {
  _prefPlurksMainFilter = plurksMainFilter;
  _prefs.setString(
      Define.plurksMainFilterPreferencesKey, EnumToString.convertToString(_prefPlurksMainFilter));
}

//A helper getter for translate PlurksMainFilter to Plurdart.PlurkFilter
Plurdart.PlurkFilter usingPlurkFilter() {
  switch(prefPlurksMainFilter()) {
    case PlurksMainFilter.AllPlurks:
      return null;
    case PlurksMainFilter.MyPlurks:
      return Plurdart.PlurkFilter.my;
    case PlurksMainFilter.Private:
      return Plurdart.PlurkFilter.private;
    case PlurksMainFilter.Responded:
      return Plurdart.PlurkFilter.responded;
    case PlurksMainFilter.Replurked:
      return Plurdart.PlurkFilter.replurked;
    case PlurksMainFilter.Liked:
      return Plurdart.PlurkFilter.favorite;
    case PlurksMainFilter.Mentioned:
      return Plurdart.PlurkFilter.mentioned;
    case PlurksMainFilter.Friends:
      return null;
    case PlurksMainFilter.Following:
      return null;
    case PlurksMainFilter.MyPrivate:
      return Plurdart.PlurkFilter.private;
    case PlurksMainFilter.Spy:
      return null;
    default:
      return null;
  }
}

//Default to None
Plurdart.PlurkQualifier prefPlurkQualifier = Plurdart.PlurkQualifier.None;
savePlurkQualifier(Plurdart.PlurkQualifier qualifier) {
  prefPlurkQualifier = qualifier;
  _prefs.setString(
      Define.plurkQualifierPreferencesKey, EnumToString.convertToString(prefPlurkQualifier));
}

//Default to None
Plurdart.ResponseQualifier prefResponseQualifier = Plurdart.ResponseQualifier.None;
saveResponseQualifier(Plurdart.ResponseQualifier qualifier) {
  prefResponseQualifier = qualifier;
  _prefs.setString(
      Define.responseQualifierPreferencesKey, EnumToString.convertToString(prefResponseQualifier));
}

//Default to 0
int emoticonTabIndex = 0;
saveEmoticonTabIndex(int index) {
  emoticonTabIndex = index;
  _prefs.setInt(
      Define.emoticonTabIndexPreferencesKey, emoticonTabIndex);
}

//New Plurk draft
String newPlurkDraft = '';
saveNewPlurkDraft(String draft) {
  newPlurkDraft = draft;
  _prefs.setString(
      Define.newPlurkDraftPreferencesKey, newPlurkDraft);
}

clearNewPlurkDraft() {
  newPlurkDraft = '';
  _prefs.remove(Define.newPlurkDraftPreferencesKey);
}

//For toggle viewing UnreadOnly
bool _isViewingUnreadOnly = false;
isViewingUnreadOnly() {
  return _isViewingUnreadOnly;
}

saveIsViewingUnreadOnly(bool b) {
  _isViewingUnreadOnly = b;
  _prefs.setBool(
      Define.isViewingUnreadOnlyPreferencesKey, _isViewingUnreadOnly);
}

//------ settings pref ------

AppTheme settingsAppTheme = AppTheme.DeepOrange;
saveAppTheme(AppTheme appTheme) {
  settingsAppTheme = appTheme;
  _prefs.setString(
      Define.appThemePreferencesKey, EnumToString.convertToString(settingsAppTheme));
}

ThemeMode settingsThemeMode = ThemeMode.system;
saveThemeMode(ThemeMode themeMode) {
  settingsThemeMode = themeMode;
  _prefs.setString(
      Define.themeModePreferencesKey, EnumToString.convertToString(settingsThemeMode));
}

CanvasColor settingsCanvasColor = CanvasColor.LowContrast;
saveCanvasColor(CanvasColor canvasColor) {
  settingsCanvasColor = canvasColor;
  _prefs.setString(
      Define.canvasColorPreferencesKey, EnumToString.convertToString(settingsCanvasColor));
}

double settingsTextScale = 1.1;
saveTextScale(double textScale) {
  settingsTextScale = textScale;
  _prefs.setDouble(
      Define.textScalePreferencesKey, settingsTextScale);
}

bool settingsAcrylicEffect = true;
saveAcrylicEffect(bool b) {
  settingsAcrylicEffect = b;
  _prefs.setBool(
      Define.acrylicEffectPreferencesKey, settingsAcrylicEffect);
}

bool settingsDownloadFullImageInPlurkCard = true;
saveDownloadFullImageInPlurkCard(bool b) {
  settingsDownloadFullImageInPlurkCard = b;
  _prefs.setBool(
      Define.downloadFullImageInPlurkCardPreferencesKey, settingsDownloadFullImageInPlurkCard);
}

bool settingsTapToRevealAdultsOnlyContent = true;
saveTapToRevealAdultsOnlyContent(bool b) {
  settingsTapToRevealAdultsOnlyContent = b;
  _prefs.setBool(
      Define.tapToRevealAdultsOnlyContentPreferencesKey, settingsTapToRevealAdultsOnlyContent);
}

bool settingsRemoveLinksInPlurkContent = true;
saveRemoveLinksInPlurkContent(bool b) {
  settingsRemoveLinksInPlurkContent = b;
  _prefs.setBool(
      Define.removeLinksInPlurkContentPreferencesKey, settingsRemoveLinksInPlurkContent);
}

//[Dep]
// bool settingsDisplayMuteButtonAtBottomRight = false;
// saveDisplayMuteButtonAtBottomRight(bool b) {
//   settingsDisplayMuteButtonAtBottomRight = b;
//   _prefs.setBool(
//       Define.displayMuteButtonAtBottomRightPreferencesKey, settingsDisplayMuteButtonAtBottomRight);
// }

String apiLanguage = '';
saveAPILanguage(String apiLang) {
  apiLanguage = apiLang;
  _prefs.setString(
      Define.apiLanguagePreferencesKey, apiLanguage);
}

bool settingsDisplayVTWatchNotifyText = true;
saveDisplayVTWatchNotifyText(bool b) {
  settingsDisplayVTWatchNotifyText = b;
  _prefs.setBool(
      Define.displayVTWatchNotifyTextPreferencesKey, settingsDisplayVTWatchNotifyText);
}

//--------- pref ----------

//This will be cached in global space.
SharedPreferences _prefs;
//This will load all setting vars above.
loadPrefs() async {
  _prefs = await SharedPreferences.getInstance();

  //-- Settings --

  String appThemeStr;
  try {
    // This could throw an exception!
    appThemeStr = _prefs.getString(Define.appThemePreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (appThemeStr != null) {
    List<String> enumNames = EnumToString.toList(AppTheme.values);
    int index = enumNames.indexOf(appThemeStr);
    if (index != -1 && index < AppTheme.values.length) {
      settingsAppTheme = AppTheme.values[index];
      print('settingsAppTheme restored: [' +
          EnumToString.convertToString(settingsAppTheme) +
          ']');
    }
  }

  String themeModeStr;
  try {
    // This could throw an exception!
    themeModeStr = _prefs.getString(Define.themeModePreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (themeModeStr != null) {
    List<String> enumNames = EnumToString.toList(ThemeMode.values);
    int index = enumNames.indexOf(themeModeStr);
    if (index != -1 && index < ThemeMode.values.length) {
      settingsThemeMode = ThemeMode.values[index];
      print('settingsThemeMode restored: [' +
          EnumToString.convertToString(settingsThemeMode) +
          ']');
    }
  }

  String canvasColorStr;
  try {
    // This could throw an exception!
    canvasColorStr = _prefs.getString(Define.canvasColorPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (canvasColorStr != null) {
    List<String> enumNames = EnumToString.toList(CanvasColor.values);
    int index = enumNames.indexOf(canvasColorStr);
    if (index != -1 && index < CanvasColor.values.length) {
      settingsCanvasColor = CanvasColor.values[index];
      print('settingsCanvasColor restored: [' +
          EnumToString.convertToString(settingsCanvasColor) +
          ']');
    }
  }

  try {
    // This could throw an exception!
    settingsTextScale = _prefs.getDouble(Define.textScalePreferencesKey);
    if (settingsTextScale == null) {
      settingsTextScale = 1.1;
    }
  } catch (e) {
    print(e.toString());
  }

  try {
    // This could throw an exception!
    settingsAcrylicEffect = _prefs.getBool(Define.acrylicEffectPreferencesKey);
    //Prevent from being null.
    if (settingsAcrylicEffect == null) {
      settingsAcrylicEffect = true;
    }
  } catch (e) {
    print(e.toString());
  }

  try {
    // This could throw an exception!
    settingsDownloadFullImageInPlurkCard = _prefs.getBool(Define.downloadFullImageInPlurkCardPreferencesKey);
    //Prevent from being null.
    if (settingsDownloadFullImageInPlurkCard == null) {
      settingsDownloadFullImageInPlurkCard = true;
    }
  } catch (e) {
    print(e.toString());
  }

  try {
    // This could throw an exception!
    settingsTapToRevealAdultsOnlyContent = _prefs.getBool(Define.tapToRevealAdultsOnlyContentPreferencesKey);
    //Prevent from being null.
    if (settingsTapToRevealAdultsOnlyContent == null) {
      settingsTapToRevealAdultsOnlyContent = true;
    }
  } catch (e) {
    print(e.toString());
  }

  try {
    // This could throw an exception!
    settingsRemoveLinksInPlurkContent = _prefs.getBool(Define.removeLinksInPlurkContentPreferencesKey);
    //Prevent from being null.
    if (settingsRemoveLinksInPlurkContent == null) {
      settingsRemoveLinksInPlurkContent = true;
    }
  } catch (e) {
    print(e.toString());
  }

  // try {
  //   // This could throw an exception!
  //   settingsDisplayMuteButtonAtBottomRight = _prefs.getBool(Define.displayMuteButtonAtBottomRightPreferencesKey);
  //   //Prevent from being null.
  //   if (settingsDisplayMuteButtonAtBottomRight == null) {
  //     settingsDisplayMuteButtonAtBottomRight = false;
  //   }
  // } catch (e) {
  //   print(e.toString());
  // }

  try {
    // This could throw an exception!
    apiLanguage = _prefs.getString(Define.apiLanguagePreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (apiLanguage == null) {
    //Whatever... just set a default value.
    if (Platform.localeName.startsWith('zh')) {
      apiLanguage = 'zh';
    } else {
      apiLanguage = 'en';
    }
  }

  try {
    // This could throw an exception!
    settingsDisplayVTWatchNotifyText = _prefs.getBool(Define.displayVTWatchNotifyTextPreferencesKey);
    //Prevent from being null.
    if (settingsDisplayVTWatchNotifyText == null) {
      settingsDisplayVTWatchNotifyText = true;
    }
  } catch (e) {
    print(e.toString());
  }

  //-- non Settings --

  //meId
  try {
    // This could throw an exception!
    meId = _prefs.getInt(Define.meIdPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  String plurksMainFilter;
  try {
    // This could throw an exception!
    plurksMainFilter = _prefs.getString(Define.plurksMainFilterPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (plurksMainFilter != null) {
    List<String> enumNames = EnumToString.toList(PlurksMainFilter.values);
    int index = enumNames.indexOf(plurksMainFilter);
    if (index != -1 && index < PlurksMainFilter.values.length) {
      _prefPlurksMainFilter = PlurksMainFilter.values[index];
      print('prefPlurksMainFilter restored: [' +
          EnumToString.convertToString(_prefPlurksMainFilter) +
      ']');
    }
  }

  //Plurk qualifier.
  String plurkQualifier;
  try {
    // This could throw an exception!
    plurkQualifier = _prefs.getString(Define.plurkQualifierPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (plurkQualifier != null) {
    List<String> enumNames = EnumToString.toList(Plurdart.PlurkQualifier.values);
    int index = enumNames.indexOf(plurkQualifier);
    if (index != -1 && index < Plurdart.PlurkQualifier.values.length) {
      prefPlurkQualifier = Plurdart.PlurkQualifier.values[index];
      print('prefPlurkQualifier restored: [' +
          EnumToString.convertToString(prefPlurkQualifier) +
          ']');
    }
  }

  //Response qualifier.
  String responseQualifier;
  try {
    // This could throw an exception!
    responseQualifier = _prefs.getString(Define.responseQualifierPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (responseQualifier != null) {
    List<String> enumNames = EnumToString.toList(Plurdart.ResponseQualifier.values);
    int index = enumNames.indexOf(responseQualifier);
    if (index != -1 && index < Plurdart.ResponseQualifier.values.length) {
      prefResponseQualifier = Plurdart.ResponseQualifier.values[index];
      print('prefResponseQualifier restored: [' +
          EnumToString.convertToString(prefResponseQualifier) +
          ']');
    }
  }

  try {
    // This could throw an exception!
    emoticonTabIndex = _prefs.getInt(Define.emoticonTabIndexPreferencesKey);
    if (emoticonTabIndex == null) {
      emoticonTabIndex = 0;
    }
  } catch (e) {
    print(e.toString());
  }

  //NewPlurkDraft
  try {
    // This could throw an exception!
    newPlurkDraft = _prefs.getString(Define.newPlurkDraftPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (newPlurkDraft == null) {
    newPlurkDraft = '';
  }

  //isViewingUnreadOnly
  try {
    // This could throw an exception!
    _isViewingUnreadOnly = _prefs.getBool(Define.isViewingUnreadOnlyPreferencesKey);
  } catch (e) {
    print(e.toString());
  }

  if (_isViewingUnreadOnly == null) {
    _isViewingUnreadOnly = false;
  }
}

//---- Emoticon Collection

//User's emotion cache.
EmoticonCollection emoticonCollection = EmoticonCollection();

//---- Global events

StreamController htmlWidgetUrlTapController = new StreamController.broadcast();
Stream get urlTap => htmlWidgetUrlTapController.stream;