part of 'alerts_get_unread_count_cubit.dart';

@immutable
abstract class AlertsGetUnreadCountState {}

class AlertsGetUnreadCountInitial extends AlertsGetUnreadCountState {}

class AlertsGetUnreadCountSuccess extends AlertsGetUnreadCountState {
  AlertsGetUnreadCountSuccess(this.alertsUnreadCount);
  final Plurdart.AlertsUnreadCount alertsUnreadCount;
}

class AlertsGetUnreadCountFailed extends AlertsGetUnreadCountState {

}
