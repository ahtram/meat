import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'plurk_edit_complete_state.dart';

class PlurkEditCompleteCubit extends Cubit<PlurkEditCompleteState> {
  PlurkEditCompleteCubit() : super(PlurkEditCompleteInitial());

  success(Plurdart.Plurk plurk) async {
    emit(PlurkEditCompleteSuccess(plurk));
  }
}
