//The response page. (Should we include the plurk content at the top?)
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:after_layout/after_layout.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/rest/blocks_block_cubit.dart';
import 'package:meat/bloc/rest/responses_response_add_cubit.dart';
import 'package:meat/bloc/rest/responses_response_edit_cubit.dart';
import 'package:meat/bloc/rest/timeline_plurk_delete_cubit.dart';
import 'package:meat/bloc/ui/acrylic_background_switch_cubit.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/bloc/ui/report_abuse_dialog_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/plurk_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/response_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/bloc/ui/viewer_background_switch_cubit.dart';
import 'package:meat/bloc/ui/plurk_viewer_modal_prep_cubit.dart';
import 'package:meat/models/plurk/responses_collector.dart';
import 'package:meat/screens/user_viewer.dart';
// import 'package:meat/widgets/ad/comment_ad_widget.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/plurk/plurk_card.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';
import 'package:meat/widgets/plurk/response_card.dart';
import 'package:meat/widgets/plurk/response_poster.dart';
import 'package:meat/widgets/plurk/responses_tri_load_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:pretty_json/pretty_json.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tuple/tuple.dart';
import 'package:meat/system/vibrator.dart' as Vibrator;

import 'package:meat/screens/plurk_poster.dart' as PlurkPoster;
import 'package:meat/screens/response_editor.dart' as ResponseEditor;
import 'package:meat/screens/tags_trunk.dart' as TagsTrunk;
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/theme/app_themes.dart';

void show(BuildContext context, PlurkViewerArgs args) {
  Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (BuildContext context) {
          return _PlurkViewer(args);
        },
        settings: RouteSettings(
          name: 'plurk_viewer',
          arguments: args,
        ),
      ));
}

enum PlurkViewerMode {
  FromCardState,
  FromPlurkId,
}

class PlurkViewerArgs {
  final PlurkViewerMode mode;
  //FromCardState
  final PlurkCardState plurkCardState;
  final bool showPlurkCard;
  final Uint8List backgroundImage;
  //FromPlurkId
  final int plurkId;

  PlurkViewerArgs(this.mode,
      {this.plurkCardState,
      this.showPlurkCard,
      this.backgroundImage,
      this.plurkId});
}

class _PlurkViewer extends StatefulWidget {
  _PlurkViewer(this.args);

  final PlurkViewerArgs args;

  @override
  _PlurkViewerState createState() => _PlurkViewerState();
}

class _PlurkViewerState extends State<_PlurkViewer>
    with
        AfterLayoutMixin,
        SingleTickerProviderStateMixin,
        WidgetsBindingObserver {
  _PlurkViewerState();

  //Store the key for getting background.
  GlobalKey<PlurkCardState> _plurkCardKey = GlobalKey<PlurkCardState>();
  GlobalKey<AcrylicContainerState> _backgroundAcrylicContainerKey =
      GlobalKey<AcrylicContainerState>();

  //These are either set by CardState or get from API by plurkId...
  Plurdart.Plurk plurk;
  Plurdart.User owner;
  Plurdart.User replurker;

  List<Plurdart.User> favorers;
  List<Plurdart.User> replurkers;

  BuildContext _blocProviderContext;
//  BuildContext _scaffoldContext;

  //I cannot find a way to access the PrimaryScrollController so I have to perform this voodoo.
  CustomScrollView _customScrollView;
  RefreshController _refreshController = RefreshController();

  //We'll need a fancy class to store ResponsesList.
  ResponsesCollector _responsesCollector;
  // For blur SliverAppBar.
  // final double _sigmaX = 5.0; // from 0-10
  // final double _sigmaY = 5.0; // from 0-10
  // final double _opacity = 0.5; // from 0-1.0

  //For operate ResponseEditor. (like add nickName to comment)
  GlobalKey<ResponsePosterState> _responseEditorKey =
      GlobalKey<ResponsePosterState>();

  GlobalKey<PlurkCardPrimeHeaderState> _primeHeaderKey =
      GlobalKey<PlurkCardPrimeHeaderState>();

  //This is stupid...but I think this'll work.
  //We need the actual soft keyboard height to use as Emoticon's height.
  //The system doesn't give the actual number so we are better measure it ourselves.
  double measuredSoftKeyboardHeight = kMinInteractiveDimension;

  //Test cache all response cards for getting their render object.
  List<GlobalKey> _responseCardKeys = [];

  //For floating arrow button.
  // AnimationController _arrowAnimationController;
  // Animation<double> _arrowRotateAnimation;
  double _arrowRotate = 0;
  bool _arrowPointToDown = true;
  double _lastScrollPosition = 0.0;
  // double _lastMaxScrollExtent = 0.0;

  bool _enableComment = false;
  bool _showAuthorResponsesOnly = false;

  //Long press a responseCard to highlight a user for better readability.
  String highlightingUserName = '';

  //Image grid expand mode for all response cards.
  bool _expandResponsesImageGrids = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    //[Dep] This will cause a bit scrolling lag.
    // _arrowAnimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 150,));
    // _arrowRotateAnimation = Tween<double>(begin: 0.0, end: pi,).animate(_arrowAnimationController);
    // _arrowAnimationController.addListener(() {
    //   setState(() {
    //
    //   });
    // });

    // print('initState copyDataFromWidget() mode[' + widget.args.mode.toString() + ']');
    if (widget.args.mode == PlurkViewerMode.FromCardState) {
      copyDataFromCardState();
      //Try Init collector.
      if (plurk != null) {
        _responsesCollector = ResponsesCollector(
            plurkID: plurk.plurkId, initialResponseCount: plurk.responseCount);

        //這邊判斷是否要允許回文 _enableComment
        _decideEnableComment();
      }
    }
  }

  //判斷是否允許回文
  void _decideEnableComment() async {
    if (plurk != null) {
      //這邊判斷是否要允許回文 _enableComment
      //判斷這個噗文是不是我自己的
      if (plurk.ownerId == Static.me.id) {
        //自己的文，允許回文
        _enableComment = true;
      } else {
        switch (plurk.noComments) {
          case Plurdart.Comment.Allow:
            _enableComment = true;
            break;
          case Plurdart.Comment.DisableComment:
            _enableComment = false;
            break;
          case Plurdart.Comment.FriendsOnly:
          //這邊要判斷是否這個人是我的朋友?
          //目前似乎沒有比較有效率的判斷手法．
            Plurdart.Profile profile = await Plurdart.profileGetPublicProfile(Plurdart.ProfileGetPublicProfile(
              userId: plurk.ownerId,
            ));
            if (profile != null) {
              if (!profile.hasError()) {
                if (profile.areFriends) {
                  //是好友，因此允許回文
                  _enableComment = true;
                }
              }
            }
            break;
          default:
          //不知道啥狀況
            _enableComment = true;
            break;
        }
      }
    }

    setState(() { });
  }

  copyDataFromCardState() {
    plurk = widget.args.plurkCardState.widget.plurk;
    owner = widget.args.plurkCardState.widget.owner;
    replurker = widget.args.plurkCardState.widget.replurker;
    favorers = widget.args.plurkCardState.widget.favorers;
    replurkers = widget.args.plurkCardState.widget.replurkers;
  }

  @override
  void dispose() {
    _responseCardKeys.clear();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    //A voodoo hack for naturally getting _lastMaxScrollExtent updated.
    // _customScrollView.controller.animateTo(_lastScrollPosition + 1,
    //     duration: Duration(milliseconds: 10), curve: Curves.linear);
    // _lastScrollPosition = _lastScrollPosition + 1;

    if (mounted) {
      //SO SO stupid we have to delay the measure timing.
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (mounted) {
          if (MediaQuery.of(context).viewInsets.bottom >
              measuredSoftKeyboardHeight) {
            //Just remember the highest viewInsets.bottom.
            measuredSoftKeyboardHeight =
                MediaQuery.of(context).viewInsets.bottom;
            //        print('MediaQuery.of(context).viewInsets.bottom: [' + MediaQuery.of(context).viewInsets.bottom.toString() + ']');
          }
        }

        //This keeps getting problems. I cannot solve it...
        //Damn so hacky I have to wait for _lastMaxScrollExtent change naturally...
        // Future.delayed(Duration(milliseconds: 20), () {
        //   // print('check keyboard and scroll animateTo...');
        //   // This is getting wrong animation.
        //   if (isShowingKeyboard() && _customScrollView != null) {
        //     // print('_lastScrollPosition:[' + _lastScrollPosition.toString() + '] | measuredSoftKeyboardHeight:[' + measuredSoftKeyboardHeight.toString() + '] | _lastMaxScrollExtent:[' + _lastMaxScrollExtent.toString() + ']');
        //     double wantAnimateToTarget =
        //         _lastScrollPosition + measuredSoftKeyboardHeight - 2;
        //
        //     //[Crap] The _lastMaxScrollExtent is not accurate for now...
        //
        //     //Try limit to the max extent.
        //     if (wantAnimateToTarget > _lastMaxScrollExtent - 2) {
        //       wantAnimateToTarget = _lastMaxScrollExtent - 2;
        //     }
        //
        //     _customScrollView.controller.animateTo(wantAnimateToTarget,
        //         duration: Duration(milliseconds: 100), curve: Curves.linear);
        //     _lastScrollPosition = wantAnimateToTarget;
        //
        //     // print('KeyboardVisibilityController().onChange: [' + wantAnimateToTarget.toString() + ']');
        //   }
        // });
      });
    }
  }

  //A hacky way to see if the virtual is showing.
  bool isShowingKeyboard() {
    return KeyboardVisibilityController().isVisible;
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //Test if this change bloc's search.
    widget.args.plurkCardState?.didChangeDependencies();
    if (mounted) {
      if (widget.args.mode == PlurkViewerMode.FromPlurkId) {
        //Call the getPlurk API by plurkId.
        _prepForPlurkModal();
        // widget.args.plurkId
      } else if (widget.args.mode == PlurkViewerMode.FromCardState) {
        //Request responses. (First time refresh)
        _refreshController.requestRefresh();
      }
    }
  }

  //Use Plurk API to get the modal we need..
  _prepForPlurkModal() async {
    // print('[_prepForPlurkModal]');

    Plurdart.PlurkWithUser plurkWithUser =
        await Plurdart.timelineGetPlurk(Plurdart.TimelineGetPlurk(
      plurkId: widget.args.plurkId,
      favorersDetail: true,
      limitedDetail: true,
      replurkersDetail: true,
    ));

    if (!mounted) {
      return;
    }

    if (plurkWithUser != null && !plurkWithUser.hasError()) {
      plurk = plurkWithUser.plurk;

      if (plurk != null) {
        //Try get owner and Replurker.
        if (plurkWithUser.user != null) {
          owner = plurkWithUser.user;
        } else if (plurkWithUser.plurkUsers
            .containsKey(plurk.ownerId.toString())) {
          owner = plurkWithUser.plurkUsers[plurk.ownerId.toString()];
        } else if (plurkWithUser.plurkUsers
            .containsKey(plurk.userId.toString())) {
          owner = plurkWithUser.plurkUsers[plurk.userId.toString()];
        } else {
          print('Oops! plurkWithUser.plurkUsers doesn\'t contains user?!');
        }

        //Replurker
        if (plurk.replurkerId != null) {
          if (plurkWithUser.plurkUsers
              .containsKey(plurk.replurkerId.toString())) {
            replurker = plurkWithUser.plurkUsers[plurk.replurkerId.toString()];
          } else {
            print(
                'Oops! plurkWithUser.plurkUsers doesn\'t contains replurker?!');
          }
        }

        //Try Init collector.
        _responsesCollector = ResponsesCollector(
            plurkID: plurk.plurkId, initialResponseCount: plurk.responseCount);

        //這邊判斷是否要允許回文 _enableComment
        _decideEnableComment();
      }

      //Modal ready UI bloc.
      _blocProviderContext.read<PlurkViewerModalPrepCubit>().complete();

      // print('emit PlurkViewerModalPrepCubit complete');

      //Request responses. (First time refresh)
      _refreshController.requestRefresh();
    } else {
      //Failed?
      // print('emit PlurkViewerModalPrepCubit failed');
      _blocProviderContext.read<PlurkViewerModalPrepCubit>().failed(
          (plurkWithUser != null)
              ? (plurkWithUser.errorText)
              : ('Unknown error.'));
    }
  }

  //Check if the modals are ready for display. (For use in the bloc builder)
  bool modalsAreReady() {
    return (plurk != null);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      //A hack for prevent WillPopScope from stop swipe back on iOS.
      //iOS's guide-line: never prevent from back swipe gesture.
      onWillPop: (Platform.isIOS)
          ? null
          : () async {
              //Check if we need to exit emoticon insert mode.
              if (_responseEditorKey.currentState != null) {
                if (_responseEditorKey.currentState.isInEmoticonsMode) {
                  //Exit emoticon mode.
                  _responseEditorKey.currentState.toggleEmoticonMode(false);
                  Vibrator.vibrateShortNote();
                  return false;
                } else if (_responseEditorKey.currentState.isInEditingMode) {
                  _responseEditorKey.currentState.tryUnfocusCommentTextField();
                  Vibrator.vibrateShortNote();
                  return false;
                }
              }

              //Just refresh the one associate with me before pop.
              // widget.args.plurkCardState?.refresh();
              return true;
            },
      child: MultiBlocProvider(
        providers: [
          BlocProvider<ResponsesResponseAddCubit>(
            create: (BuildContext context) => ResponsesResponseAddCubit(),
          ),
          BlocProvider<ResponsesResponseEditCubit>(
            create: (BuildContext context) => ResponsesResponseEditCubit(),
          ),
          BlocProvider<RequestIndicatorCubit>(
            create: (BuildContext context) => RequestIndicatorCubit(),
          ),
          BlocProvider<PlurkViewerModalPrepCubit>(
            create: (BuildContext context) => PlurkViewerModalPrepCubit(),
          ),
          //For PlurkCard switch PlurkViewer background.
          BlocProvider<ViewerBackgroundSwitchCubit>(
            create: (BuildContext context) => ViewerBackgroundSwitchCubit(),
          ),
        ],
        child: MultiBlocListener(
          listeners: [
//            BlocListener<YoutubeFullscreenCubit, YoutubeFullscreenState>(
//              listener: (context, state) {
//                if (state is YoutubeFullscreenEnterState) {
////                  print('_onYoutubeFullscreenEnterState');
//                  _onYoutubeFullscreenEnterState(state);
//                } else if (state is YoutubeFullscreenLeaveState) {
////                  print('_onYoutubeFullscreenLeaveState');
//                  _onYoutubeFullscreenLeaveState(state);
//                }
//              },
//            ),
//            BlocListener<ImageLinksViewerCubit, ImageLinksViewerState>(
//              listener: (context, state) {
//                if (state is ImageLinksViewerEnterState) {
////                  print('_onImageLinksViewerEnterState');
//                  _onImageLinksViewerEnterState(state);
//                }
//              },
//            ),
            BlocListener<ResponsesResponseAddCubit, ResponsesResponseAddState>(
              listener: (context, state) {
                if (state is ResponsesResponseAddSuccess) {
//                  print('_onResponsesGetState');
                  _onResponseAddSuccessState(state);
                } else if (state is ResponsesResponseAddFailed) {
                  _showFlushBar(_blocProviderContext, Icons.warning,
                      'Response add failed!', 'Maybe it\'s a network problem?');
                }
              },
            ),
            BlocListener<ResponseEditCompleteCubit, ResponseEditCompleteState>(
              listener: (context, state) {
                if (state is ResponseEditCompleteSuccess) {
                  //Self update this response content.
                  setState(() {
                    if (_responsesCollector != null) {
                      _responsesCollector.updateEditResponse(
                          state.responseId, state.responseEditResult);
                    }
                  });
                }
              },
            ),
            BlocListener<FlushBarCubit, FlushBarState>(
              listener: (context, state) {
                if (state is FlushBarRequest) {
                  //Only display FlushBar when I am the current route.
                  if (ModalRoute.of(context).isCurrent) {
                    _showFlushBar(_blocProviderContext, state.icon, state.title,
                        state.message);
                  }
                }
              },
            ),
            BlocListener<TimelinePlurkDeleteCubit, TimelinePlurkDeleteState>(
              listener: (context, state) {
                if (state is TimelinePlurkDeleteSuccess) {
                  //If success leave the plurk view.
                  if (state.plurkId == plurk.plurkId) {
                    Navigator.pop(context);
                  }
                }
              },
            ),
            BlocListener<PlurkEditCompleteCubit, PlurkEditCompleteState>(
              listener: (context, state) {
                if (state is PlurkEditCompleteSuccess) {
                  //Override self displaying plurk ...
                  setState(() {
                    plurk = state.plurk;
                  });
                }
              },
            ),

            //[Very Tricky]: When success block a user. Check if the user is this one and remove the route.
            BlocListener<BlocksBlockCubit, BlocksBlockState>(
              listener: (context, state) {
                if (state is BlocksBlockSuccess) {
                  if (state.userId == plurk.userId) {
                    //We should remove this viewer from the route...
                    // print('[Detect user blocked. Remove the route...]');
                    Navigator.removeRoute(context, ModalRoute.of(context));
                  }
                }
              },
            ),
          ],
          child: Builder(
            builder: (context) {
              // print('PlurkViewer PlurkViewerBackgroundSwitchCubit [' + state.runtimeType.toString() + ']');

              //Try get the background image of PlurkCard.
              //This is for CardStateMode only. (This could be null in FromPlurkId mode)
              ImageProvider backgroundProvider;

              //Try use the passed in background (from PlurkCardState)
              if (null != widget.args.backgroundImage) {
                backgroundProvider = MemoryImage(widget.args.backgroundImage);
              } else {
                //Use the default BahKutTeh asset image.
                backgroundProvider =
                    AssetImage('assets/images/background001.jpg');
              }

              return Container(
                // This is for the bottom most background color!
                color: Theme.of(context).canvasColor,
                child: AcrylicContainer(
                  BlocListener<ViewerBackgroundSwitchCubit,
                      ViewerBackgroundSwitchState>(
                    listener: (context, state) {
                      if (state is ViewerBackgroundSwitchActive) {
                        //[Very Tricky!]: We have to listen to the child PlurkCard's bloc event
                        //And inform the background AcrylicContainer of mine to set its background.

                        // print('PlurkViewer PlurkViewerBackgroundSwitchCubit [' + state.runtimeType.toString() + ']');
                        //Inform the background acrylic container to set background.
                        if (_backgroundAcrylicContainerKey.currentState !=
                            null) {
                          if (_backgroundAcrylicContainerKey
                                  .currentState.blocProviderContext !=
                              null) {
                            //Not sure if this will trigger PlurkCard rebuild and loop again...
                            _backgroundAcrylicContainerKey
                                .currentState.blocProviderContext
                                .read<AcrylicBackgroundSwitchCubit>()
                                .active(state.image);
                          }
                        }
                      }
                    },
                    child: Scaffold(
                      backgroundColor: Colors.transparent,
                      resizeToAvoidBottomInset: true,
                      floatingActionButtonLocation:
                          FloatingActionButtonLocation.miniEndFloat,
                      //Display load indicator on the floating action button place.
                      floatingActionButton: BlocBuilder<RequestIndicatorCubit,
                          RequestIndicatorState>(
                        builder: (context, state) {
                          //Check state.
                          List<Widget> columnChildren = [];

                          //Only when showing request indicator.
                          if (state is RequestIndicatorShow) {
                            //Busy indicator.
                            columnChildren.add(Container(
                              width: 56,
                              height: 56,
                              child: FlareActor(
                                "assets/rive/MeatLoader.flr",
                                animation: "Untitled",
                              ),
                            ));
                          }

                          columnChildren.add(FloatingActionButton(
                            elevation: 1,
                            backgroundColor: Theme.of(context)
                                .lowContrastPrimaryColor
                                .withOpacity(0.75),
                            foregroundColor:
                                Theme.of(context).textTheme.bodyText1.color,
                            child: Transform.rotate(
                              angle: _arrowRotate,
                              child: Icon(
                                MdiIcons.arrowDownBold,
                              ),
                            ),
                            onPressed: () {
                              //Scroll to top or bottom
                              if (_arrowPointToDown) {
                                //Scroll to bottom
                                // _startLoadingToTheEnd();
                                ScrollController sc =
                                    PrimaryScrollController.of(context);
                                if (sc != null) {
                                  //Not working...
                                  double maxScrollExtent =
                                      sc.position.maxScrollExtent;
                                  // print('Got maxScrollExtent [' + maxScrollExtent.toString() + ']');

                                  //Just jump to max position.
                                  sc.position.animateTo(maxScrollExtent,
                                      duration: Duration(microseconds: 500),
                                      curve: Curves.ease);
                                }
                              } else {
                                //Scroll to top.
                                ScrollController sc =
                                    PrimaryScrollController.of(context);
                                if (sc != null) {
                                  // print('Scroll to top');
                                  sc.position.animateTo(0.0,
                                      duration: Duration(microseconds: 500),
                                      curve: Curves.ease);
                                  _lastScrollPosition = 0.0;
                                }
                                // //Check the current position to see if instant set or animateTo
                                // if (_refreshController.position.pixels > 5000) {
                                //   //Too far! Just set pos to 0.0
                                //   _refreshController.position.jumpTo(5000.0);
                                //   _refreshController.position.animateTo(0.0,
                                //       duration: Duration(
                                //           milliseconds:
                                //           (_refreshController.position.pixels * 0.1).toInt()),
                                //       curve: Curves.linear);
                                // } else {
                                //   //AnimateTo 0.0
                                //   _refreshController.position.animateTo(0.0,
                                //       duration: Duration(
                                //           milliseconds:
                                //           (_refreshController.position.pixels * 0.1).toInt()),
                                //       curve: Curves.linear);
                                // }
                              }
                            },
                          ));

                          return Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: columnChildren,
                          );
                        },
                      ),
                      body: Builder(
                        builder: (context) {
                          _blocProviderContext = context;
                          Color indicatorColor =
                              Theme.of(context).textTheme.bodyText1.color;

                          //Voodoo cache!
                          _customScrollView = CustomScrollView(
                            primary: true,
                            slivers: _customScrollViewSliverWidgets(),
                          );

                          return NotificationListener<ScrollNotification>(
                              child: GestureDetector(
                                  child: SmartRefresher(
                                    controller: _refreshController,
                                    header: WaterDropMaterialHeader(
                                      color: indicatorColor,
                                    ),
                                    onRefresh: _onRefresh,
                                    child: _customScrollView,
                                  ),
                                  onVerticalDragDown: (detail) {
                                    //Hide keyboard for good.
                                    if (_responseEditorKey.currentState !=
                                        null) {
                                      if (_responseEditorKey
                                          .currentState.isInEmoticonsMode) {
                                        //Exit emoticon mode.
                                        _responseEditorKey.currentState
                                            .toggleEmoticonMode(false);
                                      }

                                      if (_responseEditorKey
                                          .currentState.isInEditingMode) {
                                        _responseEditorKey.currentState
                                            .tryUnfocusCommentTextField();
                                      }
                                    }
                                  },
                                  onVerticalDragStart: (detail) {
                                    //This is not working due to flutter bug.
                                    // print('onVerticalDragStart: ' +
                                    //     detail.localPosition.toString());
                                  },
                                  onVerticalDragUpdate: (detail) {
                                    // print('onVerticalDragUpdate: ' +
                                    //     detail.localPosition.toString());
                                  }),
                              onNotification: (scrollNotification) {
                                // print('Got scrollNotification.metrics.maxScrollExtent:[' + scrollNotification.metrics.maxScrollExtent.toString() + ']');
                                if (scrollNotification
                                    is ScrollUpdateNotification) {
                                  if (scrollNotification.metrics.pixels >=
                                      scrollNotification
                                          .metrics.maxScrollExtent) {
                                    // call fetch more method here
                                    _loadBottomEndMore();
                                  }

                                  if (scrollNotification.metrics.pixels <=
                                      0.0) {
                                    if (!_arrowPointToDown) {
                                      _arrowDirectToDown();
                                    }
                                  } else if (scrollNotification
                                          .metrics.pixels >=
                                      scrollNotification
                                          .metrics.maxScrollExtent) {
                                    if (_arrowPointToDown) {
                                      _arrowDirectToUp();
                                    }
                                  } else {
                                    //Track the scroll delta to decide the arrow direction.
                                    if (scrollNotification.scrollDelta < -5.0) {
                                      //Prevent from point to down if we are near 0.
                                      if (scrollNotification.metrics.pixels >
                                          100) {
                                        if (_arrowPointToDown) {
                                          _arrowDirectToUp();
                                        }
                                      }
                                    } else if (scrollNotification.scrollDelta >
                                        5.0) {
                                      //Prevent from point to down if we are near the maxScrollExtent.
                                      if (scrollNotification.metrics.pixels <
                                          scrollNotification
                                                  .metrics.maxScrollExtent -
                                              100) {
                                        if (!_arrowPointToDown) {
                                          _arrowDirectToDown();
                                        }
                                      }
                                    }
                                  }

                                  //This is a bit hacky! I don't know why sometimes scrollNotification.metrics.pixels is 0.0!
                                  //Not sure why but when keyboard showing the _lastScrollPosition is problematic!

                                  //The scroll notification did some weired things I have to prevent some crazy offset when the keyboard is showing!
                                  //Or the _lastScrollPosition get screwed!
                                  bool isScrollTooMuch = false;
                                  if ((scrollNotification.metrics.pixels -
                                              _lastScrollPosition)
                                          .abs() >
                                      360.0) {
                                    isScrollTooMuch = true;
                                  }

                                  if (scrollNotification.metrics.pixels > 1.0 &&
                                      !isShowingKeyboard() &&
                                      !isScrollTooMuch) {
                                    _lastScrollPosition =
                                        scrollNotification.metrics.pixels;
                                    // print('got _lastScrollPosition:[' + _lastScrollPosition.toString() + ']');
                                  }

                                  //This is a bit hacky! I don't know why sometimes scrollNotification.metrics.maxScrollExtent is 0.0!
                                  // if (scrollNotification.metrics.maxScrollExtent > 1.0 && !isShowingKeyboard()) {
                                  //   //THIS IS AWFULLY INACCURATE!!!!
                                  //   _lastMaxScrollExtent = scrollNotification.metrics.maxScrollExtent;
                                  //   // print('got _lastMaxScrollExtent:[' + _lastMaxScrollExtent.toString() + ']');
                                  // }
                                }

                                return true;
                              });
                        },
                      ),
                      //Need a ui bloc builder here for checking Plurk downloaded.
                      bottomNavigationBar: BlocBuilder<
                          PlurkViewerModalPrepCubit,
                          PlurkViewerModalPrepState>(builder: (context, state) {
                        // print('bottomNavigationBar BlocBuilder: ' + state.runtimeType.toString());
                        //Just check if the modal is ready.
                        if (modalsAreReady()) {

                          return BottomAppBar(
                            elevation: 0,
                            color: Colors.transparent,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                // Input UIs here...
                                ResponsePoster(
                                  plurk.plurkId,
                                  //I have no way to know if I can response this Plurk!
                                  _enableComment,
                                  key: _responseEditorKey,
                                ),

                                SizedBox(
                                  height:
                                      MediaQuery.of(context).viewInsets.bottom,
                                ),
                              ],
                            ),
                          );
                        } else {
                          //Empty view. (still loading)
                          //This will need a height or it will expand to full screen.
                          return Container(
                            height: kMinInteractiveDimension,
                          );
                        }
                      }),
                    ),
                  ),
                  //The background image provider could be null.
                  initialImage: backgroundProvider,
                  transparentBackLayer: false,
                  key: _backgroundAcrylicContainerKey,
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  //The sliverWidgets for main custom view
  List<Widget> _customScrollViewSliverWidgets() {
    // print('[_customScrollViewSliverWidgets]');

    List<Widget> returnWidgets = [];

    //SliverAppBar.
    // print('add SliverAppBar');
    returnWidgets.add(SliverAppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      floating: true,
      flexibleSpace: AcrylicContainer(
        Container(),
        backgroundColor: Theme.of(context).lowContrastPrimaryColor,
        transparentBackLayer: false,
        mode: AcrylicContainerMode.BackdropFilter,
      ),
      leading: AppBarActionButton(Icons.arrow_back, onTap: () {
        if (_responseEditorKey.currentState != null) {
          if (_responseEditorKey.currentState.isInEmoticonsMode) {
            //Exit emoticon mode.
            _responseEditorKey.currentState.toggleEmoticonMode(false);
            Vibrator.vibrateShortNote();
          } else if (_responseEditorKey.currentState.isInEditingMode) {
            _responseEditorKey.currentState.tryUnfocusCommentTextField();
            Vibrator.vibrateShortNote();
          }
        }

        //Just refresh the one associate with me before pop.
        // widget.args.plurkCardState?.refresh();
        Navigator.pop(context);
      }),
      titleSpacing: 0.0,
      centerTitle: false,
      title: BlocBuilder<PlurkViewerModalPrepCubit, PlurkViewerModalPrepState>(
          builder: (context, state) {
        // print('title BlocBuilder: ' + state.runtimeType.toString());

        if (modalsAreReady()) {
          return PlurkCardPrimeHeader(
            plurk,
            owner,
            replurker,
            false,
            false,
            key: _primeHeaderKey,
            onUserNameTap: (user) {
              //Add this user's nickName to the comment TextField.
              _responseEditorKey.currentState
                  .tryInsertNickNameStr('@' + user.nickName);
            },
            onReplurkerNameTap: (user) {
              //Add this user's nickName to the comment TextField.
              _responseEditorKey.currentState
                  .tryInsertNickNameStr('@' + user.nickName);
            },
            onAvatarTap: (user) {
              //Bloc to open UserViewer.
              context
                  .read<UserViewerCubit>()
                  .open(UserViewerArgs(user.nickName));
            },
            onWantEditPlurk: (plurk) {
              // print('PlurkViewer onWantEditPlurk.');
              //Open Plurk Poster for edit post.
              PlurkPoster.show(
                  context,
                  PlurkPoster.PlurkPosterArgs(
                    PlurkPoster.PlurkPosterMode.Edit,
                    editingPlurk: plurk,
                  ));
            },
            onWantDeletePlurk: (plurk) {
              //onWantDeletePlurk
              _blocProviderContext
                  .read<TimelinePlurkDeleteCubit>()
                  .request(plurk.plurkId);
              // print('PlurkViewer onWantDeletePlurk');
            },
            onWantReportAbuse: (plurkId) {
              //Bloc to open UserViewer.
              context.read<ReportAbuseDialogCubit>().reportPlurk(plurkId);
            },
            onWantEditBookmarkTags: () {
              _openTagsTrunk();
            },
          );
        } else {
          //Empty view.
          return Container(
            child: Text(''),
          );
        }
      }),
    ));

    //PlurkCard + ResponseCards
    returnWidgets.add(
        BlocBuilder<PlurkViewerModalPrepCubit, PlurkViewerModalPrepState>(
            builder: (context, state) {
      // print('SliverList BlocBuilder: ' + state.runtimeType.toString());
      if (state is PlurkViewerModalPrepFailed) {
        return SliverList(
          delegate: SliverChildListDelegate([
            SizedBox(
              height: 128,
            ),
            Icon(
              MdiIcons.alertCircle,
              size: 128,
            ),
            SizedBox(
              height: 8,
            ),
            Center(
              child: Container(
                  child: Text(
                state.errorText,
                style: Theme.of(context).textTheme.subtitle1,
              )),
            ),
            SizedBox(
              height: 128,
            ),
          ]),
        );
      } else {
        if (modalsAreReady()) {
          return SliverList(
            delegate: SliverChildListDelegate(
              _buildPlurkCardAndResponseCards(),
            ),
          );
        } else {
          //Empty view
          return SliverList(
            delegate: SliverChildListDelegate([
              //Empty list.
            ]),
          );
        }
      }
    }));

    return returnWidgets;
  }

  List<Widget> _buildPlurkCardAndResponseCards() {
    List<Widget> returnList = [];

    List<Tuple2<Plurdart.Response, Plurdart.User>> feed =
        _responsesCollector.getFeed();

    //In FromPlurkId mode the showPlurkCard will always be true.
    bool showPlurkCard =
        widget.args.showPlurkCard == null ? true : widget.args.showPlurkCard;

    if (widget.args.mode == PlurkViewerMode.FromPlurkId) {
      showPlurkCard = true;
    }

    //PlurkCard??
    if (showPlurkCard) {
      returnList.add(PlurkCard(
        plurk,
        owner,
        replurker,
        favorers,
        replurkers,
        PlurkCardMode.PlurkView,
        false,
        false,
        useImageCollectionBackground: true,
        key: _plurkCardKey,
        hintPorn: false,
        keepAliveWhenFoundImageLinks: true,
        onBodyLongPress: () {
          //Try show the popup menu.
          _primeHeaderKey.currentState?.showButtonMenu();
        },
      ));
    }

    //Display ADWidgets??
    bool shouldDisplayADs = Static.shouldDisplayAds();
    int currentFeedCountPerAd = Define.COMMENT_COUNT_PER_AD;
    int adAppearCountDown = currentFeedCountPerAd;

    //First one
    //Only when not premium and set to show ads.
    if (shouldDisplayADs) {
      //[Dep]
      // returnList.add(CommentAdWidget());
    }

    //Response Count.
    String responseCountBarDisplayStr = '';
    if (_showAuthorResponsesOnly) {
      //很智障，好像沒有比較好的方式預算...
      int showingAuthorResponseCount = 0;
      for (int i = 0; i < feed.length; ++i) {
        if (feed[i] != null && feed[i].item2 != null && feed[i].item2.id == plurk.ownerId) {
          showingAuthorResponseCount++;
        }
      }

      responseCountBarDisplayStr += 'Responses: ' + showingAuthorResponseCount.toString();
      responseCountBarDisplayStr += ' (From Author)';
    } else {
      responseCountBarDisplayStr += 'Responses: ' + plurk.responseCount.toString();
      responseCountBarDisplayStr += ' (All)';
    }
    returnList.add(_buttonBar(responseCountBarDisplayStr, () {
      //Switch response display filter all/authorOnly.
      setState(() {
        _showAuthorResponsesOnly = !_showAuthorResponsesOnly;
      });
      Vibrator.vibrateShortNote();
    }));

    //Expand all image grid button.
    if (plurk.responseCount > 0) {
      String expandImageGridsBarDisplayStr = '';
      Color expandImageGridsBarColor = Colors.transparent;
      if (_expandResponsesImageGrids) {
        expandImageGridsBarDisplayStr = FlutterI18n.translate(context, 'btnLabelCollapseAllResponseImageGrids');
      } else {
        expandImageGridsBarDisplayStr = FlutterI18n.translate(context, 'btnLabelExpandAllResponseImageGrids');
        expandImageGridsBarColor = Theme.of(context).primaryColor.withOpacity(0.5);
      }

      returnList.add(_buttonBar(expandImageGridsBarDisplayStr, () {
        //Switch _expandResponsesImageGrids.
        setState(() {
          _expandResponsesImageGrids = !_expandResponsesImageGrids;
        });
        Vibrator.vibrateShortNote();
      }, color: expandImageGridsBarColor));
    }

    //ResponseCards.
    _responseCardKeys.clear();
    bool hasMeetANullFeedElement = false;
    for (int i = 0; i < feed.length; ++i) {
      //Only when not premium and set to show ads.
      if (shouldDisplayADs) {
        adAppearCountDown--;
      }

      if (feed[i] == null) {
        hasMeetANullFeedElement = true;
        //Skip this responseCard
      } else {
        //Check if this is the end of MeetANullFeedElement
        if (hasMeetANullFeedElement) {
          hasMeetANullFeedElement = false;
          //Add the 3 in 1 load response button.
          returnList.add(Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: ResponsesTriLoadButton(
              onLoadTopDown: () {
                _loadTopDown();
              },
              onLoadAll: () {
                _loadAll();
              },
              onLoadBottomUp: () {
                _loadBottomUp();
              },
            ),
          ));
        }

        //Add the new response card.
        bool showDisplayResponseCard = true;
        if (_showAuthorResponsesOnly) {
          if (feed[i].item2.id == plurk.ownerId) {
            showDisplayResponseCard = true;
          } else {
            showDisplayResponseCard = false;
          }
        }

        if (showDisplayResponseCard) {
          Widget newResponseCard =
          _buildResponseCard(plurk, feed[i].item1, feed[i].item2, i);
          _responseCardKeys.add(newResponseCard.key);
          returnList.add(newResponseCard);
        }

        //Only when not premium and set to show ads.
        if (shouldDisplayADs && adAppearCountDown == 0 && !_showAuthorResponsesOnly) {
          //[Dep]
          // returnList.add(CommentAdWidget());
        }
      }

      //Only when not premium and set to show ads.
      if (shouldDisplayADs && adAppearCountDown == 0) {
        if (currentFeedCountPerAd < Define.COMMENT_COUNT_PER_AD_MAX) {
          currentFeedCountPerAd += Define.COMMENT_COUNT_PER_AD_INC;
        }
        adAppearCountDown = currentFeedCountPerAd;
      }
    }

    //EndOfResponsesBar
    //We need a proper footer
    returnList.add(_endOfResponsesBar(plurk.responseCount));

    return returnList;
  }

  //Display response count widget.
  Widget _buttonBar(String displayStr, Function onTap, {Color color = Colors.transparent}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        color: color,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 8,
            ),
            Text(
              displayStr,
              style: TextStyle(
                color: Theme.of(context).hintColor,
                fontSize: 13,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(height: 0.5, thickness: 0.5),
          ],
        ),
      ),
    );
  }

  Widget _endOfResponsesBar(int responseCount) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 8,
          ),
          Text('End Of Responses',
              style: TextStyle(
                color: Theme.of(context).hintColor,
                fontSize: 13,
              )),
          SizedBox(
            height: 10,
          ),
          Divider(height: 0.5, thickness: 0.5),
          SizedBox(
            height: 80,
          ),
        ],
      ),
    );
  }

  _openTagsTrunk() {
    TagsTrunk.show(
        context,
        TagsTrunk.TagsTrunkArgs(
            plurkID: plurk.plurkId,
            onTagsDecided: (int bookmarkID, List<String> tags) async {
              //Update the plurk's tags silently.
              Plurdart.Bookmark bookmark =
                  await Plurdart.bookmarksUpdateBookmark(
                      Plurdart.BookmarksUpdateBookmark(
                bookmarkID: bookmarkID,
                tags: tags,
              ));

              //Should we do any error check or just ignore it??
              // if (bookmark != null) {
              //   if (bookmark.hasError()) {
              //     print('[bookmark error]: ' + bookmark.errorText);
              //   } else {
              //     print('[bookmark success]: ' + bookmark.plurkId.toString());
              //   }
              // }
            }));
  }

  _onRefresh() {
//    print('_onRefresh');
    _loadInitial();
  }

  _arrowDirectToUp() {
    setState(() {
      _arrowRotate = pi;
      _arrowPointToDown = false;
    });
  }

  _arrowDirectToDown() {
    setState(() {
      _arrowRotate = 0.0;
      _arrowPointToDown = true;
    });
  }

  // _onYoutubeFullscreenEnterState(YoutubeFullscreenEnterState state) {
  //   //Enter Youtube fullscreen player.
  //   YoutubeFullScreenViewer.show(context, state.args);
  // }
  //
  // _onYoutubeFullscreenLeaveState(YoutubeFullscreenLeaveState state) {
  //   //Maybe we don't even need this?
  // }
  //
  // _onImageLinksViewerEnterState(ImageLinksViewerEnterState state) {
  //   // Do we need to listen to the onExitFullscreen event??
  //   ImageLinksViewer.show(context, state.args);
  // }

  // Build the responseCard.
  Widget _buildResponseCard(Plurdart.Plurk plurk, Plurdart.Response response,
      Plurdart.User user, int index) {
    bool highlighting = false;
    String displayNameStr = (response != null && response.handle != null)
        ? response.handle
        : user.displayName;

    if (highlightingUserName != '') {
      if (highlightingUserName == displayNameStr) {
        highlighting = true;
      }
    }

    return ResponseCard(plurk, response, user, index, (response, user) {
      String nickNameStr = (response != null && response.handle != null)
          ? response.handle
          : '@' + user.nickName;

      //Add this user's nickName to the comment TextField.
      _responseEditorKey.currentState.tryInsertNickNameStr(nickNameStr);
      // print('plurkViewer responseCard onNameTap ');
    }, (user) {
      //Bloc to open UserViewer.
      context.read<UserViewerCubit>().open(UserViewerArgs(user.nickName));
    }, (response, user) {
      //Try edit response.
      //Open Response Editor for edit post.
      ResponseEditor.show(
          context,
          ResponseEditor.ResponseEditorArgs(
            plurk,
            response,
            user,
            index,
          ));
    }, (response) {
      //Try delete response.
      _deleteResponse(response);
    }, (userDisplayName) {
      if (userDisplayName != null) {
        //Highlight this user.
        setState(() {
          if (userDisplayName != highlightingUserName) {
            highlightingUserName = userDisplayName;
          } else {
            highlightingUserName = '';
          }
        });
      }
    }, highlighting, _expandResponsesImageGrids ? ImageCollectionType.ExpandAll : ImageCollectionType.Summery);
  }

  //-- Responses request relative

  // The will clear the whole list and start from the beginning.
  _loadInitial() async {
    if (_responsesCollector != null && !_responsesCollector.isLoading()) {
      Vibrator.vibrateShortNote();
      await _responsesCollector.loadInitial();

      if (!mounted) return;

      _refreshController.refreshCompleted();
      setState(() {});
    } else {
      _refreshController.refreshCompleted();
    }
  }

  _loadTopDown() async {
    if (_responsesCollector != null && !_responsesCollector.isLoading()) {
      Vibrator.vibrateShortNote();
      _blocProviderContext.read<RequestIndicatorCubit>().show();
      //We should just update the response count for the Plurk data...
      await _responsesCollector.loadTopDown();

      if (!mounted) return;

      _blocProviderContext.read<RequestIndicatorCubit>().hide();
      setState(() {});
    }
  }

  _loadAll() async {
    if (_responsesCollector != null && !_responsesCollector.isLoading()) {
      Vibrator.vibrateShortNote();
      _blocProviderContext.read<RequestIndicatorCubit>().show();
      //We should just update the response count for the Plurk data...
      await _responsesCollector.loadAll();

      if (!mounted) return;

      _blocProviderContext.read<RequestIndicatorCubit>().hide();
      setState(() {});
    }
  }

  _loadBottomUp() async {
    if (_responsesCollector != null && !_responsesCollector.isLoading()) {
      Vibrator.vibrateShortNote();
      _blocProviderContext.read<RequestIndicatorCubit>().show();
      //We should just update the response count for the Plurk data...
      await _responsesCollector.loadBottomUp();

      if (!mounted) return;

      _blocProviderContext.read<RequestIndicatorCubit>().hide();
      setState(() {});
    }
  }

  _loadBottomEndMore() async {
    if (_responsesCollector != null && !_responsesCollector.isLoading()) {
      // Vibrator.vibrateShortNote();
      _blocProviderContext.read<RequestIndicatorCubit>().show();
      //We should just update the response count for the Plurk data...
      int totalResponseCount = await _responsesCollector.loadBottomEndMore();

      if (!mounted) return;

      _blocProviderContext.read<RequestIndicatorCubit>().hide();
      setState(() {
        if (plurk != null) {
          //Just save the response count.
          plurk.responseCount = totalResponseCount;
        }
        if (widget.args.plurkCardState != null) {
          widget.args.plurkCardState.widget.plurk.responseCount =
              totalResponseCount;
        }
      });
    }
  }

  _deleteResponse(Plurdart.Response response) async {
    print('_deleteResponse');
    if (_responsesCollector != null && !_responsesCollector.isLoading()) {
      _blocProviderContext.read<RequestIndicatorCubit>().show();
      //We should just update the response count for the Plurk data...
      int totalResponseCount =
          await _responsesCollector.requestDeleteResponse(response);

      if (!mounted) return;

      _blocProviderContext.read<RequestIndicatorCubit>().hide();
      setState(() {
        if (plurk != null) {
          //Just save the response count.
          plurk.responseCount = totalResponseCount;
        }
        if (widget.args.plurkCardState != null) {
          widget.args.plurkCardState.widget.plurk.responseCount =
              totalResponseCount;
        }
      });
    }
  }

  _onResponseAddSuccessState(ResponsesResponseAddSuccess state) {
    //Jst trigger load once.
    _loadBottomEndMore();
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }
}
