part of 'official_news_cubit.dart';

@immutable
abstract class OfficialNewsState {}

class OfficialNewsInitial extends OfficialNewsState {}

class OfficialNewsSuccess extends OfficialNewsState {
  OfficialNewsSuccess(this.officialNewsPlurks);
  final List<Plurdart.Plurk> officialNewsPlurks;
}

class OfficialNewsFailed extends OfficialNewsState { }