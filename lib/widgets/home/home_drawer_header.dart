import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/ui/add_account_cubit.dart';
import 'package:meat/bloc/ui/logout_cubit.dart';
import 'package:meat/bloc/ui/select_account_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/screens/user_viewer.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';
import 'package:meat/system/account_keeper.dart' as AccountKeeper;
import 'package:meat/theme/app_themes.dart';

class HomeDrawerHeader extends StatefulWidget {
  HomeDrawerHeader();

  @override
  _HomeDrawerHeaderState createState() => _HomeDrawerHeaderState();
}

class _HomeDrawerHeaderState extends State<HomeDrawerHeader> {
  _HomeDrawerHeaderState();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Static.settingsAcrylicEffect == true
          ? Theme.of(context).lowContrastPrimaryColor.withOpacity(0.5)
          : Theme.of(context).lowContrastPrimaryColor,
      child: Builder(
        builder: (context) {
          if (Static.me != null) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                //Top bar: PopupMenuButton
                Row(
                  children: [
                    Expanded(child: SizedBox()),
                    PopupMenuButton(
                      child: SizedBox(
                        width: 44,
                        height: 36,
                        child: Icon(
                          Icons.expand_more,
                          size: 24,
                          color: Theme.of(context).iconTheme.color,
                        ),
                      ),
                      itemBuilder: (BuildContext context) {
                        List<PopupMenuEntry> returnWidgets = [];

                        //ist all existing accounts and user nickNames as the display index.
                        //From AccountKeeper:
                        List<String> accNickNames =
                            AccountKeeper.getAccountNickNameList();

                        for (int i = 0; i < accNickNames.length; ++i) {
                          //Check if this is the selecting
                          IconData previewIcon =
                              MdiIcons.at;
                          if (AccountKeeper.meNickName() ==
                              accNickNames[i]) {
                            previewIcon =
                                MdiIcons.checkCircle;
                          }

                          returnWidgets.add(menuItem(
                              HomeDrawerHeaderMenuOption(
                                  HomeDrawerHeaderMenuOptionType
                                      .SelectAccount,
                                  nickName: accNickNames[i]),
                              Icon(
                                previewIcon,
                                color: Theme.of(context).iconTheme.color,
                              ),
                              accNickNames[i]));
                        }

                        returnWidgets.add(PopupMenuDivider());

                        //Logout from this account.
                        returnWidgets.add(menuItem(
                            HomeDrawerHeaderMenuOption(
                                HomeDrawerHeaderMenuOptionType.LogOut),
                            Icon(
                              MdiIcons.logout,
                              color: Theme.of(context).iconTheme.color,
                            ),
                            'Log Out'));

                        // returnWidgets.add(PopupMenuDivider());

                        //Add an account.
                        returnWidgets.add(menuItem(
                            HomeDrawerHeaderMenuOption(
                                HomeDrawerHeaderMenuOptionType.AddAccount),
                            Icon(
                              Icons.add,
                              color: Theme.of(context).iconTheme.color,
                            ),
                            'Add Account'));

                        return returnWidgets;
                      },
                      onSelected: (option) {
                        //Identify the option type:
                        switch(option.optionType) {
                          case HomeDrawerHeaderMenuOptionType.SelectAccount:
                            context.read<SelectAccountCubit>().request(option.nickName);
                            break;
                          case HomeDrawerHeaderMenuOptionType.LogOut:
                            context.read<LogoutCubit>().request();
                            break;
                          case HomeDrawerHeaderMenuOptionType.AddAccount:
                            context.read<AddAccountCubit>().request();
                            break;
                        }
                      },
                    ),
                  ],
                ),

                //Logo + Nickname + Karma
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      //Logo
                      Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.all(Radius.circular(60)),
                        ),
                        child: GestureDetector(
                          child: CircleAvatar(
                            backgroundColor: Colors.transparent,
                            backgroundImage: NetworkImage(Static.me.avatarBig),
                            // child: Image(
                            //   fit: BoxFit.cover,
                            //   image: NetworkImage(Static.me.avatarBig),
                            // ),
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                            //Bloc to open UserViewer.
                            context
                                .read<UserViewerCubit>()
                                .open(UserViewerArgs(Static.me.nickName));
                          },
                        ),
                      ),

                      SizedBox(
                        height: 12,
                      ),

                      //[Dep] Damn those users who like to use a looooong name...
                      // //Color display name.
                      // PlurkCardPrimeHeaderState.buildUserDisplayName(context, state.user, false, (user) {
                      //   //Bloc to open UserViewer.
                      //   context.read<UserViewerCubit>().open(UserViewerArgs(
                      //     UserViewerMode.FromUserModal,
                      //     user: user,
                      //   ));
                      // },
                      // fontSize: 16),

                      //Nickname.
                      PlurkCardPrimeHeaderState.buildHeaderNickName(
                          context, Static.me),

                      SizedBox(
                        height: 4,
                      ),

                      //Karma
                      Text('Karma ' + Static.me.karma.toString()),
                    ],
                  ),
                ),

                SizedBox(
                  height: 16,
                ),
              ],
            );
          } else {
            //Failed or initial.
            return Container();
          }
        },
      ),
    );
  }

  PopupMenuItem menuItem(dynamic value, Widget iconWidget, String text) {
    return PopupMenuItem(
      value: value,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          iconWidget,
          SizedBox(
            width: 8,
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 5), child: Text(text)),
        ],
      ),
    );
  }
}

enum HomeDrawerHeaderMenuOptionType {
  SelectAccount,
  LogOut,
  AddAccount,
}

//For popupMenuButton select data.
class HomeDrawerHeaderMenuOption {
  HomeDrawerHeaderMenuOption(this.optionType, {this.nickName});

  final HomeDrawerHeaderMenuOptionType optionType;
  final String nickName;
}
