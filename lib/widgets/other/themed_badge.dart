import 'package:flutter/material.dart';

class ThemedBadge extends StatelessWidget {
  ThemedBadge(this.backgroundColor, this.text, );
  final Color backgroundColor;
  final String text;

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            height: 22,
            padding: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: backgroundColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  blurRadius: 2.0,
                  spreadRadius: 0.0,
                  offset: Offset(1.0, 1.0), // shadow direction: bottom right
                )
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0.5),
          child: Transform.translate(
            offset: Offset(0, -1.5),
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.caption,
            ),
          ),
        )
      ],
    );
  }
}
