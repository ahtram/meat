part of 'bookmarks_get_bookmarks_cubit.dart';

@immutable
abstract class BookmarksGetBookmarksState {}

class BookmarksGetBookmarksInitial extends BookmarksGetBookmarksState {}

class BookmarksGetBookmarksSuccess extends BookmarksGetBookmarksState {
  BookmarksGetBookmarksSuccess(this.bookmarks);
  final Plurdart.Bookmarks bookmarks;
}

class BookmarksGetBookmarksFailed extends BookmarksGetBookmarksState {
  BookmarksGetBookmarksFailed(this.errorText);
  final String errorText;
}