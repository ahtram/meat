part of 'open_uri_cubit.dart';

@immutable
abstract class OpenUriState {}

class OpenUriInitial extends OpenUriState {}

class OpenUriRequest extends OpenUriState {
  OpenUriRequest(this.uri);
  final Uri uri;
}
