import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'viewer_background_switch_state.dart';

class ViewerBackgroundSwitchCubit extends Cubit<ViewerBackgroundSwitchState> {
  ViewerBackgroundSwitchCubit() : super(ViewerBackgroundSwitchInitial());

  void active(Uint8List image) {
    emit(ViewerBackgroundSwitchActive(image));
  }
}
