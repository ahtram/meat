import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:meat/models/chikkutteh/res/living_channel_res.dart';
import 'package:meat/chikkutteh/chikkutteh_endpoints.dart' as ChikKutTehEndpoints;

http.Client _client = http.Client();

//Get the living channel info.
Future<LivingChannelsRes> getLivingChannels() async {
  try {
    Response res = await _client
        .get(ChikKutTehEndpoints.livingChannels());
    if (res.statusCode == HttpStatus.ok) {
      return LivingChannelsRes.fromJson(jsonDecode(res.body));
    } else {
      print('Oops! getLivingChannels got status [' + res.statusCode.toString() + ']');
      return null;
    }
  } catch (e) {
    //The server doesn't define any error code for now!
    print('Oops! getLivingChannels got error [' + e.toString() + ']');
    return null;
  }
}