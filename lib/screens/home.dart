import 'dart:async';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/rest/alerts_get_unread_count_cubit.dart';
import 'package:meat/bloc/rest/blocks_block_cubit.dart';
import 'package:meat/bloc/rest/emoticons_add_from_url_cubit.dart';
import 'package:meat/bloc/rest/emoticons_delete_cubit.dart';
import 'package:meat/bloc/rest/emoticons_get_cubit.dart';
import 'package:meat/bloc/rest/timeline_get_plurks_cubit.dart';
import 'package:meat/bloc/rest/timeline_plurk_delete_cubit.dart';
import 'package:meat/bloc/rest/users_me_cubit.dart';
import 'package:meat/bloc/ui/add_account_cubit.dart';
import 'package:meat/bloc/ui/alert_number_changed_cubit.dart';
import 'package:meat/bloc/ui/emoticon_ask_add_cubit.dart';
import 'package:meat/bloc/ui/emoticon_ask_delete_cubit.dart';
import 'package:meat/bloc/ui/emoticon_notify_owned_cubit.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/bloc/ui/home_refresh_cubit.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/bloc/ui/plurk_got_new_response_cubit.dart';
import 'package:meat/bloc/ui/refresh_unread_count_cubit.dart';
import 'package:meat/bloc/ui/report_abuse_dialog_cubit.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/bloc/ui/logout_cubit.dart';
import 'package:meat/bloc/ui/plurk_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/plurk_post_complete_cubit.dart';
import 'package:meat/bloc/ui/select_account_cubit.dart';
import 'package:meat/bloc/ui/users_browser_cubit.dart';
import 'package:meat/bloc/ui/viewer_background_switch_cubit.dart';
import 'package:meat/bloc/ui/plurk_viewer_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/models/app/account_store.dart';
import 'package:meat/models/notification/notification_payload.dart';
import 'package:meat/screens/image_links_viewer.dart' as ImageLinksViewer;
import 'package:meat/screens/youtube_fullscreen_viewer.dart'
    as YoutubeFullScreenViewer;
import 'package:meat/screens/plurk_viewer.dart' as PlurkViewer;
import 'package:meat/screens/plurk_poster.dart' as PlurkPoster;
import 'package:meat/screens/user_viewer.dart' as UserViewer;
import 'package:meat/screens/users_browser.dart' as UsersBrowser;
import 'package:meat/screens/debug_viewer.dart' as DebugBrowser;
import 'package:meat/screens/report_abuse.dart' as ReportAbuse;
import 'package:meat/screens/search_browser.dart' as SearchBrowser;
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/theme/app_themes.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:transparent_image/transparent_image.dart';

import 'package:meat/widgets/home/home_drawer_header.dart';
import 'package:meat/widgets/home/home_drawer_body.dart';
import 'package:meat/widgets/home/home_drawer_footer.dart';
import 'package:meat/widgets/home/home_app_bar_title.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';

import 'package:meat/bloc/rest/expire_token_cubit.dart';

import 'package:meat/bloc/ui/youtube_fullscreen_cubit.dart';
import 'package:meat/bloc/ui/image_links_viewer_cubit.dart';

import 'package:after_layout/after_layout.dart';

import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/bloc_dialog.dart' as BlocDialog;
import 'package:meat/system/vibrator.dart' as Vibrator;

import 'package:another_flushbar/flushbar.dart';
import 'package:uni_links/uni_links.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:meat/system/rocket.dart' as Rocket;
import 'package:meat/system/account_keeper.dart' as AccountKeeper;
import 'package:meat/screens/add_account.dart' as AddAccount;

import 'package:meat/widgets/plurk/plurk_cards_viewer.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';

import 'package:fluttertoast/fluttertoast.dart';


class Home extends StatefulWidget {
  Home();

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with AfterLayoutMixin {
  _HomeState();

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  BuildContext _blocProviderContext;

  GlobalKey<PlurkCardsViewerState> _plurkCardsViewerKey =
      GlobalKey<PlurkCardsViewerState>();

  bool _showReturnToTopButton = false;

  //For listen the static url tap stream.
  StreamSubscription _urlTapSubscription;
  StreamSubscription _appLinkSubscription;
  //For receive share intent.
  StreamSubscription _intentDataStreamSubscription;

  int _newPlurkCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //Tricky! Process open link before check expire token for faster UX!
    // print('[afterFirstLayout]');

    //Initial app link
    // Uri parsing may fail, so we use a try/catch FormatException.
    try {
      Uri initialUri = await getInitialUri();
      // Use the uri and warn the user, if it is not correct,
      // but keep in mind it could be `null`.
      if (initialUri != null) {
        //WHAT? This is not working! And I don't know why!
        // print('call _openAppHandleLink from getInitialUri() [' + initialUri.toString() + ']');
        _openAppHandleLink(initialUri);
      }
    } on FormatException {
      print('Oops! try getInitialUri() but got a FormatException!');
      // Handle exception by warning the user their action did not succeed
      // return?
    } on PlatformException {
      print('Oops! try getInitialUri() but got a PlatformException!');
      // Handle exception by warning the user their action did not succeed
      // return?
    }

    //Subscription of app link event.
    // Attach a listener to the stream
    _appLinkSubscription = uriLinkStream.listen((Uri uri) {
      // Use the uri and warn the user, if it is not correct
      if (uri != null) {
        // print('call _openAppHandleLink from uriLinkStream [' + uri.toString() + ']');
        _openAppHandleLink(uri);
      }
    }, onError: (err) {
      print('Oops! uriLinkStream got an error! [' + err.toString() + ']');
      // Handle exception by warning the user their action did not succeed
    });

    //--

    // //WHAT?!
    // await Future.delayed(Duration(seconds: 5));
    //
    // final _appLinks = AppLinks(
    //   // Called when a new uri has been redirected to the app
    //   onAppLink: (Uri uri, String stringUri) {
    //     // Do something (navigation, ...)
    //     print('[AppLinks] onAppLink got [' + stringUri +']');
    //   },
    // );
    //
    // // Get the initial/first link.
    // // This is also useful when app was terminated (i.e. not started)
    // final initialAppLinkStr = await _appLinks.getInitialAppLinkString();
    // if (initialAppLinkStr != null) {
    //   print('[AppLinks] initialAppLinkStr is [' + initialAppLinkStr +']');
    // }
    //
    // // Maybe later. Get the latest link.
    // final latestAppLinkStr = await _appLinks.getLatestAppLinkString();
    // if (latestAppLinkStr != null) {
    //   print('[AppLinks] latestAppLinkStr is [' + latestAppLinkStr +']');
    // }

    //--

    // FirebaseDynamicLinks.instance.onLink(
    //     onSuccess: (PendingDynamicLinkData dynamicLink) async {
    //       final Uri deepLink = dynamicLink?.link;
    //
    //       if (deepLink != null) {
    //         print('[FirebaseDynamicLinks] onLink [' + deepLink.toString() + ']');
    //       }
    //     },
    //     onError: (OnLinkErrorException e) async {
    //       print('onLinkError');
    //       print(e.message);
    //     }
    // );
    //
    // final PendingDynamicLinkData initialLinkData = await FirebaseDynamicLinks.instance.getInitialLink();
    // final Uri initialDeepLink = initialLinkData?.link;

    // if (initialDeepLink != null) {
    //   print('[FirebaseDynamicLinks] getInitialLink [' + initialDeepLink.toString() + ']');
    // }

    //--

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
      // print('call _handleShareTextIntent from getTextStream [' + value + ']');
      _handleShareTextIntent(value);
    }, onError: (err) {
      print("ReceiveSharingIntent getLinkStream error: $err");
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String value) {
      // if (value != null) {
      //   print('call _handleShareTextIntent from getInitialText [' + value +']');
      // }
      //Remember this will be called on each time app launched!
      _handleShareTextIntent(value);
    });

    //--

    //Check token expire before do anything...
    // Test an API call to see if the client still work...
    Plurdart.CheckedExpiredToken checkedExpiredToken =
        await Plurdart.checkToken();
    if (checkedExpiredToken != null && !checkedExpiredToken.hasError()) {
//        _showSnackBarMessage('[Credential restored. Plurdart has been setup]');
      print('[Credential verified.]');
    } else {
//        _showSnackBarMessage('[Credential expired. Re-Auth is required.]');
      print('[Credential expired. Re-Auth is required.]');
      _onExpireTokenFinish();
      return;
    }

    try {
      //Get Users. Nothing more important than this...(Auth flow will try get this done)
      //Static.me could be filled by login when 1st time login. So we may pass this step here.
      await _blocProviderContext.read<UsersMeCubit>().request();
    } catch (error) {
      DebugBrowser.show(context, error.toString());
      print(error.toString());
    }

    //We'll want to refresh one time when Home is opened.
    //Todo??
    // _plurkCardsViewerKey.currentState.refresh();

    //Get the first (current) alerts unread counts.
    await _blocProviderContext.read<AlertsGetUnreadCountCubit>().request();

    //Update user's emoticon collection cache.
    await _blocProviderContext.read<EmoticonsGetCubit>().request();

    //This is where we process the HtmlWidget onUrlTap static event!
    _urlTapSubscription = Static.urlTap.listen(_handleTapUrl);

    //Request permissions for receiving Push Notifications. (iOS)
    // if (Platform.isIOS) {
    //   _firebaseMessaging.requestNotificationPermissions();
    // }

    //Listen to messages.
    // _firebaseMessaging.configure(
    //   onMessage: _onFirebaseMessage,
    //   onResume: _onFirebaseResume,
    //   onLaunch: _onFirebaseLaunch,
    // );

    //Read the Comet server url.
    if (Static.me != null) {

      //Start the Comet poll background task..
      Rocket.takeOff(Static.me.id,
          onAlertNumberChanged: _onAlertNumberChanged,
          onNewPlurk: _onNewPlurk,
          onNewResponse: _onNewResponse);

      //Save the comet url.
      // if (userChannel != null/* && prefs != null*/) {
      //   //Save the url and start the Rocket isolate.
      //   Rocket.takeOff(Static.me.id,
      //       onAlertNumberChanged: _onAlertNumberChanged,
      //       onNewPlurk: _onNewPlurk,
      //       onNewResponse: _onNewResponse);
      //
      //   // //Start the Rocket!
      //   // //Start the isolate using AlarmManager.
      //   // await AndroidAlarmManager.initialize();
      //   //
      //   // //This will start the
      //   // await AndroidAlarmManager.oneShot(Duration(seconds: 0), Static.me.id, Rocket.launch,
      //   //   allowWhileIdle: true,
      //   //   exact: true,
      //   //   rescheduleOnReboot: true,
      //   // );
      //
      //   //Test debug viewer!
      //   // DebugBrowser.show(context, 'Test debug viewer...');
      // }
    }

    //Start keep update LivingChannelsRes info.
    Static.startLivingChannelsUpdateTimer();
  }

  @override
  void dispose() {
    Static.stopLivingChannelsUpdateTimer();
    _urlTapSubscription?.cancel();
    _appLinkSubscription?.cancel();
    _intentDataStreamSubscription?.cancel();
    super.dispose();
  }

  //----- Comet notification relative ------

  _onAlertNumberChanged(int noti, int req) {
    // print('_onAlertNumberChanged: [' + noti.toString() + ', ' + req.toString() + ']');
    _blocProviderContext.read<AlertNumberChangedCubit>().request(
          noti: noti,
          req: req,
        );
  }

  _onNewPlurk(Plurdart.Plurk plurk) {
    // print('_onNewPlurk [' + plurk.plurkId.toString() + ']');
    //Update the new plurks count?
    setState(() {
      _newPlurkCount += 1;
    });
  }

  _onNewResponse(Plurdart.Plurk plurk) {
    // print('_onNewResponse [' + plurk.plurkId.toString() + ']');
    //Send a cubit to notify anyone who need this. (Could be every PlurkCard and TitleBar)
    //This will notify all PlurkCards.
    BlocProvider.of<PlurkGotNewResponseCubit>(context).success(plurk);
    //This will notify HomeAppBarTitle.
    BlocProvider.of<RefreshUnreadCountCubit>(context).request();
  }

  //----- App links relative -----

  //Accept String url
  void _handleTapUrl(dynamic url) async {
    //There should be two type of links appear here:
    //1. emoticon links (which was hacked by us)
    //https://emos.plurk.com/822079c3b34e8e392af8c4902194efb7_w48_h48.png
    //2. user profile links
    //https://www.plurk.com/SOSOBI
    //3. Plurk
    //https://www.plurk.com/p/nzit3l

    HapticFeedback.lightImpact();

    //This will make things easy.
    Uri tappedUri = Uri.parse(url);
    if (tappedUri.authority == Define.plurkEmoticonServiceAuthority) {
      //This is a emoticon url. Show a dialog to ask if we should try add this one.
      // print('tappedUri.authority == ' + Define.plurkEmoticonServiceAuthority);

      //Check if the user owned this custom emoticon.
      if (Static.emoticonCollection.hasCustomEmoticonByUrl(url)) {
        //Notify the user already own that emoticon. (Use cubit)
        if (_blocProviderContext != null) {
          _blocProviderContext.read<EmoticonNotifyOwnedCubit>().show(url);
        } else {
          print('Oops! context is null??!');
        }
      } else {
        //Ask if the user want to add this emoticon to his custom set.
        //Display a asking dialog. (Use cubit)
        if (_blocProviderContext != null) {
          _blocProviderContext.read<EmoticonAskAddCubit>().show(url);
        } else {
          print('Oops! context is null??!');
        }
      }
    } else if (tappedUri.authority == Define.plurkAuthority) {
      //App self handle this url. (Plurk Authority)
      await _openAppHandleLink(tappedUri);
    } else if (tappedUri.isScheme(Define.bktHashTagScheme)) {
      //App hashTag deep link url (BKT Hash Tag)
      String hashTagUrl = url;
      String hashTagString =
          hashTagUrl.substring(Define.bktHashTagScheme.length + 3);
      Vibrator.vibrateShortNote();
      SearchBrowser.show(
          context, SearchBrowser.SearchBrowserArgs(hashTagString));
    } else if (tappedUri.isScheme(Define.bktScheme)) {
      //App internal link url (BKT Scheme)
      await _openAppHandleLink(tappedUri);
    } else {
      // print('_handleUrl other: ' + url + ' | path: ' + tappedUri.path);
      if (await canLaunch(tappedUri.toString())) {
        await launch(tappedUri.toString());
      }
    }
  }

  //Handle open a link...
  _openAppHandleLink(Uri linkUri) async {
    try {
      // print('_openAppHandleLink [' + linkUri.toString() + ']');

      if (linkUri.isScheme('https')) {
        //Plurk link.
        //Check the link and see what viewer we should use to open it.
        // print('Authority [' + linkUri.authority + '] segments [' + linkUri.pathSegments.join(',') + ']' );

        //We'll check the segments...
        List<String> pathSegments = List<String>.from(linkUri.pathSegments);

        if (pathSegments.length > 0) {
          //Remove the heading 'm' (mobile url path) is there's one.
          if (pathSegments[0] == 'm') {
            // print('Removing m');
            pathSegments.removeAt(0);
          }

          //Remove the heading 's' (mobile url path) is there's one.
          if (pathSegments[0] == 's') {
            // print('Removing s');
            pathSegments.removeAt(0);
          }

          //Plurk or User?
          if (pathSegments != null && pathSegments.length > 0) {
            // print('pathSegments.length > 0');
            if (pathSegments[0] == 'p') {
              //Plurk Viewer
              if (pathSegments.length > 1) {
                int plurkId = int.tryParse(pathSegments[1], radix: 36);
                if (plurkId != null) {
                  // print('Open plurk view. plurkId: [' +
                  //     plurkId.toString() +
                  //     '][' +
                  //     pathSegments[1] +
                  //     ']');
                  PlurkViewer.show(
                      context,
                      PlurkViewer.PlurkViewerArgs(
                        PlurkViewer.PlurkViewerMode.FromPlurkId,
                        plurkId: plurkId,
                      ));
                }
              }
            } else if (pathSegments[0] == 'u') {
              //User Viewer
              if (pathSegments.length > 1) {
                String userNickName = pathSegments[1];
                if (userNickName != null) {
                  // print('Open user view. userNickName: [' +
                  //     userNickName.toString() +
                  // ']');
                  UserViewer.show(
                      context, UserViewer.UserViewerArgs(userNickName));
                }
              }
            } else {
              //User Viewer (treat pathSegments[0] as nickName)
              // print('Open user view. nickName: [' + pathSegments[0] + ']');
              String userNickName = pathSegments[0];
              if (userNickName != null && userNickName.isEmpty == false) {
                // print('Open user view. userNickName: [' +
                //     userNickName.toString() +
                // ']');
                UserViewer.show(
                    context, UserViewer.UserViewerArgs(userNickName));
              } else {
                //Ok...this is a plain Plurk url without any username. So we'll just launch it in app Chrome custom tab.
                // print('The user id is empty! launch as a plain url [' + linkUri.toString() + ']');
                _openInChromeCustomTab(linkUri.toString());
              }
            }
          } else {
            // print('No segments! launch as a plain url [' + linkUri.toString() + ']');
            _openInChromeCustomTab(linkUri.toString());
          }
        } else {
          // print('No segments! launch as a plain url [' + linkUri.toString() + ']');
          _openInChromeCustomTab(linkUri.toString());
        }
      } else if (linkUri.isScheme(Define.bktHashTagScheme)) {
        //Hashtag link.
        //Todo
      } else if (linkUri.isScheme(Define.bktScheme)) {
        print('Handle url as bktScheme [' + linkUri.authority + ']');
        //Is this an uri?

        if (NotificationPayload.isValid(linkUri)) {
          //Open BKT scheme
          NotificationPayload notificationPayload =
              NotificationPayload.fromUri(linkUri);
          switch (notificationPayload.alertType) {
            case Plurdart.AlertType.FriendshipRequest:
            case Plurdart.AlertType.FriendshipPending:
            case Plurdart.AlertType.NewFan:
            case Plurdart.AlertType.FriendshipAccepted:
            case Plurdart.AlertType.NewFriend:
              UserViewer.show(context, UserViewer.UserViewerArgs(
                  notificationPayload.queryParameters['userNickName'],
              ));
              break;
            case Plurdart.AlertType.PrivatePlurk:
            case Plurdart.AlertType.PlurkLiked:
            case Plurdart.AlertType.PlurkReplurked:
              PlurkViewer.show(context, PlurkViewer.PlurkViewerArgs(
                  PlurkViewer.PlurkViewerMode.FromPlurkId,
                  showPlurkCard: true,
                  plurkId: int.tryParse(notificationPayload.queryParameters['plurkId']),
              ));
              break;
            case Plurdart.AlertType.Mentioned:
            case Plurdart.AlertType.MyResponded:
              PlurkViewer.show(context, PlurkViewer.PlurkViewerArgs(
                PlurkViewer.PlurkViewerMode.FromPlurkId,
                showPlurkCard: true,
                plurkId: int.tryParse(notificationPayload.queryParameters['plurkId']),
                //How about responseId?
                // responseId: notificationPayload.queryParameters['responseId'],
              ));
            break;
            default:
              //Do nothing. Unknown type.
              break;
          }
        } else if (linkUri.authority == Define.plurkAuthority) {
          //Alter it to https links (kinda hacky...)
          // print('[Alter plurkAuthority]: [' + linkUri.toString() + '] to https one...');
          _handleTapUrl(Uri.https(
                  linkUri.authority, linkUri.path, linkUri.queryParameters)
              .toString());
        } else {
          // print('[Alter other]: [' + linkUri.toString() + '] to https one...');
          _handleTapUrl(Uri.https(
                  linkUri.authority, linkUri.path, linkUri.queryParameters)
              .toString());
        }
      } else {
        //???? Not a valid url? Treat it as a share text.
      }
    } catch (e) {
      print('_openLink [' + e.toString() + ']');
    }
  }

  _openInChromeCustomTab(String url) {
    FlutterWebBrowser.openWebPage(
      url: url,
      customTabsOptions: CustomTabsOptions(
        colorScheme: CustomTabsColorScheme.system,
        toolbarColor: Theme.of(context).primaryColor,
        secondaryToolbarColor: Theme.of(context).primaryColor,
        navigationBarColor: Theme.of(context).primaryColor,
        addDefaultShareMenuItem: true,
        instantAppsEnabled: true,
        showTitle: true,
        urlBarHidingEnabled: true,
      ),
      safariVCOptions: SafariViewControllerOptions(
        barCollapsingEnabled: true,
        preferredBarTintColor: Theme.of(context).primaryColor,
        preferredControlTintColor: Theme.of(context).primaryColor,
        dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
        modalPresentationCapturesStatusBarAppearance: true,
      ),
    );
  }

  //Received a share text intent.
  _handleShareTextIntent(String value) {
    if (value != null && value.isNotEmpty) {
      //Skip some wired situation
      Uri uri = Uri.tryParse(value);
      if (uri != null && uri.scheme == Define.bktScheme) {
        //Skip of it's an app scheme text from notification click.
        return;
      }

      if (uri != null && uri.authority == Define.plurkAuthority) {
        //Skip of it's a Plurk authority.
        return;
      }

      //Open Plurk Poster for new plurk with the shared value.
      PlurkPoster.show(
          context,
          PlurkPoster.PlurkPosterArgs(
            PlurkPoster.PlurkPosterMode.New,
            initialContent: value,
          ));
    }
  }

  //------------------------------

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        //See if the draw is open.
        if (_scaffoldKey.currentState.isDrawerOpen) {
          Navigator.pop(context);
          return false;
        } else {
          //Todo: find a way to scroll back?
          //Check it the scroll is on the top.
          // if (_refreshController.position.pixels > 255) {
          //   // print('pixels: ' + _refreshController.position.pixels.toString());
          //
          //   //Check the current position to see if instant set or animateTo
          //   if (_refreshController.position.pixels > 5000) {
          //     //Too far! Just set pos to 0.0
          //     _refreshController.position.jumpTo(5000.0);
          //     _refreshController.position.animateTo(0.0,
          //         duration: Duration(
          //             milliseconds:
          //                 (_refreshController.position.pixels * 0.1).toInt()),
          //         curve: Curves.linear);
          //   } else {
          //     //AnimateTo 0.0
          //     _refreshController.position.animateTo(0.0,
          //         duration: Duration(
          //             milliseconds:
          //                 (_refreshController.position.pixels * 0.1).toInt()),
          //         curve: Curves.linear);
          //   }
          //
          //   return false;
          // } else {
          //   if (Platform.isAndroid) {
          //     MoveToBackground.moveTaskToBack();
          //     return false;
          //   }
          //   //Just let the OS handle this.
          //   return true;
          // }

          //[Nope] We should not do this. This will stop the back key from go back
          //to another app. (from User report)
          // if (Platform.isAndroid) {
          //   MoveToBackground.moveTaskToBack();
          //   return false;
          // }

          //Just let the OS handle this.
          return true;
        }
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider<ExpireTokenCubit>(
            create: (BuildContext context) => ExpireTokenCubit(),
          ),
          BlocProvider<RequestIndicatorCubit>(
            create: (BuildContext context) => RequestIndicatorCubit(),
          ),
          //For PlurkCard switch PlurkViewer background.
          //This is useless. But PlurkCard seems need it...
          BlocProvider<ViewerBackgroundSwitchCubit>(
            create: (BuildContext context) => ViewerBackgroundSwitchCubit(),
          ),
          BlocProvider<AlertsGetUnreadCountCubit>(
            create: (BuildContext context) => AlertsGetUnreadCountCubit(),
          ),
          //[Dep]: Replaced by Rocket.
          // BlocProvider<GetCometCubit>(
          //   create: (BuildContext context) => GetCometCubit(),
          // ),
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<LogoutCubit, LogoutState>(
              listener: (context, state) {
                if (state is LogoutSuccess) {
                  //Hide drawer.
                  Navigator.pop(context);
                  //The dialogue should do expire token for us.
                  BlocDialog.askLogOutDialog(
                      context, AccountKeeper.meNickName());
                }
              },
            ),
            // ExpireTokenCubit.

            BlocListener<ExpireTokenCubit, ExpireTokenState>(
              listener: (context, state) async {
                //Ok, we shouldn't bother if the API success.
                // if (state.checkedExpiredToken != null &&
                //     !state.checkedExpiredToken.hasError()) {
                //
                // } else {
                //
                // }

                //Hide busy.
                Navigator.popUntil(context, (route) {
                  return (route.settings.name == '/home');
                });

                //Clear cache...
                Static.clearUserCache();
                Static.clearTagsCache();

                //Return to login...
                _onExpireTokenFinish();
              },
            ),
            BlocListener<AddAccountCubit, AddAccountState>(
              listener: (context, state) {
                if (state is AddAccountRequest) {
                  //Hide drawer.
                  Navigator.pop(context);
                  //Display the add account page. (A page similar to login page screen)
                  AddAccount.show(context, _onAddAccountSuccess);
                }
              },
            ),
            BlocListener<SelectAccountCubit, SelectAccountState>(
              listener: (context, state) {
                //Hide drawer.
                Navigator.pop(context);

                if (state is SelectAccountSuccess) {
                  //Refresh all the home content.
                  _refreshActivatedAccount();
                } else if (state is SelectAccountFailed) {}
              },
            ),
            BlocListener<UsersMeCubit, UsersMeState>(
              listener: (context, state) {
                if (state is UsersMeSuccess) {
                } else if (state is UsersMeFailed) {
                  //Todo: network issue? or maybe we should log out?
                }
              },
            ),
            BlocListener<ImageLinksViewerCubit, ImageLinksViewerState>(
              listener: (context, state) {
                if (state is ImageLinksViewerEnterState) {
//                  print('_onImageLinksViewerEnterState');
                  _onImageLinksViewerEnterState(state);
                }
              },
            ),
            BlocListener<YoutubeFullscreenCubit, YoutubeFullscreenState>(
              listener: (context, state) {
                if (state is YoutubeFullscreenEnterState) {
//                  print('_onImageLinksViewerEnterState');
                  _onYoutubeFullscreenEnterState(state);
                } else if (state is YoutubeFullscreenLeaveState) {
                  //Actually we don't need this.
                  // _onYoutubeFullscreenLeaveState(state);
                }
              },
            ),
            BlocListener<PlurkViewerCubit, PlurkViewerState>(
              listener: (context, state) {
                if (state is PlurkViewerOpen) {
                  _onPlurkViewerOpen(state);
                }
              },
            ),
            BlocListener<UserViewerCubit, UserViewerState>(
              listener: (context, state) {
                if (state is UserViewerOpen) {
                  _onUserViewerOpen(state);
                }
              },
            ),
            BlocListener<UsersBrowserCubit, UsersBrowserState>(
              listener: (context, state) {
                if (state is UsersBrowserOpen) {
                  _onUsersBrowserOpen(state);
                }
              },
            ),
            BlocListener<ReportAbuseDialogCubit, ReportAbuseDialogState>(
              listener: (context, state) {
                if (state is ReportAbuseDialogOpenPlurk) {
                  _onReportAbuseDialogOpenPlurk(state.plurkId);
                } else if (state is ReportAbuseDialogOpenResponse) {
                  _onReportAbuseDialogOpenResponse(state.responseId);
                }
              },
            ),
            BlocListener<EmoticonAskAddCubit, EmoticonAskAddState>(
              listener: (context, state) {
                if (state is EmoticonAskAddShow) {
                  BlocDialog.emoticonAskAddDialog(
                      _blocProviderContext, state.url);
                }
              },
            ),
            BlocListener<EmoticonAskDeleteCubit, EmoticonAskDeleteState>(
              listener: (context, state) {
                if (state is EmoticonAskDeleteShow) {
                  Vibrator.vibrateShortNote();
                  BlocDialog.emoticonAskDeleteDialog(
                      _blocProviderContext, state.url);
                }
              },
            ),
            BlocListener<EmoticonNotifyOwnedCubit, EmoticonNotifyOwnedState>(
              listener: (context, state) {
                if (state is EmoticonNotifyOwnedShow) {
                  BlocDialog.emoticonNotifyOwnedDialog(
                      _blocProviderContext, state.url);
                }
              },
            ),
            BlocListener<EmoticonsAddFromUrlCubit, EmoticonsAddFromUrlState>(
              listener: (context, state) {
                if (state is EmoticonsAddFromUrlSuccess) {
                  _blocProviderContext.read<FlushBarCubit>().request(
                      Icons.check_circle,
                      'Add emoticon success!',
                      'Keyword: ' + state.keyword);
//                  print('Home EmoticonsAddFromUrlCubit: success!');
                  _blocProviderContext.read<EmoticonsGetCubit>().request();
                } else if (state is EmoticonsAddFromUrlFailed) {
                  _blocProviderContext.read<FlushBarCubit>().request(
                      Icons.warning, 'Cannot add emoticon!', state.errorText);
                }
              },
            ),
            BlocListener<EmoticonsDeleteCubit, EmoticonsDeleteState>(
              listener: (context, state) {
                if (state is EmoticonsDeleteSuccess) {
//                  print('Home EmoticonsDeleteCubit: success!');
                  _blocProviderContext.read<EmoticonsGetCubit>().request();
                } else if (state is EmoticonsDeleteFailed) {
                  _blocProviderContext.read<FlushBarCubit>().request(
                      Icons.warning,
                      'Cannot delete emoticon!',
                      state.errorText);
                }
              },
            ),
            BlocListener<PlurkPostCompleteCubit, PlurkPostCompleteState>(
              listener: (context, state) {
                if (state is PlurkPostCompleteSuccess) {
                  _onPlurkPostCompleteSuccess(state);
                }
              },
            ),
            BlocListener<PlurkEditCompleteCubit, PlurkEditCompleteState>(
              listener: (context, state) {
                if (state is PlurkEditCompleteSuccess) {
                  _onPlurkEditCompleteSuccess(state);
                }
              },
            ),
            BlocListener<TimelinePlurkDeleteCubit, TimelinePlurkDeleteState>(
              listener: (context, state) {
                if (state is TimelinePlurkDeleteSuccess) {
                  _onPlurkDeleteSuccess(state);
                }
              },
            ),
            BlocListener<FlushBarCubit, FlushBarState>(
              listener: (context, state) {
                if (state is FlushBarRequest) {
                  //Only display FlushBar when I am the current route.
                  if (ModalRoute.of(context).isCurrent) {
                    _showFlushBar(_blocProviderContext, state.icon, state.title,
                        state.message);
                  }
                }
              },
            ),
            BlocListener<AlertsGetUnreadCountCubit, AlertsGetUnreadCountState>(
              listener: (context, state) {
                if (state is AlertsGetUnreadCountSuccess) {
                  //Just emit the ui Cubit.
                  _blocProviderContext.read<AlertNumberChangedCubit>().request(
                        noti: state.alertsUnreadCount.noti,
                        req: state.alertsUnreadCount.req,
                      );
                }
              },
            ),
            BlocListener<HomeRefreshCubit, HomeRefreshState>(
              listener: (context, state) {
                if (state is HomeRefreshRequest) {
                  _plurkCardsViewerKey.currentState.refresh();
                } else if (state is HomeRefreshComplete) {
                  //Clear the new plurks count.
                  setState(() {
                    _newPlurkCount = 0;
                  });
                }
              },
            ),
            BlocListener<TimelineGetPlurksCubit, TimelineGetPlurksState>(
              listener: (context, state) {
                if (state is TimelineGetNewPlurksSuccess) {
                  //Clear the new plurks count.
                  setState(() {
                    _newPlurkCount = 0;
                  });
                }
              },
            ),
            // BlocListener<GetCometCubit, GetCometState>(
            //   listener: (context, state) {
            //     _onCometState(state);
            //   },
            // ),

            //[Very Tricky]: When success block a user. Just refresh the timeline.
            BlocListener<BlocksBlockCubit, BlocksBlockState>(
              listener: (context, state) {
                if (state is BlocksBlockSuccess) {
                  print('[Detect user blocked. Refresh timeline]');
                  _plurkCardsViewerKey.currentState.refresh();
                }
              },
            ),
            BlocListener<OpenUriCubit, OpenUriState>(
              listener: (context, state) {
                if (state is OpenUriRequest) {
                  // print('[OpenUriState]');
                  _handleTapUrl(Uri.decodeFull(state.uri.toString()));
                }
              },
            ),
          ],
          child: Builder(
            builder: (context) {
              _blocProviderContext = context;
              return Scaffold(
                drawerScrimColor: Theme.of(context).cardColor.withOpacity(0.33),
                resizeToAvoidBottomInset: false,
                key: _scaffoldKey,
                floatingActionButtonLocation:
                    FloatingActionButtonLocation.miniEndFloat,
                floatingActionButton: BlocBuilder<UsersMeCubit, UsersMeState>(
                  builder: (context, state) {
                    if (Static.me != null) {
                      return BlocBuilder<RequestIndicatorCubit,
                          RequestIndicatorState>(
                        builder: (context, state) {
                          //Check if we got new plurk to decide row content
                          Widget newPlurksNotifyButton = Container();

                          //Show the new plurk button if there's any.
                          if (_newPlurkCount > 0 &&
                              !Static.isViewingUnreadOnly()) {
                            newPlurksNotifyButton = Container(
                                height: kToolbarHeight,
                                alignment: Alignment.center,
                                child: Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30),
                                      color: Theme.of(context)
                                          .lowContrastPrimaryColor
                                          .withOpacity(0.9),
                                    ),
                                    child: InkWell(
                                      onTap: () {
                                        //Get new plurks (_newPlurkCount)
                                        _plurkCardsViewerKey.currentState
                                            .getNewPlurks(_newPlurkCount);
                                        // setState(() {
                                        //   _newPlurkCount = 0;
                                        // });
                                      },
                                      child: Transform.translate(
                                          offset: Offset(0, -1.5),
                                          child: Text(
                                            'New Plurks (' +
                                                _newPlurkCount.toString() +
                                                ')',
                                            textAlign: TextAlign.center,
                                          )),
                                    )));
                          }

                          //Check state.
                          List<Widget> columnChildren = [];

                          //Only when showing request indicator.
                          if (state is RequestIndicatorShow) {
                            //Busy indicator.
                            columnChildren.add(Container(
                              width: 56,
                              height: 56,
                              child: FlareActor(
                                "assets/rive/MeatLoader.flr",
                                animation: "Untitled",
                              ),
                            ));
                          }

                          columnChildren.add(FloatingActionButton(
                            elevation: 1,
                            backgroundColor:
                                Theme.of(context).lowContrastPrimaryColor.withOpacity(0.9),
                            foregroundColor:
                                Theme.of(context).textTheme.bodyText1.color,
                            child: Icon(
                              Icons.add,
                            ),
                            onPressed: () {
                              HapticFeedback.lightImpact();
                              //Open Plurk Poster for new plurk.
                              PlurkPoster.show(
                                  context,
                                  PlurkPoster.PlurkPosterArgs(
                                      PlurkPoster.PlurkPosterMode.New));
                            },
                          ));

                          //Bottom margin.
                          columnChildren.add(SizedBox(
                            height: MediaQuery.of(context).viewInsets.bottom,
                          ));

                          return Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              SizedBox(
                                width: 24,
                              ),
                              //New plurk notify button.
                              Expanded(child: newPlurksNotifyButton),
                              SizedBox(
                                width: 16,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: columnChildren,
                              )
                            ],
                          );
                        },
                      );
                    } else {
                      //No Static.me.
                      return Container();
                    }
                  },
                ),
                drawer: Builder(
                  builder: (context) {
                    //Predefine drawer canvas color. (This is tricky) (affect by acrylic effect)
                    Color drawerCanvasColor =
                        Static.settingsAcrylicEffect == true
                            ? Theme.of(context).canvasColor.withOpacity(0.0)
                            : Theme.of(context).canvasColor;

                    return Theme(
                      data: Theme.of(context).copyWith(
                        // Set the transparency here
                        canvasColor: drawerCanvasColor,
                      ),
                      child: SizedBox(
                        width: 240,
                        child: Drawer(
                          elevation: 0,
                          child: AcrylicContainer(
                            SafeArea(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  HomeDrawerHeader(),
                                  Expanded(
                                    child: Container(
                                      child: HomeDrawerBody(
                                        onTopTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(
                                              context, '/home/top_browser');
                                        },
                                        onAnonymousTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(context,
                                              '/home/anonymous_browser');
                                        },
                                        onHotLinksTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(context,
                                              '/home/hot_links_browser');
                                        },
                                        onSearchTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(
                                              context, '/home/search');
                                        },
                                        onOfficialNewsTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(
                                              context, '/home/official_news');
                                        },
                                        onFriendAndFansTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(context,
                                              '/home/friends_and_fans');
                                        },
                                        onBookmarksTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(
                                              context, '/home/bookmarks');
                                        },
                                        onVTWatchTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(context,
                                              '/home/vt_watch_browser');
                                        },
                                        //Todo
                                        // onCliquesTap: () {
                                        //   Navigator.pop(context);
                                        //   Navigator.pushNamed(context, '/home/cliques');
                                        // },
                                        onAboutTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(
                                              context, '/home/about');
                                        },
                                        //We don't need this anymore...
                                        // onLogoutTap: () {
                                        //   Navigator.pop(context);
                                        //   BlocDialog.askLogOutDialog(
                                        //       _blocProviderContext);
                                        // },
                                        onPlaygroundTap: () {
                                          Navigator.pop(context);
                                          HapticFeedback.lightImpact();
                                          Navigator.pushNamed(
                                              context, '/theme_playground');
                                        },
                                      ),
                                    ),
                                  ),
                                  HomeDrawerFooter(
                                    onSettingsTap: () {
                                      Navigator.pop(context);
                                      HapticFeedback.lightImpact();
                                      Navigator.pushNamed(
                                          context, '/home/settings');
                                    },
                                    onQRCodeTap: () {
                                      Navigator.pop(context);
                                      HapticFeedback.lightImpact();
                                      Navigator.pushNamed(
                                          context, '/home/qr_viewer');
                                    },
                                  ),
                                ],
                              ),
                            ),
                            transparentBackLayer: true,
                            mode: AcrylicContainerMode.BackdropFilter,
                          ),
                        ),
                      ),
                    );
                  },
                ),
                body: BlocBuilder<UsersMeCubit, UsersMeState>(
                  builder: (context, state) {
                    if (Static.me != null) {
                      return PlurkCardsViewer(
                        PlurkCardsViewerArgs(
                          BrowsingType.Home,
                          false,
                          leading: _appBarLeading(),
                          title: _appBarTitle(),
                          actions: _appBarActions(),
                          onLeaveTop: _onViewerLeaveTop,
                          onReturnTop: _onViewerReturnTop,
                        ),
                        key: _plurkCardsViewerKey,
                      );
                    } else {
                      //No Static.me.
                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  //AppBar left leading
  Widget _appBarLeading() {
    return Container(
      padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
      child: GestureDetector(
        child: Center(
          child: Container(
            width: 32,
            height: 32,
            decoration: BoxDecoration(
              // border: Border.all(
              //   color: Theme.of(context).hintColor,
              //   width: 1,
              // ),
              shape: BoxShape.circle,
            ),
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              backgroundImage: Static.me != null ? NetworkImage(Static.me.avatarMedium) : kTransparentImage,
            ),
          ),
        ),
        onTap: () {
          HapticFeedback.lightImpact();
          _scaffoldKey.currentState.openDrawer();
        },
      ),
    );
  }

  Widget _appBarTitle() {
    List<Widget> stackChildren = [];

    stackChildren.add(HomeAppBarTitle(() {
      return _plurkCardsViewerKey.currentState.isRefreshingFeed();
    }, (value) {
      _plurkCardsViewerKey.currentState.refresh();
    }, _getCurrentOffset));

    if (_showReturnToTopButton) {
      stackChildren.add(Container(
        child: TextButton(
          child: Container(),
          onPressed: () {
            //Back to top.
            HapticFeedback.lightImpact();
            _plurkCardsViewerKey.currentState.returnToTop();
          },
        ),
      ));
    }

    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: stackChildren,
      ),
    );
  }

  DateTime _getCurrentOffset() {
    if (_plurkCardsViewerKey.currentState != null) {
      return _plurkCardsViewerKey.currentState.getCurrentOffset();
    }
    return null;
  }

  _onViewerLeaveTop() {
    setState(() {
      _showReturnToTopButton = true;
    });
  }

  _onViewerReturnTop() {
    setState(() {
      _showReturnToTopButton = false;
    });
  }

  List<Widget> _appBarActions() {
    //View unread only button
    Widget viewUnreadOnlyButton = AppBarActionButton(
      Static.isViewingUnreadOnly()
          ? MdiIcons.cardTextOutline
          : MdiIcons.cardText,
      onTap: () {
        HapticFeedback.lightImpact();
        if (!_plurkCardsViewerKey.currentState.isRefreshingFeed()) {
          //Switch the static value of this one.
          bool current = Static.isViewingUnreadOnly();
          setState(() {

            //Switch and save!
            Static.saveIsViewingUnreadOnly(!current);

            //Display toast to notify if we are viewing unread only!
            if (!current) {
              Fluttertoast.showToast(
                msg: FlutterI18n.translate(context, 'toastViewingUnreadOnly'),
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.SNACKBAR,
                timeInSecForIosWeb: 1,
                backgroundColor: Theme.of(context).backgroundColor,
                textColor: Theme.of(context).textTheme.bodyText1.color,
                fontSize: Theme.of(context).textTheme.subtitle2.fontSize
              );
            } else {
              Fluttertoast.showToast(
                msg: FlutterI18n.translate(context, 'toastViewingAllPlurks'),
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.SNACKBAR,
                timeInSecForIosWeb: 1,
                backgroundColor: Theme.of(context).backgroundColor,
                textColor: Theme.of(context).textTheme.bodyText1.color,
                fontSize: Theme.of(context).textTheme.subtitle2.fontSize
              );
            }

            _plurkCardsViewerKey?.currentState?.setIsViewingUnreadOnly(!current);
          });
        }
      },
    );

    //Alert button
    Widget alertButton =
        BlocBuilder<AlertNumberChangedCubit, AlertNumberChangedState>(
      builder: (context, state) {
        List<Widget> stackChildren = [];

        //Icon
        stackChildren.add(AppBarActionButton(
          Icons.notifications,
          onTap: () {
            HapticFeedback.lightImpact();
            //Open AlertBrowser.
            Navigator.pushNamed(context, '/home/alert_browser', arguments: 0);
          },
        ));

        //Dot?
        if (state is AlertNumberChangedSuccess) {
          if (Static.noti > 0 || Static.req > 0) {
            Color dotColor = Theme.of(context).lowContrastPrimaryColor;
            // if (Static.settingsAppTheme == AppTheme.Pure) {
            //   //Force the Pure theme indicator color to text color.
            //   dotColor = Colors.white;
            // }

            stackChildren.add(Positioned(
              top: 16,
              right: 8,
              child: Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: dotColor,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset:
                          Offset(1.0, 1.0), // shadow direction: bottom right
                    )
                  ],
                ),
              ),
            ));
          }
        }

        //Stack for display notification dot
        return Center(
          child: Stack(
            alignment: AlignmentDirectional.topEnd,
            children: stackChildren,
          ),
        );
      },
    );

    return [
      //1
      viewUnreadOnlyButton,
      //2
      alertButton,
      SizedBox(
        width: 4,
      ),
    ];
  }

  _onYoutubeFullscreenEnterState(YoutubeFullscreenEnterState state) {
    //Enter Youtube fullscreen player.
    YoutubeFullScreenViewer.show(context, state.args);
  }

  // _onYoutubeFullscreenLeaveState(YoutubeFullscreenLeaveState state) {
  //   //Maybe we don't even need this?
  // }

  _onImageLinksViewerEnterState(ImageLinksViewerEnterState state) {
    // Do we need to listen to the onExitFullscreen event??
    ImageLinksViewer.show(context, state.args);
  } // Since ImageLinksViewer doesn't reply on Exit event. So we don't need to pop it.

  _onPlurkViewerOpen(PlurkViewerOpen state) {
    PlurkViewer.show(context, state.args);
  }

  _onUserViewerOpen(UserViewerOpen state) {
    UserViewer.show(context, state.args);
  }

  _onUsersBrowserOpen(UsersBrowserOpen state) {
    UsersBrowser.show(context, state.args);
  }

  _onReportAbuseDialogOpenPlurk(int plurkId) {
    ReportAbuse.show(context, plurkId: plurkId);
  }

  _onReportAbuseDialogOpenResponse(int responseId) {
    ReportAbuse.show(context, plurkId: responseId);
  }

  _onPlurkPostCompleteSuccess(PlurkPostCompleteSuccess state) {
    // _showFlushBar(_blocProviderContext, MdiIcons.post_outline, 'Yay!', 'Plurk Success!');
    //Check the current filter to see if we should refresh.
    if (Static.usingPlurkFilter() == null ||
        Static.usingPlurkFilter() == Plurdart.PlurkFilter.my ||
        Static.usingPlurkFilter() == Plurdart.PlurkFilter.private) {
      //Do a refresh request.
      _plurkCardsViewerKey.currentState.refresh();
    }
  }

  _onPlurkEditCompleteSuccess(PlurkEditCompleteSuccess state) {
    setState(() {
      _plurkCardsViewerKey.currentState.overridePlurk(state.plurk);
    });
  }

  _onPlurkDeleteSuccess(TimelinePlurkDeleteSuccess state) {
    // print('Home got TimelinePlurkDeleteSuccess. delete the plurk [' + state.plurkId.toString() + ']');
    setState(() {
      _plurkCardsViewerKey.currentState.deletePlurk(state.plurkId);
    });
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(12, 12, 88, 8),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      flushbarPosition: FlushbarPosition.BOTTOM,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  //The is called when select account success.
  _refreshActivatedAccount() async {
    //Request a refresh.
    _plurkCardsViewerKey.currentState.clear();

    //Stop the Rocket.
    Rocket.land();
    Static.removeMeId();
    Static.me = null;
    Static.balance = null;

    //Clear cache...
    Static.clearUserCache();
    Static.clearTagsCache();

    //Try get another usable account from AccountKeeper.
    AccountTrunk at = AccountKeeper.getActivatedOrAnUsableAccount();
    if (at == null) {
      //Release the Plurdart client.
      Plurdart.release();
      //We have no usable account remained..
      //Return to welcome login.
      Navigator.pushNamedAndRemoveUntil(
          context, '/welcome_login', (route) => false);
    } else if (!(at.accessToken?.isEmpty ?? true) &&
        !(at.tokenSecret?.isEmpty ?? true)) {
      //Got a new active account. Setup Plurdart again (like the we did in launch)
      Plurdart.updateClient(at.accessToken, at.tokenSecret);

      try {
        //Get Users. Nothing more important than this...(Auth flow will try get this done)
        //Static.me could be filled by login when 1st time login. So we may pass this step here.
        await _blocProviderContext.read<UsersMeCubit>().request();
      } catch (error) {
        DebugBrowser.show(context, error.toString());
        print(error.toString());
      }

      //Request a refresh.
      _plurkCardsViewerKey.currentState?.refresh();

      //Get the first (current) alerts unread counts.
      await _blocProviderContext.read<AlertsGetUnreadCountCubit>().request();

      //Update user's emoticon collection cache.
      await _blocProviderContext.read<EmoticonsGetCubit>().request();

      //Read the Comet server url.
      if (Static.me != null) {
        //Start the comet polling loop.
        Rocket.takeOff(Static.me.id,
            // userChannel.cometServer,
            onAlertNumberChanged: _onAlertNumberChanged,
            onNewPlurk: _onNewPlurk,
            onNewResponse: _onNewResponse);
      }
    }
  }

  _onAddAccountSuccess() async {
    Navigator.popUntil(
        context, ModalRoute.withName('/home'));

    print('[_onAddAccountSuccess]');
    await _refreshActivatedAccount();
  }

  _onExpireTokenFinish() async {
    print('[_onExpireTokenFinish]');
    await AccountKeeper.removeActivatedAccount();
    await _refreshActivatedAccount();
  }

  //-- Fire Base messages income --

  // Future<dynamic> _onFirebaseMessage(Map<String, dynamic> message) {
  //   print('_onFirebaseMessage: ' + prettyJson(message));
  //   return null;
  // }
  //
  // Future<dynamic> _onFirebaseResume(Map<String, dynamic> message) {
  //   print('_onFirebaseResume: ' + prettyJson(message));
  //   return null;
  // }
  //
  // Future<dynamic> _onFirebaseLaunch(Map<String, dynamic> message) {
  //   print('_onFirebaseLaunch: ' + prettyJson(message));
  //   return null;
  // }
}
