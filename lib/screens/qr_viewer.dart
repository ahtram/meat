import 'dart:io';

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:url_launcher/url_launcher.dart';

class QrViewer extends StatefulWidget {
  QrViewer({Key key}) : super(key: key);

  @override
  _QrViewerState createState() => _QrViewerState();
}

class _QrViewerState extends State<QrViewer> with WidgetsBindingObserver {

  final GlobalKey _qrKey = GlobalKey(debugLabel: 'QR');
  // Barcode _scanResult;
  QRViewController _qrController;

  PageController _pageController = PageController();
  int _currentPageIndex = 0;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      _qrController.pauseCamera();
    } else if (Platform.isIOS) {
      _qrController.resumeCamera();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    _qrController?.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive || state == AppLifecycleState.paused) {
      _canLaunchUrl = false;
    } else if (state == AppLifecycleState.resumed) {
      _canLaunchUrl = true;
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          //Background (Scanner camera view)
          // To ensure the Scanner view is properly sizes after rotation
          // we need to listen for Flutter SizeChanged notification and update controller
          QRView(
            key: _qrKey,
            onQRViewCreated: _onQRViewCreated,
          ),

          //Front layer to display a back arrow button on the left top.
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [

              //Top SafeArea padding
              SizedBox(height: MediaQuery.of(context).padding.top,),

              Container(
                padding: EdgeInsets.symmetric(horizontal: 8),
                height: kToolbarHeight,
                child: Row(
                  children: [
                    AppBarActionButton(Icons.arrow_back,
                        iconColor: Colors.white,
                        onTap: () {
                          Navigator.pop(context);
                        }),

                    Expanded(child: Container()),

                    //Share button?
                    // PopupMenuButton<String>(
                    //     onSelected: _onPopupSelected,
                    //     itemBuilder: (context) {
                    //       return [
                    //         menuItem('Share', Icons.share, 'Share'),
                    //         menuItem('Save', Icons.save_alt, 'Save'),
                    //       ];
                    //     }),
                  ],
                ),
              ),
              //Page view: My QRCode or Scanning Frame.
              Expanded(
                flex: 3,
                child: Container(
                  height: 300,
                  child: PageView(
                    controller: _pageController,
                    physics: NeverScrollableScrollPhysics(),
                    onPageChanged: _onPageChanged,
                    children: [
                      //1. My QRCode
                      _myQRCodeView(),
                      //2. Scan Frame
                      _scanFrameView(),
                    ],
                  ),
                ),
              ),

              //Switch Page Button.
              Flexible(
                flex: 1,
                child: Center(
                  child: Container(

                    child: IconButton(
                      padding: EdgeInsets.all(0),
                      iconSize: 48,
                      icon: Icon(
                        _currentPageIndex == 0 ?
                        MdiIcons.qrcode :
                        MdiIcons.qrcodeScan,
                      ),
                      onPressed: () {
                        //Switch page
                        if (_currentPageIndex == 0) {
                          _pageController.animateToPage(1, duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
                        } else {
                          _pageController.animateToPage(0, duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
                        }
                      },
                    ),
                  ),
                ),
              ),

              // Flexible(child: Container()),

              SizedBox(
                height: kToolbarHeight,
              ),

              SizedBox(
                height: kMinInteractiveDimension,
              ),
            ],
          ),
        ],
      ),
    );
  }

  bool _canLaunchUrl = true;
  void _onQRViewCreated(QRViewController controller) {
    this._qrController = controller;
    controller.scannedDataStream.listen((scanData) async {
      //Only process result if we are in the scan view
      if (_currentPageIndex == 1) {
        if (scanData != null && _canLaunchUrl) {
          _canLaunchUrl = false;

          if (await canLaunch(scanData.code)) {
            await launch(scanData.code);
          }
        }
      }
    });
  }

  Widget _myQRCodeView() {
    return Container(
      height: 300,
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor.withOpacity(0.33), width: 12),
            borderRadius: BorderRadius.circular(5),
          ),
          child: QrImage(
            data: _userProfileLink(),
            embeddedImage: NetworkImage(Static.me.avatarBig),
            embeddedImageStyle: QrEmbeddedImageStyle(
              size: Size(44, 44),
            ),
            version: QrVersions.auto,
            size: 300.0,
            backgroundColor: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _scanFrameView() {
    return Container(
      height: 300,
      child: Center(
        child: Container(
          width: 300,
          height: 300,
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).accentColor, width: 1.5),
          ),
          child: Container(
            child: Icon(
              MdiIcons.crosshairs,
              size: 24,
              color: Theme.of(context).accentColor,
            ),
          ),
        ),
      )
    );
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPageIndex = index;
    });
  }

  String _userProfileLink() {
    // print('[_userProfileLink][' + Define.plurkMobileUserPath + Static.me.nickName + ']');
    return Define.plurkMobileUserPath + Static.me.nickName;
  }
}