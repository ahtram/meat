import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'home_refresh_state.dart';

class HomeRefreshCubit extends Cubit<HomeRefreshState> {
  HomeRefreshCubit() : super(HomeRefreshInitial());

  request() async {
    emit(HomeRefreshRequest());
  }

  complete() async {
    emit(HomeRefreshComplete());
  }
}
