import 'package:bloc/bloc.dart';
import 'package:meat/screens/plurk_viewer.dart';
import 'package:meta/meta.dart';

part 'plurk_viewer_state.dart';

class PlurkViewerCubit extends Cubit<PlurkViewerState> {
  PlurkViewerCubit() : super(PlurkViewerInitial());

  void open(PlurkViewerArgs args) async {
    emit(PlurkViewerOpen(args));
  }
}
