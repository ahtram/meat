
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PlurkCardActionButton extends StatelessWidget {
  PlurkCardActionButton(this.iconData, this.colorIcon, this.iconColor, {this.iconSize = 20, this.count, this.text, this.onTap, this.onLongPress });

  final IconData iconData;
  final double iconSize;
  final bool colorIcon;
  final Color iconColor;
  final int count;
  final String text;
  final Function onTap;
  final Function onLongPress;

  @override
  Widget build(BuildContext context) {
    Widget buttonWidget;

    if (count != null) {
      List<Widget> rowChildren = [];
      rowChildren.add(Icon(
        iconData,
        size: iconSize,
        color: (colorIcon) ? iconColor : Theme.of(context).hintColor,
      ));

      rowChildren.add(SizedBox(
        width: 8,
      ));

      rowChildren.add(Text(
        NumberFormat.compact().format(count),
        style: TextStyle(
          fontSize: 13,
          color: Theme.of(context).hintColor,
        ),
      ));

      buttonWidget = Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: rowChildren,
      );
    } else if (text != null) {
      List<Widget> rowChildren = [];
      rowChildren.add(Icon(
        iconData,
        size: iconSize,
        color: (colorIcon) ? iconColor : Theme.of(context).hintColor,
      ));

      rowChildren.add(SizedBox(
        width: 8,
      ));

      rowChildren.add(Text(
        text,
        style: TextStyle(
          fontSize: 13,
          color: Theme.of(context).hintColor,
        ),
      ));

      buttonWidget = Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: rowChildren,
      );
    } else {
      buttonWidget = Center(
        child: Icon(
          iconData,
          size: iconSize,
          color: (colorIcon) ? iconColor : Theme.of(context).hintColor,
        ),
      );
    }

    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: buttonWidget,
      ),
      onTap: () {
        onTap?.call();
      },
      onLongPress: () {
        onLongPress?.call();
      },
    );
  }
}