import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'emoticon_notify_owned_state.dart';

class EmoticonNotifyOwnedCubit extends Cubit<EmoticonNotifyOwnedState> {
  EmoticonNotifyOwnedCubit() : super(EmoticonNotifyOwnedInitial());

  show(String url) async {
    emit(EmoticonNotifyOwnedShow(url));
  }
}
