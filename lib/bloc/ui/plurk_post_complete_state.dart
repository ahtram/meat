part of 'plurk_post_complete_cubit.dart';

@immutable
abstract class PlurkPostCompleteState {}

class PlurkPostCompleteInitial extends PlurkPostCompleteState {}

class PlurkPostCompleteSuccess extends PlurkPostCompleteState {
  PlurkPostCompleteSuccess(this.plurk);
  final Plurdart.Plurk plurk;
}