part of 'timeline_favorite_plurks_cubit.dart';

@immutable
abstract class TimelineFavoritePlurksState {}

class TimelineFavoritePlurksInitial extends TimelineFavoritePlurksState {}

class TimelineFavoritePlurkPressed extends TimelineFavoritePlurksState {}
class TimelineFavoritePlurkSuccess extends TimelineFavoritePlurksState {}
class TimelineFavoritePlurkFailed extends TimelineFavoritePlurksState {}

class TimelineUnFavoritePlurkPressed extends TimelineFavoritePlurksState {}
class TimelineUnFavoritePlurkSuccess extends TimelineFavoritePlurksState {}
class TimelineUnFavoritePlurkFailed extends TimelineFavoritePlurksState {}