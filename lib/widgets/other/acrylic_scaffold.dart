import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:meat/bloc/ui/settings_acrylic_changed_cubit.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:flutter_bloc/flutter_bloc.dart';

// A general themed Scaffold use across the project.
class AcrylicScaffold extends StatefulWidget {
  AcrylicScaffold(
      {Key key,
      this.child,
      this.appBar,
      this.floatingActionButton,
      this.resizeToAvoidBottomInset = true,
      this.bottomNavigationBar,
      this.transparentColor = Colors.transparent})
      : super(key: key);

  final Widget child;
  final AppBar appBar;
  final Widget floatingActionButton;
  final bool resizeToAvoidBottomInset;
  final Widget bottomNavigationBar;
  final Color transparentColor;

  @override
  AcrylicScaffoldState createState() => AcrylicScaffoldState();
}

class AcrylicScaffoldState extends State<AcrylicScaffold> {
  AcrylicScaffoldState();

  // For blur image filter.
  double _sigmaX = 20.0; // from 0-10
  double _sigmaY = 20.0; // from 0-10

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsAcrylicChangedCubit,
        SettingsAcrylicChangedState>(builder: (context, state) {
      // print('builder: ' + state.runtimeType.toString());
      if (Static.settingsAcrylicEffect) {
        return Scaffold(
          backgroundColor: widget.transparentColor,
          appBar: widget.appBar,
          floatingActionButton: widget.floatingActionButton,
          resizeToAvoidBottomInset: widget.resizeToAvoidBottomInset,
          body: BackdropFilter(
            filter: ImageFilter.blur(
                sigmaX: _sigmaX, sigmaY: _sigmaY, tileMode: TileMode.mirror),
            child: Material(
              color: Colors.transparent,
              child: Container(
                color: Colors.transparent,
                child: widget.child,
              ),
            ),
          ),
          bottomNavigationBar: widget.bottomNavigationBar,
        );
      } else {
        return Scaffold(
          backgroundColor: Theme.of(context).canvasColor,
          appBar: widget.appBar,
          floatingActionButton: widget.floatingActionButton,
          resizeToAvoidBottomInset: widget.resizeToAvoidBottomInset,
          body: Container(
            color: Theme.of(context).canvasColor,
            child: widget.child,
          ),
          bottomNavigationBar: widget.bottomNavigationBar,
        );
      }
    });
  }
}
