part of 'blocks_unblock_cubit.dart';

@immutable
abstract class BlocksUnblockState {}

class BlocksUnblockInitial extends BlocksUnblockState {}

class BlocksUnblockSuccess extends BlocksUnblockState {}

class BlocksUnblockFailed extends BlocksUnblockState {
  BlocksUnblockFailed(this.errorText);
  final String errorText;
}
