part of 'timeline_mark_as_read_cubit.dart';

@immutable
abstract class TimelineMarkAsReadState {}

class TimelineMarkAsReadInitial extends TimelineMarkAsReadState {}

//[Dep]: This one is not useful...
class TimelineMarkAsReadPressed extends TimelineMarkAsReadInitial {}
class TimelineMarkAsReadSuccess extends TimelineMarkAsReadInitial {
  TimelineMarkAsReadSuccess(this.plurkIDs);
  final List<int> plurkIDs;
}
class TimelineMarkAsReadFailed extends TimelineMarkAsReadInitial {}