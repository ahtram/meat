import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:meat/screens/youtube_fullscreen_viewer.dart';

part 'youtube_fullscreen_state.dart';

class YoutubeFullscreenCubit extends Cubit<YoutubeFullscreenState> {
  YoutubeFullscreenCubit() : super(YoutubeFullscreenInitial());

  void enter(YoutubeFullscreenViewerArgs args) async {
    emit(YoutubeFullscreenEnterState(args));
  }

  void leave() {
    emit(YoutubeFullscreenLeaveState());
  }

}
