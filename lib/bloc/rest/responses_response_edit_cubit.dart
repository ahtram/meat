import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'responses_response_edit_state.dart';

class ResponsesResponseEditCubit extends Cubit<ResponsesResponseEditState> {
  ResponsesResponseEditCubit() : super(ResponsesResponseEditInitial());

  post(Plurdart.ResponsesEdit responseEdit) async {
    Plurdart.ResponseEditResult responseEditResult = await Plurdart.responsesResponseEdit(responseEdit);

    //Check post success.
    if (responseEditResult != null) {
      if (!responseEditResult.hasError()) {
        // print('responsesResponseEdit: ' + prettyJson(plurk.toJson()));
        emit(ResponsesResponseEditSuccess(responseEditResult));
      } else {
        emit(ResponsesResponseEditFailed(responseEditResult.errorText));
      }
    } else {
      emit(ResponsesResponseEditFailed('Unknown error'));
    }
  }
}
