part of 'image_links_viewer_cubit.dart';

@immutable
abstract class ImageLinksViewerState {}

class ImageLinksViewerInitial extends ImageLinksViewerState {}

class ImageLinksViewerEnterState extends ImageLinksViewerState {
  ImageLinksViewerEnterState(this.args);
  final ImageLinksViewerArgs args;
}
