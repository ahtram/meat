part of 'select_account_cubit.dart';

@immutable
abstract class SelectAccountState {}

class SelectAccountInitial extends SelectAccountState {}
class SelectAccountSuccess extends SelectAccountState {}
class SelectAccountFailed extends SelectAccountState {}
