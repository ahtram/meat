part of 'stats_top_favorites_cubit.dart';

@immutable
abstract class StatsTopFavoritesState {}

class StatsTopFavoritesInitial extends StatsTopFavoritesState {}

class StatsTopFavoritesSuccess extends StatsTopFavoritesState {
  StatsTopFavoritesSuccess(this.topPlurks);
  final Plurdart.TopPlurks topPlurks;
}

class StatsTopFavoritesFailed extends StatsTopFavoritesState { }