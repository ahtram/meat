import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

part 'alert_number_changed_state.dart';

class AlertNumberChangedCubit extends Cubit<AlertNumberChangedState> {
  AlertNumberChangedCubit() : super(AlertNumberChangedInitial());

  request({int noti, int req}) {

    //This will change the static vars.
    if (noti != null) {
      // print('noti changed [' + noti.toString() + ']');
      Static.noti = noti;
    }

    if (req != null) {
      // print('req changed [' + req.toString() + ']');
      Static.req = req;
    }

    emit(AlertNumberChangedSuccess());
  }
}
