import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'stats_top_responded_state.dart';

class StatsTopRespondedCubit extends Cubit<StatsTopRespondedState> {
  StatsTopRespondedCubit() : super(StatsTopRespondedInitial());

  request() async {
    Plurdart.TopPlurks topPlurks = await Plurdart.statsTopResponded(Plurdart.StatsTop(lang: Static.apiLanguage));
    if (topPlurks != null) {
      emit(StatsTopRespondedSuccess(topPlurks));
    } else {
      emit(StatsTopRespondedFailed());
    }
  }
}
