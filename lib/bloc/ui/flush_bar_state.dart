part of 'flush_bar_cubit.dart';

@immutable
abstract class FlushBarState {}

class FlushBarInitial extends FlushBarState {}

class FlushBarRequest extends FlushBarState {
  FlushBarRequest(this.icon, this.title, this.message);

  final IconData icon;
  final String title;
  final String message;
}
