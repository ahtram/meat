
// Display a dialog for logout.
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/bloc/rest/emoticons_add_from_url_cubit.dart';
import 'package:meat/bloc/rest/emoticons_delete_cubit.dart';
import 'package:meat/bloc/rest/expire_token_cubit.dart';
import 'package:meat/screens/busy.dart' as Busy;

askLogOutDialog(BuildContext context, String nickName) {
  //Display a asking dialog
  AwesomeDialog(
      context: context,
      dialogType: DialogType.WARNING,
      animType: AnimType.SCALE,
      headerAnimationLoop: false,
      title: 'Log Out',
      desc: 'Log out @' + nickName + '?',
      btnOkText: 'Yeah',
      btnCancelText: 'Forget It',
      btnOkOnPress: () async {
        Busy.show(context);
        context.read<ExpireTokenCubit>().expireToken();
      },
      btnCancelOnPress: () {})
    ..show();
}

emoticonAskAddDialog(BuildContext context, String url) {
  TextEditingController keywordEditorController = TextEditingController();
  AwesomeDialog(
      context: context,
      customHeader: Container(
        width: 48,
        height: 48,
        child: Image(
          fit: BoxFit.scaleDown,
          image: CachedNetworkImageProvider(url),
        ),
      ),
      animType: AnimType.SCALE,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          // Text('Nice Emoticon!',
          // style: Theme.of(context).textTheme.headline6,),
          //
          // SizedBox(
          //   height: 12,
          // ),

          Text('Add this emoticon?',
            style: Theme.of(context).textTheme.headline6,),

          SizedBox(
            height: 16,
          ),

          SizedBox(
            width: 200,
            child: TextField(
              controller: keywordEditorController,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(6),
                border: OutlineInputBorder(
                ),
                hintText: 'Keyword (optional)',
                hintStyle: TextStyle(
                  color: Theme.of(context).hintColor,
                  fontSize: 14,
                ),
                isDense: true,
              ),
              textAlign: TextAlign.center,
              cursorColor: Theme.of(context).accentColor,
              keyboardAppearance: Theme.of(context).brightness,
            ),
          ),
        ],
      ),
      btnOkText: 'Yeah',
      btnCancelText: 'Forget It',
      btnOkOnPress: () async {
        //Call cubit to add this emoticon.
        context.read<EmoticonsAddFromUrlCubit>().request(url, keyword: keywordEditorController.text);
      },
      btnCancelOnPress: () {})
    ..show();
}

emoticonAskDeleteDialog(BuildContext context, String url) {
  AwesomeDialog(
      context: context,
      customHeader: Container(
        width: 48,
        height: 48,
        child: Image(
          fit: BoxFit.scaleDown,
          image: CachedNetworkImageProvider(url),
        ),
      ),
      animType: AnimType.SCALE,
      title: 'Delete This!',
      desc: 'Are you sure?',
      btnOkText: 'Yeah',
      btnCancelText: 'Forget It',
      btnOkOnPress: () async {
        //Call cubit to add this emoticon.
        context.read<EmoticonsDeleteCubit>().request(url);
      },
      btnCancelOnPress: () {})
    ..show();
}

emoticonNotifyOwnedDialog(BuildContext context, String url) {
  AwesomeDialog(
    context: context,
    customHeader: Container(
      width: 48,
      height: 48,
      child: Image(
        fit: BoxFit.scaleDown,
        image: CachedNetworkImageProvider(url),
      ),
    ),
    animType: AnimType.SCALE,
    title: 'You own this one!',
    desc: 'Oh so happy!',
    btnOkText: 'Ok',
    btnOkOnPress: () { },)
    ..show();
}