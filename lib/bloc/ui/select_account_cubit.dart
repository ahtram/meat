import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:meat/system/account_keeper.dart' as AccountKeeper;

part 'select_account_state.dart';

class SelectAccountCubit extends Cubit<SelectAccountState> {
  SelectAccountCubit() : super(SelectAccountInitial());

  request(String nickName) async {
    //Failed if select the same account.
    if (nickName != AccountKeeper.meNickName()) {
      bool result = await AccountKeeper.setAccountActive(nickName);
      if (result) {
        emit(SelectAccountSuccess());
      } else {
        emit(SelectAccountFailed());
      }
    } else {
      emit(SelectAccountFailed());
    }
  }

}
