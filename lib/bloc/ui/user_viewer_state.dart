part of 'user_viewer_cubit.dart';

@immutable
abstract class UserViewerState {}

class UserViewerInitial extends UserViewerState {}

class UserViewerOpen extends UserViewerState {
  UserViewerOpen(this.args);
  final UserViewerArgs args;
}
