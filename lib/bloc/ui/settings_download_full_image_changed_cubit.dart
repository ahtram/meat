import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'settings_download_full_image_changed_state.dart';

class SettingsDownloadFullImageChangedCubit extends Cubit<SettingsDownloadFullImageChangedState> {
  SettingsDownloadFullImageChangedCubit() : super(SettingsDownloadFullImageChangedInitial());

  success() {
    emit(SettingsDownloadFullImageChangedSuccess());
  }
}
