import 'dart:async';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/bloc/ui/image_links_viewer_cubit.dart';
import 'package:meat/bloc/ui/plurk_viewer_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/bloc/ui/users_browser_cubit.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';
import 'package:video_player/video_player.dart';
import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:wakelock/wakelock.dart';

//The Youtube player made by YoutubeExplode and video_player packages.
class YoutubeVideoPlayer extends StatefulWidget {
  YoutubeVideoPlayer(this.videoId,
      {Key key,
      this.isInFullScreenSize = false,
      this.onFullScreenTap,
      this.onChanged,
      this.startAt = 0,
      this.autoPlay = false,
      this.fitVideoAspectRatio = true,
      this.sourceState})
      : super(key: key);

  final String videoId;
  final bool isInFullScreenSize;
  final Function onFullScreenTap;
  //In milliseconds.
  final Function(int) onChanged;
  //In microseconds.
  final int startAt;
  final bool autoPlay;
  final bool fitVideoAspectRatio;
  //Only when you need an exist youtube player as external state.
  final YoutubeVideoPlayerState sourceState;

  //The preview image if any.
  //This is too ugly! I don't want to use this.
  // final String previewImageUrl;

  @override
  YoutubeVideoPlayerState createState() => YoutubeVideoPlayerState();
}

class YoutubeVideoPlayerState extends State<YoutubeVideoPlayer> {
  VideoPlayerController videoPlayerController;
  VideoMeta video;

  //Progress length and progress in inMicroseconds.
  int videoDuration = 0;
  int videoPlayingPosition = 0;

  //Get from Video data.
  String thumbNail;

  //Decide if the control UI is showing
  bool showControlUI = true;

  //If the video is playing. Start the auto hide timer.
  Timer autoHideControlTimer;

  bool hasPlayed = false;

  //For un support link. We'll just pop an open link button for the user.
  String errorMessage;

  static const double DEFAULT_ASPECT_RATIO = 16.0 / 9.0;
  //This maybe changed be loaded video resolution...
  double videoAspectRatio = DEFAULT_ASPECT_RATIO;

  //In milliseconds.
  static const int AUTO_HIDE_CONTROL_UI_TIME = 3000;

  // static const String YOUTUBE_VIDEO_URL_PREFIX = 'https://youtube.com/watch?v=';

  @override
  void initState() {
    super.initState();
    _prepAndTryAutoPlay();
  }

  _prepAndTryAutoPlay() async {
    await _prep();
    if (mounted && widget.autoPlay) {
      //Auto play from startAt time...
      if (widget.startAt != 0) {
        playAt(widget.startAt);
      } else {
        play();
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    _cancelAutoHideUITimer();

    //Only if the state is not borrowed.
    videoPlayerController?.removeListener(_onControllerChanged);
    if (widget.sourceState == null) {
      videoPlayerController?.dispose();
    }

    if (videoPlayerController != null) {
      if (videoPlayerController.value.isPlaying) {
        // print('Wakelock.disable()');
        Wakelock.disable();
      }
    }
  }

  _prep() async {
    if (widget.sourceState != null) {
      //Copy the existing reference params.
      _prepFromSourceState();
    } else {
      //Make our owns.
      //Try get the video.
      _prepVideo();
    }
  }

  _prepFromSourceState() {
    videoPlayerController = widget.sourceState.videoPlayerController;
    videoPlayerController?.addListener(_onControllerChanged);
    video = widget.sourceState.video;
    videoDuration = widget.sourceState.videoDuration;
    videoPlayingPosition = widget.sourceState.videoPlayingPosition;
    thumbNail = widget.sourceState.thumbNail;
    showControlUI = widget.sourceState.showControlUI;
    autoHideControlTimer = widget.sourceState.autoHideControlTimer;
    errorMessage = widget.sourceState.errorMessage;
    hasPlayed = widget.sourceState.hasPlayed;
    videoAspectRatio = widget.sourceState.videoAspectRatio;
    if (hasPlayed) {
      _startAutoHideUITimer();
    }
  }

  _prepVideo() async {
    //Try get the video data.
    //Read the youtube Stream.
    video = await compute(getVideoData, widget.videoId);

    if (!mounted) return;

    if (video != null && !video.isError()) {
      setState(() {
        //Set a preview thumbnail image.
        thumbNail = video.thumbnailsMediumResUrl;
        videoDuration = video.durationInMicroSeconds;
      });

      // print('[_prepVideo] title [' +
      //     video.title +
      //     '] author [' +
      //     video.author +
      //     '] duration [' +
      //     video.duration.inSeconds.toString() +
      //     '] videoId [' +
      //     widget.videoId +
      //     ']');

      try {
        String networkUrl = video.networkUrl;

        if (!video.isLive) {
          videoAspectRatio = video.videoAspectRatio;
        }

        if (!mounted) return;

        if (networkUrl != null && networkUrl.isNotEmpty) {
          // Get the actual stream
          // print('[_prepVideo] got stream. [' + streamInfo.url.toString() + ']');
          // Stream<List<int>> stream = ye.videos.streamsClient.get(streamInfo);

          //Let's play this thing by Video player.
          videoPlayerController = VideoPlayerController.network(networkUrl);
          videoPlayerController.addListener(_onControllerChanged);

          //Is this thing starting a video pre-download?
          // (We should do this when first time hit the play button)
          // await videoPlayerController.initialize();

          // if (!mounted) return;

          // print('[_prepVideo] Video initialized.');

          setState(() {});
        } else {
          print('[_prepVideo] oops! steamInfo is null?!');

          if (!mounted) return;

          //Show the open link button.
          setState(() {
            errorMessage = 'Unable to play the video.';
          });
        }
      } catch (error) {
        print('[YoutubePlayer] got error message: ' + error.message);

        if (!mounted) return;

        //Show the open link button.
        setState(() {
          errorMessage = 'Unable to play the video.';
        });

        if (error.message.contains('is an ongoing live stream')) {
        } else {}
      }
    } else {
      //Cannot get the video or is error.

      if (!mounted) return;

      //Show the open link button.
      if (video != null && video.isError()) {
        setState(() {
          errorMessage = video.getReason();
        });
      } else {
        setState(() {
          errorMessage = 'Cannot load the video.';
        });
      }
    }
  }

  _onControllerChanged() {
    if (mounted) {
      setState(() {
        //This will notify and update the progress bar position.
        //It seems that we cannot detect that another player stop us.
        // print('_onControllerChanged: ' + _videoPlayerController.value.isPlaying.toString());

        //Detect stop by other videos.
        if (!videoPlayerController.value.isPlaying) {
          _cancelAutoHideUITimer();
          showControlUI = true;
        }
      });
    }
  }

  _startAutoHideUITimer() {
    autoHideControlTimer?.cancel();
    autoHideControlTimer =
        Timer(Duration(milliseconds: AUTO_HIDE_CONTROL_UI_TIME), () {
      if (!mounted) return;

      setState(() {
        showControlUI = false;
      });
    });
  }

  _cancelAutoHideUITimer() {
    autoHideControlTimer?.cancel();
  }

  // _toggleUIVisible() {
  //   setState(() {
  //     _showControlUI = !_showControlUI;
  //   });
  // }

  //Reload video data.
  _reloadVideo() {
    videoPlayerController = null;
    video = null;

    //Progress length and progress in inMicroseconds.
    videoDuration = 0;
    videoPlayingPosition = 0;

    //Get from Video data.
    thumbNail = null;

    //Decide if the control UI is showing
    showControlUI = true;

    //If the video is playing. Start the auto hide timer.
    if (autoHideControlTimer != null) {
      autoHideControlTimer.cancel();
    }

    hasPlayed = false;

    //For un support link. We'll just pop an open link button for the user.
    errorMessage = null;

    if (widget.sourceState != null) {
      //Copy the existing reference params.
      _prepFromSourceState();
    } else {
      //Make our owns.
      //Try get the video.
      _prepVideo();
    }
  }

  @override
  Widget build(BuildContext context) {
    //Detect data changed and force reload the video player!
    if (video != null && !video.isError() && video.id.value != widget.videoId) {
      //Video changed!!
      //Force rebuild.
      _reloadVideo();
      // print('widget.videoId: ' + widget.videoId + " | video.id.value: " + video.id.value);
    }

    return MultiBlocListener(
      listeners: [
        BlocListener<ImageLinksViewerCubit, ImageLinksViewerState>(
          listener: (context, state) {
            if (state is ImageLinksViewerEnterState) {
              pause();
            }
          },
        ),
        BlocListener<PlurkViewerCubit, PlurkViewerState>(
          listener: (context, state) {
            if (state is PlurkViewerOpen) {
              pause();
            }
          },
        ),
        BlocListener<UserViewerCubit, UserViewerState>(
          listener: (context, state) {
            if (state is UserViewerOpen) {
              pause();
            }
          },
        ),
        BlocListener<UsersBrowserCubit, UsersBrowserState>(
          listener: (context, state) {
            if (state is UsersBrowserOpen) {
              pause();
            }
          },
        ),
      ],
      child: Container(
        child: Center(
          child: _videoPlayer(),
        ),
      ),
    );
  }

  Widget _videoPlayer() {
    //Initialize completed.
    List<Widget> stackChildren = [];

    if (videoPlayerController != null &&
        videoPlayerController.value.isInitialized) {
      //Video player itself.
      stackChildren.add(VideoPlayer(videoPlayerController));
    }

    //Preview image? (wtf do we need this?)
    if (thumbNail != null && !hasPlayed && widget.startAt == 0) {
      stackChildren.add(Image.network(
        thumbNail,
        fit: BoxFit.contain,
      ));
    }

    //Display UI?
    if (videoPlayerController != null) {
      if (showControlUI) {
        //Control UI play button.
        if (!videoPlayerController.value.isPlaying) {
          //Play/pause control.
          stackChildren.add(

            //A row contains 3 buttons. (copy url / play / open link)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //Copy button.
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(12),
                      color: Colors.black.withOpacity(0.33),
                      child: Icon(
                        Icons.copy,
                        color: Colors.white,
                      ),
                    ),
                    onTap: () {
                      //Copy video url
                      if (video != null) {
                        FlutterClipboard.copy(video.url);
                        context.read<FlushBarCubit>().request(
                            Icons.content_copy, 'Success!', 'Video url copied.');
                        Vibrator.vibrateShortNote();
                      }
                    },
                  ),

                  SizedBox(
                    width: 32,
                  ),

                  //Play button display
                  Container(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Center(
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.black.withOpacity(0.33),
                          border: Border.all(
                            color: Colors.white.withOpacity(0.1),
                            width: 1,
                          ),
                        ),
                        child: Icon(
                          Icons.play_arrow,
                          color: Colors.white,
                          size: 64,
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    width: 32,
                  ),

                  //Open link button.
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(12),
                      color: Colors.black.withOpacity(0.33),
                      child: Icon(
                        Icons.open_in_new,
                        color: Colors.white,
                      ),
                    ),
                    onTap: () async {
                      //Open link.
                      if (video != null) {
                        if (await canLaunch(video.url)) {
                          Vibrator.vibrateShortNote();
                          await launch(video.url);
                        }
                      }
                    },
                  ),
                ],
              ));
        }

        //Live icon or time left.
        Widget timeLeftWidget = Container();
        if (video != null && !video.isLive) {
          timeLeftWidget = Text(
            _formattedDuration(Duration(microseconds: videoPlayingPosition)) +
                '/' +
                _formattedDuration(Duration(microseconds: videoDuration)),
            style: TextStyle(
              color: Colors.white,
            ),
          );
        } else {
          timeLeftWidget = Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  MdiIcons.circleSlice8,
                  color: Colors.red,
                ),
                SizedBox(
                  width: 8,
                ),
                Transform.translate(
                  child: Text('Live'),
                  offset: Offset(0, -1.5),
                ),
              ],
            ),
          );
        }

        //Progress slider
        Widget sliderWidget = Container();
        if (videoPlayerController != null && video != null && !video.isLive) {
          //Slider param update.
          double sliderMax =
          videoPlayerController.value.duration.inMilliseconds.toDouble();
          double sliderValue =
          videoPlayerController.value.position.inMilliseconds.toDouble();
          if (sliderValue > sliderMax) {
            sliderValue = sliderMax;
          }

          sliderWidget = Slider(
            min: 0,
            max: sliderMax,
            activeColor: Colors.white.withOpacity(0.5),
            inactiveColor: Colors.white.withOpacity(0.2),
            value: sliderValue,
            onChanged: (value) {
              Duration duration = Duration(milliseconds: value.toInt());
              setState(() {
                videoPlayerController.seekTo(duration);
                videoPlayingPosition = duration.inMicroseconds;
              });
              //Reset auto hide timer.
              _startAutoHideUITimer();
            },
          );
        }

        //Play time text update.
        if (videoPlayerController != null && videoPlayingPosition !=
            videoPlayerController.value.position.inMicroseconds) {
          videoPlayingPosition =
              videoPlayerController.value.position.inMicroseconds;
          widget.onChanged?.call(videoPlayingPosition);
        }

        //Bottom control bar.
        stackChildren.add(Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Container(),
            ),

            //Slider Control bar / Fullscreen button
            Container(
              padding: EdgeInsets.symmetric(horizontal: 12),
              color: Colors.black.withOpacity(0.4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //Time left. (milliseconds)
                  timeLeftWidget,

                  Expanded(
                    child: sliderWidget,
                  ),

                  InkWell(
                    child: Icon(
                      widget.isInFullScreenSize
                          ? Icons.fullscreen_exit
                          : Icons.fullscreen,
                      size: 32,
                      color: Colors.white,
                    ),
                    onTap: () {
                      //Tell the outside we should enter or leave fullscreen player...
                      if (widget.onFullScreenTap != null) {
                        Vibrator.vibrateShortNote();
                        widget.onFullScreenTap.call();
                      }
                    },
                  )
                ],
              ),
            ),
          ],
        )); // ProgressBar
      }

      //Progress indicator.
      if (!showControlUI && video != null && !video.isLive) {
        double progressValue =
            videoPlayerController.value.position.inMilliseconds /
                videoPlayerController.value.duration.inMilliseconds.toDouble();
        //Progress indicator.
        stackChildren.add(Align(
          alignment: Alignment.bottomCenter,
          child: LinearProgressIndicator(
            minHeight: 2,
            value: progressValue,
          ),
        ));
      }
    }

    //Is this video un support by the player?
    if (errorMessage == null) {
      if (videoPlayerController == null ||
          videoPlayerController.value.isBuffering) {
        //Circle progress indicator.
        stackChildren.add(Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
          child: Center(
            child: SizedBox(
                width: 100,
                height: 100,
                child: CircularProgressIndicator(
                  strokeWidth: 6,
                )),
          ),
        ));
      }
    } else {
      //Show the open link button.
      stackChildren.add(Container(
        padding: EdgeInsets.all(8),
        color: Colors.black.withOpacity(0.75),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 280,
                child: Text(
                  errorMessage,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              OutlinedButton.icon(
                  onPressed: () async {
                    if (video != null && !video.isError()) {
                      if (await canLaunch(video.url)) {
                        launch(video.url);
                      }
                    } else {
                      //What? We'll try fallback to use our own way!
                      String videoUrl =
                          'https://www.youtube.com/watch?v=' + widget.videoId;
                      if (await canLaunch(videoUrl)) {
                        launch(videoUrl);
                      }
                    }
                  },
                  icon: Icon(
                    Icons.open_in_new,
                    color: Colors.white,
                  ),
                  label: Text(
                    'Open Video Link',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  )),
              SizedBox(
                height: 16,
              ),
              InkWell(
                child: Icon(
                  Icons.refresh,
                  color: Colors.white,
                ),
                onTap: () {
                  HapticFeedback.lightImpact();
                  _reloadVideo();
                },
              ),
            ],
          ),
        ),
      ));
    }

    return GestureDetector(
      child: AspectRatio(
        aspectRatio: widget.fitVideoAspectRatio
            ? videoAspectRatio
            : DEFAULT_ASPECT_RATIO,
        child: ClipRect(
          child: Stack(
            alignment: Alignment.center,
            fit: StackFit.expand,
            children: stackChildren,
          ),
        ),
      ),
      onTap: () {
        setState(() {
          //!!!!!!! Main control! !!!!!!!!
          if (videoPlayerController != null) {
            if (videoPlayerController.value.isPlaying) {
              if (!showControlUI) {
                showControlUI = true;
                _startAutoHideUITimer();
              } else {
                HapticFeedback.lightImpact();
                pause();
                _cancelAutoHideUITimer();
              }
            } else {
              if (videoPlayerController != null && !videoPlayerController.value.isBuffering) {
                if (videoPlayerController.value.duration.inMilliseconds -
                        videoPlayerController.value.position.inMilliseconds <
                    100) {
                  videoPlayerController.seekTo(Duration(milliseconds: 0));
                }
              }

              HapticFeedback.lightImpact();
              play();
            }
          }
        });
        //Maybe not
        //Hide/Show control UI.
        // _toggleUIVisible();
      },
    );
  }

  String _formattedDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    int durationHours = duration.inHours;
    if (durationHours > 0) {
      return '${twoDigits(durationHours)}:$twoDigitMinutes:$twoDigitSeconds';
    } else {
      return '$twoDigitMinutes:$twoDigitSeconds';
    }
  }

  //For open any viewers...
  pause() {
    // print('pause [' + widget.videoId + ']');
    videoPlayerController?.pause();
    Wakelock.disable();
  }

  Future playAt(int startAt) async {
    await play();
    if (videoPlayerController == null) {
      videoPlayerController?.seekTo(Duration(milliseconds: startAt));
    }
  }

  Future play() async {
    if (!videoPlayerController.value.isPlaying &&
        !videoPlayerController.value.isBuffering) {
      if (!videoPlayerController.value.isInitialized) {
        videoPlayerController.initialize();
        setState(() {});
      }

      // print('play [' + widget.videoId + ']');
      videoPlayerController?.play();

      _startAutoHideUITimer();
      hasPlayed = true;

      Wakelock.enable();
    }
  }
}

//The essential video meta we send over isolate from YoutubeExplode!
class VideoMeta {
  VideoMeta(
      {this.id, this.url, this.isLive, this.thumbnailsMediumResUrl, this.durationInMicroSeconds, this.networkUrl, this.videoAspectRatio, this.errorMessage});

  final VideoId id;
  final String url;
  final bool isLive;
  final String thumbnailsMediumResUrl;
  final int durationInMicroSeconds;
  final String networkUrl;
  final double videoAspectRatio;

  final String errorMessage;

  bool isError() {
    return errorMessage != null;
  }

  String getReason() {
    if (errorMessage != null) {
      const startKeyWord = 'Reason: ';
      final startIndex = errorMessage.indexOf(startKeyWord);
      if (startIndex != -1) {
        return errorMessage.substring(startIndex + startKeyWord.length);
      }
    }
    return 'Cannot load the video.';
  }
}

//A global method for compute() to get the video data.
//By using compute we may get rid of the UI stutter!
Future<VideoMeta> getVideoData(String videoId) async {
  //Read the youtube Stream.
  try {
    YoutubeExplode ye = YoutubeExplode();
    Video video = await ye.videos.get(videoId);
    String networkUrl;
    double videoAspectRatio;

    //Try get manifest and networkUrl.
    if (video.isLive) {
      //Bug workaround.
      //https://github.com/Hexer10/youtube_explode_dart/issues/134
      // networkUrl = video.watchPage.getPlayerResponse().hlsManifestUrl;
      networkUrl = await ye.videos.streamsClient
          .getHttpLiveStreamUrl(VideoId(videoId));

      //Test try print upload and publish date.
      print('[Live]: [' + video.uploadDate.toString() + '][' + video.publishDate.toString() + ']');

      // print('Got live stream url [' + networkUrl + ']');
    } else {
      //Get the stream url.
      StreamManifest manifest =
      await ye.videos.streamsClient.getManifest(videoId);
      // Get highest quality muxed stream
      // manifest.video.withHighestBitrate() is Bugged!
      List<MuxedStreamInfo> infos = manifest.muxed.sortByVideoQuality();

      // for (int i = 0 ; i < infos.length ; ++i) {
      //   print('info [' + i.toString() + '] audio [' + infos[i].audioCodec + ']');
      // }

      //[Temp]There's a bug which videos has no audio. This maybe a temp solution.
      VideoStreamInfo streamInfo =
      infos.firstWhere((info) => info.audioCodec.startsWith('mp4'));
      //Check the resolution?
      videoAspectRatio = streamInfo.videoResolution.width.toDouble() /
          streamInfo.videoResolution.height.toDouble();

      networkUrl = streamInfo.url.toString();
    }

    return VideoMeta(
        id: video.id,
        url: video.url,
        isLive: video.isLive,
        thumbnailsMediumResUrl: video.thumbnails.mediumResUrl,
        durationInMicroSeconds: video.duration.inMicroseconds,
        networkUrl: networkUrl,
        videoAspectRatio: videoAspectRatio,
    );
  } catch (e) {
    YoutubeExplodeException youtubeExplodeException = e as VideoUnplayableException;
    print('getVideoData got exception: ' + youtubeExplodeException.message);
    return VideoMeta(errorMessage: youtubeExplodeException.message);
  }
}
