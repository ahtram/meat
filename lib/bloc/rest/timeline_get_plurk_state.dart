part of 'timeline_get_plurk_cubit.dart';

@immutable
class TimelineGetPlurkState {
  TimelineGetPlurkState(this.plurkWithUser);
  final Plurdart.PlurkWithUser plurkWithUser;
}
