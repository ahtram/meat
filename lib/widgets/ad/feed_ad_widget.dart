//[Dep]

// import 'package:after_layout/after_layout.dart';
// import 'package:flutter/material.dart';
// import 'package:meat/system/define.dart' as Define;
// import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:shimmer/shimmer.dart';
//
// class FeedAdWidget extends StatefulWidget {
//   FeedAdWidget({Key key, this.padding = EdgeInsets.zero}) : super(key: key);
//
//   final EdgeInsetsGeometry padding;
//
//   @override
//   _FeedAdWidgetState createState() => _FeedAdWidgetState();
// }
//
// class _FeedAdWidgetState extends State<FeedAdWidget> with AfterLayoutMixin {
//   BannerAd _inlineAdaptiveAd;
//   bool _isAdLoaded = false;
//   AdSize _adSize;
//
//   double get _adWidth =>
//       MediaQuery.of(context).size.width - widget.padding.horizontal;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   void afterFirstLayout(BuildContext context) async {
//     await _loadAd();
//   }
//
//   Future<void> _loadAd() async {
//     await _inlineAdaptiveAd?.dispose();
//
//     if (!mounted)
//       return;
//
//     setState(() {
//       _inlineAdaptiveAd = null;
//     });
//
//     // Get an inline adaptive size for the current orientation.
//     AdSize size = AdSize.getCurrentOrientationInlineAdaptiveBannerAdSize(
//             _adWidth.truncate());
//
//     if (size == null) {
//       print('Unable to get height of banner.');
//       return;
//     }
//
//     if (!mounted)
//       return;
//
//     //Load banner.
//     if (mounted) {
//       _inlineAdaptiveAd = BannerAd(
//         adUnitId: Define.commentADUnitID(),
//         size: size,
//         request: AdRequest(),
//         listener: BannerAdListener(
//           onAdLoaded: (Ad ad) async {
//             // print('onAdLoaded: ' + ad.responseInfo.toString());
//
//             // After the ad is loaded, get the platform ad size and use it to
//             // update the height of the container. This is necessary because the
//             // height can change after the ad is loaded.
//             BannerAd bannerAd = (ad as BannerAd);
//             final AdSize size = await bannerAd.getPlatformAdSize();
//             if (size == null) {
//               print('Error: getPlatformAdSize() returned null for $bannerAd');
//               return;
//             }
//
//             _inlineAdaptiveAd = bannerAd;
//             _isAdLoaded = true;
//             _adSize = size;
//
//             if (mounted) {
//               setState(() {
//
//               });
//             }
//           },
//           onAdFailedToLoad: (Ad ad, LoadAdError error) {
//             print('onAdFailedToLoad: ' + error.toString());
//             ad.dispose();
//           },
//         ),
//       );
//       await _inlineAdaptiveAd.load();
//     }
//   }
//
//   @override
//   void dispose() {
//     //Dispose banner.
//     _inlineAdaptiveAd?.dispose();
//     _inlineAdaptiveAd = null;
//     _isAdLoaded = false;
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     //This plugin sucks hard!
//     Widget ad;
//     if (_inlineAdaptiveAd != null && _isAdLoaded) {
//       ad = AdWidget(ad: _inlineAdaptiveAd);
//     } else {
//       ad = _shimmerLoading();
//     }
//
//     double adHeight = 316;
//     if (_adSize != null) {
//       adHeight = _adSize.height.toDouble();
//     }
//
//     return Column(
//       mainAxisSize: MainAxisSize.min,
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         Container(
//           height: adHeight + widget.padding.vertical,
//           padding: widget.padding,
//           child: ad,
//         ),
//         Divider(
//           height: 0.5,
//           thickness: 0.5,
//         ),
//       ],
//     );
//   }
//
//   Widget _shimmerLoading() {
//     return Shimmer.fromColors(
//         child: Container(
//           padding: EdgeInsets.all(4),
//           decoration: BoxDecoration(
//             border: Border.all(
//               color: Colors.white,
//               width: 4,
//             ),
//           ),
//           child: Column(
//             children: [
//               Flexible(
//                 flex: 3,
//                 child: Row(
//                   children: [
//                     Flexible(
//                       flex: 2,
//                       child: Container(
//                         margin: EdgeInsets.all(4),
//                         color: Colors.white,
//                       ),
//                     ),
//                     Flexible(
//                       flex: 3,
//                       child: Column(
//                         children: [
//                           Flexible(
//                             child: Container(
//                               margin: EdgeInsets.all(4),
//                               color: Colors.white,
//                             ),
//                             flex: 1,
//                           ),
//                           Flexible(
//                             child: Container(
//                               margin: EdgeInsets.all(4),
//                               color: Colors.white,
//                             ),
//                             flex: 1,
//                           ),
//                           Flexible(
//                             child: Container(
//                               margin: EdgeInsets.all(4),
//                               color: Colors.white,
//                             ),
//                             flex: 1,
//                           ),
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//               Flexible(
//                 flex: 2,
//                 child: Container(
//                   margin: EdgeInsets.all(4),
//                   color: Colors.white,
//                 ),
//               )
//             ],
//           ),
//         ),
//         baseColor: Theme.of(context).colorScheme.primary.withOpacity(0.4),
//         highlightColor: Theme.of(context).colorScheme.secondary);
//   }
// }
