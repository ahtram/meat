import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_mark_as_read_state.dart';

class TimelineMarkAsReadCubit extends Cubit<TimelineMarkAsReadState> {
  TimelineMarkAsReadCubit() : super(TimelineMarkAsReadInitial());

  void mark(List<int> ids) async {
    //Nope...
    // emit(TimelineMarkAsReadPressed());

    Plurdart.Base base = await Plurdart.timelineMarkAsRead(
        Plurdart.TimelineMarkAsRead(ids: ids, notePosition: true));
    if (base != null) {
      if (base.hasSuccess()) {
        emit(TimelineMarkAsReadSuccess(ids));
      } else {
        emit(TimelineMarkAsReadFailed());
      }
    } else {
      emit(TimelineMarkAsReadFailed());
    }
  }
}
