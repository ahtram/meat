part of 'theme_bloc.dart';

class ThemeState extends Equatable {
  final AppTheme appTheme;
  final ThemeMode themeMode;
  final double textScale;
  final CanvasColor canvasColor;

  const ThemeState(
      {@required this.appTheme,
      @required this.themeMode,
      @required this.textScale,
      @required this.canvasColor});

  @override
  List<Object> get props => [appTheme, themeMode, textScale, canvasColor];
}
