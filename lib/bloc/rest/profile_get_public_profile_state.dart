part of 'profile_get_public_profile_cubit.dart';

@immutable
abstract class ProfileGetPublicProfileState {}

class ProfileGetPublicProfileInitial extends ProfileGetPublicProfileState {}

class ProfileGetPublicProfileSuccess extends ProfileGetPublicProfileState {
  ProfileGetPublicProfileSuccess(this.profile);
  final Plurdart.Profile profile;
}

class ProfileGetPublicProfileFailed extends ProfileGetPublicProfileState {
  ProfileGetPublicProfileFailed(this.error);
  final String error;
}
