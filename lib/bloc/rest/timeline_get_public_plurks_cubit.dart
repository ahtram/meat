import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'timeline_get_public_plurks_state.dart';

class TimelineGetPublicPlurksCubit extends Cubit<TimelineGetPublicPlurksState> {
  TimelineGetPublicPlurksCubit() : super(TimelineGetPublicPlurksInitial());

  request(int userId) async {
    Plurdart.Plurks plurks = await Plurdart.timelineGetPublicPlurks(Plurdart.TimelineGetPublicPlurks(
      userId: userId,
      favorersDetail: true,
      limitedDetail: true,
      replurkersDetail: true,
    ));

    //Collect Users to global User Cache.
    if (plurks != null) {
      plurks.plurkUsers.forEach((key, value) {
        // print('TimelineGetPlurksCubit addToUserCache: ' + value.id.toString());
        Static.addToUserCache(value);
      });
    }

    emit(TimelineGetPublicPlurksSuccess(plurks));
  }

  trackback(int userId, DateTime offset) async {
    Plurdart.Plurks plurks = await Plurdart.timelineGetPublicPlurks(Plurdart.TimelineGetPublicPlurks(
      userId: userId,
      offset: offset,
      favorersDetail: true,
      limitedDetail: true,
      replurkersDetail: true,
    ));

    //Collect Users to global User Cache.
    if (plurks != null) {
      plurks.plurkUsers.forEach((key, value) {
        // print('TimelineGetPlurksCubit addToUserCache: ' + value.id.toString());
        Static.addToUserCache(value);
      });
    }

    emit(TimelineGetPublicTrackBackSuccess(plurks));
  }

}
