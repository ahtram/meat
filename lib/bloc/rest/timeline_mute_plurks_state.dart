part of 'timeline_mute_plurks_cubit.dart';

@immutable
abstract class TimelineMutePlurksState {}

class TimelineMutePlurksInitial extends TimelineMutePlurksState {}

class TimelineMutePlurksSuccess extends TimelineMutePlurksState {}
class TimelineMutePlurksFailed extends TimelineMutePlurksState {}

class TimelineUnMutePlurksSuccess extends TimelineMutePlurksState {}
class TimelineUnMutePlurksFailed extends TimelineMutePlurksState {}