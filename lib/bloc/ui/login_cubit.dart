import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());

  success(Plurdart.Credentials credentials) {
    emit(LoginSuccess(credentials));
  }

  failed() {
    emit(LoginFailed());
  }
}
