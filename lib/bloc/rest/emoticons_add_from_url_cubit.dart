import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'emoticons_add_from_url_state.dart';

class EmoticonsAddFromUrlCubit extends Cubit<EmoticonsAddFromUrlState> {
  EmoticonsAddFromUrlCubit() : super(EmoticonsAddFromUrlInitial());

  request(String url, {String keyword}) async {
    Plurdart.EmoticonAdded emoticonAdded = await Plurdart.emoticonsAddFromUrl(Plurdart.EmoticonsAddFromUrl(
      url: url,
      keyword: keyword,
    ));

    if (emoticonAdded != null && !emoticonAdded.hasError()) {
      emit(EmoticonsAddFromUrlSuccess(emoticonAdded.keyword));
    } else {
      emit(EmoticonsAddFromUrlFailed(emoticonAdded.errorText));
    }
  }
}
