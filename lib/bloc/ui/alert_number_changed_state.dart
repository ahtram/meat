part of 'alert_number_changed_cubit.dart';

@immutable
abstract class AlertNumberChangedState {}

class AlertNumberChangedInitial extends AlertNumberChangedState {}

class AlertNumberChangedSuccess extends AlertNumberChangedState {}
