import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_plurk_edit_state.dart';

class TimelinePlurkEditCubit extends Cubit<TimelinePlurkEditState> {
  TimelinePlurkEditCubit() : super(TimelinePlurkEditInitial());

  post(Plurdart.TimelinePlurkEdit timelinePlurkEdit) async {
    Plurdart.Plurk plurk = await Plurdart.timelinePlurkEdit(timelinePlurkEdit);

    //Check post success.
    if (plurk != null) {
      if (!plurk.hasError()) {

        // print('TimelinePlurkAddCubit: ' + prettyJson(plurk.toJson()));

        emit(TimelinePlurkEditSuccess(plurk));
      } else {
        emit(TimelinePlurkEditFailed(plurk.errorText));
      }
    } else {
      emit(TimelinePlurkEditFailed('Unknown error'));
    }
  }
}
