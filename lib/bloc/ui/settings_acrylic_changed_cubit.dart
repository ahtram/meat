import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'settings_acrylic_changed_state.dart';

class SettingsAcrylicChangedCubit extends Cubit<SettingsAcrylicChangedState> {
  SettingsAcrylicChangedCubit() : super(SettingsAcrylicChangedInitial());

  success() {
    emit(SettingsAcrylicChangedSuccess());
  }
}
