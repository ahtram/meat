import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_mute_plurks_state.dart';

//[Dep]: PlurkCardActionBar will do the API directly.
class TimelineMutePlurksCubit extends Cubit<TimelineMutePlurksState> {
  TimelineMutePlurksCubit() : super(TimelineMutePlurksInitial());

  void mutePlurk(int plurkID) async {
    Plurdart.Base responseBase = await Plurdart.timelineMutePlurks(Plurdart.PlurkIDs(ids: [plurkID]));
    if (responseBase != null) {
      if(responseBase.hasSuccess()) {
        emit(TimelineMutePlurksSuccess());
      } else {
        emit(TimelineMutePlurksFailed());
      }
    } else {
      emit(TimelineMutePlurksFailed());
    }
  }

  void unmutePlurk(int plurkID) async {
    Plurdart.Base responseBase = await Plurdart.timelineUnmutePlurks(Plurdart.PlurkIDs(ids: [plurkID]));
    if (responseBase != null) {
      if(responseBase.hasSuccess()) {
        emit(TimelineUnMutePlurksSuccess());
      } else {
        emit(TimelineUnMutePlurksFailed());
      }
    } else {
      emit(TimelineUnMutePlurksFailed());
    }
  }
}
