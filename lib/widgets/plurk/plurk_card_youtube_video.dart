import 'package:flutter/material.dart';
import 'package:meat/bloc/ui/youtube_fullscreen_cubit.dart';
import 'package:meat/screens/youtube_fullscreen_viewer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/widgets/youtube_video_player/youtube_video_player.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/theme/app_themes.dart';

class PlurkCardYoutubeVideo extends StatefulWidget {
  PlurkCardYoutubeVideo(this.youtubeVideoLinks);

  final List<PlurkCardYoutubeVideoLink> youtubeVideoLinks;

  @override
  _PlurkCardYoutubeVideoState createState() => _PlurkCardYoutubeVideoState();
}

class _PlurkCardYoutubeVideoState extends State<PlurkCardYoutubeVideo> {
  final PageController _youtubePageIndicatorController = PageController();

  List<GlobalKey> _youtubePlayerKeys = [];

  //In milliseconds.
  int _playingPosition = 0;

  @override
  Widget build(BuildContext context) {
    //Make sure we have enough global keys for youtube players.
    for (int i = 0; i < widget.youtubeVideoLinks.length; ++i) {
      if (_youtubePlayerKeys.length < widget.youtubeVideoLinks.length) {
        _youtubePlayerKeys.add(GlobalKey());
      }
    }

    if (widget.youtubeVideoLinks.length == 1) {
      // Only 1 video link. So we don't need the CarouselSlider.
      //Test just build one Youtube video.
      return _buildYoutubePlayer(
          context, widget.youtubeVideoLinks[0], _youtubePlayerKeys[0]);
    } else {
      return _buildYoutubePlayersView(
          context, widget.youtubeVideoLinks, _youtubePageIndicatorController);
    }
  }

  Widget _buildYoutubePlayer(BuildContext context,
      PlurkCardYoutubeVideoLink youtubeVideoLink, GlobalKey key) {
    return YoutubeVideoPlayer(
      youtubeVideoLink.youtubeVideoID,
      key: key,
      isInFullScreenSize: false,
      onFullScreenTap: () {
        //Try enter fullscreen player.
        // print('onEnterFullScreen [' + youtubeVideoLink.youtubeVideoID + ']');
        //We may need a cubit for the fullscreen player event.
        //After all its Home who should handle the fullscreen playing. Not this card.
        context
            .read<YoutubeFullscreenCubit>()
            .enter(YoutubeFullscreenViewerArgs(
              youtubeVideoLink.youtubeVideoID,
              _playingPosition,
              true,
              key.currentState,
            ));
      },
      onChanged: (value) {
        _playingPosition = value;
      },
    );

    //Bye bye.
//     return YoutubePlayerBuilder(
//       player: YoutubePlayer(
//         controller: youtubeVideoLink.controller,
//         showVideoProgressIndicator: true,
//         actionsPadding: EdgeInsets.all(16),
//         progressIndicatorColor: Theme.of(context).accentColor,
//         bottomActions: [
//           CurrentPosition(
//             controller: youtubeVideoLink.controller,
//           ),
//           SizedBox(
//             width: 8,
//           ),
//           ProgressBar(
//             controller: youtubeVideoLink.controller,
//             isExpanded: true,
//           ),
//           SizedBox(
//             width: 8,
//           ),
//           //It looks like we'll need
//           GestureDetector(
//             child: Container(
//               child: Icon(
//                 Icons.fullscreen,
//               ),
//             ),
//             onTap: () {
//               youtubeVideoLink.controller.pause();
//               //Stop the video playing and enter full screen.
//               // This stupid thing could be called multiple times. Not my fault.
//               print('onEnterFullScreen [' + youtubeVideoLink.youtubeVideoID + ']');
//               //We may need a cubit for the fullscreen player event.
//               //After all its Home who should handle the fullscreen playing. Not this card.
//               context
//                   .read<YoutubeFullscreenCubit>()
//                   .enter(YoutubeFullscreenViewerArgs(
//                     youtubeVideoLink.youtubeVideoID,
//                     youtubeVideoLink.controller.value.position?.inSeconds ?? 0,
//                   ));
//             },
//           )
//         ],
//       ),
//       builder: (context, player) {
//
//         return Container(
//           child: player,
//         );
//       },
// //      onEnterFullScreen: () {
// //        // This stupid thing could be called multiple times. Not my fault.
// //        print('onEnterFullScreen [' + youtubeVideoLink.youtubeVideoID + ']');
// //      },
// //      onExitFullScreen: () {
// //        // This stupid thing could be called multiple times. Not my fault.
// //        print('onExitFullScreen [' + youtubeVideoLink.youtubeVideoID + ']');
// ////        context.read<YoutubeFullscreenCubit>().leave();
// //      },
//     );
  }

  //Multiple youtube players.
  Widget _buildYoutubePlayersView(
      BuildContext context,
      List<PlurkCardYoutubeVideoLink> youtubeVideoLink,
      PageController youtubePageIndicatorController) {
    Color indicatorColor = Theme.of(context).primaryColor;
    if (Static.settingsAppTheme == AppTheme.Pure) {
      //Force the Pure theme indicator color to white color. The black color looks suck.
      indicatorColor = Theme.of(context).textTheme.bodyText1.color;
    }

    return Container(
      decoration: BoxDecoration(
          border: Border.all(
        width: 0.5,
        color: Theme.of(context).hintColor,
      )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // PageView constraints.
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Container(
                // Calculate the container height from youtube video aspect ratio.
                height: (constraints.maxWidth * 9.0 / 16.0),
                child: PageView.builder(
                  controller: youtubePageIndicatorController,
                  itemCount: youtubeVideoLink.length,
                  // This will prevent from pre allocate YoutubePlayerBuilder.
                  itemBuilder: (BuildContext context, int index) {
                    return _buildYoutubePlayer(context, youtubeVideoLink[index],
                        _youtubePlayerKeys[index]);
                  },
                ),
              );
            },
          ),

          // Smooth Page Indicator.
          Container(
            padding: EdgeInsets.all(8),
            child: SmoothPageIndicator(
              controller: youtubePageIndicatorController,
              count: youtubeVideoLink.length,
              effect: WormEffect(
                activeDotColor: indicatorColor,
              ),
              onDotClicked: (index) {
                if (youtubePageIndicatorController.hasClients) {
                  youtubePageIndicatorController.jumpToPage(index);
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
