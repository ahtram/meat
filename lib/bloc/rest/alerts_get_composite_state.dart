part of 'alerts_get_composite_cubit.dart';

@immutable
abstract class AlertsGetCompositeState {}

class AlertsGetCompositeInitial extends AlertsGetCompositeState {}

class AlertsGetCompositeSuccess extends AlertsGetCompositeState {
  AlertsGetCompositeSuccess(this.alerts);
  final List<Plurdart.Alert> alerts;
}

class AlertsGetCompositeFailed extends AlertsGetCompositeState {

}
