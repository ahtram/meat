import 'package:after_layout/after_layout.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/ui/request_indicator_cubit.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/other/bottom_search_bar.dart';
import 'package:meat/widgets/user/users_viewer.dart';
import 'package:meat/system/static_stuffs.dart' as Static;

class FriendsAndFans extends StatefulWidget {
  FriendsAndFans({Key key}) : super(key: key);

  @override
  _FriendsAndFansState createState() => _FriendsAndFansState();
}

class _FriendsAndFansState extends State<FriendsAndFans> with AfterLayoutMixin, SingleTickerProviderStateMixin {

  BuildContext scaffoldBodyContext;
  TabController _tabController;

  GlobalKey<BottomSearchBarState> _bottomInputBarKey = GlobalKey<BottomSearchBarState>();

  GlobalKey<UsersViewerState> _friendsViewerKey = GlobalKey<UsersViewerState>();
  GlobalKey<UsersViewerState> _followingViewerKey = GlobalKey<UsersViewerState>();
  GlobalKey<UsersViewerState> _fansViewerKey = GlobalKey<UsersViewerState>();

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 3);
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (!mounted) return;

    // _tabController = TabController(vsync: this, length: 3);
    // _tabController.addListener(() {
    //   //This is a hack to block tab bar from switching when refreshing.
    //   if (isRefreshing()) {
    //     int index = _tabController.previousIndex;
    //     setState(() {
    //       _tabController.index = index;
    //     });
    //   }
    // });

    _tabController.addListener((){
      //When ever tab changed
      _bottomInputBarKey.currentState.clear();
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  //Is someone refreshing?
  // bool isRefreshing() {
  //   if (_friendsViewerKey.currentState != null) {
  //     return _friendsViewerKey.currentState.isRefreshing();
  //   }
  //   if (_followingViewerKey.currentState != null) {
  //     return _followingViewerKey.currentState.isRefreshing();
  //   }
  //   if (_fansViewerKey.currentState != null) {
  //     return _fansViewerKey.currentState.isRefreshing();
  //   }
  //   return false;
  // }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<RequestIndicatorCubit>(
          create: (BuildContext context) => RequestIndicatorCubit(),
        ),
      ],
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            appBar: AppBar(
              centerTitle: true,
              brightness: Theme.of(context).brightness,
              backgroundColor: Colors.transparent,
              flexibleSpace: AcrylicContainer(
                Container(),
                backgroundColor: Theme.of(context).primaryColor,
                transparentBackLayer: false,
                mode: AcrylicContainerMode.BackdropFilter,
              ),
              elevation: 0,
              titleSpacing: 0.0,
              leading: AppBarActionButton(Icons.arrow_back, onTap: () {
                Navigator.pop(context);
              }),
              automaticallyImplyLeading: false,
              // actions: [
              //   AppBarActionButton(Icons.close, onTap: () {
              //     Navigator.pop(context);
              //   }),
              //   SizedBox(
              //     width: 2,
              //   )
              // ],
              title: TabBar(
                controller: _tabController,
                tabs: [
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'myFriendsFriends'),
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'myFriendsFollowing'),
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'myFriendsFans'),
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButton: BlocBuilder<RequestIndicatorCubit,
                RequestIndicatorState>(
              builder: (context, state) {
                //Check state.
                List<Widget> columnChildren = [];

                //Only when showing request indicator.
                if (state is RequestIndicatorShow) {
                  //Busy indicator.
                  columnChildren.add(Container(
                    width: 56,
                    height: 56,
                    child: FlareActor(
                      "assets/rive/MeatLoader.flr",
                      animation: "Untitled",
                    ),
                  ));
                }

                //Bottom margin.
                columnChildren.add(SizedBox(
                  height: MediaQuery.of(context).viewInsets.bottom,
                ));

                return Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: columnChildren,
                );
              },
            ),
            body: AcrylicContainer(
              Column(
                children: [
                  Expanded(
                    child: Container(
                      child: TabBarView(
                        // physics: NeverScrollableScrollPhysics(),
                        controller: _tabController,
                        children: [
                          //Friends viewer
                          UsersViewer(UsersViewerArgs(
                            'Friends',
                            UsersBrowsingType.Friends,
                            userId: Static.meId,
                            isToggleList: false,
                            definitelyIncludeSelf: false,
                          ),
                          key: _friendsViewerKey,),

                          //Following viewer
                          UsersViewer(UsersViewerArgs(
                            'Following',
                            UsersBrowsingType.Following,
                            userId: Static.meId,
                            isToggleList: false,
                            definitelyIncludeSelf: false,
                          ),
                          key: _followingViewerKey,),

                          //Fans viewer
                          UsersViewer(UsersViewerArgs(
                            'Fans',
                            UsersBrowsingType.Fans,
                            userId: Static.meId,
                            isToggleList: false,
                            definitelyIncludeSelf: false,
                          ),
                          key: _fansViewerKey,),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
              initialImage: AssetImage('assets/images/background001.jpg'),
            ),
          bottomNavigationBar: BottomSearchBar(
            onSearchChanged: _onSearchTermChanged,
            withSearchButton: false,
            key: _bottomInputBarKey,
          ),
        ),
      ),
    );
  }

  _onSearchTermChanged(String term) {
    //Search User
    if (_tabController.index == 0) {
      //Search friends
      _friendsViewerKey.currentState?.setFilterTerm(term);
    } else if (_tabController.index == 1) {
      //Search following
      _followingViewerKey.currentState?.setFilterTerm(term);
    } else if (_tabController.index == 2) {
      //Search fans
      _fansViewerKey.currentState?.setFilterTerm(term);
    }
  }

}