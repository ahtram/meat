import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:intl/intl.dart';
import 'package:meat/bloc/rest/blocks_block_cubit.dart';
import 'package:meat/bloc/rest/blocks_unblock_cubit.dart';
import 'package:meat/bloc/rest/users_me_cubit.dart';
import 'package:meat/bloc/ui/image_links_viewer_cubit.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/bloc/ui/report_abuse_dialog_cubit.dart';
import 'package:meat/bloc/ui/users_browser_cubit.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:meat/screens/image_links_viewer.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/plurk_card_link_stack.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:pretty_json/pretty_json.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strings/strings.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:share_plus/share_plus.dart';
import 'package:meat/screens/plurk_poster.dart' as PlurkPoster;
import 'package:meat/widgets/user/users_viewer.dart' as UsersViewer;
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:after_layout/after_layout.dart';

class UserCard extends StatefulWidget {
  UserCard(this.profile, {Key key}) : super(key: key);

  //We need profile to check the friend status. Not User modal.
  final Plurdart.Profile profile;

  @override
  _UserCardState createState() => _UserCardState();
}

enum UserCardButtonMenuOption {
  Share,
  Edit, //If this is my profile.
  ReportAbuse,
  BlockUser,
  UnBlockUser,
  BlockList,
}

//These options are used by Friend button drop down items.
enum FriendButtonMenuOption {
  AddAsFriend,
  AddAsFan,
  DoNotAdd,
  CancelFriendshipRequest,
  Unfriend,
}

enum FanButtonMenuOption {
  Unfollow,
  UnfollowReplurk,
  FollowReplurk,
}

class _UserCardState extends State<UserCard>
    with PlurkContentParts, AfterLayoutMixin {

  BuildContext _blocProviderContext;
  GlobalKey _acrylicContainerKey = GlobalKey<AcrylicContainerState>();

  GlobalKey<PopupMenuButtonState> _popupMenuKey =
      GlobalKey<PopupMenuButtonState>();

  @override
  void afterFirstLayout(BuildContext context) {

  }

  @override
  Widget build(BuildContext context) {

    // Pre process all display elements.
    // print('[Build about (raw)]: ' + widget.user.about);
    // print('[Build about renderer (html)]: ' + widget.user.aboutRenderred);
    preProcess(context, widget.profile.userInfo.aboutRenderred, false);

    return MultiBlocListener(
      listeners: [
        BlocListener<BlocksBlockCubit, BlocksBlockState>(
          listener: (context, state) {
            if (state is BlocksBlockSuccess) {
              _showFlushBar(_blocProviderContext, Icons.check_circle,
                  'Success!', 'User blocked.');

              setState(() {
                widget.profile.blockedByMe = true;
              });
            } else if (state is BlocksBlockFailed) {
              _showFlushBar(_blocProviderContext, Icons.warning, 'Oops!',
                  'Cannot block user.');
            }
          },
        ),
        BlocListener<BlocksUnblockCubit, BlocksUnblockState>(
          listener: (context, state) {
            if (state is BlocksUnblockSuccess) {
              _showFlushBar(_blocProviderContext, Icons.check_circle,
                  'Success!', 'User unblocked.');
              setState(() {
                widget.profile.blockedByMe = false;
              });
            } else if (state is BlocksUnblockFailed) {
              _showFlushBar(_blocProviderContext, Icons.warning, 'Oops!',
                  'Cannot unblock user.');
            }
          },
        ),
      ],
      child: Column(
        children: [
          AcrylicContainer(
            Builder(
              builder: (builderContext) {
                //Save the context because we need it for switching background.
//                  print('update _blocProviderContext');
                _blocProviderContext = builderContext;

                List<Widget> columnChildren = [];

                //Right button
                //This is the best solution.
                columnChildren.add(Row(
                  children: [
                    Expanded(
                      child: Container(),
                    ),
                    PopupMenuButton(
                        key: _popupMenuKey,
                        child: SizedBox(
                          width: 36,
                          height: 36,
                          child: Icon(
                            Icons.expand_more,
                            size: 24,
                            color: Theme.of(context).iconTheme.color,
                          ),
                        ),
                        onSelected: (result) {
                          //These are very plain actions so we'll just do it here...
                          switch (result) {
                            case UserCardButtonMenuOption.Share:
                              String link = Define.plurkUserPath +
                                  widget.profile.userInfo.nickName;
                              Share.share(link);
                              break;
                            case UserCardButtonMenuOption.Edit:
                              //Todo: profile editor
                              break;
                            case UserCardButtonMenuOption.ReportAbuse:
                              //Open report abuse. (Not sure if the API is suit for report user)
                              context
                                  .read<ReportAbuseDialogCubit>()
                                  .reportPlurk(widget.profile.userInfo.id);
                              break;
                            case UserCardButtonMenuOption.BlockUser:
                              _blocProviderContext
                                  .read<BlocksBlockCubit>()
                                  .request(widget.profile.userInfo.id);
                              break;
                            case UserCardButtonMenuOption.UnBlockUser:
                              _blocProviderContext
                                  .read<BlocksUnblockCubit>()
                                  .request(widget.profile.userInfo.id);
                              break;
                            case UserCardButtonMenuOption.BlockList:
                              //Open users browser.
                              _blocProviderContext
                                  .read<UsersBrowserCubit>()
                                  .open(UsersViewer.UsersViewerArgs(
                                    FlutterI18n.translate(
                                        context, 'titleBlockList'),
                                    UsersViewer.UsersBrowsingType.Blocks,
                                    isToggleList: false,
                                  ));
                              break;
                          }
                        },
                        itemBuilder: (BuildContext context) {
                          List<PopupMenuEntry> returnWidgets = [];

                          //Share profile link
                          returnWidgets.add(menuItem(
                              UserCardButtonMenuOption.Share,
                              Icons.share,
                              FlutterI18n.translate(
                                  context, 'dropdownShareProfile')));

                          //Todo: Open editor if this is my profile.
                          // if (widget.profile.userInfo.id == Static.meId) {
                          //   returnWidgets.add(menuItem(
                          //       UserCardButtonMenuOption.Edit,
                          //       Icons.edit,
                          //       'Edit profile'));
                          // }

                          //Report abuse? (only when this is not my profile)
                          if (widget.profile.userInfo.id != Static.meId) {
                            returnWidgets.add(menuItem(
                                UserCardButtonMenuOption.ReportAbuse,
                                MdiIcons.alertOctagramOutline,
                                FlutterI18n.translate(
                                    context, 'dropdownReportAbuse')));
                          }

                          //Block User / Un Block User? (only when this is not my profile)
                          if (widget.profile.userInfo.id != Static.meId) {
                            if (!widget.profile.blockedByMe) {
                              //Block
                              returnWidgets.add(menuItem(
                                  UserCardButtonMenuOption.BlockUser,
                                  MdiIcons.minusCircleOutline,
                                  FlutterI18n.translate(
                                      context, 'dropdownBlockUser')));
                            } else {
                              //Un Block
                              returnWidgets.add(menuItem(
                                  UserCardButtonMenuOption.UnBlockUser,
                                  MdiIcons.plusCircleOutline,
                                  FlutterI18n.translate(
                                      context, 'dropdownUnblockUser')));
                            }
                          }

                          //Block List? (only when this is my profile)
                          if (widget.profile.userInfo.id == Static.meId) {
                            returnWidgets.add(menuItem(
                                UserCardButtonMenuOption.BlockList,
                                MdiIcons.alertOctagonOutline,
                                FlutterI18n.translate(
                                    context, 'titleBlockList')));
                          }

                          return returnWidgets;
                        }),
                  ],
                ));

                //Plurk coin icon
                Widget plurkCoinIcon = Container();
                if (widget.profile.userInfo.premium) {
                  plurkCoinIcon = Image.asset(
                    'assets/images/plurk_coin.png',
                    width: 36,
                    height: 36,
                  );
                }

                columnChildren.add(
                  Container(
                    width: 160,
                    height: 160,
                    child: Stack(
                      children: [
                        Container(
                          width: 160,
                          height: 160,
                          child: CircleAvatar(
                            backgroundColor: Colors.transparent,
                            backgroundImage: NetworkImage(
                                widget.profile.userInfo.bigAvatarUrl()),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: plurkCoinIcon
                        )
                      ],
                    ),
                  ),
                );

                columnChildren.add(SizedBox(
                  height: 24,
                ));

                //Color display name.
                columnChildren.add(
                    PlurkCardPrimeHeaderState.buildUserDisplayName(
                        context, null, widget.profile.userInfo, false, (user) {
                  //Anything?
                }, fontSize: 18));

                columnChildren.add(SizedBox(
                  height: 8,
                ));

                //Karma
                columnChildren.add(Text(
                  'Karma ' + widget.profile.userInfo.karma.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ));

                columnChildren.add(SizedBox(
                  height: 8,
                ));

                // //Plurk Coin?
                // if (_premiumStatus != null) {
                //
                // }
                // columnChildren.add(Text(
                //   'Karma ' + widget.profile.userInfo.karma.toString(),
                //   style: TextStyle(fontWeight: FontWeight.bold),
                // ));
                //
                // columnChildren.add(SizedBox(
                //   height: 8,
                // ));

                //If this user is blocked by me. Do not show any content.
                if (widget.profile.blockedByMe != null &&
                    widget.profile.blockedByMe) {
                  //Display a warning message.
                  columnChildren.add(TextButton.icon(
                      icon: Icon(
                        MdiIcons.alertBox,
                        color: Colors.red,
                        size: 28,
                      ),
                      label: Text(
                        'You have blocked this user.',
                        style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      onPressed: () {
                        //Do we need anything?
                      }));
                } else {
                  //Complete content of this user.
                  //Enable send private plurk button??
                  columnChildren.add(BlocBuilder<UsersMeCubit, UsersMeState>(
                    builder: (context, state) {
                      //Has meId and this is not me.
                      if (Static.meId != null &&
                          widget.profile.userInfo.id != Static.meId) {
                        //Private plurk button
                        return OutlinedButton.icon(
                          style: ButtonStyle(
                            foregroundColor: MaterialStateProperty.all(
                                Theme.of(context).textTheme.bodyText1.color),
                          ),
                          icon: Icon(MdiIcons.messageBulleted),
                          label: Text('Send private plurk',
                              overflow: TextOverflow.ellipsis),
                          onPressed: () async {
                            //Open Plurk Poster for private plurk.
                            PlurkPoster.show(
                                context,
                                PlurkPoster.PlurkPosterArgs(
                                  PlurkPoster.PlurkPosterMode.Private,
                                  privatePlurkUser: widget.profile.userInfo,
                                ));
                          },
                        );
                      } else {
                        //Empty container to hide the private plurk button.
                        return Container();
                      }
                    },
                  ));

                  //Friends and fans.
                  columnChildren.add(_buildFriendsAndFansPart());

                  columnChildren.add(Divider(
                    height: 1,
                    thickness: 1,
                  ));

                  columnChildren.add(SizedBox(
                    height: 8,
                  ));

                  //== Text Content == (Maybe empty)
                  Widget textContentPart = buildTextContentPart(textContent);
                  if (textContentPart != null) {
                    columnChildren.add(textContentPart);
                  }

                  //== Hash tags == (Maybe empty)
                  Widget hashTagsPart = buildHashTagsPart(hashTags);
                  if (hashTagsPart != null) {
                    columnChildren.add(hashTagsPart);
                  }

                  //== Youtube Video Content == (Maybe empty)
                  Widget youtubeVideoPart =
                      buildYoutubeVideoPart(youtubeVideoLinks);
                  if (youtubeVideoPart != null) {
                    columnChildren.add(youtubeVideoPart);
                  }

                  //== Image Collection Content == (Maybe empty)
                  Widget imageCollectionPart = buildImageCollectionPartByType(
                      plurkImageLinks, (heroTag) {
                    //Get the initial index
                    int initialIndex = 0;
                    for (int i = 0; i < plurkImageLinks.length; ++i) {
                      if (plurkImageLinks[i].getHeroTag() == heroTag) {
                        initialIndex = i;
                        break;
                      }
                    }

                    _blocProviderContext
                        .read<ImageLinksViewerCubit>()
                        .enter(ImageLinksViewerArgs(
                          plurkImageLinks,
                          initialIndex,
                        ));
                  }, () {
                    //Anything?
                  }, ImageCollectionType.ExpandAll);

                  if (imageCollectionPart != null) {
                    columnChildren.add(imageCollectionPart);
                  }

                  //== Link Stack Content == (Maybe empty)
                  Widget linkStackPart = buildLinkStackPartByType(
                      urlLinks, LinkStackType.All, (uri) {
                    BlocProvider.of<OpenUriCubit>(context).request(uri);
                  });
                  if (linkStackPart != null) {
                    columnChildren.add(linkStackPart);
                  }

                  columnChildren.add(SizedBox(
                    height: 12,
                  ));

                  columnChildren.add(Divider(
                    height: 1,
                    thickness: 1,
                  ));

                  columnChildren.add(SizedBox(
                    height: 4,
                  ));

                  //Many misc infos.
                  columnChildren.add(_miscItem(
                      MdiIcons.cardText,
                      widget.profile.userInfo.plurksCount.toString() +
                          ' Plurks'));

                  columnChildren.add(_miscItem(
                      MdiIcons.messageTextOutline,
                      widget.profile.userInfo.responseCount.toString() +
                          ' Responses'));

                  if (widget.profile.userInfo.joinDateTime() != null) {
                    columnChildren.add(_miscItem(
                        MdiIcons.calendarCheck,
                        'Join at ' +
                            DateFormat('yyyy-MM-dd').format(
                                widget.profile.userInfo.joinDateTime())));
                  }

                  if (widget.profile.userInfo.location != null &&
                      widget.profile.userInfo.location.isEmpty == false) {
                    columnChildren.add(_miscItem(
                        Icons.location_on, widget.profile.userInfo.location));
                  }

                  if (widget.profile.userInfo.birthDateTime() != null) {
                    columnChildren.add(_miscItem(
                        MdiIcons.cake,
                        'Birthday ' +
                            DateFormat('yyyy-MM-dd').format(
                                widget.profile.userInfo.birthDateTime())));
                  }

                  columnChildren.add(_miscItem(
                      MdiIcons.genderMaleFemale,
                      widget.profile.userInfo.genderStr()));

                  columnChildren.add(_miscItem(
                      Icons.favorite,
                      capitalize(widget.profile.userInfo.relationship)
                          .replaceAll('_', ' ')));
                }

                //I don't know how to display badges...

                return InkWell(
                  child: Container(
                    padding: EdgeInsets.all(12),
                    child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: columnChildren,
                      ),
                    ),
                  ),
                  onTap: () {
                    //Anything?
                  },
                  onLongPress: () {
                    //Show button menu?
                    // _primeHeaderKey.currentState?.showButtonMenu();
                    // widget.onBodyLongPress?.call();
                  },
                );
              },
            ),
            initialImage: MemoryImage(kTransparentImage),
            key: _acrylicContainerKey,
            mode: AcrylicContainerMode.None,
          ),
          Divider(
            height: 1,
            thickness: 1,
          ),
        ],
      ),
    );
  }

  //Check if the user is me
  bool isSelfCard() {
    if (widget.profile.userInfo.id == Static.meId) {
      return true;
    }
    return false;
  }

  //For this one we well check the state with Static.me/
  Widget _buildFriendsAndFansPart() {
    //Friends
    List<Widget> friendsColumnChildren = [];

    if (!isSelfCard()) {
      // friendsColumnChildren.add(SizedBox(
      //   height: 4,
      // ));

      //Check if this guy is a friend of mine.
      if (widget.profile.areFriends) {
        //This is my friend. (or friendStatus == 1) [Friend] (grey)
        //1. 'Unfriend' (FriendsFans/removeAsFriend) (set -1)
        friendsColumnChildren.add(PopupMenuButton(
          padding: EdgeInsets.all(0),
          icon: Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            height: 36,
            color: Colors.grey.withOpacity(0.2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(MdiIcons.accountCheck),
                SizedBox(
                  width: 4,
                ),
                Padding(
                    padding: EdgeInsets.only(bottom: 2),
                    child: Text('Friend', overflow: TextOverflow.ellipsis))
              ],
            ),
          ),
          itemBuilder: (BuildContext context) {
            List<PopupMenuEntry> returnWidgets = [];
            //Add as my friend
            returnWidgets.add(menuItem(
                FriendButtonMenuOption.Unfriend, Icons.cancel, 'Unfriend'));
            return returnWidgets;
          },
          onSelected: (result) async {
            if (result == FriendButtonMenuOption.Unfriend) {
              //Confirm dialog.
              AwesomeDialog(
                  context: context,
                  dialogType: DialogType.WARNING,
                  animType: AnimType.SCALE,
                  headerAnimationLoop: false,
                  title: 'Unfriend!',
                  desc: 'Are you sure?',
                  btnOkText: 'Yeah',
                  btnCancelText: 'Forget It',
                  btnOkOnPress: () async {
                    Plurdart.Base base =
                        await Plurdart.friendsFansRemoveAsFriend(
                            Plurdart.FriendsFansBecomeRemoveAsFriend(
                      friendId: widget.profile.userInfo.id,
                    ));

                    if (base != null && !base.hasError()) {
                      //Success.
                      setState(() {
                        widget.profile.friendStatus = -1;
                        widget.profile.friendsCount -= 1;
                        widget.profile.userInfo.friendsCount -= 1;
                        widget.profile.areFriends = false;
                      });
                    }
                  },
                  btnCancelOnPress: () {})
                ..show();
            }
          },
        ));
      } else {
        if (widget.profile.friendStatus == 0) {
          //[Request sent] (color)
          //1. 'Cancel friendship request' (Alerts/removeNotification) (set -1)
          friendsColumnChildren.add(PopupMenuButton(
            padding: EdgeInsets.all(0),
            icon: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              height: 36,
              color: Theme.of(context).primaryColor.withOpacity(0.4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(MdiIcons.accountArrowRight),
                  SizedBox(
                    width: 4,
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 2),
                      child:
                          Text('Request sent', overflow: TextOverflow.ellipsis))
                ],
              ),
            ),
            itemBuilder: (BuildContext context) {
              List<PopupMenuEntry> returnWidgets = [];
              //Cancel friendship request
              returnWidgets.add(menuItem(
                  FriendButtonMenuOption.CancelFriendshipRequest,
                  Icons.cancel,
                  'Cancel request'));
              return returnWidgets;
            },
            onSelected: (result) async {
              if (result == FriendButtonMenuOption.CancelFriendshipRequest) {
                Plurdart.Base base =
                    await Plurdart.alertsRemoveNotification(Plurdart.UserID(
                  userId: widget.profile.userInfo.id,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.friendStatus = -1;
                  });
                }
              }
            },
          ));
        } else if (widget.profile.friendStatus == -1) {
          //Nothing between two of you. (grey)
          //1. [Add as friend] (FriendsFans/becomeFriend) (set 0)
          friendsColumnChildren.add(SizedBox(
            child: OutlinedButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(
                    Theme.of(context).textTheme.bodyText1.color),
              ),
              icon: Icon(MdiIcons.accountPlus),
              label: Text('Befriend', overflow: TextOverflow.ellipsis),
              onPressed: () async {
                //Plurdart request.
                Plurdart.Base base = await Plurdart.friendsFansBecomeFriend(
                    Plurdart.FriendsFansBecomeRemoveAsFriend(
                        friendId: widget.profile.userInfo.id));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.friendStatus = 0;
                  });
                }
              },
            ),
          ));
        } else if (widget.profile.friendStatus == 3) {
          //I've been request to become friend by this guy. [Respond request] (color)
          //1. 'Add as my friend' (Alert/addAsFriend) (set 1)
          //2. 'Add as my fan' (Alert/addAsFan) (set -1)
          //3. 'Don't add' (Alert/denyFriendship) (set -1)
          friendsColumnChildren.add(PopupMenuButton(
            padding: EdgeInsets.all(0),
            icon: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              height: 36,
              color: Theme.of(context).primaryColor.withOpacity(0.4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Transform.translate(
                      offset: Offset(-10, 0),
                      child: Icon(
                          MdiIcons.accountArrowLeftOutline)),
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 2),
                      child:
                          Text('Got request', overflow: TextOverflow.ellipsis))
                ],
              ),
            ),
            itemBuilder: (BuildContext context) {
              List<PopupMenuEntry> returnWidgets = [];
              //Add as my friend
              returnWidgets.add(menuItem(FriendButtonMenuOption.AddAsFriend,
                  MdiIcons.accountArrowLeft, 'Add as friend'));
              returnWidgets.add(menuItem(FriendButtonMenuOption.AddAsFan,
                  MdiIcons.accountHeart, 'Add as fan'));
              returnWidgets.add(menuItem(
                  FriendButtonMenuOption.DoNotAdd, Icons.cancel, 'Don\'t add'));
              return returnWidgets;
            },
            onSelected: (result) async {
              if (result == FriendButtonMenuOption.AddAsFriend) {
                Plurdart.Base base =
                    await Plurdart.alertsAddAsFriend(Plurdart.UserID(
                  userId: widget.profile.userInfo.id,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.friendStatus = 1;
                    widget.profile.friendsCount += 1;
                    widget.profile.userInfo.friendsCount += 1;
                    widget.profile.areFriends = true;
                  });
                }
              } else if (result == FriendButtonMenuOption.AddAsFan) {
                Plurdart.Base base =
                    await Plurdart.alertsAddAsFan(Plurdart.UserID(
                  userId: widget.profile.userInfo.id,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.friendStatus = -1;
                  });
                }
              } else if (result == FriendButtonMenuOption.DoNotAdd) {
                Plurdart.Base base =
                    await Plurdart.alertsDenyFriendship(Plurdart.UserID(
                  userId: widget.profile.userInfo.id,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.friendStatus = -1;
                  });
                }
              }
            },
          ));
        }
      }
    }

    //Friend list button.
    friendsColumnChildren.add(OutlinedButton(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all(
            Theme.of(context).textTheme.bodyText1.color),
      ),
      child: Text(
        widget.profile.userInfo.friendsCount.toString() + ' Friends',
        textAlign: TextAlign.center,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () {
        //Open users browser.
        _blocProviderContext
            .read<UsersBrowserCubit>()
            .open(UsersViewer.UsersViewerArgs(
              'Friends (' +
                  widget.profile.userInfo.friendsCount.toString() +
                  ')',
              UsersViewer.UsersBrowsingType.Friends,
              userId: widget.profile.userInfo.id,
              isToggleList: false,
            ));
      },
    ));

    //Fans
    List<Widget> fansColumnChildren = [];

    if (!isSelfCard()) {
      // fansColumnChildren.add(SizedBox(
      //   height: 4,
      // ));

      //Check fans status
      if (widget.profile.isFollowing) {
        if (widget.profile.isFollowingReplurk) {
          //I am his fan. [Following] (grey)
          //1. 'Unfollow' FriendsFans/setFollowing false
          //2. 'Unfollow replurk' FriendsFans/setFollowingReplurk false
          fansColumnChildren.add(PopupMenuButton(
            padding: EdgeInsets.all(0),
            icon: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              height: 36,
              color: Colors.grey.withOpacity(0.2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(MdiIcons.eyeCheck),
                  SizedBox(
                    width: 4,
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 2),
                      child: Text('Following', overflow: TextOverflow.ellipsis))
                ],
              ),
            ),
            itemBuilder: (BuildContext context) {
              List<PopupMenuEntry> returnWidgets = [];
              //Add as my friend
              returnWidgets.add(menuItem(FanButtonMenuOption.Unfollow,
                  MdiIcons.eyeOff, 'Unfollow'));
              returnWidgets.add(menuItem(FanButtonMenuOption.UnfollowReplurk,
                  MdiIcons.eyeOff, 'Unfollow replurk'));
              return returnWidgets;
            },
            onSelected: (result) async {
              if (result == FanButtonMenuOption.Unfollow) {
                Plurdart.Base base = await Plurdart.friendsFansSetFollowing(
                    Plurdart.FriendsFansSetFollowing(
                  userId: widget.profile.userInfo.id,
                  follow: false,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.fansCount -= 1;
                    widget.profile.userInfo.fansCount -= 1;
                    widget.profile.isFollowing = false;
                    widget.profile.isFollowingReplurk = false;
                  });
                }
              } else if (result == FanButtonMenuOption.UnfollowReplurk) {
                Plurdart.Base base =
                    await Plurdart.friendsFansSetFollowingReplurk(
                        Plurdart.FriendsFansSetFollowing(
                  userId: widget.profile.userInfo.id,
                  follow: false,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.isFollowingReplurk = false;
                  });
                }
              }
            },
          ));
        } else {
          //I am his fan. [Following] (grey)
          //1. 'Unfollow' FriendsFans/setFollowing false
          //2. 'Follow replurk' FriendsFans/setFollowingReplurk true
          fansColumnChildren.add(PopupMenuButton(
            padding: EdgeInsets.all(0),
            icon: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              height: 36,
              color: Colors.grey.withOpacity(0.2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(MdiIcons.eyeCheck),
                  SizedBox(
                    width: 4,
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 2),
                      child: Text('Following', overflow: TextOverflow.ellipsis))
                ],
              ),
            ),
            itemBuilder: (BuildContext context) {
              List<PopupMenuEntry> returnWidgets = [];
              //Add as my friend
              returnWidgets.add(menuItem(FanButtonMenuOption.Unfollow,
                  MdiIcons.eyeOff, 'Unfollow'));
              returnWidgets.add(menuItem(FanButtonMenuOption.FollowReplurk,
                  MdiIcons.eyeOff, 'Follow replurk'));
              return returnWidgets;
            },
            onSelected: (result) async {
              if (result == FanButtonMenuOption.Unfollow) {
                Plurdart.Base base = await Plurdart.friendsFansSetFollowing(
                    Plurdart.FriendsFansSetFollowing(
                  userId: widget.profile.userInfo.id,
                  follow: false,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.fansCount -= 1;
                    widget.profile.userInfo.fansCount -= 1;
                    widget.profile.isFollowing = false;
                    widget.profile.isFollowingReplurk = false;
                  });
                }
              } else if (result == FanButtonMenuOption.FollowReplurk) {
                Plurdart.Base base =
                    await Plurdart.friendsFansSetFollowingReplurk(
                        Plurdart.FriendsFansSetFollowing(
                  userId: widget.profile.userInfo.id,
                  follow: true,
                ));

                if (base != null && !base.hasError()) {
                  //Success.
                  setState(() {
                    widget.profile.isFollowingReplurk = true;
                  });
                }
              }
            },
          ));
        }
      } else {
        //I am not his fan. [Follow] (grey)
        //1. 'Follow' FriendsFans/setFollowing true
        fansColumnChildren.add(SizedBox(
          child: OutlinedButton.icon(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all(
                  Theme.of(context).textTheme.bodyText1.color),
            ),
            icon: Icon(MdiIcons.eye),
            label: Text('Follow', overflow: TextOverflow.ellipsis),
            onPressed: () async {
              //Plurdart request.
              Plurdart.Base base = await Plurdart.friendsFansSetFollowing(
                  Plurdart.FriendsFansSetFollowing(
                userId: widget.profile.userInfo.id,
                follow: true,
              ));

              if (base != null && !base.hasError()) {
                //Success.
                setState(() {
                  widget.profile.fansCount += 1;
                  widget.profile.userInfo.fansCount += 1;
                  widget.profile.isFollowing = true;
                  widget.profile.isFollowingReplurk = true;
                });
              }
            },
          ),
        ));
      }
    }

    //Fans list button.
    fansColumnChildren.add(OutlinedButton(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all(
            Theme.of(context).textTheme.bodyText1.color),
      ),
      child: Text(
        widget.profile.fansCount.toString() + ' Fans',
        textAlign: TextAlign.center,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () {
        //Open users browser.
        _blocProviderContext
            .read<UsersBrowserCubit>()
            .open(UsersViewer.UsersViewerArgs(
              'Fans (' + widget.profile.fansCount.toString() + ')',
              UsersViewer.UsersBrowsingType.Fans,
              userId: widget.profile.userInfo.id,
              isToggleList: false,
            ));
      },
    ));

    //Following list button. (Only if this is your own profile) (Could be empty)
    Widget followingButton = Container();
    if (widget.profile.userInfo.followingCount != null) {
      followingButton = OutlinedButton(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all(
              Theme.of(context).textTheme.bodyText1.color),
        ),
        child: Text(
          widget.profile.userInfo.followingCount.toString() + ' Following',
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        onPressed: () {
          //Open users browser.
          _blocProviderContext
              .read<UsersBrowserCubit>()
              .open(UsersViewer.UsersViewerArgs(
                'Following (' +
                    widget.profile.userInfo.followingCount.toString() +
                    ')',
                UsersViewer.UsersBrowsingType.Following,
                userId: widget.profile.userInfo.id,
                isToggleList: false,
              ));
        },
      );
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Friends
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: friendsColumnChildren,
                ),
              ),

              VerticalDivider(
                width: 16,
                thickness: 1.5,
              ),

              //Fans.
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: fansColumnChildren,
                ),
              )
            ],
          ),
          followingButton,
        ],
      ),
    );
  }

  Widget _miscItem(IconData icon, String content) {
    return Row(
      children: [
        Container(
          width: 32,
          height: 32,
          child: Icon(
            icon,
            size: 20,
          ),
        ),
        SizedBox(
          width: 4,
        ),
        Expanded(
            child: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 2),
                child: Text(content))),
      ],
    );
  }

  PopupMenuItem menuItem(dynamic value, IconData icon, String text) {
    return PopupMenuItem(
      value: value,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: Theme.of(context).iconTheme.color,
          ),
          SizedBox(
            width: 8,
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 5), child: Text(text)),
        ],
      ),
    );
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }
}
