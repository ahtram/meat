// We'll try embed a Youtube video player by this.

import 'package:flutter/material.dart';
import 'package:meat/widgets/plurk/plurk_card_hash_tag_warp.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/plurk_card_link_stack.dart';
import 'package:meat/widgets/plurk/plurk_card_paste_collection.dart';
import 'package:meat/widgets/plurk/plurk_card_text_content.dart';
import 'package:meat/widgets/plurk/plurk_card_youtube_video.dart';
import 'package:meat/widgets/plurk/plurk_paste_view.dart';
import 'package:universal_html/parsing.dart';
import 'package:universal_html/html.dart' as Html;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/utils/extensions.dart';

class PlurkContentParts {
  //There are processed data for display's convenient.
  String textContent = '';
  List<PlurkCardYoutubeVideoLink> youtubeVideoLinks = [];
  List<PlurkCardImageLink> plurkImageLinks = [];
  List<PlurkCardPasteLink> plurkPasteLinks = [];
  List<PlurkCardUrlLink> urlLinks = [];

  List<PlurkCardHashTag> hashTags = [];

  // This will be put at start of build()
  preProcess(BuildContext context, String content, bool removeUrlLinks) {
//    print('[_preProcess]');

    //Prepare processed content.
    youtubeVideoLinks.clear();
    plurkImageLinks.clear();
    plurkPasteLinks.clear();
    urlLinks.clear();
    hashTags.clear();

    //We'll rely on Html DOM manipulation tricks here...
    Html.HtmlDocument htmlDocument = parseHtmlDocument(content);

    // print('[origin content]' + content);

    //This is too complicate
    // print('[htmlDocument Summary]:');
    // for (int i = 0; i < htmlDocument.body.childNodes.length; ++i) {
    //   print('[' + i.toString() + ']' + htmlDocument.body.childNodes[i].nodeType.toString() + ' | ' + htmlDocument.body.childNodes[i].toString());
    //   // if (htmlDocument.body.childNodes[i].nodeValue != null) {
    //   //   print('[' + i.toString() + ']' + htmlDocument.body.childNodes[i].nodeValue);
    //   // } else {
    //   //   print('[' + i.toString() + '] null');
    //   // }
    //
    // }

    //====== Remove way =======
    _processElement(context, htmlDocument.body, removeUrlLinks);

    // print('Processed Result: ' + htmlDocument.body.outerHtml);

    //The processed innerHtml is our text content.
    textContent = htmlDocument.body.innerHtml;
  }

  _processElement(BuildContext context, Html.Element element, bool removeUrlLinks) {
    if (element.children.length > 0) {
      // Start from tail.
      for (int i = element.children.length - 1; i >= 0; --i) {
        //Recursive
        _processElement(context, element.children[i], removeUrlLinks);

//        print('body child[' +
//            i.toString() +
//            ']: ' +
//            element.children[i].toString() +
//            ' | tagName[' +
//            element.children[i].tagName +
//            ']');
        if (element.children[i].tagName == 'BR' &&
            element.children[i].previousElementSibling != null &&
            element.children[i].previousElementSibling.tagName == 'A' &&
            removeUrlLinks) {
          //Don't preserve the <br> right after a link <A> or a <BR>
          // print('Remove this: [' + i.toString() + '] <BR>');
          //Edit: They actually need this!
          // if (removeUrlLinks) {
          //   element.children[i].remove();
          // }
        } else if (element.children[i].tagName == 'A') {
          // This will grab this link to our process data.
          bool shouldRemoveChild =
              _identifyAndStoreLink(context, element.children[i], removeUrlLinks);
          if (shouldRemoveChild) {
            //Try just remove the A stuff.
//          print('Remove this A: [' +
//              i.toString() +
//              '] href[' +
//              element.children[i].getAttribute('href') +
//              ']');
            element.children[i].remove();
          }
        } else if (element.children[i].tagName == 'SPAN' &&
            (element.children[i].className == 'hashtag')) {
          //[Dep] We decide not to move the hashTag. Just keep them where they are.
          //Extract and store hash tags from an element.
          // _extractHashTag(element.children[i]);
          // element.children[i].remove();

          String hashTagContent = element.children[i].text.substring(1);

          //Let's try replace the SPAN with a hash tag link.
          Html.Element hashTagLinkElement = Html.Element.html(
              '<a href=' + Define.bktHashTagScheme + '://' +
                  hashTagContent +
                  '>' +
                  element.children[i].text +
                  '<//a>',
              validator: Html.NodeValidatorBuilder()
                ..allowHtml5(uriPolicy: ItemUrlPolicy())
                ..allowNavigation(ItemUrlPolicy())
                ..allowImages(ItemUrlPolicy()));

          hashTagLinkElement.setAttribute(
              'style',
              'text-decoration:none;');

          element.children[i].replaceWith(hashTagLinkElement);

        } else if (element.children[i].tagName == 'IMG' &&
            (element.children[i].className == 'emoticon_my')) {
          //We should find a way to process this emoticon (add a link or something)
          String sourceUrl = element.children[i].attributes['src'];
          if (sourceUrl != null) {
//            print('This is an emoticon [' + sourceUrl + '] [' + element.children[i].outerHtml + ']');
            //We'll try transform this to a image link! (Hack!)

            //I don't know what kind of voodoo is this. It worked anyway...
            //https://stackoverflow.com/questions/62840032/flutter-web-htmlelementview-removing-disallowed-attribute
            Html.Element imgLinkElement = Html.Element.html(
                '<a href="' +
                    sourceUrl +
                    '">' +
                    element.children[i].outerHtml +
                    '<//a>',
                validator: Html.NodeValidatorBuilder()
                  ..allowHtml5(uriPolicy: ItemUrlPolicy())
                  ..allowNavigation(ItemUrlPolicy())
                  ..allowImages(ItemUrlPolicy()));

//            Html.Element emptyDivElement = Html.Element.html(
//                '<div><\/div>',
//                validator: Html.NodeValidatorBuilder()
//                  ..allowHtml5(uriPolicy: ItemUrlPolicy())
//                  ..allowNavigation(ItemUrlPolicy())
//                  ..allowImages(ItemUrlPolicy()));

            //Not sure if this go boom. Let's do it.
            element.children[i].replaceWith(imgLinkElement);
//            //Try this hack.
//            element insertBefore(emptyDivElement, element.children[i]);
          }
        }
        //This has some issues.
//        else if (element.children[i].tagName == 'IMG' && (element.children[i].className == 'emoticon')) {
//          //For built in emoticon we'll try mimic an empty link.
//          //I don't know what kind of voodoo is this. It worked anyway...
//          //https://stackoverflow.com/questions/62840032/flutter-web-htmlelementview-removing-disallowed-attribute
//          Html.Element imgLinkElement = Html.Element.html('<a href="#0">' + element.children[i].toString() + '<//a>', validator: Html.NodeValidatorBuilder()
//            ..allowHtml5(uriPolicy: ItemUrlPolicy())
//            ..allowNavigation(ItemUrlPolicy())
//            ..allowImages(ItemUrlPolicy()));
//
//          //Not sure if this go boom. Let's do it.
//          element.children[i].replaceWith(imgLinkElement);
//        }
        else {
          //Tricky!: Also ignore the <br> right before a link...
          //I guess not..
          // if (element.children[i].tagName == 'BR' &&
          //     element.children[i].nextElementSibling != null &&
          //     element.children[i].nextElementSibling.tagName == 'A') {
          //   //Dont preserve the <br> right before a link <A>
          //   // print('Remove this: [' + i.toString() + '] <BR>');
          //   element.children[i].remove();
          // } else {
          //   // This should be preserve. This could either be an emoji or text data.
          //   // Not A or preceeding BR
          // }
        }
      }

      // The body that removed all <a> stuffs.
//      print('result body: ' + htmlDocument.body.innerHtml);
    }
  }

  // Identify and store a link to processed data.
  // return if we need to actually remove the link.
  bool _identifyAndStoreLink(BuildContext context, Html.Element element, bool removeUrlLinks) {
    // print('_identifyAndStoreLink: ' + element.toString());

    String linkHref = element.getAttribute('href');
    if (linkHref?.isEmpty == true) {
      //What? The link is empty?? WTF just ignore this shit.
      return false;
    } else {
      Uri linkUri = Uri.parse(linkHref);
      //Is this a youtube link?
      if ((linkUri.authority == Define.youtubeDesktopAuthority ||
              linkUri.authority == Define.youtubeMobileAuthority) &&
          linkUri.queryParameters['v'] != null) {
        //A youtube playlist link will not have the param 'v'
        //We'll just ignore it and treat it as a normal link for now.

        //This is a Youtube video link.
        String youtubeVideoID = linkUri.queryParameters['v'];
        String videoTitle = element.text;
        String previewImageUrl = '';
        int dataWidth = 480;
        int dataHeight = 270;

        if (element.children.length > 0) {
          if (element.children[0].tagName == 'IMG') {
            previewImageUrl = element.children[0].getAttribute('src');

            String widthAttr = element.children[0].getAttribute('width');
            if (widthAttr != null) {
              int width = int.tryParse(widthAttr);
              if (width != null) {
                dataWidth = width;
              }
            }

            String heightAttr = element.children[0].getAttribute('height');
            if (heightAttr != null) {
              int height = int.tryParse(heightAttr);
              if (height != null) {
                dataHeight = height;
              }
            }
          }
        }

//        print('[This is a Youtube link] ID[' +
//            youtubeVideoID +
//            '] videoTitle[' +
//            videoTitle +
//            '] previewImageUrl[' +
//            previewImageUrl +
//            '][' +
//            dataWidth.toString() +
//            '][' +
//            dataHeight.toString() +
//            ']');

        //Store this data.
        youtubeVideoLinks.insert(
            0,
            PlurkCardYoutubeVideoLink(videoTitle, youtubeVideoID,
                previewImageUrl, dataWidth, dataHeight));

        return removeUrlLinks;
      } else if (linkUri.authority == Define.youtubeShortenAuthority) {
        //A youtube playlist link will not have the param 'v'
        //We'll just ignore it and treat it as a normal link for now.

        //This is a Youtube video link.
        String youtubeVideoID = linkUri.path.substring(1, linkUri.path.length);
        String videoTitle = element.text;
        String previewImageUrl = '';
        int dataWidth = 480;
        int dataHeight = 270;

        if (element.children.length > 0) {
          if (element.children[0].tagName == 'IMG') {
            previewImageUrl = element.children[0].getAttribute('src');

            String widthAttr = element.children[0].getAttribute('width');
            if (widthAttr != null) {
              int width = int.tryParse(widthAttr);
              if (width != null) {
                dataWidth = width;
              }
            }

            String heightAttr = element.children[0].getAttribute('height');
            if (heightAttr != null) {
              int height = int.tryParse(heightAttr);
              if (height != null) {
                dataHeight = height;
              }
            }
          }
        }

       // print('[This is a Youtube shorten link] ID[' +
       //     youtubeVideoID +
       //     '] videoTitle[' +
       //     videoTitle +
       //     '] previewImageUrl[' +
       //     previewImageUrl +
       //     '][' +
       //     dataWidth.toString() +
       //     '][' +
       //     dataHeight.toString() +
       //     ']');

        //Store this data.
        youtubeVideoLinks.insert(
            0,
            PlurkCardYoutubeVideoLink(videoTitle, youtubeVideoID,
                previewImageUrl, dataWidth, dataHeight));

        return removeUrlLinks;
      } else if (linkUri.authority == Define.plurkImageServiceAuthority ||
          Define.urlEndWithSupportImageExtensions(linkUri.toString())) {
        //This is a Plurk image service link.
        String thumbnailUrl = '';
        if (element.children.length > 0) {
          if (element.children[0].tagName == 'IMG') {
            thumbnailUrl = element.children[0].getAttribute('src');
          }
        }

//        print('[This is a Plurk image link] thumbnailUrl[' +
//            thumbnailUrl +
//            '] linkUri[' +
//            linkUri.toString() +
//            ']');

        plurkImageLinks.insert(
            0, PlurkCardImageLink(linkUri, thumbnailUrl));

        return removeUrlLinks;
      } else if (linkUri.authority == Define.twitterMediaAuthority &&
          linkUri.pathSegments.length > 1 && linkUri.pathSegments[0] == Define.twitterMediaImagePathSegment) {
        //This is a Twitter image service link.
        String thumbnailUrl = '';
        if (element.children.length > 0) {
          if (element.children[0].tagName == 'IMG') {
            thumbnailUrl = element.children[0].getAttribute('src');
          }
        }

        // print('[This is a Twitter image link] thumbnailUrl[' +
        //    thumbnailUrl +
        //    '] linkUri[' +
        //    linkUri.toString() +
        //    ']');

        plurkImageLinks.insert(
            0, PlurkCardImageLink(linkUri, thumbnailUrl));

        return removeUrlLinks;
      } else if (linkUri.authority == Define.plurkPasteAuthority) {

        //This is a plurk paste url!
       //  print('[This is a Plurk paste link] [' +
       //     linkUri.toString() +
       // ']');

        plurkPasteLinks.insert(0, PlurkCardPasteLink(linkUri, element.text));

        return removeUrlLinks;
      } else if (linkUri.authority == Define.plurkAuthority) {
        //Is this a post?
        if (linkUri.path.contains('/p/')) {
          //This is a plurk post. Treat as a general link.
          String title = element.text;
          String previewImageUrl = '';
          if (element.children.length > 0) {
            if (element.children[0].tagName == 'IMG') {
              previewImageUrl = element.children[0].getAttribute('src');
              element.children[0].remove();
            }
          }

       // print('[This is a plurk post link] title[' +
       //     title +
       //     '] previewImageUrl[' +
       //     previewImageUrl +
       //     '] linkUri[' +
       //     linkUri.toString() +
       //     ']');

          urlLinks.insert(0, PlurkCardUrlLink(linkUri, title, previewImageUrl));

          return removeUrlLinks;
        } if (linkUri.path.contains('/u/')) {
          //This is a plurk user page. Treat as a general link.
          String title = element.text;
          String previewImageUrl = '';
          if (element.children.length > 0) {
            if (element.children[0].tagName == 'IMG') {
              previewImageUrl = element.children[0].getAttribute('src');
              element.children[0].remove();
            }
          }

          // print('[This is a plurk user link] title[' +
          //     title +
          //     '] previewImageUrl[' +
          //     previewImageUrl +
          //     '] linkUri[' +
          //     linkUri.toString() +
          //     ']');

          urlLinks.insert(0, PlurkCardUrlLink(linkUri, title, previewImageUrl));

          return removeUrlLinks;
        } else {
          //This is a user profile.
          //We'll just try replace the element text from a local user cache collection. (Static)
          //This could prevent from redundant API calls. (Cheap trick)

          Plurdart.User user = Static.getUserByNickName(element.text);
          if (user != null) {
            //Force change the text to this user's displayName.
            element.text = user.displayName;

            //Force change the text Color to user's nameColor and remove underline and give bold text...(phew..)
            //Hmm?? Sometimes the nameColor is null?? Why? Because the user use the default color...
            if (user.nameColor != null) {
              element.setAttribute(
                  'style',
                  'text-decoration:none;font-weight:bold;color:#' +
                      user.nameColor +
                      ';');
            } else {
              //Use the theme text color since this guy is non-color.
              element.setAttribute(
                  'style',
                  'text-decoration:none;font-weight:bold;color:#' +
                      Theme.of(context)
                          .textTheme
                          .bodyText1
                          .color.toHexWithoutAlpha(leadingHashSign: false) +
                      ';');
            }

            // print('Got user profile Link [' + element.innerHtml + '] [' + element.outerHtml + ']');
          } else {
            //No User data found cannot parse!
            return false;

            //[Dep] Forget it. Don't touch it. It's better stay as a plain nickname link.
            // //This is a plurk user page. Treat as a general link.
            // String title = element.text;
            // String previewImageUrl = '';
            // if (element.children.length > 0) {
            //   if (element.children[0].tagName == 'IMG') {
            //     previewImageUrl = element.children[0].getAttribute('src');
            //     element.children[0].remove();
            //   }
            // }
            //
            // // print('[This is a plurk user link with no User data] title[' +
            // //     title +
            // //     '] previewImageUrl[' +
            // //     previewImageUrl +
            // //     '] linkUri[' +
            // //     linkUri.toString() +
            // //     ']');
            //
            // urlLinks.insert(0, PlurkCardUrlLink(linkUri, title, previewImageUrl));
            //
            // //Tricky! We don't like previewImageUrl in the text content.
            // //So if the previewImageUrl is not empty we'll force remove it.
            // return removeUrlLinks;
          }
          return false;
        }

//        //We need to ask user names from the Plurdart API. (Nope)
//        try {
//          //Plan changed.
////          PlurDart.Profile userProfile =  await PlurDart.profileGetPublicProfile(PlurDart.ProfileGetPublicProfile(userId: '', nickName: element.text));
////          if (userProfile != null) {
////            element.text = userProfile.userInfo.displayName;
////          } else {
////            //OOps! Something wrong with the get use API.
////            print('Oops! _identifyAndStoreLink userProfile is null?! [' + element.text + ']');
////          }
//        } catch (e) {
//          //OOps! Something wrong with the get use API.
//          print('Oops! _identifyAndStoreLink cannot get user [' + element.text + ']');
//        }

      } else {
        //This is a general link.
        String title = element.text;
        String previewImageUrl = '';
        if (element.children.length > 0) {
          if (element.children[0].tagName == 'IMG') {
            previewImageUrl = element.children[0].getAttribute('src');
            element.children[0].remove();
          }
        }

       // print('[This is a general link] title[' +
       //     title +
       //     '] previewImageUrl[' +
       //     previewImageUrl +
       //     '] linkUri[' +
       //     linkUri.toString() +
       //     '] authority [' +
       //     linkUri.authority +
       //     '] pathSegments [' + linkUri.pathSegments.join(',') + ']');

        urlLinks.insert(0, PlurkCardUrlLink(linkUri, title, previewImageUrl));

        return removeUrlLinks;
      }
    }
  }

  //Try extract all hash tags and store them from this element.
  // bool _extractHashTag(Html.Element element) {
  //   String hashTagContent = element.text.substring(1);
  //   hashTags.add(PlurkCardHashTag(hashTagContent));
  //   return true;
  // }

  //==== Part builders

  //== Text Content == (Maybe empty)
  Widget buildTextContentPart(String textContent) {
    if (textContent?.isEmpty ?? true) {
      //Empty content.
      return null;
    } else {
      return PlurkCardTextContent(textContent);
    }
  }

  //== Image Grid Content == (Maybe empty)
  Widget buildImageCollectionPartByType(List<PlurkCardImageLink> imageLinks,
      Function onTap, Function onDownloadComplete, ImageCollectionType type) {
    if (imageLinks.length > 0) {
      return PlurkCardImageCollection(imageLinks, onTap, onDownloadComplete, type);
    } else {
      //No image links.
      return null;
    }
  }

  //== Youtube Video Content == (Maybe empty)
  Widget buildYoutubeVideoPart(
      List<PlurkCardYoutubeVideoLink> youtubeVideoLinks) {
    if (youtubeVideoLinks.length > 0) {
      return PlurkCardYoutubeVideo(youtubeVideoLinks);
    } else {
      //No video needed.
      return null;
    }
  }

  //== Plurk Paste Content == (Maybe empty)
  Widget buildPlurkPastePart(
      List<PlurkCardPasteLink> plurkCardPasteLinks, PlurkPasteViewType type, Function(Uri) onLinkOpen) {
    if (plurkCardPasteLinks.length > 0) {
      return PlurkCardPasteCollection(plurkCardPasteLinks, type, onLinkOpen);
    } else {
      //No PlurkCardPasteCollection needed.
      return null;
    }
  }

  //== Link Stack Content == (Maybe empty)
  Widget buildLinkStackPart(List<PlurkCardUrlLink> urlLinks, Function(Uri) onLinkOpen) {
    if (urlLinks.length > 0) {
      if (urlLinks.length <= 3) {
        return PlurkCardLinkStack(urlLinks, LinkStackType.All, onLinkOpen);
      } else {
        return PlurkCardLinkStack(urlLinks, LinkStackType.Pages, onLinkOpen);
      }
    } else {
      return null;
    }
  }

  //== Link Stack Content == (Maybe empty)
  Widget buildLinkStackPartByType(List<PlurkCardUrlLink> urlLinks, LinkStackType type, Function(Uri) onLinkOpen) {
    if (urlLinks.length > 0) {
      return PlurkCardLinkStack(urlLinks, type, onLinkOpen);
    } else {
      return null;
    }
  }

  //== Hash tags content == (Maybe empty)
  Widget buildHashTagsPart(List<PlurkCardHashTag> hashTags) {
    if (hashTags.length > 0) {
      return PlurkCardHashTagWarp(
        hashTags,
        onHashtagTap: (hashTag) {
          //Search this hash tag?
          print('onHashtagTap [' + hashTag + ']');
        },
      );
    } else {
      return null;
    }
  }
}

//-- These are just intermediate data structures --

class PlurkCardYoutubeVideoLink {
  PlurkCardYoutubeVideoLink(this.videoTitle, this.youtubeVideoID,
      this.previewImageUrl, this.dataWidth, this.dataHeight)
      // : controller = YoutubePlayerController(
      //     initialVideoId: youtubeVideoID,
      //     flags: YoutubePlayerFlags(
      //       autoPlay: false,
      //       mute: false,
      //       disableDragSeek: true,
      //     ),
      //   )
  ;

  final String videoTitle;
  final String youtubeVideoID;
  final String previewImageUrl;
  final int dataWidth;
  final int dataHeight;
  // This is for YoutubePlayer initialize.
  // final YoutubePlayerController controller;
}

// We'll store a Plurk Image Service Link here...
class PlurkCardImageLink {
  PlurkCardImageLink(this.imgUri, this.thumbnailUrl)
      : heroTag = UniqueKey();

  final Uri imgUri;
  // This is from img src.
  final String thumbnailUrl;
  final UniqueKey heroTag;

  // This is for store downloaded image info. (Done by ImageCollection) (Possible null)
  ImageRawInfo fullImageRawInfo;
  ImageRawInfo thumbnailRawInfo;

  //Get the link url.
  String getFullImageUrl() {
    return imgUri.toString();
  }

  String getFullImageFileName() {
    return imgUri.pathSegments.last;
  }

  String getThumbnailUrl() {
    return thumbnailUrl;
  }

  //This will get url by settings
  String getImageUrl() {
    if (Static.settingsDownloadFullImageInPlurkCard) {
      return getFullImageUrl();
    } else {
      return getThumbnailUrl();
    }
  }

  ImageRawInfo getImageRawInfo() {
    if (Static.settingsDownloadFullImageInPlurkCard) {
      return fullImageRawInfo;
    } else {
      return thumbnailRawInfo;
    }
  }

  ImageProvider getImageRawInfoProvider() {
    if (Static.settingsDownloadFullImageInPlurkCard) {
      return fullImageRawInfo.imageProvider;
    } else {
      return thumbnailRawInfo.imageProvider;
    }
  }

  void setImageRawInfo(ImageRawInfo imageRawInfo) {
    if (Static.settingsDownloadFullImageInPlurkCard) {
      fullImageRawInfo = imageRawInfo;
    } else {
      thumbnailRawInfo = imageRawInfo;
    }
  }

  String getHeroTag() {
    return heroTag.toString();
  }
}

//A plurk paste link!
class PlurkCardPasteLink {
  PlurkCardPasteLink(this.uri, this.title);

  final Uri uri;
  final String title;
}

// Store a general link data.
class PlurkCardUrlLink {
  PlurkCardUrlLink(this.uri, this.title, this.previewImageUrl);
  //Note that anything could be empty...
  final Uri uri;
  final String title;
  final String previewImageUrl;

  //Get the link url.
  String getUrl() {
    return Uri.decodeFull(uri.toString());
  }

  //Get the site authority.
  String getAuthority() {
    return Uri.decodeFull(uri.authority);
  }

  //Simple and stupid solution for display something.
  // String getFaviconUrl() {
  //   return Uri.https(uri.authority, 'favicon.ico').toString();
  // }

  String getFaviconUrlFromGoogle() {
    return 'https://www.google.com/s2/favicons?domain=' + uri.authority;
  }
}

//A hash tag.
class PlurkCardHashTag {
  PlurkCardHashTag(this.term);
  final String term;
}

class ItemUrlPolicy implements Html.UriPolicy {
  RegExp regex = RegExp(r'(?:http://|https://)?.*');

  bool allowsUri(String uri) {
    return regex.hasMatch(uri);
  }
}
