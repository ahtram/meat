import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'friends_fans_get_fans_by_offset_state.dart';

class FriendsFansGetFansByOffsetCubit extends Cubit<FriendsFansGetFansByOffsetState> {
  FriendsFansGetFansByOffsetCubit() : super(FriendsFansGetFansByOffsetInitial());

  request(int userId, int offset, int limit) async {
    List<Plurdart.User> users = await Plurdart.friendsFansGetFansByOffset(Plurdart.FriendsFansGetFriendsFansByOffset(
      userId: userId,
      offset: offset,
      limit: limit,
    ));

    if (users != null) {
      emit(FriendsFansGetFansByOffsetSuccess(users));
    } else {
      emit(FriendsFansGetFansByOffsetFailed());
    }
  }
}
