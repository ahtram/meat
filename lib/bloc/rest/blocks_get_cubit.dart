import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'blocks_get_state.dart';

class BlocksGetCubit extends Cubit<BlocksGetState> {
  BlocksGetCubit() : super(BlocksGetInitial());

  request(int offset) async {
    Plurdart.Blocks blocks = await Plurdart.blocksGet(Plurdart.BlocksGet(offset: offset));
    if (blocks != null) {
      emit(BlocksGetSuccess(blocks.users));
    } else {
      emit(BlocksGetFailed());
    }
  }
}
