part of 'emoticons_get_cubit.dart';

@immutable
abstract class EmoticonsGetState {}

class EmoticonsGetInitial extends EmoticonsGetState {}

class EmoticonsGetSuccess extends EmoticonsGetState {
  EmoticonsGetSuccess(this.emoticons);
  final Plurdart.Emoticons emoticons;
}

class EmoticonsGetFailed extends EmoticonsGetState { }
