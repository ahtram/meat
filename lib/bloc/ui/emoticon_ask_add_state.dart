part of 'emoticon_ask_add_cubit.dart';

@immutable
abstract class EmoticonAskAddState {}

class EmoticonAskAddInitial extends EmoticonAskAddState {}

class EmoticonAskAddShow extends EmoticonAskAddState {
  EmoticonAskAddShow(this.url);
  final String url;
}
