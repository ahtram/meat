import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'flush_bar_state.dart';

class FlushBarCubit extends Cubit<FlushBarState> {
  FlushBarCubit() : super(FlushBarInitial());

  //Request a flush bar.
  //Different routes should listen to this and customize their own flush bar layout.
  request(IconData icon, String title, String message) {
    emit(FlushBarRequest(icon, title, message));
  }
}
