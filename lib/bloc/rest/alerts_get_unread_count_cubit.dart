import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'alerts_get_unread_count_state.dart';

class AlertsGetUnreadCountCubit extends Cubit<AlertsGetUnreadCountState> {
  AlertsGetUnreadCountCubit() : super(AlertsGetUnreadCountInitial());

  Future request() async {
    Plurdart.AlertsUnreadCount alertsUnreadCount = await Plurdart.alertsGetUnreadCounts();
    if (alertsUnreadCount != null) {
      emit(AlertsGetUnreadCountSuccess(alertsUnreadCount));
    } else {
      emit(AlertsGetUnreadCountFailed());
    }
  }
}
