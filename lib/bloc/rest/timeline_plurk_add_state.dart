part of 'timeline_plurk_add_cubit.dart';

@immutable
abstract class TimelinePlurkAddState {}

class TimelinePlurkAddInitial extends TimelinePlurkAddState {}

class TimelinePlurkAddSuccess extends TimelinePlurkAddState {
  TimelinePlurkAddSuccess(this.resultPlurk);
  final Plurdart.Plurk resultPlurk;
}

class TimelinePlurkAddFailed extends TimelinePlurkAddState {
  TimelinePlurkAddFailed(this.errorText);
  final String errorText;
}