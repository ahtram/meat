import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'users_me_state.dart';

class UsersMeCubit extends Cubit<UsersMeState> {
  UsersMeCubit() : super(UsersMeInitial());

  Future request() async {
    Plurdart.User me = await Plurdart.usersMe();
    Plurdart.PremiumBalance balance = await Plurdart.premiumGetBalance();

    //Collect Users to global User Cache.
    Static.addToUserCache(me);

    if (me != null && balance != null) {
      //Do this before emit success. To ensure every get me from Static.
      Static.me = me;
      Static.saveMeId(me.id);

      //Also update the user's balance cache.
      Static.balance = balance;

      emit(UsersMeSuccess(me));
    } else {
      emit(UsersMeFailed());
    }
  }
}
