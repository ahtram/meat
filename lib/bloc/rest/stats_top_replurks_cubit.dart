import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'stats_top_replurks_state.dart';

class StatsTopReplurksCubit extends Cubit<StatsTopReplurksState> {
  StatsTopReplurksCubit() : super(StatsTopReplurksInitial());

  request() async {
    Plurdart.TopPlurks topPlurks = await Plurdart.statsTopReplurks(Plurdart.StatsTop(lang: Static.apiLanguage));
    if (topPlurks != null) {
      emit(StatsTopReplurksSuccess(topPlurks));
    } else {
      emit(StatsTopReplurksFailed());
    }
  }
}
