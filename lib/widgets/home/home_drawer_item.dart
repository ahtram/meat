import 'package:flutter/material.dart';

class HomeDrawerItem extends StatelessWidget {
  HomeDrawerItem(
    this.leading,
    this.title, {
    this.trailing,
    this.onTap,
  });

  final IconData leading;
  final String title;
  final Widget trailing;
  final Function onTap;

  @override
  Widget build(BuildContext context) {

    Widget trailingWidget = Container();
    if (trailing != null) {
      trailingWidget = trailing;
    }

    return InkWell(
      enableFeedback: (onTap != null),
      child: Row(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(8),
              child: Icon(
                leading,
                color: Theme.of(context).iconTheme.color,
              )),
          Padding(
            padding: EdgeInsets.all(8),
            child: Text(
              title,
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
          SizedBox(width: 4,),
          trailingWidget,
          Expanded(child: SizedBox()),
        ],
      ),
      onTap: onTap,
    );
  }
}
