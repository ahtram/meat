part of 'alerts_get_history_cubit.dart';

@immutable
abstract class AlertsGetHistoryState {}

class AlertsGetHistoryInitial extends AlertsGetHistoryState {}

class AlertsGetHistorySuccess extends AlertsGetHistoryState {
  AlertsGetHistorySuccess(this.alerts);
  final List<Plurdart.Alert> alerts;
}

class AlertsGetHistoryFailed extends AlertsGetHistoryState {

}
