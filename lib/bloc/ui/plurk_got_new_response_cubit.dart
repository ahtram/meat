import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'plurk_got_new_response_state.dart';

class PlurkGotNewResponseCubit extends Cubit<PlurkGotNewResponseState> {
  PlurkGotNewResponseCubit() : super(PlurkGotNewResponseInitial());

  success(Plurdart.Plurk plurk) async {
    emit(PlurkGotNewResponseSuccess(plurk));
  }
}
