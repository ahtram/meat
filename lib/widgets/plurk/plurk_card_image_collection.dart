import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter/material.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'dart:io';
import 'package:meat/system/define.dart' as Define;
import 'package:meat/bloc/ui/settings_download_full_image_changed_cubit.dart';
import 'package:shimmer/shimmer.dart';
import 'package:transparent_image/transparent_image.dart';

enum ImageOrientation {
  Square,
  Landscape,
  Portrait,
}

//A structure to store image info
class ImageRawInfo {
  ImageRawInfo(this.source, this.uint8list, this.uiImage, this.contentLength)
      : imageProvider = MemoryImage(uint8list);

  final String source;
  final Uint8List uint8list;
  final ui.Image uiImage;
  //Size read from head in bytes.
  final int contentLength;
  final ImageProvider imageProvider;

  double getImageSlope() {
    return uiImage.height / uiImage.width;
  }

  //For Column child flex
  int getImageSlopeAsFlexRef() {
    return (getImageSlope() * 100.0).toInt();
  }

  //For row child flex
  int getReverseImageSlopeAsFlexRef() {
    return ((1.0 / getImageSlope()) * 100.0).toInt();
  }

  ImageOrientation getImageOrientation() {
    if (uiImage.height == uiImage.width) {
      return ImageOrientation.Square;
    } else {
      double slope = getImageSlope();
      if (slope > 1.0) {
        return ImageOrientation.Portrait;
      } else {
        return ImageOrientation.Landscape;
      }
    }
  }

  //A helper for calculate average slope.
  static double calculateAverageSlope(List<ImageRawInfo> infos) {
    if (infos.length >= 1) {
      double total = 0.0;
      infos.forEach((element) {
        if (element != null) {
          total += element.getImageSlope();
        }
      });
      return total / infos.length;
    }
    return 1.0;
  }
}

// This is a global cache object.
Map<String, ImageRawInfo> _imageRawInfoCache = Map<String, ImageRawInfo>();

// This will do what you think.
void clearImageCollectionCache() {
  _imageRawInfoCache.clear();
}

enum ImageCollectionType {
  Summery,
  ExpandAll,
}

class PlurkCardImageCollection extends StatefulWidget {
  PlurkCardImageCollection(this.imageLinks, this.onTap, this.onDownloadComplete,
      this.imageCollectionType);

  final List<PlurkCardImageLink> imageLinks;
  final Function onTap;
  final Function onDownloadComplete;
  final ImageCollectionType imageCollectionType;

  //The max download size in bytes...
  //This will limit the download size and decide how many preview we will have in the ImageCollection.
  static const int SUMMERY_LIMIT_SIZE = 1572864;

  @override
  _PlurkCardImageCollectionState createState() =>
      _PlurkCardImageCollectionState();
}

class _PlurkCardImageCollectionState extends State<PlurkCardImageCollection> {

  //[Dep] Try avoiding use of FutureBuilder to prevent from getting error.
  // Future _prepSummeryLinksFuture;
  // Future _prepAllLinksFuture;
  bool _downloadComplete = false;

  @override
  void initState() {
    super.initState();
    _prepByImageCollectionType();
  }

  Future<void> _prepByImageCollectionType() async {
    if (widget.imageCollectionType == ImageCollectionType.Summery) {
      await _prepLinksForSummery(widget.imageLinks);
    } else {
      await _prepareLinksForAll(widget.imageLinks);
    }
  }

  Future<void> _prepLinksForSummery(List<PlurkCardImageLink> imageLinks) async {
    //Download for imageLinks until pass SUMMERY_LIMIT_SIZE or finish.
    int accumulatedCompleteContentLength = 0;
    // print('_prepLinksForSummery: ' + imageLinks.length.toString() + ' [' + imageLinks[0].getUrl() + ']');
    for (int i = 0; i < imageLinks.length; ++i) {
      String url = imageLinks[i].getImageUrl();
      //Exist in cache?
      if (_imageRawInfoCache.containsKey(url)) {
        //We already have this guy. Saved some time.
        imageLinks[i].setImageRawInfo(_imageRawInfoCache[url]);
        accumulatedCompleteContentLength +=
            _imageRawInfoCache[url].contentLength;
        // print('found cache for [' + url + '] try restore it.');
      } else {
        // print('getting head for [' + i.toString() + '] url [' + url + ']');
        //Not exist in cache. We have to download this one.
        //Try probe the size of the image.
        http.Response headResponse = await http.head(Uri.parse(url));

        // print('url [' +
        //     url +
        //     '] content-length [' +
        //     headResponse.headers["content-length"] +
        //     ']');

        http.Response response = await http.get(Uri.parse(url));

        if (response != null) {
          // print('await decodeImageFromList(response.bodyBytes)');
          //Also depend on dart.ui to find the images width/height.
          ui.Image image = await decodeImageFromList(response.bodyBytes);

          // print('contentLength');
          int contentLength = 0;
          if (headResponse != null &&
              headResponse.statusCode == HttpStatus.ok &&
              headResponse.headers.containsKey('content-length')) {
            contentLength =
                int.tryParse(headResponse.headers["content-length"]);
          }

          // print('ImageRawInfo(url, response.bodyBytes, image, contentLength)');
          ImageRawInfo newImageRawInfo =
              ImageRawInfo(url, response.bodyBytes, image, contentLength);

          //Map url to ImageRawInfo and cache them!
          //Cache this for Image Links Viewer.
          _imageRawInfoCache[url] = newImageRawInfo;

          //Store this.
          // print('setImageRawInfo(newImageRawInfo)');
          imageLinks[i].setImageRawInfo(newImageRawInfo);
          accumulatedCompleteContentLength += newImageRawInfo.contentLength;
        } else {
          print('Oops! Response is null?! cannot get image from url [' +
              url +
              ']');
        }
      }

      //Check complete size to leave the loop.
      if (accumulatedCompleteContentLength >=
          PlurkCardImageCollection.SUMMERY_LIMIT_SIZE) {
        //We have enough content.
        break;
      }
    }

    if (mounted) {
      setState(() {
        _downloadComplete = true;
      });
      widget.onDownloadComplete?.call();
    }

    // print('[_prepLinksForSummery end]');
  }

  Future<void> _prepareLinksForAll(List<PlurkCardImageLink> imageLinks) async {
    //This will try download all images and cache them into imageLinks.

    for (int i = 0; i < imageLinks.length; ++i) {
      String url = imageLinks[i].getImageUrl();

      //Check the cache
      if (_imageRawInfoCache.containsKey(url)) {
        //We already have this guy. Saved some time.
        imageLinks[i].setImageRawInfo(_imageRawInfoCache[url]);
      } else {
        //Try probe the size of the image.
        http.Response headResponse = await http.head(Uri.parse(url));

        // print('url [' +
        //     url +
        //     '] content-length [' +
        //     headResponse.headers["content-length"] +
        //     ']');

        http.Response response = await http.get(Uri.parse(url));
        if (response != null) {
          //Also depend on dart.ui to find the images width/height.
          ui.Image image = await decodeImageFromList(response.bodyBytes);
          ImageRawInfo newImageRawInfo = ImageRawInfo(url, response.bodyBytes,
              image, int.tryParse(headResponse.headers["content-length"]));

          //Map url to ImageRawInfo and cache them!
          //Cache this for Image Links Viewer.
          _imageRawInfoCache[url] = newImageRawInfo;

          //Store this.
          imageLinks[i].setImageRawInfo(newImageRawInfo);
        }
      }
    }

    if (mounted) {
      setState(() {
        _downloadComplete = true;
      });
      widget.onDownloadComplete?.call();
    }

    // await Future.wait(
    //     imageLinks.map((e) async {
    //       String url = e.getUrl();
    //
    //       //Check the cache
    //       if (_imageRawInfoCache.containsKey(url)) {
    //         //We already have this guy. Saved some time.
    //         e.setImageRawInfo(_imageRawInfoCache[url]);
    //       } else {
    //         //Try probe the size of the image.
    //         http.Response headResponse = await http.head(url);
    //
    //         // print('url [' +
    //         //     url +
    //         //     '] content-length [' +
    //         //     headResponse.headers["content-length"] +
    //         //     ']');
    //
    //         http.Response response = await http.get(url);
    //         if (response != null) {
    //           //Also depend on dart.ui to find the images width/height.
    //           ui.Image image = await decodeImageFromList(response.bodyBytes);
    //           ImageRawInfo newImageRawInfo = ImageRawInfo(
    //               url,
    //               response.bodyBytes,
    //               image,
    //               int.tryParse(headResponse.headers["content-length"]));
    //
    //           //Map url to ImageRawInfo and cache them!
    //           //Cache this for Image Links Viewer.
    //           _imageRawInfoCache[url] = newImageRawInfo;
    //
    //           //Store this.
    //           e.setImageRawInfo(newImageRawInfo);
    //         }
    //       }
    //     }
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
        listeners: [
          BlocListener<SettingsDownloadFullImageChangedCubit,
              SettingsDownloadFullImageChangedState>(
            listener: (context, state) async {
              if (state is SettingsDownloadFullImageChangedSuccess) {
                clearImageCollectionCache();
                await _prepByImageCollectionType();
              }
            },
          ),
        ],
        child: buildImageCollection(context, widget.imageLinks, widget.onTap,
            widget.onDownloadComplete, widget.imageCollectionType));
  }

  Widget buildImageCollection(
      BuildContext context,
      List<PlurkCardImageLink> imageLinks,
      Function onTap,
      Function onDownloadComplete,
      ImageCollectionType type) {
    // print('imageLinks.length: ' + imageLinks.length.toString());

    //Try restore all links from cache first.
    for (int i = 0; i < imageLinks.length; ++i) {
      if (imageLinks[i].getImageRawInfo() == null) {
        String url = imageLinks[i].getImageUrl();
        //Check the cache
        if (_imageRawInfoCache.containsKey(url)) {
          //We already have this guy. Saved some time.
          imageLinks[i].setImageRawInfo(_imageRawInfoCache[url]);
        }
      }
    }

    if (type == ImageCollectionType.Summery) {
      //Summery: Need to limit display count by their size.
      bool linksCacheHavePassedLimit = false;
      int accumulateContentLength = 0;
      int linksHasContent = 0;
      for (int i = 0; i < imageLinks.length; ++i) {
        if (imageLinks[i].getImageRawInfo() != null) {
          accumulateContentLength +=
              imageLinks[i].getImageRawInfo().contentLength;
          linksHasContent += 1;
        }
      }

      if (accumulateContentLength >=
              PlurkCardImageCollection.SUMMERY_LIMIT_SIZE ||
          linksHasContent == imageLinks.length) {
        linksCacheHavePassedLimit = true;
      }

      if (linksCacheHavePassedLimit) {

        // print('[allLinksHaveOldCache] build directly!');

        //We can now construct all Image Widgets instantly (No FutureBuilder need)
        return _buildImageCollectionSummeryForLinks(context, imageLinks, onTap);
      } else {
        if (_downloadComplete) {
          //We can now construct all Image Widgets
          return _buildImageCollectionSummeryForLinks(
              context, imageLinks, onTap);
        } else {
          //Not finish yet!
          return _shimmerLoading();
        }
      }
    } else {
      //ExpandAll: Need to download all images!

      //Check if we have all cache. If yes we don't need a FutureBuilder.
      bool allLinksHaveOldCache = true;
      for (int i = 0; i < imageLinks.length; ++i) {
        if (imageLinks[i].getImageRawInfo() == null) {
          allLinksHaveOldCache = false;
          break;
        }
      }

      if (allLinksHaveOldCache) {
        //Just use these links to build. No need for a FutureBuilder...
        //Tell the boss we have download and cache all imageRawInfo complete.
        //This will emit a card background switch event cubit.
        onDownloadComplete();

//    print('[allLinksHaveOldCache] build directly!');

        //We can now construct all Image Widgets instantly (No FutureBuilder need)
        return _buildImageCollectionStripeForLinks(context, imageLinks, onTap);
      } else {
        if (_downloadComplete) {
          //We can now construct all Image Widgets
          if (type == ImageCollectionType.Summery) {
            return _buildImageCollectionSummeryForLinks(
                context, imageLinks, onTap);
          } else {
            return _buildImageCollectionStripeForLinks(
                context, imageLinks, onTap);
          }
        } else {
          //Not finish yet!
          return _shimmerLoading();
        }
      }
    }
  }

  Widget _buildImageCollectionSummeryForLinks(BuildContext context,
      List<PlurkCardImageLink> imageLinks, Function onTap) {
    //Limit displaying info to what we have in the link.
    List<PlurkCardImageLink> displayingLinks = [];

    //Get rid of null ImageRawInfo.
    imageLinks.forEach((imageLink) {
      if (imageLink.getImageRawInfo() != null && displayingLinks.length < 4) {
        displayingLinks.add(imageLink);
      }
    });

    //Decide Row-first or column first layout by average slope.
    //[Dep]: Nope, we should just use slope 1.
//        double averageSlope = ImageRawInfo.calculateAverageSlope(displayingLinks.map((e) => e.imageRawInfo).toList());

    if (displayingLinks.length == 1) {
      //No need for neat magic. Just 1 image.
      return LayoutBuilder(
        builder: (context, constraints) {
          //Decide the main column children
          List<Widget> mainColumnChildren = [];

          //Show hero.
          mainColumnChildren.add(
              Expanded(flex: 19, child: _buildHero(displayingLinks[0], onTap)));

          //Show _additionalCountDisplay?
          if (imageLinks.length > displayingLinks.length) {
            //Display the additional count widget
            mainColumnChildren.add(Expanded(
              flex: 1,
              child: _additionalCountDisplay(
                  context, imageLinks.length - displayingLinks.length),
            ));
          }

          return Container(
            height: constraints.maxWidth,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: mainColumnChildren,
            ),
          );
        },
      );
    } else {
      double firstSlope = ImageRawInfo.calculateAverageSlope(displayingLinks
          .sublist(0, 1)
          .map((e) => e.getImageRawInfo())
          .toList());

      //More then 1 images.
      //Split into two sets.
      int firstSetCount = displayingLinks.length ~/ 2;
      List<PlurkCardImageLink> firstSet =
          displayingLinks.sublist(0, firstSetCount);
      List<PlurkCardImageLink> secondSet =
          displayingLinks.sublist(firstSetCount);

      if (firstSlope <= 1.0) {
        //Decide the main column children
        List<Widget> mainColumnChildren = [];
        mainColumnChildren.add(Expanded(
          flex: 19,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: _buildImageCollectionRow(firstSet, onTap),
              ),
              Expanded(
                child: _buildImageCollectionRow(secondSet, onTap),
              )
            ],
          ),
        ));

        if (imageLinks.length > displayingLinks.length) {
          //Display the additional count widget
          mainColumnChildren.add(Expanded(
            flex: 1,
            child: _additionalCountDisplay(
                context, imageLinks.length - displayingLinks.length),
          ));
        }

        //Column first.
        return LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              height: constraints.maxWidth,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: mainColumnChildren,
              ),
            );
          },
        );
      } else {
        //Decide the main column children
        List<Widget> mainColumnChildren = [];
        mainColumnChildren.add(Expanded(
          flex: 19,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: _buildImageCollectionColumn(firstSet, onTap),
              ),
              Expanded(
                child: _buildImageCollectionColumn(secondSet, onTap),
              )
            ],
          ),
        ));

        if (imageLinks.length > displayingLinks.length) {
          //Display the additional count widget
          mainColumnChildren.add(Expanded(
            flex: 1,
            child: _additionalCountDisplay(
                context, imageLinks.length - displayingLinks.length),
          ));
        }

        //Row first.
        return LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              height: constraints.maxWidth,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: mainColumnChildren,
              ),
            );
          },
        );
      }
    }
  }

  Widget _additionalCountDisplay(BuildContext context, int count) {
    return Container(
      alignment: AlignmentDirectional.centerEnd,
      decoration: BoxDecoration(
//        border: Border(
//          bottom: BorderSide(
//            color: Theme.of(context).hintColor.withOpacity(0.2),
//          ),
//          left: BorderSide(
//            color: Theme.of(context).hintColor.withOpacity(0.2),
//          ),
//          right: BorderSide(
//            color: Theme.of(context).hintColor.withOpacity(0.2),
//          ),
//        ),
          ),
      child: Text(
        '[+' + count.toString() + "]",
        style: TextStyle(
          fontSize: 14,
        ),
      ),
    );
  }

  Row _buildImageCollectionRow(List<PlurkCardImageLink> links, Function onTap) {
    List<Widget> rowChildren = [];
    links.forEach((element) {
      rowChildren.add(Expanded(
        child: _buildHero(element, onTap),
        flex: element.getImageRawInfo().getReverseImageSlopeAsFlexRef(),
      ));
    });

    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: rowChildren,
    );
  }

  Column _buildImageCollectionColumn(
      List<PlurkCardImageLink> links, Function onTap) {
    List<Widget> columnChildren = [];
    links.forEach((element) {
      columnChildren.add(Expanded(
        child: _buildHero(element, onTap),
        flex: element.getImageRawInfo().getImageSlopeAsFlexRef(),
      ));
    });

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: columnChildren,
    );
  }

  Widget _buildHero(PlurkCardImageLink imageLink, Function onTap) {
    //Ok... sometimes when the network is bad and we cannot get a imageLink.imageRawInfo properly...
    if (imageLink.getImageRawInfo() != null) {
      //We're fine.
      return GestureDetector(
        child: Hero(
          tag: imageLink.getHeroTag(),
          child: FadeInImage(
            fadeInDuration: const Duration(milliseconds: 250),
            placeholder: MemoryImage(kTransparentImage),
            image: imageLink.getImageRawInfoProvider(),
            fit: BoxFit.cover,
          ),
        ),
        onTap: () {
          onTap(imageLink.getHeroTag());
        },
      );
    } else {
      //Oops! No good.
      return _shimmerLoading();
    }
  }

  // Widget _buildBusyPlaceHolder() {
  //   return LayoutBuilder(
  //     builder: (context, constraints) {
  //       return Container(
  //         height: constraints.maxWidth,
  //         child: Center(
  //           child: SizedBox(
  //             width: 128,
  //             height: 128,
  //             child: FlareActor(
  //               "assets/rive/MeatLoader.flr",
  //               animation: "Untitled",
  //               fit: BoxFit.scaleDown,
  //             ),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }

  Widget _shimmerLoading() {
    return LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            height: constraints.maxWidth,
            child: Shimmer.fromColors(
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 4,
                    ),
                  ),
                  child: Column(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.all(4),
                                color: Colors.white,
                              ),
                              flex: 3,
                            ),
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.all(4),
                                color: Colors.white,
                              ),
                              flex: 2,
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.all(4),
                                color: Colors.white,
                              ),
                              flex: 2,
                            ),
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.all(4),
                                color: Colors.white,
                              ),
                              flex: 3,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                baseColor: Theme.of(context).colorScheme.primary.withOpacity(0.4),
                highlightColor: Theme.of(context).colorScheme.secondary),
          );
        }
    );
  }

  Widget _buildImageCollectionStripeForLinks(BuildContext context,
      List<PlurkCardImageLink> imageLinks, Function onTap) {
    List<Widget> columnChildren = [];

    imageLinks.forEach((element) {
      double slope = element.getImageRawInfo().getImageSlope();
      if (slope < 1.0) {
        //We need this image be square because it's too wide.
        columnChildren.add(LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              margin:
                  EdgeInsets.symmetric(vertical: Define.verticalPadding * 0.5),
              height: constraints.maxWidth,
              child: _buildHero(element, onTap),
              //[Dep] I cannot center the scrollview...
              // child: SingleChildScrollView(
              //   scrollDirection: Axis.horizontal,
              //   //I don't know how to center the scrollView.
              //   // controller: ScrollController(initialScrollOffset: element.imageRawInfo.uiImage.width * 0.1),
              //   child: _buildHero(element, onTap),
              // ),
            );
          },
        ));
      } else {
        //Don't limit this one.
        columnChildren.add(Container(
          margin: EdgeInsets.symmetric(vertical: Define.verticalPadding * 0.5),
          child: _buildHero(element, onTap),
        ));
      }
    });

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: columnChildren,
      ),
    );
  }
}
