import 'package:plurdart/plurdart.dart' as Plurdart;

class UserToggle {
  UserToggle({this.user, this.toggle});
  Plurdart.User user;
  bool toggle = false;
}

class UsersCollection {
  UsersCollection();

  // All of the results goes here.
  List<Plurdart.User> _collection = [];

  // The helper collection that remember which users has been toggled.
  List<UserToggle> _collectionCache = [];

  String _filterTerm = '';

  // Add a new result here.
  void add(List<Plurdart.User> users) {
    users.forEach((user) {
      Plurdart.User foundUser;

      for (int i = 0; i < _collection.length; ++i) {
        if (_collection[i].id == user.id) {
          foundUser = _collection[i];
          break;
        }
      }

      if (foundUser == null) {
        //User not exist. Add it.
        _collection.add(user);
        _collectionCache.add(UserToggle(user: user, toggle: false));
      } //If user already exist just ignore it.
    });
  }

  setFilterTerm(String filterTerm) {
    _filterTerm = filterTerm;
  }

  // Clear all.
  void clear() {
    _collection.clear();
    _collectionCache.clear();
    _filterTerm = '';
  }

  //Return all users data and a toggled state.
  List<UserToggle> getUsersToggles() {
    //Check do we have a filter term
    if (_filterTerm != null && _filterTerm.isEmpty == false) {
      return _collectionCache
          .where((element) => (element.user.nickName
                  .toString()
                  .toLowerCase()
                  .contains(_filterTerm.toLowerCase()) ||
              element.user.displayName
                  .toLowerCase()
                  .contains(_filterTerm.toLowerCase())))
          .toList();
    } else {
      return _collectionCache;
    }
  }

  // How users to we have?
  int getUsersLength() {
    return _collection.length;
  }
}
