part of 'timeline_replurk_cubit.dart';

@immutable
abstract class TimelineReplurkState {}

class TimelineReplurkInitial extends TimelineReplurkState {}

class TimelineReplurkPressed extends TimelineReplurkInitial {}
class TimelineReplurkSuccess extends TimelineReplurkInitial {}
class TimelineReplurkFailed extends TimelineReplurkInitial {}

class TimelineUnReplurkPressed extends TimelineReplurkInitial {}
class TimelineUnReplurkSuccess extends TimelineReplurkInitial {}
class TimelineUnReplurkFailed extends TimelineReplurkInitial {}