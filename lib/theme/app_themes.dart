import 'package:flutter/material.dart';
import 'package:meat/utils/extensions.dart';

enum AppTheme {
  Pure,
  Pink,
  Red,
  DeepOrange,
  Orange,
  Amber,
  Yellow,
  Lime,
  LightGreen,
  Green,
  Teal,
  Cyan,
  LightBlue,
  Blue,
  Indigo,
  Purple,
  DeepPurple,
  BlueGrey,
  Brown,
  Grey,
}

enum FullTheme {
  Black,
  White,
  PinkLight,
  PinkDark,
  RedLight,
  RedDark,
  DeepOrangeLight,
  DeepOrangeDark,
  OrangeLight,
  OrangeDark,
  AmberLight,
  AmberDark,
  YellowLight,
  YellowDark,
  LimeLight,
  LimeDark,
  LightGreenLight,
  LightGreenDark,
  GreenLight,
  GreenDark,
  TealLight,
  TealDark,
  CyanLight,
  CyanDark,
  LightBlueLight,
  LightBlueDark,
  BlueLight,
  BlueDark,
  IndigoLight,
  IndigoDark,
  PurpleLight,
  PurpleDark,
  DeepPurpleLight,
  DeepPurpleDark,
  BlueGreyLight,
  BlueGreyDark,
  BrownLight,
  BrownDark,
  GreyLight,
  GreyDark,
}

//3 types of canvas color setting.
enum CanvasColor {
  LowContrast,    //(Default)
  HighContrast,
  Ivory,
  CornSilk,
  NightLight,
  ThemeAccent,
}

// A generic theme builder.
ThemeData _buildTheme(
  Brightness brightness,
  MaterialColor primaryColor,
  Color accentColor,
  Color primaryIconColor,
  Color accentIconColor,
  CanvasColor canvasColor,
  double fontSizeFactor,
) {

  // print('_buildTheme');

  TextTheme textTheme = TextTheme (
    headline1: TextStyle (
      fontSize: 112,
      height: 1.25,
    ),
    headline2: TextStyle (
      fontSize: 56,
      height: 1.25,
    ),
    headline3: TextStyle (
      fontSize: 45,
      height: 1.25,
    ),
    headline4: TextStyle (
      fontSize: 34,
      height: 1.25,
    ),
    headline5: TextStyle (
      fontSize: 24,
      height: 1.25,
    ),
    headline6: TextStyle (
      fontSize: 20,
      height: 1.25,
    ),
    subtitle1: TextStyle (
      fontSize: 16,
      height: 1.25,
    ),
    subtitle2: TextStyle (
      fontSize: 14,
      height: 1.25,
    ),
    bodyText1: TextStyle (
      fontSize: 14,
      height: 1.25,
    ),
    bodyText2: TextStyle (
      fontSize: 14,
      height: 1.25,
    ),
    caption: TextStyle (
      fontSize: 12,
      height: 1.25,
    ),
    button: TextStyle (
      fontSize: 14,
      height: 1.25,
    ),
    overline: TextStyle (
      fontSize: 10,
      height: 1.25,
    ),
  ).apply(fontSizeFactor: fontSizeFactor);

  //Decide canvasColor logic.
  // This is for AMOLED black and pure white canvas.
  Color useCanvasColor = (brightness == Brightness.dark) ? (Colors.black) : (Colors.white);

  switch(canvasColor) {
    case CanvasColor.LowContrast:
      useCanvasColor = (brightness == Brightness.dark) ? (Colors.grey.shade800) : (Colors.grey.shade300);
      break;
    case CanvasColor.Ivory:
      useCanvasColor = (brightness == Brightness.dark) ? (HexColor.fromHex('#00000f', Colors.black)) : (HexColor.fromHex('#fffff0', Colors.white));
      break;
    case CanvasColor.CornSilk:
      useCanvasColor = (brightness == Brightness.dark) ? (HexColor.fromHex('#000723', Colors.black)) : (HexColor.fromHex('#fff8dc', Colors.white));
      break;
    case CanvasColor.NightLight:
      useCanvasColor = (brightness == Brightness.dark) ? (HexColor.fromHex('#1e3966', Colors.black)) : (HexColor.fromHex('#e1c699', Colors.white));
      break;
    case CanvasColor.ThemeAccent:
      useCanvasColor = (brightness == Brightness.dark) ? (primaryColor.shade900) : (primaryColor.shade50);
      break;
    default:
      break;
  }

  // This is for AMOLED black and pure white canvas.
  // canvasColor: (brightness == Brightness.dark) ? (Colors.black) : (Colors.white),
  //The is accent setting
  // (brightness == Brightness.dark) ? (primaryColor.shade50.darken(88)) : (primaryColor.shade50),

  ThemeData newThemeData = ThemeData(
    brightness: brightness,
    primarySwatch: primaryColor,
    primaryColor: primaryColor,
    primaryColorDark: primaryColor[700],
    primaryColorLight: primaryColor[300],
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: primaryColor,
      primaryColorDark: primaryColor[700],
      accentColor: accentColor.withOpacity(0.75),
      brightness: brightness,
    ),
    textSelectionTheme: TextSelectionThemeData(
      selectionHandleColor: primaryColor,
    ),
    toggleableActiveColor: accentColor.withOpacity(0.75),
    canvasColor: useCanvasColor,
    primaryIconTheme: IconThemeData(color: primaryIconColor),
    snackBarTheme: SnackBarThemeData(

    ),
    fontFamily: 'Huninn',
    textTheme: textTheme,
    splashFactory: InkRipple.splashFactory,
  );

  return newThemeData;
}

MaterialColor _black = MaterialColor(
  0xFF000000,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    350: Color(
        0xFF000000), // only for raised button while pressed in light theme
    400: Color(0xFF000000),
    500: Color(0xFF000000),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    850: Color(0xFF000000), // only for background color in dark theme
    900: Color(0xFF000000),
  },
);

MaterialColor _white = MaterialColor(
  0xFFFFFFFF,
  <int, Color>{
    50: Color(0xFFFFFFFF),
    100: Color(0xFFFFFFFF),
    200: Color(0xFFFFFFFF),
    300: Color(0xFFFFFFFF),
    350: Color(
        0xFFFFFFFF), // only for raised button while pressed in light theme
    400: Color(0xFFFFFFFF),
    500: Color(0xFFFFFFFF),
    600: Color(0xFFFFFFFF),
    700: Color(0xFFFFFFFF),
    800: Color(0xFFFFFFFF),
    850: Color(0xFFFFFFFF), // only for background color in dark theme
    900: Color(0xFFFFFFFF),
  },
);

ThemeData _getFromFullTheme(
    FullTheme fullTheme,
    CanvasColor canvasColor,
    double fontSizeFactor) {
  switch (fullTheme) {
    //Black
    case FullTheme.Black:
      return _buildTheme(Brightness.dark, _black, Colors.grey, Colors.white, Colors.white, canvasColor, fontSizeFactor);

    //White
    case FullTheme.White:
      return _buildTheme(Brightness.light, _white, Colors.blueGrey, Colors.black, Colors.black, canvasColor, fontSizeFactor);

    //Pink
    case FullTheme.PinkLight:
      return _buildTheme(Brightness.light, Colors.pink, Colors.pinkAccent, Colors.pink, Colors.pinkAccent, canvasColor, fontSizeFactor);
    case FullTheme.PinkDark:
      return _buildTheme(Brightness.dark, Colors.pink, Colors.pinkAccent, Colors.pink, Colors.pinkAccent, canvasColor, fontSizeFactor);

    //Red
    case FullTheme.RedLight:
      return _buildTheme(Brightness.light, Colors.red, Colors.redAccent, Colors.red, Colors.redAccent, canvasColor, fontSizeFactor);
    case FullTheme.RedDark:
      return _buildTheme(Brightness.dark, Colors.red, Colors.redAccent, Colors.red, Colors.redAccent, canvasColor, fontSizeFactor);

    //DeepOrange
    case FullTheme.DeepOrangeLight:
      return _buildTheme(Brightness.light, Colors.deepOrange, Colors.deepOrangeAccent, Colors.deepOrange, Colors.deepOrangeAccent, canvasColor, fontSizeFactor);
    case FullTheme.DeepOrangeDark:
      return _buildTheme(Brightness.dark, Colors.deepOrange, Colors.deepOrangeAccent, Colors.deepOrange, Colors.deepOrangeAccent, canvasColor, fontSizeFactor);

    //Orange
    case FullTheme.OrangeLight:
      return _buildTheme(Brightness.light, Colors.orange, Colors.orangeAccent, Colors.orange, Colors.orangeAccent, canvasColor, fontSizeFactor);
    case FullTheme.OrangeDark:
      return _buildTheme(Brightness.dark, Colors.orange, Colors.orangeAccent, Colors.orange, Colors.orangeAccent, canvasColor, fontSizeFactor);

    //Amber
    case FullTheme.AmberLight:
      return _buildTheme(Brightness.light, Colors.amber, Colors.amberAccent, Colors.amber, Colors.amberAccent, canvasColor, fontSizeFactor);
    case FullTheme.AmberDark:
      return _buildTheme(Brightness.dark, Colors.amber, Colors.amberAccent, Colors.amber, Colors.amberAccent, canvasColor, fontSizeFactor);

    //Yellow
    case FullTheme.YellowLight:
      return _buildTheme(Brightness.light, Colors.yellow, Colors.yellowAccent, Colors.yellow, Colors.yellowAccent, canvasColor, fontSizeFactor);
    case FullTheme.YellowDark:
      return _buildTheme(Brightness.dark, Colors.yellow, Colors.yellowAccent, Colors.yellow, Colors.yellowAccent, canvasColor, fontSizeFactor);

    //Lime
    case FullTheme.LimeLight:
      return _buildTheme(Brightness.light, Colors.lime, Colors.limeAccent, Colors.lime, Colors.limeAccent, canvasColor, fontSizeFactor);
    case FullTheme.LimeDark:
      return _buildTheme(Brightness.dark, Colors.lime, Colors.limeAccent, Colors.lime, Colors.limeAccent, canvasColor, fontSizeFactor);

    //LightGreen
    case FullTheme.LightGreenLight:
      return _buildTheme(Brightness.light, Colors.lightGreen, Colors.lightGreenAccent, Colors.lightGreen, Colors.lightGreenAccent, canvasColor, fontSizeFactor);
    case FullTheme.LightGreenDark:
      return _buildTheme(Brightness.dark, Colors.lightGreen, Colors.lightGreenAccent, Colors.lightGreen, Colors.lightGreenAccent, canvasColor, fontSizeFactor);

    //Green
    case FullTheme.GreenLight:
      return _buildTheme(Brightness.light, Colors.green, Colors.greenAccent, Colors.green, Colors.greenAccent, canvasColor, fontSizeFactor);
    case FullTheme.GreenDark:
      return _buildTheme(Brightness.dark, Colors.green, Colors.greenAccent, Colors.green, Colors.greenAccent, canvasColor, fontSizeFactor);

    //Teal
    case FullTheme.TealLight:
      return _buildTheme(Brightness.light, Colors.teal, Colors.tealAccent, Colors.teal, Colors.tealAccent, canvasColor, fontSizeFactor);
    case FullTheme.TealDark:
      return _buildTheme(Brightness.dark, Colors.teal, Colors.tealAccent, Colors.teal, Colors.tealAccent, canvasColor, fontSizeFactor);

    //Cyan
    case FullTheme.CyanLight:
      return _buildTheme(Brightness.light, Colors.cyan, Colors.cyanAccent, Colors.cyan, Colors.cyanAccent, canvasColor, fontSizeFactor);
    case FullTheme.CyanDark:
      return _buildTheme(Brightness.dark, Colors.cyan, Colors.cyanAccent, Colors.cyan, Colors.cyanAccent, canvasColor, fontSizeFactor);

    //LightBlue
    case FullTheme.LightBlueLight:
      return _buildTheme(Brightness.light, Colors.lightBlue, Colors.lightBlueAccent, Colors.lightBlue, Colors.lightBlueAccent, canvasColor, fontSizeFactor);
    case FullTheme.LightBlueDark:
      return _buildTheme(Brightness.dark, Colors.lightBlue, Colors.lightBlueAccent, Colors.lightBlue, Colors.lightBlueAccent, canvasColor, fontSizeFactor);

    //Blue
    case FullTheme.BlueLight:
      return _buildTheme(Brightness.light, Colors.blue, Colors.blueAccent, Colors.blue, Colors.blueAccent, canvasColor, fontSizeFactor);
    case FullTheme.BlueDark:
      return _buildTheme(Brightness.dark, Colors.blue, Colors.blueAccent, Colors.blue, Colors.blueAccent, canvasColor, fontSizeFactor);

    //Indigo
    case FullTheme.IndigoLight:
      return _buildTheme(Brightness.light, Colors.indigo, Colors.indigoAccent, Colors.indigo, Colors.indigoAccent, canvasColor, fontSizeFactor);
    case FullTheme.IndigoDark:
      return _buildTheme(Brightness.dark, Colors.indigo, Colors.indigoAccent, Colors.indigo, Colors.indigoAccent, canvasColor, fontSizeFactor);

    //Purple
    case FullTheme.PurpleLight:
      return _buildTheme(Brightness.light, Colors.purple, Colors.purpleAccent, Colors.purple, Colors.purpleAccent, canvasColor, fontSizeFactor);
    case FullTheme.PurpleDark:
      return _buildTheme(Brightness.dark, Colors.purple, Colors.purpleAccent, Colors.purple, Colors.purpleAccent, canvasColor, fontSizeFactor);

    //DeepPurple
    case FullTheme.DeepPurpleLight:
      return _buildTheme(Brightness.light, Colors.deepPurple, Colors.deepPurpleAccent, Colors.deepPurple, Colors.deepPurpleAccent, canvasColor, fontSizeFactor);
    case FullTheme.DeepPurpleDark:
      return _buildTheme(Brightness.dark, Colors.deepPurple, Colors.deepPurpleAccent, Colors.deepPurple, Colors.deepPurpleAccent, canvasColor, fontSizeFactor);

    //BlueGrey
    case FullTheme.BlueGreyLight:
      return _buildTheme(Brightness.light, Colors.blueGrey, Colors.blueGrey[400], Colors.blueGrey, Colors.blueGrey[400], canvasColor, fontSizeFactor);
    case FullTheme.BlueGreyDark:
      return _buildTheme(Brightness.dark, Colors.blueGrey, Colors.blueGrey[400], Colors.blueGrey, Colors.blueGrey[400], canvasColor, fontSizeFactor);

    //Brown
    case FullTheme.BrownLight:
      return _buildTheme(Brightness.light, Colors.brown, Colors.brown[400], Colors.brown, Colors.brown[400], canvasColor, fontSizeFactor);
    case FullTheme.BrownDark:
      return _buildTheme(Brightness.dark, Colors.brown, Colors.brown[400], Colors.brown, Colors.brown[400], canvasColor, fontSizeFactor);

    //Grey
    case FullTheme.GreyLight:
      return _buildTheme(Brightness.light, Colors.grey, Colors.grey[400], Colors.grey, Colors.grey[400], canvasColor, fontSizeFactor);
    case FullTheme.GreyDark:
      return _buildTheme(Brightness.dark, Colors.grey, Colors.grey[400], Colors.grey, Colors.grey[400], canvasColor, fontSizeFactor);

    default:
      return _buildTheme(Brightness.light, Colors.orange, Colors.orangeAccent, Colors.orange, Colors.orangeAccent, canvasColor, fontSizeFactor);
  }
}

ThemeData _getLightTheme(
    AppTheme appTheme,
    CanvasColor canvasColor,
    double fontSizeFactor) {
  switch (appTheme) {
    case AppTheme.Pure: return _getFromFullTheme(FullTheme.White, canvasColor, fontSizeFactor);
    case AppTheme.Pink: return _getFromFullTheme(FullTheme.PinkLight, canvasColor, fontSizeFactor);
    case AppTheme.Red: return _getFromFullTheme(FullTheme.RedLight, canvasColor, fontSizeFactor);
    case AppTheme.DeepOrange: return _getFromFullTheme(FullTheme.DeepOrangeLight, canvasColor, fontSizeFactor);
    case AppTheme.Orange: return _getFromFullTheme(FullTheme.OrangeLight, canvasColor, fontSizeFactor);
    case AppTheme.Amber: return _getFromFullTheme(FullTheme.AmberLight, canvasColor, fontSizeFactor);
    case AppTheme.Yellow: return _getFromFullTheme(FullTheme.YellowLight, canvasColor, fontSizeFactor);
    case AppTheme.Lime: return _getFromFullTheme(FullTheme.LimeLight, canvasColor, fontSizeFactor);
    case AppTheme.LightGreen: return _getFromFullTheme(FullTheme.LightGreenLight, canvasColor, fontSizeFactor);
    case AppTheme.Green: return _getFromFullTheme(FullTheme.GreenLight, canvasColor, fontSizeFactor);
    case AppTheme.Teal: return _getFromFullTheme(FullTheme.TealLight, canvasColor, fontSizeFactor);
    case AppTheme.Cyan: return _getFromFullTheme(FullTheme.CyanLight, canvasColor, fontSizeFactor);
    case AppTheme.LightBlue: return _getFromFullTheme(FullTheme.LightBlueLight, canvasColor, fontSizeFactor);
    case AppTheme.Blue: return _getFromFullTheme(FullTheme.BlueLight, canvasColor, fontSizeFactor);
    case AppTheme.Indigo: return _getFromFullTheme(FullTheme.IndigoLight, canvasColor, fontSizeFactor);
    case AppTheme.Purple: return _getFromFullTheme(FullTheme.PurpleLight, canvasColor, fontSizeFactor);
    case AppTheme.DeepPurple: return _getFromFullTheme(FullTheme.DeepPurpleLight, canvasColor, fontSizeFactor);
    case AppTheme.BlueGrey: return _getFromFullTheme(FullTheme.BlueGreyLight, canvasColor, fontSizeFactor);
    case AppTheme.Brown: return _getFromFullTheme(FullTheme.BrownLight, canvasColor, fontSizeFactor);
    case AppTheme.Grey: return _getFromFullTheme(FullTheme.GreyLight, canvasColor, fontSizeFactor);
    default: return _getFromFullTheme(FullTheme.OrangeLight, canvasColor, fontSizeFactor);
  }
}

ThemeData _getDarkTheme(
    AppTheme appTheme,
    CanvasColor canvasColor,
    double fontSizeFactor) {
  switch (appTheme) {
    case AppTheme.Pure: return _getFromFullTheme(FullTheme.Black, canvasColor, fontSizeFactor);
    case AppTheme.Pink: return _getFromFullTheme(FullTheme.PinkDark, canvasColor, fontSizeFactor);
    case AppTheme.Red: return _getFromFullTheme(FullTheme.RedDark, canvasColor, fontSizeFactor);
    case AppTheme.DeepOrange: return _getFromFullTheme(FullTheme.DeepOrangeDark, canvasColor, fontSizeFactor);
    case AppTheme.Orange: return _getFromFullTheme(FullTheme.OrangeDark, canvasColor, fontSizeFactor);
    case AppTheme.Amber: return _getFromFullTheme(FullTheme.AmberDark, canvasColor, fontSizeFactor);
    case AppTheme.Yellow: return _getFromFullTheme(FullTheme.YellowDark, canvasColor, fontSizeFactor);
    case AppTheme.Lime: return _getFromFullTheme(FullTheme.LimeDark, canvasColor, fontSizeFactor);
    case AppTheme.LightGreen: return _getFromFullTheme(FullTheme.LightGreenDark, canvasColor, fontSizeFactor);
    case AppTheme.Green: return _getFromFullTheme(FullTheme.GreenDark, canvasColor, fontSizeFactor);
    case AppTheme.Teal: return _getFromFullTheme(FullTheme.TealDark, canvasColor, fontSizeFactor);
    case AppTheme.Cyan: return _getFromFullTheme(FullTheme.CyanDark, canvasColor, fontSizeFactor);
    case AppTheme.LightBlue: return _getFromFullTheme(FullTheme.LightBlueDark, canvasColor, fontSizeFactor);
    case AppTheme.Blue: return _getFromFullTheme(FullTheme.BlueDark, canvasColor, fontSizeFactor);
    case AppTheme.Indigo: return _getFromFullTheme(FullTheme.IndigoDark, canvasColor, fontSizeFactor);
    case AppTheme.Purple: return _getFromFullTheme(FullTheme.PurpleDark, canvasColor, fontSizeFactor);
    case AppTheme.DeepPurple: return _getFromFullTheme(FullTheme.DeepPurpleDark, canvasColor, fontSizeFactor);
    case AppTheme.BlueGrey: return _getFromFullTheme(FullTheme.BlueGreyDark, canvasColor, fontSizeFactor);
    case AppTheme.Brown: return _getFromFullTheme(FullTheme.BrownDark, canvasColor, fontSizeFactor);
    case AppTheme.Grey: return _getFromFullTheme(FullTheme.GreyDark, canvasColor, fontSizeFactor);
    default: return _getFromFullTheme(FullTheme.OrangeDark, canvasColor, fontSizeFactor);
  }
}

ThemeData getThemeData(Brightness brightness,
    AppTheme appTheme,
    CanvasColor canvasColor,
    double fontSizeFactor) {

  if (brightness == Brightness.dark) {
    return _getDarkTheme(appTheme, canvasColor, fontSizeFactor);
  } else {
    return _getLightTheme(appTheme, canvasColor, fontSizeFactor);
  }
}

extension ThemeDataExt on ThemeData {
  //Return the low contrast version of current primary color.
  Color get lowContrastPrimaryColor {
    if (brightness == Brightness.dark) {
      return this.primaryColorDark;
    } else {
      return this.primaryColorLight;
    }
  }

  Color get highContrastPrimaryColor {
    if (brightness == Brightness.dark) {
      return this.primaryColorLight;
    } else {
      return this.primaryColorDark;
    }
  }
}