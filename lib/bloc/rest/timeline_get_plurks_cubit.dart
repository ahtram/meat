import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'timeline_get_plurks_state.dart';

class TimelineGetPlurksCubit extends Cubit<TimelineGetPlurksState> {
  TimelineGetPlurksCubit() : super(TimelineGetPlurksInitial());

  //Refresh timeline
  Future getPlurks(Plurdart.TimelineGetPlurks timelineGetPlurks, {int repeatTimes = 1, bool isViewingUnreadOnly = false}) async {
    List<Plurdart.Plurks> plurksList = [];

    for (int i = 0 ; i < repeatTimes ; ++i) {
      Plurdart.Plurks plurks;

      //Cheat here
      if (isViewingUnreadOnly) {
        plurks = await Plurdart.timelineGetUnreadPlurks(timelineGetPlurks);
      } else {
        plurks = await Plurdart.timelineGetPlurks(timelineGetPlurks);
      }

      //Collect Users to global User Cache.
      if (plurks != null) {
        plurks.plurkUsers.forEach((key, value) {
          // print('TimelineGetPlurksCubit addToUserCache: ' + value.id.toString());
          Static.addToUserCache(value);
        });
      }

      //Ignore those empty
      if (plurks.plurks.length > 0) {
        plurksList.add(plurks);

        //Alter the offset to last one!
        Plurdart.Plurk lastPlurk = plurks.plurks.last;
        timelineGetPlurks.offset = DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parse(lastPlurk.posted);
      }
    }

    emit(TimelineGetPlurksSuccess(plurksList));
  }

  //Polling get new plurks
  void getNewPlurks(Plurdart.PollingGetPlurks pollingGetPlurks) async {
    Plurdart.Plurks plurks = await Plurdart.pollingGetPlurks(pollingGetPlurks);
    emit(TimelineGetNewPlurksSuccess(plurks));
  }

}
