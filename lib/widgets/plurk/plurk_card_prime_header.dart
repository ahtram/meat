import 'package:cached_network_image/cached_network_image.dart';
import 'package:clipboard/clipboard.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:intl/intl.dart';
import 'package:meat/bloc/rest/timeline_mute_plurks_cubit.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/utils/extensions.dart';
import 'package:meat/widgets/other/plurk_coin_sign.dart';
import 'package:meat/widgets/plurk/plurk_card_action_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:strings/strings.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:share_plus/share_plus.dart';
import 'package:meat/screens/plurk_poster.dart' as PlurkPoster;
import 'package:meat/screens/send_gift.dart' as SendGift;

enum PlurkMenuAction {
  Share,
  Mute,
  Unmute,
  CopyLink,
  QuoteThisPlurk,
  CopyTheContent,
  SendGift,
  Edit,
  Delete,
  Pin,
  Unpin,
  ReportAbuse,
}

class PlurkCardPrimeHeader extends StatefulWidget {
  PlurkCardPrimeHeader(this.plurk, this.owner, this.replurker, this.showPinIcon,
      this.withNameShadow,
      {this.onUserNameTap,
      this.onReplurkerNameTap,
      this.onAvatarTap,
      this.onWantEditPlurk,
      this.onWantDeletePlurk,
      this.onWantPinPlurk,
      this.onWantUnpinPlurk,
      this.onWantReportAbuse,
      this.onWantEditBookmarkTags,
      Key key})
      : super(key: key);

  final Plurdart.Plurk plurk;
  final Plurdart.User owner;
  final Plurdart.User replurker;

  final bool showPinIcon;
  final bool withNameShadow;
  final Function onUserNameTap;
  final Function onReplurkerNameTap;
  final Function onAvatarTap;
  final Function(Plurdart.Plurk) onWantEditPlurk;
  final Function(Plurdart.Plurk) onWantDeletePlurk;
  final Function(Plurdart.Plurk) onWantPinPlurk;
  final Function(Plurdart.Plurk) onWantUnpinPlurk;
  final Function(int) onWantReportAbuse;
  final Function onWantEditBookmarkTags;

  @override
  PlurkCardPrimeHeaderState createState() => PlurkCardPrimeHeaderState();
}

class PlurkCardPrimeHeaderState extends State<PlurkCardPrimeHeader> {
  BuildContext _blocProviderContext;
  GlobalKey<PopupMenuButtonState> _popupMenuKey =
      GlobalKey<PopupMenuButtonState>();

  bool _isBookmarking = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> rowChildren = [];

    // Left Avatar
    rowChildren.add(headerAvatar(context, widget.owner,
        margin: EdgeInsets.fromLTRB(0, 4, 4, 4), onTap: (user) {
      widget.onAvatarTap?.call(user);
    }));

    //Mid
    rowChildren.add(Expanded(
      child: Container(
        margin: EdgeInsets.all(3),
        height: 48,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            //Upper line: post time + replurker
            Flexible(
              flex: 1,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: _buildHeaderPostedTimeReplurker(
                    context,
                    widget.plurk,
                    widget.replurker,
                    widget.withNameShadow,
                    widget.onReplurkerNameTap),
              ),
            ),

            //Bottom line: displayName + qualifier.
            Flexible(
              flex: 1,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: _buildHeaderDisplayNameAndQualifierRow(
                    context,
                    widget.plurk,
                    widget.owner,
                    widget.withNameShadow,
                    widget.onUserNameTap),
              ),
            ),
          ],
        ),
      ),
    ));

    rowChildren.add(SizedBox(
      width: 8,
    ));

    //Adult only icon
    if (widget.plurk.porn) {
      rowChildren.add(SizedBox(
        width: 24,
        height: 24,
        child: Center(
          child: Icon(
            Icons.warning,
            size: 20,
            color: Theme.of(context).iconTheme.color,
          ),
        ),
      ));
    }

    //Muted icon
    //Check muted
    if (widget.plurk.isUnread == 2) {
      rowChildren.add(SizedBox(
        width: 24,
        height: 24,
        child: Center(
          child: Icon(
            Icons.volume_off,
            size: 20,
            color: Theme.of(context).iconTheme.color,
          ),
        ),
      ));
    }

    //Plurk coin icon.
    if (widget.plurk.coins != null && widget.plurk.coins > 0) {
      //We got a plurk coin!
      rowChildren.add(PlurkCoinSign(widget.plurk.coins));
    }

    //Pin icon
    if (widget.showPinIcon) {
      rowChildren.add(SizedBox(
        width: 24,
        height: 24,
        child: Center(
          child: Icon(
            MdiIcons.pin,
            size: 24,
            color: Theme.of(context).iconTheme.color,
          ),
        ),
      ));
    }

    //Bookmark button.
    //Do we have coin? The bookmark button require we have a Plurk coin.
    if (Static.meIsPremium()) {
      rowChildren.add(PlurkCardActionButton(
        Icons.star,
        widget.plurk.bookmark,
        Colors.yellow,
        iconSize: 24,
        onTap: () async {
          if (!_isBookmarking) {
            Vibrator.vibrateShortNote();
            if (!widget.plurk.bookmark) {
              //Do set bookmark.
              _isBookmarking = true;
              Plurdart.Bookmark bookmark =
                  await Plurdart.bookmarksSetBookmark(
                      Plurdart.BookmarksSetBookmark(
                plurkID: widget.plurk.plurkId,
                bookmark: true,
              ));

              if (bookmark != null && !bookmark.hasError()) {
                setState(() {
                  widget.plurk.bookmark = true;
                });
              }
              _isBookmarking = false;
            }

            //Do selection tags.
            widget.onWantEditBookmarkTags?.call();
          }
        },
        onLongPress: () async {
          if (!_isBookmarking) {
            Vibrator.vibrateShortNote();
            if (widget.plurk.bookmark) {
              //Do set bookmark.
              _isBookmarking = true;
              Plurdart.Bookmark bookmark =
                  await Plurdart.bookmarksSetBookmark(
                      Plurdart.BookmarksSetBookmark(
                plurkID: widget.plurk.plurkId,
                bookmark: false,
              ));

              if (bookmark != null && !bookmark.hasError()) {
                setState(() {
                  widget.plurk.bookmark = false;
                });
              }
              _isBookmarking = false;
            }
          }
        },
      ));
    }

    //Right button
    //This is the best solution.
    rowChildren.add(PopupMenuButton(
        key: _popupMenuKey,
        child: SizedBox(
          width: 36,
          height: 36,
          child: Icon(
            Icons.expand_more,
            size: 24,
            color: Theme.of(context).iconTheme.color,
          ),
        ),
        onSelected: (result) {
          //These are very plain actions so we'll just do it here...
          switch (result) {
            case PlurkMenuAction.Share:
              //Share the plurk post. (OS share menu)
              Share.share(Define.getPlurkPostLink(widget.plurk.plurkId));
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.Mute:
              //Mute the plurk
              _blocProviderContext
                  .read<TimelineMutePlurksCubit>()
                  .mutePlurk(widget.plurk.plurkId);
              //The cheap way.
              setState(() {
                widget.plurk.isUnread = 2;
              });
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.Unmute:
              _blocProviderContext
                  .read<TimelineMutePlurksCubit>()
                  .unmutePlurk(widget.plurk.plurkId);
              //The cheap way.
              setState(() {
                widget.plurk.isUnread = 0;
              });
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.CopyLink:
              FlutterClipboard.copy(Define.getPlurkPostLink(widget.plurk.plurkId));
              context
                  .read<FlushBarCubit>()
                  .request(Icons.content_copy, 'Success!', 'Link copied.');
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.QuoteThisPlurk:
              //Open Plurk Poster for new plurk.
              PlurkPoster.show(
                  context,
                  PlurkPoster.PlurkPosterArgs(
                    PlurkPoster.PlurkPosterMode.New,
                    initialContent: Define.getPlurkPostLink(widget.plurk.plurkId),
                  ));
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.CopyTheContent:
              FlutterClipboard.copy(widget.plurk.contentRaw);
              context
                  .read<FlushBarCubit>()
                  .request(Icons.content_copy, 'Success!', 'Content copied.');
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.SendGift:
              //Open the SendGift dialog.
              SendGift.show(
                context,
                plurkId: widget.plurk.plurkId
              );
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.Edit:
              widget.onWantEditPlurk?.call(widget.plurk);
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.Delete:
              widget.onWantDeletePlurk?.call(widget.plurk);
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.Pin:
              widget.onWantPinPlurk?.call(widget.plurk);
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.Unpin:
              widget.onWantUnpinPlurk?.call(widget.plurk);
              Vibrator.vibrateShortNote();
              break;
            case PlurkMenuAction.ReportAbuse:
              widget.onWantReportAbuse?.call(widget.plurk.plurkId);
              Vibrator.vibrateShortNote();
              break;
          }
        },
        itemBuilder: (BuildContext context) {
          Vibrator.vibrateShortNote();
          List<PopupMenuEntry> returnWidgets = [];

          //[Dep]
          //Mute or share button??
          // if (Static.settingsDisplayMuteButtonAtBottomRight) {
          //   //Share button.
          //   returnWidgets.add(menuItem(PlurkMenuAction.Share, Icons.share,
          //       FlutterI18n.translate(context, 'dropdownShare')));
          // } else {
          //   //Mute / Unmute button.
          //   //Mute
          //   if (widget.plurk.isUnread != 2) {
          //     returnWidgets.add(menuItem(
          //         PlurkMenuAction.Mute,
          //         Icons.volume_off_outlined,
          //         FlutterI18n.translate(context, 'dropdownMute')));
          //   }
          //
          //   //Unmute
          //   if (widget.plurk.isUnread == 2) {
          //     returnWidgets.add(menuItem(
          //         PlurkMenuAction.Unmute,
          //         Icons.volume_up_outlined,
          //         FlutterI18n.translate(context, 'dropdownUnmute')));
          //   }
          // }

          //Tricky: this is what I found work correctly!
          bool thisIsMyPlurk = false;
          if (!widget.plurk.anonymous) {
            thisIsMyPlurk = widget.plurk.ownerId == Static.meId;
          } else {
            //Anonymous plurk: userId.
            thisIsMyPlurk = widget.plurk.userId == Static.meId;
          }

          if (thisIsMyPlurk && Static.meIsPremium()) {
            //That's mine. I can pin/unpin this if I want.
            if (Static.me.pinnedPlurkId == widget.plurk.plurkId) {
              returnWidgets.add(menuItem(PlurkMenuAction.Unpin, Icons.push_pin_outlined,
                  FlutterI18n.translate(context, 'dropdownUnpin')));
            } else {
              returnWidgets.add(menuItem(PlurkMenuAction.Pin, Icons.push_pin,
                  FlutterI18n.translate(context, 'dropdownPin')));
            }
          }

          returnWidgets.add(menuItem(PlurkMenuAction.CopyLink, Icons.link,
              FlutterI18n.translate(context, 'dropdownCopyLink')));

          returnWidgets.add(menuItem(
              PlurkMenuAction.QuoteThisPlurk,
              Icons.format_quote,
              FlutterI18n.translate(context, 'dropdownQuoteThisPlurk')));

          returnWidgets.add(menuItem(
              PlurkMenuAction.CopyTheContent,
              Icons.content_copy,
              FlutterI18n.translate(context, 'dropdownCopyTheContent')));

          if (Static.canSendGift()) {
            returnWidgets.add(menuItem(
                PlurkMenuAction.SendGift,
                MdiIcons.giftOutline,
                FlutterI18n.translate(context, 'dropdownSendGift')));
          }

          //Is this plurk mine? (Delete button)
          if (thisIsMyPlurk) {
            returnWidgets.add(menuItem(PlurkMenuAction.Edit, Icons.edit,
                FlutterI18n.translate(context, 'dropdownEdit')));
          }

          //Is this plurk mine? (Delete button)
          if (thisIsMyPlurk) {
            //That's mine. I can delete this if I want.
            returnWidgets.add(menuItem(PlurkMenuAction.Delete, Icons.delete,
                FlutterI18n.translate(context, 'dropdownDelete')));
          }

          //Report abuse? (only when this is not my plurk)
          if (!thisIsMyPlurk) {
            returnWidgets.add(menuItem(
                PlurkMenuAction.ReportAbuse,
                MdiIcons.alertOctagramOutline,
                FlutterI18n.translate(context, 'dropdownReportAbuse')));
          }

          return returnWidgets;
        }));

    return MultiBlocProvider(
      providers: [
        BlocProvider<TimelineMutePlurksCubit>(
          create: (context) => TimelineMutePlurksCubit(),
        )
      ],
      child: Builder(builder: (context) {
        _blocProviderContext = context;
        return Container(
          height: kToolbarHeight,
          child: Row(
            children: rowChildren,
          ),
        );
      }),
    );
  }

  PopupMenuItem menuItem(dynamic value, IconData icon, String text) {
    return PopupMenuItem(
      value: value,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: Theme.of(context).iconTheme.color,
          ),
          SizedBox(
            width: 8,
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 5), child: Text(text)),
        ],
      ),
    );
  }

  //This will show the button menu on the header.
  showButtonMenu() {
    if (_popupMenuKey != null) {
      _popupMenuKey.currentState.showButtonMenu();
    }
  }

  static Widget headerAvatar(BuildContext context, Plurdart.User user,
      {EdgeInsets margin,
      Function onTap,
      HitTestBehavior hitTestBehavior = HitTestBehavior.deferToChild}) {
    if (user != null) {
      return GestureDetector(
        behavior: hitTestBehavior,
        child: Container(
          margin: margin,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            backgroundImage: CachedNetworkImageProvider(user.bigAvatarUrl()),
            // child: Image(
            //   fit: BoxFit.fill,
            //   // placeholder: MemoryImage(kTransparentImage),
            //   image: CachedNetworkImageProvider(user.bigAvatarUrl()),
            // ),
          ),
        ),
        onTap: () {
          onTap?.call(user);
        },
      );
    } else {
      //Treat as anonymous user.
      return Container(
        margin: margin,
        width: 48,
        height: 48,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
              width: 1.0,
              color:
                  Theme.of(context).textTheme.bodyText1.color.withOpacity(0.5)),
        ),
        child: Center(
          child: Icon(
            MdiIcons.incognito,
          ),
        ),
      );
    }
  }

  //NickName
  static Widget buildHeaderNickName(BuildContext context, Plurdart.User user,
      {Function onNickNameTap,
      double fontSize = 13,
      HitTestBehavior hitTestBehavior = HitTestBehavior.deferToChild}) {
    return Flexible(
      child: GestureDetector(
        behavior: hitTestBehavior,
        child: Text(
          '@' + user.nickName,
          style: TextStyle(
            color: Theme.of(context).hintColor,
            fontSize: fontSize,
          ),
          overflow: TextOverflow.ellipsis,
        ),
        onTap: () {
          onNickNameTap?.call();
        },
      ),
    );
  }

  List<Widget> _buildHeaderPostedTimeReplurker(
      BuildContext context,
      Plurdart.Plurk plurk,
      Plurdart.User replurker,
      bool withShadow,
      Function onNameTap) {
    List<Widget> returnWidgets = [];

    //Is this a replurk post??
    if (replurker != null) {
      //This is a replurk.
      returnWidgets.add(buildHeaderTime(context, plurk.posted));

      returnWidgets.add(SizedBox(
        width: 4,
      ));

      returnWidgets.add(buildUserDisplayName(
          context, null, replurker, withShadow, onNameTap));

      returnWidgets.add(SizedBox(
        width: 4,
      ));

      returnWidgets.add(Container(
        padding: EdgeInsets.fromLTRB(2, 2, 2, 2),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(3),
          color: Colors.green,
        ),
        child: Icon(
          Icons.cached,
          size: 18,
          color: Colors.white,
        ),
      ));
    } else {
      //No. A normal post.
      returnWidgets.add(buildHeaderTime(context, plurk.posted));
    }

    return returnWidgets;
  }

  static Widget buildHeaderTime(BuildContext context, String timeString) {
    return Text(
      _timeAgoFromPosted(context, timeString),
      style: TextStyle(
        color: Theme.of(context).hintColor,
        fontSize: 13,
      ),
      overflow: TextOverflow.ellipsis,
    );
  }

  static Widget buildUserDisplayName(BuildContext context, Plurdart.Plurk plurk,
      Plurdart.User user, bool withShadow, Function onNameTap,
      {FlexFit fit = FlexFit.loose,
      double fontSize = 14.0,
      HitTestBehavior hitTestBehavior = HitTestBehavior.deferToChild}) {
    //For the light brightness we'll need white shadow on the colored user name...
    List<Shadow> shadows = [];

    if (withShadow) {
      //    if (Theme.of(context).brightness == Brightness.light) {
      shadows = [
        // Shadow(
        //   offset: Offset(10.0, 0.0),
        //   blurRadius: fontSize,
        //   color: Theme.of(context).canvasColor,
        // ),
        Shadow(
          offset: Offset(0.0, 0.0),
          blurRadius: fontSize,
          color: Theme.of(context).textTheme.bodyText1.color,
        ),
        // Shadow(
        //   offset: Offset(-10.0, 0.0),
        //   blurRadius: fontSize,
        //   color: Theme.of(context).canvasColor,
        // )
      ];
//    } // Dark brightness don't use shadow.
    }

    if (user != null) {
      //Display name color of this user.
      Color displayNameColor = HexColor.fromHex(
          user.nameColor, Theme.of(context).textTheme.bodyText1.color);

      //This could be use to display my color on my anonymous Plurk.
      if (plurk != null && plurk.anonymous != null && plurk.anonymous == true) {
        if (Static.me != null && plurk.userId == Static.meId) {
          displayNameColor = HexColor.fromHex(
              Static.me.nameColor, Theme.of(context).textTheme.bodyText1.color);
        }
      }

      return Flexible(
        fit: fit,
        child: GestureDetector(
          behavior: hitTestBehavior,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Text(
              user.displayName == null ? '[null]' : user.displayName,
              style: TextStyle(
                // Default Color is text body color.
                color: displayNameColor,
                fontWeight: FontWeight.w600,
                fontSize: fontSize,
                shadows: shadows,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          onTap: () {
            onNameTap?.call(user);
          },
        ),
      );
    } else {
      return Flexible(
        fit: fit,
        child: Text(
          'ಠ_ಠ',
          style: TextStyle(
            // Default Color is text body color.
            color: Theme.of(context).textTheme.bodyText1.color,
            fontWeight: FontWeight.w600,
            fontSize: fontSize,
            shadows: shadows,
          ),
          overflow: TextOverflow.ellipsis,
        ),
      );
    }
  }

  List<Widget> _buildHeaderDisplayNameAndQualifierRow(
      BuildContext context,
      Plurdart.Plurk plurk,
      Plurdart.User owner,
      bool withShadow,
      Function onNameTap) {
    List<Widget> returnWidgets = [];

    //DisplayName
    returnWidgets.add(
        buildUserDisplayName(context, plurk, owner, withShadow, onNameTap));

    Plurdart.PlurkQualifier qualifier = EnumToString.fromString(
        Plurdart.PlurkQualifier.values, capitalize(plurk.qualifier));

    //None qualifier will be ignore here...
    if (qualifier != null) {
      //Display the qualifier.
      returnWidgets.add(SizedBox(
        width: 4,
      ));
      returnWidgets.add(Container(
        padding: EdgeInsets.fromLTRB(4, 0, 4, 2),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(3),
          color: Color(plurk.qualifierColorHex()),
        ),
        child: Text(
          FlutterI18n.translate(
              context, EnumToString.convertToString(qualifier)),
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
          overflow: TextOverflow.ellipsis,
        ),
      ));
    }

    return returnWidgets;
  }

  // Return a format time ago string from the posted time.
  static String _timeAgoFromPosted(BuildContext context, String postedTime) {
    try {
//      print('postedTime: ' + postedTime);
      DateTime postedDateTime = DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
          .parseUTC(postedTime)
          .toLocal();
      Duration difDuration = DateTime.now().difference(postedDateTime);
      return timeago.format(DateTime.now().subtract(difDuration),
          locale: Localizations.localeOf(context).toString());
//      return timeago.format(DateTime.now().subtract(difDuration), locale: 'en_short');
    } catch (e) {
      print('Oops! postedTime [' + postedTime + '] cannot be parsed!');
      return '??';
    }
  }
}
