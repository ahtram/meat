part of 'stats_get_anonymous_plurks_cubit.dart';

@immutable
abstract class StatsGetAnonymousPlurksState {}

class StatsGetAnonymousPlurksInitial extends StatsGetAnonymousPlurksState {}

class StatsGetAnonymousPlurksSuccess extends StatsGetAnonymousPlurksState {
  StatsGetAnonymousPlurksSuccess(this.topicPlurks);
  final Plurdart.TopicPlurks topicPlurks;
}

class StatsGetAnonymousPlurksFailed extends StatsGetAnonymousPlurksState { }