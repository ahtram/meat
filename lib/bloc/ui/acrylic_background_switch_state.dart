part of 'acrylic_background_switch_cubit.dart';

@immutable
abstract class AcrylicBackgroundSwitchState {}

class AcrylicBackgroundSwitchInitial extends AcrylicBackgroundSwitchState {}

class AcrylicBackgroundSwitchActive extends AcrylicBackgroundSwitchState {
  AcrylicBackgroundSwitchActive(this.image);
  final Uint8List image;
}
