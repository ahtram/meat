
import 'package:flutter/material.dart';

class AppBarActionButton extends StatefulWidget {
  AppBarActionButton(this.icon, {this.onTap, this.iconColor, this.buttonColor});

  final IconData icon;
  final GestureTapCallback onTap;
  final Color iconColor;
  final Color buttonColor;

  @override
  _ActionButtonState createState() => _ActionButtonState();
}

class _ActionButtonState extends State<AppBarActionButton> {
  _ActionButtonState();

  @override
  Widget build(BuildContext context) {

    Color iconColor = Theme.of(context).iconTheme.color;
    if (widget.iconColor != null) {
      iconColor = widget.iconColor;
    }

    Color buttonColor = Colors.transparent;
    if (widget.buttonColor != null) {
      buttonColor = widget.buttonColor;
    }

    return InkWell(
      customBorder: CircleBorder(),
      child: Center(
        child: Container(
          width: 40,
          height: kToolbarHeight,
          decoration: BoxDecoration(
            color: buttonColor,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Icon(
            widget.icon,
            size: 24,
            color: iconColor,
          ),
        ),
      ),
      onTap: widget.onTap,
    );
  }
}