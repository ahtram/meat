import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'users_selected_state.dart';

class UsersSelectedCubit extends Cubit<UsersSelectedState> {
  UsersSelectedCubit() : super(UsersSelectedInitial());

  //Complete the users select action.
  complete(List<Plurdart.User> users) {
    emit(UsersSelectedComplete(users));
  }
}
