import 'dart:async';
import 'dart:typed_data';

import 'package:after_layout/after_layout.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/rest/timeline_favorite_plurks_cubit.dart';
import 'package:meat/bloc/rest/timeline_get_plurk_cubit.dart';
import 'package:meat/bloc/rest/timeline_replurk_cubit.dart';
import 'package:meat/bloc/rest/timeline_mark_as_read_cubit.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/bloc/ui/plurk_got_new_response_cubit.dart';
import 'package:meat/bloc/ui/report_abuse_dialog_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/bloc/ui/users_browser_cubit.dart';

import 'package:meat/bloc/ui/plurk_viewer_cubit.dart';
import 'package:meat/bloc/ui/viewer_background_switch_cubit.dart';
import 'package:meat/screens/plurk_viewer.dart';
import 'package:meat/screens/user_viewer.dart';
import 'package:meat/widgets/plurk/plurk_card_action_bar.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/plurk_card_link_stack.dart';
import 'package:meat/widgets/plurk/plurk_card_prime_header.dart';

import 'package:flutter/material.dart';
import 'package:meat/bloc/ui/image_links_viewer_cubit.dart';

import 'package:meat/bloc/ui/acrylic_background_switch_cubit.dart';
import 'package:meat/screens/image_links_viewer.dart';
import 'package:meat/widgets/plurk/plurk_paste_view.dart';

import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/define.dart' as Define;
import 'package:meat/widgets/user/users_viewer.dart' as UsersViewer;
import 'package:share_plus/share_plus.dart';
import 'package:meat/system/vibrator.dart' as Vibrator;
import 'package:meat/screens/tags_trunk.dart' as TagsTrunk;

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:meat/models/plurk/plurk_content_parts.dart';

import 'package:visibility_detector/visibility_detector.dart';

// Different mode means the same button emit different Cubit when tapped.
enum PlurkCardMode { FeedView, PlurkView }

class PlurkCard extends StatefulWidget {
  PlurkCard(this.plurk, this.owner, this.replurker, this.favorers,
      this.replurkers, this.mode, this.withHeader, this.openViewerById,
      {Key key,
      this.bookmark,
      this.hintPorn = true,
      this.showPinIcon = false,
      this.keepAliveWhenFoundImageLinks = false,
      this.keepAliveWhenFoundPlurkPaste = true,
      this.useImageCollectionBackground = true,
      this.onWantEditPlurk,
      this.onWantDeletePlurk,
      this.onWantPinPlurk,
      this.onWantUnpinPlurk,
      this.onBodyLongPress})
      : super(key: key);

  final Plurdart.Plurk plurk;
  final Plurdart.User owner;
  final Plurdart.User replurker;
  //This one is optional. Bookmark viewer only. And we should only use the tags for display bookmark chips.
  final Plurdart.Bookmark bookmark;
  final bool hintPorn;
  final bool showPinIcon;

  //Sometimes we want a very long card just keep alive. (In plurk_viewer)
  final bool keepAliveWhenFoundImageLinks;
  final bool keepAliveWhenFoundPlurkPaste;

  //Should we set background from image collection callback?
  final bool useImageCollectionBackground;

  //For long press the like button and replurk button to show in Users browser.
  final List<Plurdart.User> favorers;
  final List<Plurdart.User> replurkers;

  //FeedView:
  //  Message button: Open responses view and scroll to responses.
  //  Text content tap: Open responses view.
  //PlurkView:
  //  Message button: Scroll to responses.
  //  Text content tap: Do nothing.
  final PlurkCardMode mode;
  final bool withHeader;
  //Should we open the viewer by cache datas or by Id?
  //For Top and Anonymous Plurks we'll want this be true.
  final bool openViewerById;

  final Function(Plurdart.Plurk) onWantEditPlurk;
  final Function(Plurdart.Plurk) onWantDeletePlurk;
  final Function(Plurdart.Plurk) onWantPinPlurk;
  final Function(Plurdart.Plurk) onWantUnpinPlurk;
  final Function onBodyLongPress;

  //[Dep]
  // final bool needKeepAlive;

  @override
  PlurkCardState createState() => PlurkCardState();
}

class PlurkCardState extends State<PlurkCard>
    with PlurkContentParts, AfterLayoutMixin, AutomaticKeepAliveClientMixin {
  PlurkCardState();

  BuildContext _blocProviderContext;
  // GlobalKey _acrylicContainerKey = GlobalKey<AcrylicContainerState>();
  GlobalKey<PlurkCardPrimeHeaderState> _primeHeaderKey =
      GlobalKey<PlurkCardPrimeHeaderState>();

  // Plurdart.Plurk plurk;
  Plurdart.User fetchedOwner;
  Plurdart.User getOwner() {
    if (widget.owner != null) {
      return widget.owner;
    } else if (fetchedOwner != null) {
      return fetchedOwner;
    }
    return null;
  }

  // Plurdart.User replurker;
  //
  // List<Plurdart.User> favorers;
  // List<Plurdart.User> replurkers;

  //Milliseconds
  static const int SELF_UPDATE_PERIOD = 10000;
  Timer _startDelayTimer;
  Timer _selfUpdateTimer;

  //Has the user reveled porn?
  bool _hasReveledPorn = false;

  // @override
  // bool get wantKeepAlive => widget.needKeepAlive;

  bool _scrollViewVisible = false;

  //Should this card self keep alive?
  bool _keepAlive = false;

  //Has been present to ViewerBackgroundSwitchCubit.
  bool _hasPresentToBackground = false;

  @override
  void initState() {
    super.initState();

    _hasPresentToBackground = false;
    _hasReveledPorn = false;
    // copyDataFromWidget();

    //This will cause lag so we should think a better way doing it.
    //Start the self update timer.
    //[Dep]: This one still cause issues like override bookmark status.
    // int randomStartDelay = Random().nextInt(SELF_UPDATE_PERIOD);
    // _startDelayTimer = Timer(Duration(milliseconds: randomStartDelay), () {
    //   _selfUpdate();
    //   _selfUpdateTimer =
    //       Timer.periodic(Duration(milliseconds: SELF_UPDATE_PERIOD), (timer) {
    //     _selfUpdate();
    //     if (!mounted) {
    //       _selfUpdateTimer?.cancel();
    //     }
    //   });
    // });

    checkAndTryFetchExtras();
  }

  checkAndTryFetchExtras() async {
    if (widget.owner == null) {
      //Try to read from cache first!
      Plurdart.User cachedUser = Static.getUserByUserId(widget.plurk.ownerId);
      if (cachedUser != null) {
        setState(() {
          fetchedOwner = cachedUser;
        });
      } else {
        //Try to fetch the owner by ourself
        Plurdart.Profile profile = await Plurdart.profileGetPublicProfile(
            Plurdart.ProfileGetPublicProfile(userId: widget.plurk.ownerId));
        if (profile != null && mounted) {
          Static.addToUserCache(profile.userInfo);
          setState(() {
            fetchedOwner = profile.userInfo;
          });
        }
      }
    }
  }

  //Todo: We should think a better way to do updates
  // _selfUpdate() async {
  //   // print('[PlurkCard Self Update] for [' + plurk.plurkId.toString() + "]");
  //   if (!mounted) return;
  //
  //   //Guess we don't actually need a cubit here...
  //   Plurdart.PlurkWithUser plurkWithUser =
  //       await Plurdart.timelineGetPlurk(Plurdart.TimelineGetPlurk(
  //     plurkId: widget.plurk.plurkId,
  //   ));
  //
  //   if (mounted && plurkWithUser != null && !plurkWithUser.hasError()) {
  //     setState(() {
  //       // The override cause trouble because plurkWithUser does not contains replurkerId!!
  //       // I guess timelineGetPlurk is not for getting a replurk...
  //       // plurk.override(plurkWithUser.plurk);
  //       widget.plurk.update(plurkWithUser.plurk);
  //     });
  //   }
  // }

  @override
  void afterFirstLayout(BuildContext context) async {
    if (mounted) {
      setState(() {
        _scrollViewVisible = true;
      });

      // Pre process all display elements.
      preProcess(context, widget.plurk.content,
          Static.settingsRemoveLinksInPlurkContent);

      //Check if I should keep myself alive.
      if ((widget.keepAliveWhenFoundImageLinks && plurkImageLinks.length > 0) ||
          (widget.keepAliveWhenFoundPlurkPaste && plurkPasteLinks.length > 0)) {
        _keepAlive = true;
        updateKeepAlive();
      }
    }
  }

  @override
  bool get wantKeepAlive {
    return _keepAlive;
  }

  // copyDataFromWidget() {
  //   plurk = widget.plurk;
  //   owner = widget.owner;
  //   replurker = widget.replurker;
  //   favorers = widget.favorers;
  //   replurkers = widget.replurkers;
  //   // print('[copyDataFromWidget]: ' + plurk.contentRaw);
  // }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // print('[didChangeDependencies]');
    // Pre process all display elements.
    preProcess(context, widget.plurk.content,
        Static.settingsRemoveLinksInPlurkContent);

    //We need this for get InheritedWidget's context right...
    context.dependOnInheritedWidgetOfExactType();
  }

  @override
  void didUpdateWidget(covariant PlurkCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    // print('[didUpdateWidget]');
    preProcess(context, widget.plurk.content,
        Static.settingsRemoveLinksInPlurkContent);

    //Try switch acrylic container image.
    //Try set background image.
    // Uint8List backgroundImage = tryGetABackgroundImage();
    //Possible null. Will set a null image for empty background.
    // print('plurk [' + plurk.plurkId.toString() + ']' 'active a AcrylicBackground [' + tryGetABackgroundImageSource() + ']');

    //This one is for AcrylicContainer.
    // _blocProviderContext
    //     .read<AcrylicBackgroundSwitchCubit>()
    //     .active(backgroundImage);
  }

  @override
  void dispose() {
    super.dispose();

    // print('[dispose]');

    //Stop the self update timer.
    _startDelayTimer?.cancel();
    _selfUpdateTimer?.cancel();

    _hasPresentToBackground = false;
    _hasReveledPorn = false;
  }

  //A hack used by ResponseViewer...
  //When a ResponseViewer is closed. The PlurkCard should be force rebuild.
  //This will rebuild just one card instead of all SliverList.
  void refresh() {
    setState(() {});

    //[Maybe not]
    //We'll need a way to self update the PlurkCard.
//    _blocProviderContext.read<TimelineGetPlurkCubit>().request(Plurdart.TimelineGetPlurk(
//        plurkId: plurk.plurkId,
//    ));
  }

  // Get the current mode.
  PlurkCardMode mode() {
    return widget.mode;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    // copyDataFromWidget();
    // print('[PlurkCard build]: ' + plurk.posted);

    //Build our beautiful PlurkCard here.
    return MultiBlocProvider(
      providers: [
        BlocProvider<TimelineFavoritePlurksCubit>(
          create: (context) => TimelineFavoritePlurksCubit(),
        ),
        BlocProvider<TimelineReplurkCubit>(
          create: (context) => TimelineReplurkCubit(),
        ),
        BlocProvider<TimelineGetPlurkCubit>(
          create: (context) => TimelineGetPlurkCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<TimelineGetPlurkCubit, TimelineGetPlurkState>(
            listener: (listenerContext, state) {
              if (state.plurkWithUser != null) {
                //Update self data and setState to refresh stuffs...
                setState(() {
                  widget.plurk.update(state.plurkWithUser.plurk);
                });
              } //Ignore the null situation.
            },
          ),
          BlocListener<PlurkGotNewResponseCubit, PlurkGotNewResponseState>(
            listener: (listenerContext, state) {
              if (state is PlurkGotNewResponseSuccess) {
                // print('got PlurkGotNewResponseSuccess [' + state.plurk.plurkId.toString() + '] widget.plurk.plurkId [' + widget.plurk.plurkId.toString() + ']');
                if (state.plurk.plurkId == widget.plurk.plurkId) {
                  //Update self data and setState to refresh stuffs...
                  //Hack? Or is this right?
                  //Prevent alter the mute isUnread state.
                  if (state.plurk.isUnread != 2) {
                    state.plurk.isUnread = 1;
                  }
                  setState(() {
                    widget.plurk.update(state.plurk);
                    // print('updated plurkId [' + state.plurk.plurkId.toString() + '] isUnread [' + widget.plurk.isUnread.toString() + ']');
                  });
                }
              } //Ignore the null situation.
            },
          ),
        ],
        child: VisibilityDetector(
          key: Key(widget.plurk.plurkId.toString()),
          onVisibilityChanged: (visibilityInfo) {
            if (!_hasPresentToBackground &&
                visibilityInfo.visibleFraction >= 0.33) {
              Uint8List backgroundImage = tryGetABackgroundImage();
              if (backgroundImage != null) {
                _hasPresentToBackground = true;
                //This one is for AcrylicContainer.
                // print('PlurkCard [' + visibilityInfo.key.toString() + '] active backgroundImage');
                _blocProviderContext
                    .read<AcrylicBackgroundSwitchCubit>()
                    .active(backgroundImage);
              }
            }
          },
          child: Column(
            children: [
              Container(
                child: Builder(
                  builder: (builderContext) {
                    //Save the context because we need it for switching background.
//                  print('update _blocProviderContext');
                    _blocProviderContext = builderContext;

                    //Decide if the porn content is showed.
                    //The Plurk Card.
                    return InkWell(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: _buildCardParts(
                              widget.plurk,
                              getOwner(),
                              widget.replurker,
                              widget.bookmark,
                              widget.withHeader,
                              widget.onWantEditPlurk,
                              widget.onWantDeletePlurk,
                              widget.onWantPinPlurk,
                              widget.onWantUnpinPlurk),
                        ),
                      ),
                      onTap: () {
                        //Do mark as read.
                        _blocProviderContext
                            .read<TimelineMarkAsReadCubit>()
                            .mark([widget.plurk.plurkId]);

                        //Click on the Plurk body to reveal porn.
                        if (widget.plurk.porn &&
                            widget.hintPorn &&
                            !_hasReveledPorn) {
                          Vibrator.vibrateShortNote();
                          setState(() {
                            _hasReveledPorn = true;
                          });
                        } else {
                          if (mode() == PlurkCardMode.FeedView) {
                            //Message button: Open plurk viewer when in feedView mode.
                            if (widget.openViewerById) {
                              //For Top and Anonymous Plurks.
                              _blocProviderContext
                                  .read<PlurkViewerCubit>()
                                  .open(PlurkViewerArgs(
                                    PlurkViewerMode.FromPlurkId,
                                    plurkId: widget.plurk.plurkId,
                                    showPlurkCard: true,
                                  ));
                            } else {
                              //Feed Plurks.
                              _blocProviderContext
                                  .read<PlurkViewerCubit>()
                                  .open(PlurkViewerArgs(
                                    PlurkViewerMode.FromCardState,
                                    plurkCardState: this,
                                    showPlurkCard: true,
                                    backgroundImage: tryGetABackgroundImage(),
                                  ));
                            }
                          }
                        }
                      },
                      onLongPress: () {
                        Vibrator.vibrateShortNote();
                        //Show button menu?
                        _primeHeaderKey.currentState?.showButtonMenu();
                        widget.onBodyLongPress?.call();
                      },
                    );
                  },
                ),
                // initialImage: MemoryImage(kTransparentImage),
                // key: _acrylicContainerKey,
              ),
              Divider(height: 0.5, thickness: 0.5),
            ],
          ),
        ),
//         AnimatedOpacity(
//           opacity: _scrollViewVisible ? 1.0 : 0.0,
//           duration: Duration(milliseconds: 500),
//           child: Column(
//             children: [
//               AcrylicContainer(
//                 Builder(
//                   builder: (builderContext) {
//                     //Save the context because we need it for switching background.
// //                  print('update _blocProviderContext');
//                     _blocProviderContext = builderContext;
//
//                     //Decide if the porn content is showed.
//                     //The Plurk Card.
//                     return InkWell(
//                       child: Container(
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.stretch,
//                           children: _buildCardParts(
//                               widget.plurk,
//                               getOwner(),
//                               widget.replurker,
//                               widget.bookmark,
//                               widget.withHeader,
//                               widget.onWantEditPlurk,
//                               widget.onWantDeletePlurk),
//                         ),
//                       ),
//                       onTap: () {
//                         //Do mark as read.
//                         _blocProviderContext
//                             .read<TimelineMarkAsReadCubit>()
//                             .mark([widget.plurk.plurkId]);
//
//                         //Click on the Plurk body to reveal porn.
//                         if (widget.plurk.porn &&
//                             widget.hintPorn &&
//                             !_hasReveledPorn) {
//                           Vibrator.vibrateShortNote();
//                           setState(() {
//                             _hasReveledPorn = true;
//                           });
//                         } else {
//                           if (mode() == PlurkCardMode.FeedView) {
//                             //Message button: Open plurk viewer when in feedView mode.
//                             if (widget.openViewerById) {
//                               //For Top and Anonymous Plurks.
//                               _blocProviderContext
//                                   .read<PlurkViewerCubit>()
//                                   .open(PlurkViewerArgs(
//                                     PlurkViewerMode.FromPlurkId,
//                                     plurkId: widget.plurk.plurkId,
//                                     showPlurkCard: true,
//                                   ));
//                             } else {
//                               //Feed Plurks.
//                               _blocProviderContext
//                                   .read<PlurkViewerCubit>()
//                                   .open(PlurkViewerArgs(
//                                     PlurkViewerMode.FromCardState,
//                                     plurkCardState: this,
//                                     showPlurkCard: true,
//                                     backgroundImage: tryGetABackgroundImage(),
//                                   ));
//                             }
//                           }
//                         }
//                       },
//                       onLongPress: () {
//                         Vibrator.vibrateShortNote();
//                         //Show button menu?
//                         _primeHeaderKey.currentState?.showButtonMenu();
//                         widget.onBodyLongPress?.call();
//                       },
//                     );
//                   },
//                 ),
//                 initialImage: MemoryImage(kTransparentImage),
//                 key: _acrylicContainerKey,
//               ),
//               Divider(height: 0.5, thickness: 0.5),
//             ],
//           ),
//         ),
      ),
    );
  }

  // This will decide which part will be displayed on the Card.
  // Some of the part maybe empty.
  List<Widget> _buildCardParts(
      Plurdart.Plurk plurk,
      Plurdart.User owner,
      Plurdart.User replurker,
      Plurdart.Bookmark bookmark,
      bool withHeader,
      Function(Plurdart.Plurk) onWantEditPlurk,
      Function(Plurdart.Plurk) onWantDeletePlurk,
      Function(Plurdart.Plurk) onWantPinPlurk,
      Function(Plurdart.Plurk) onWantUnpinPlurk) {
    List<Widget> partWidgets = [];

    //== Header == (Maybe empty)
    if (withHeader) {
      partWidgets.add(_buildHeaderPart(
        plurk,
        owner,
        replurker,
        widget.showPinIcon,
        false,
        onUserNameTap: (user) {
          //Bloc to open UserViewer.
          context.read<UserViewerCubit>().open(UserViewerArgs(user.nickName));
        },
        onReplurkerNameTap: (user) {
          //Bloc to open UserViewer.
          context.read<UserViewerCubit>().open(UserViewerArgs(user.nickName));
        },
        onAvatarTap: (user) {
          //Bloc to open UserViewer.
          context.read<UserViewerCubit>().open(UserViewerArgs(user.nickName));
        },
        onWantEditPlurk: onWantEditPlurk,
        onWantDeletePlurk: onWantDeletePlurk,
        onWantPinPlurk: onWantPinPlurk,
        onWantUnpinPlurk: onWantUnpinPlurk,
        onWantReportAbuse: (plurkId) {
          //Bloc to open ReportAbuse.
          context.read<ReportAbuseDialogCubit>().reportPlurk(plurkId);
        },
      ));
    }

    if (widget.plurk.porn && widget.hintPorn && !_hasReveledPorn) {
      partWidgets.add(Container(
        padding: EdgeInsets.symmetric(
            horizontal: Define.horizontalPadding,
            vertical: Define.verticalPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              MdiIcons.alertOutline,
              size: 64,
            ),
            SizedBox(
              width: 8,
            ),
            Flexible(
                child: Text(
              'This Plurk has been marked as adults-only.',
              style: Theme.of(context).textTheme.overline,
            )),
          ],
        ),
      ));
    } else {
      partWidgets.add(SizedBox(height: Define.verticalPadding * 0.5));

      //== Text Content == (Maybe empty)
      Widget textContentPart = buildTextContentPart(textContent);
      if (textContentPart != null) {
        partWidgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
          child: textContentPart,
        ));
        partWidgets.add(SizedBox(height: Define.verticalPadding));
      }

      //== Hash tags == (Maybe empty)
      Widget hashTagsPart = buildHashTagsPart(hashTags);
      if (hashTagsPart != null) {
        partWidgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
          child: hashTagsPart,
        ));
        partWidgets.add(SizedBox(height: Define.verticalPadding));
      }

      //== Youtube Video Content == (Maybe empty)
      Widget youtubeVideoPart = buildYoutubeVideoPart(youtubeVideoLinks);
      if (youtubeVideoPart != null) {
        partWidgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
          child: youtubeVideoPart,
        ));
        partWidgets.add(SizedBox(height: Define.verticalPadding));
      }

      //== Image Collection Content == (Maybe empty)
      Widget imageCollectionPart =
          buildImageCollectionPartByType(plurkImageLinks, (heroTag) {
        //Get the initial index
        int initialIndex = 0;
        for (int i = 0; i < plurkImageLinks.length; ++i) {
          if (plurkImageLinks[i].getHeroTag() == heroTag) {
            initialIndex = i;
            break;
          }
        }

        context.read<ImageLinksViewerCubit>().enter(ImageLinksViewerArgs(
              plurkImageLinks,
              initialIndex,
            ));
      }, () {
        if (widget.useImageCollectionBackground) {
          //Try set background image.
          Uint8List backgroundImage = tryGetABackgroundImage();
          //Possible null. Will set a null image for empty background.
          // print('plurk [' + plurk.plurkId.toString() + ']' 'active a AcrylicBackground [' + tryGetABackgroundImageSource() + ']');

          //This one is for AcrylicContainer.
          // _blocProviderContext
          //     .read<AcrylicBackgroundSwitchCubit>()
          //     .active(backgroundImage);

          //This one is for PlurkViewer. (This will be an empty shot for Home screen)
          _blocProviderContext
              .read<ViewerBackgroundSwitchCubit>()
              .active(backgroundImage);
        }
      },
              widget.mode == PlurkCardMode.FeedView
                  ? ImageCollectionType.Summery
                  : ImageCollectionType.ExpandAll);

      if (imageCollectionPart != null) {
        partWidgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
          child: imageCollectionPart,
        ));
        partWidgets.add(SizedBox(height: Define.verticalPadding));
      } else {
        //Change to a transparent image
        // print('active a transparent image');

        //This one is for AcrylicContainer.
        // _blocProviderContext
        //     .watch<AcrylicBackgroundSwitchCubit>()
        //     .active(kTransparentImage);

        //This one is for PlurkViewer. (This will be an empty shot for Home screen)
        // _blocProviderContext
        //     .watch<ViewerBackgroundSwitchCubit>()
        //     .active(kTransparentImage);

      }

      //== Plurk Paste Content == (Maybe empty)

      Widget plurkPastePart = buildPlurkPastePart(
          plurkPasteLinks,
          widget.mode == PlurkCardMode.FeedView
              ? PlurkPasteViewType.Summary
              : PlurkPasteViewType.Expanded, (uri) {
        BlocProvider.of<OpenUriCubit>(context).request(uri);
      });
      if (plurkPastePart != null) {
        partWidgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
          child: plurkPastePart,
        ));
        partWidgets.add(SizedBox(height: Define.verticalPadding));
      }

      //== Link Stack Content == (Maybe empty)
      Widget linkStackPart = widget.mode == PlurkCardMode.FeedView
          ? buildLinkStackPart(urlLinks, (uri) {
              BlocProvider.of<OpenUriCubit>(context).request(uri);
            })
          : buildLinkStackPartByType(urlLinks, LinkStackType.All, (uri) {
              BlocProvider.of<OpenUriCubit>(context).request(uri);
            });
      if (linkStackPart != null) {
        partWidgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
          child: linkStackPart,
        ));
        partWidgets.add(SizedBox(height: Define.verticalPadding));
      }

      // partWidgets.add(Divider(height: 0.75, thickness: 0.75, indent: Define.horizontalPadding, endIndent: Define.horizontalPadding));

      //== Action Bar == (Required)
      // print('_buildActionBarPart plurkId [' + plurk.plurkId.toString() + '] isUnread [' + plurk.isUnread.toString() + ']');
      partWidgets.add(_buildActionBarPart(plurk));

      //== Bookmark chips == (Maybe empty)
      partWidgets.add(_buildBookmarkChipsFooter(bookmark));
    }

    return partWidgets;
  }

  //This will try return an image which can be use as a background.
  //Possible return null.
  Uint8List tryGetABackgroundImage() {
    //Decide use full image or thumbnail for background.
    if (Static.settingsDownloadFullImageInPlurkCard) {
      //Use full image.
      for (int i = 0; i < plurkImageLinks.length; ++i) {
        if (plurkImageLinks[i].fullImageRawInfo != null) {
          return plurkImageLinks[i].fullImageRawInfo.uint8list;
        }
      }
    } else {
      //Use thumbnail image.
      for (int i = 0; i < plurkImageLinks.length; ++i) {
        if (plurkImageLinks[i].thumbnailRawInfo != null) {
          return plurkImageLinks[i].thumbnailRawInfo.uint8list;
        }
      }
    }

    return null;
  }

  //This will try return an image which can be use as a background.
  String tryGetABackgroundImageSource() {
    //Decide use full image or thumbnail for background.
    if (Static.settingsDownloadFullImageInPlurkCard) {
      //Use full image.
      for (int i = 0; i < plurkImageLinks.length; ++i) {
        if (plurkImageLinks[i].fullImageRawInfo != null) {
          return plurkImageLinks[i].fullImageRawInfo.source;
        }
      }
    } else {
      //Use thumbnail image.
      for (int i = 0; i < plurkImageLinks.length; ++i) {
        if (plurkImageLinks[i].thumbnailRawInfo != null) {
          return plurkImageLinks[i].thumbnailRawInfo.source;
        }
      }
    }

    return null;
  }

  // The Header (owner + post time + qualifier + option button)
  Widget _buildHeaderPart(
    Plurdart.Plurk plurk,
    Plurdart.User owner,
    Plurdart.User replurker,
    bool showPinIcon,
    bool withNameShadow, {
    Function onUserNameTap,
    Function onReplurkerNameTap,
    Function onAvatarTap,
    Function(Plurdart.Plurk) onWantEditPlurk,
    Function(Plurdart.Plurk) onWantDeletePlurk,
    Function(Plurdart.Plurk) onWantPinPlurk,
    Function(Plurdart.Plurk) onWantUnpinPlurk,
    Function(int) onWantReportAbuse,
  }) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 16, 8, 0),
      child: PlurkCardPrimeHeader(
        plurk,
        owner,
        replurker,
        showPinIcon,
        withNameShadow,
        onUserNameTap: onUserNameTap,
        onReplurkerNameTap: onReplurkerNameTap,
        onAvatarTap: onAvatarTap,
        onWantEditPlurk: onWantEditPlurk,
        onWantDeletePlurk: onWantDeletePlurk,
        onWantPinPlurk: onWantPinPlurk,
        onWantUnpinPlurk: onWantUnpinPlurk,
        onWantReportAbuse: onWantReportAbuse,
        onWantEditBookmarkTags: () {
          _openTagsTrunk();
        },
        key: _primeHeaderKey,
      ),
    );
  }

  //== Action Bar == (Required)
  Widget _buildActionBarPart(Plurdart.Plurk plurk) {
    //Todo: PlurkCardActionBar needs static.me ready cause it will be used in open plurk link.
    return Container(
      padding: EdgeInsets.fromLTRB(
          Define.horizontalPadding + 4, 0, Define.horizontalPadding + 4, 8),
      child: PlurkCardActionBar(
        plurk,
        onFavoriteLongPress: () {
          //Show favorers on Users Browser.
          if (widget.favorers != null && widget.favorers.length > 0) {
            _blocProviderContext
                .read<UsersBrowserCubit>()
                .open(UsersViewer.UsersViewerArgs(
                  'Likes',
                  UsersViewer.UsersBrowsingType.UserModals,
                  isToggleList: false,
                  userModals: widget.favorers,
                ));
          }
        },
        onReplurkLongPress: () {
          if (widget.replurkers != null && widget.replurkers.length > 0) {
            //Show replurkers on Users Browser.
            _blocProviderContext
                .read<UsersBrowserCubit>()
                .open(UsersViewer.UsersViewerArgs(
                  'Replurkers',
                  UsersViewer.UsersBrowsingType.UserModals,
                  isToggleList: false,
                  userModals: widget.replurkers,
                ));
          }
        },
        onResponseTap: () {
          //Do mark as read.
          _blocProviderContext
              .read<TimelineMarkAsReadCubit>()
              .mark([plurk.plurkId]);

          if (mode() == PlurkCardMode.FeedView) {
            //Response message button: Open plurk viewer when in feedView mode and scroll to the responses view.
            if (widget.openViewerById) {
              //For Top and Anonymous Plurks.
              _blocProviderContext
                  .read<PlurkViewerCubit>()
                  .open(PlurkViewerArgs(
                    PlurkViewerMode.FromPlurkId,
                    plurkId: plurk.plurkId,
                    showPlurkCard: false,
                  ));
            } else {
              _blocProviderContext
                  .read<PlurkViewerCubit>()
                  .open(PlurkViewerArgs(
                    PlurkViewerMode.FromCardState,
                    plurkCardState: this,
                    showPlurkCard: false,
                    backgroundImage: tryGetABackgroundImage(),
                  ));
            }
          }
        },
        onPrivateTap: () {
          //Open users browser.
          List<int> userIds = plurk.getLimitedTo();

          // print('userIds: ' + userIds.join(','));

          //[Server Bug?]: The limited to = true doesn't include all limited Users.

          if (userIds.length > 1) {
            //Limit to some users...
            List<Plurdart.User> users = Static.getUsersByUserIds(userIds);
            _blocProviderContext
                .read<UsersBrowserCubit>()
                .open(UsersViewer.UsersViewerArgs(
                  'Plurkers',
                  UsersViewer.UsersBrowsingType.UserModals,
                  isToggleList: false,
                  userModals: users,
                ));
          } //For length == 1, it's a friends only Plurk so don't need a friend list.
        },
        onShareTap: () {
          //Share the plurk post. (OS share menu)
          //Tricky: they use base 36 as post url path
          Share.share(Define.getPlurkPostLink(plurk.plurkId));
        },
        //Moved to header
        // onWantEditBookmarkTags: () {
        //   _openTagsTrunk();
        // },
      ),
    );
  }

  _openTagsTrunk() {
    TagsTrunk.show(
        context,
        TagsTrunk.TagsTrunkArgs(
            plurkID: widget.plurk.plurkId,
            onTagsDecided: (int bookmarkID, List<String> tags) async {
              //Update the plurk's tags silently.
              Plurdart.Bookmark bookmark =
                  await Plurdart.bookmarksUpdateBookmark(
                      Plurdart.BookmarksUpdateBookmark(
                bookmarkID: bookmarkID,
                tags: tags,
              ));

              if (widget.bookmark != null) {
                setState(() {
                  widget.bookmark.tags = bookmark.tags;
                });
              }
              //Should we do any error check or just ignore it??
              // if (bookmark != null) {
              //   if (bookmark.hasError()) {
              //     print('[bookmark error]: ' + bookmark.errorText);
              //   } else {
              //     print('[bookmark success]: ' + bookmark.plurkId.toString());
              //   }
              // }
            }));
  }

  Widget _buildBookmarkChipsFooter(Plurdart.Bookmark bookmark) {
    if (bookmark != null && bookmark.tags.length > 0) {
      List<Widget> wrapChildren = [];
      for (int i = 0; i < bookmark.tags.length; ++i) {
        wrapChildren.add(Chip(
          backgroundColor: Theme.of(context).primaryColor.withOpacity(0.5),
          label: Transform.translate(
              offset: Offset(0, -2),
              child: Text(
                bookmark.tags[i],
                overflow: TextOverflow.fade,
              )),
        ));
      }
      return InkWell(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Divider(
              height: 1,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: Define.horizontalPadding),
              child: Wrap(
                spacing: 8,
                children: wrapChildren,
              ),
            ),
          ],
        ),
        onTap: () {
          //Open bookmark tag editor
          _openTagsTrunk();
        },
      );
    } else {
      return Container();
    }
  }
}
