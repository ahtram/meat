import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:meat/bloc/rest/timeline_mark_as_read_cubit.dart';
import 'package:meat/bloc/rest/users_me_cubit.dart';
import 'package:meat/widgets/plurk/plurk_card_action_button.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/system/vibrator.dart' as Vibrator;

class PlurkCardActionBar extends StatefulWidget {
  PlurkCardActionBar(this.plurk,
      {this.onFavoriteTap,
      this.onFavoriteLongPress,
      this.onReplurkTap,
      this.onReplurkLongPress,
      this.onResponseTap,
      this.onPrivateTap,
      this.onShareTap,
      this.onMuteTap,});

  final Plurdart.Plurk plurk;
  final Function onFavoriteTap;
  final Function onFavoriteLongPress;
  final Function onReplurkTap;
  final Function onReplurkLongPress;
  final Function onResponseTap;
  final Function onPrivateTap;
  final Function onShareTap;
  final Function onMuteTap;
  // final Function onWantEditBookmarkTags;

  @override
  _PlurkCardActionBarState createState() => _PlurkCardActionBarState();
}

class _PlurkCardActionBarState extends State<PlurkCardActionBar> {
  bool _isReplurking = false;
  bool _isFavorting = false;
  bool _isMuting = false;
  // bool _isBookmarking = false;

  @override
  Widget build(BuildContext context) {

    // print('_PlurkCardActionBarState build [' + widget.plurk.plurkId.toString() + '] isUnread [' + widget.plurk.isUnread.toString() + ']');

    return MultiBlocListener(
      listeners: [
        BlocListener<TimelineMarkAsReadCubit, TimelineMarkAsReadState>(
          listener: (listenerContext, state) {
            if (state is TimelineMarkAsReadSuccess) {
              //Fake move.
              if (state.plurkIDs.contains(widget.plurk.plurkId)) {
                setState(() {
                  //2 = muted
                  if (widget.plurk.isUnread != 2) {
                    //0 = read.
                    widget.plurk.isUnread = 0;
                  }
                });
              }
            } //Ignore the null situation.
          },
        ),
      ],
      child: BlocBuilder<UsersMeCubit, UsersMeState>(builder: (context, state) {
        //Check if the Static.me is ready!
        if (Static.me != null) {
          //Decide which button should be displayed.
          List<Widget> rowChildren = [];

          //Favorite button.
          rowChildren.add(Expanded(
            flex: 6,
            child: PlurkCardActionButton(
              Icons.favorite,
              widget.plurk.favorite,
              Colors.pink,
              count: widget.plurk.favoriteCount,
              onTap: () async {
                widget.onFavoriteTap?.call();

                if (!_isFavorting) {
                  _isFavorting = true;
                  Vibrator.vibrateShortNote();

                  if (!widget.plurk.favorite) {
                    //Do favorite
                    Plurdart.Base responseBase =
                        await Plurdart.timelineFavoritePlurks(
                            Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
                    if (responseBase != null) {
                      if (responseBase.hasSuccess()) {
                        setState(() {
                          widget.plurk.favoriteCount += 1;
                          widget.plurk.favorite = true;
                        });
                      }
                    }
                  } else {
                    //Do un favorite
                    Plurdart.Base responseBase =
                        await Plurdart.timelineUnfavoritePlurks(
                            Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
                    if (responseBase != null) {
                      if (responseBase.hasSuccess()) {
                        setState(() {
                          widget.plurk.favoriteCount -= 1;
                          widget.plurk.favorite = false;
                        });
                      }
                    }
                  }

                  _isFavorting = false;
                }
              },
              onLongPress: () {
                widget.onFavoriteLongPress?.call();
              },
            ),
          ));

          // print('Static.me.id: ' + Static.me.id.toString() + ' | plurk.ownerId: ' + plurk.ownerId.toString() + ' | plurk.replurkable: ' + plurk.replurkable.toString() + ' | userId: ' + plurk.userId.toString() + ' | anonymous: ' + plurk.anonymous.toString());

          //Replurk button.
          if (widget.plurk.replurkable) {
            //Is this my plurk? If it is don't let the replurk button take effect.
            //And give it the amber color.
            //[Tricky!]
            bool isMyPlurk = false;
            if (!widget.plurk.anonymous) {
              if (widget.plurk.ownerId == Static.me.id) {
                isMyPlurk = true;
              }
            } else {
              //Anonymous plurk: check owner and userId.
              if (widget.plurk.userId == Static.me.id) {
                isMyPlurk = true;
              }
            }

            if (isMyPlurk) {
              //Amber color.
              rowChildren.add(Expanded(
                flex: 6,
                child: PlurkCardActionButton(Icons.cached, true, Colors.orange,
                    count: widget.plurk.replurkersCount, onTap: () {
                  //Do nothing. I cannot replurk my own plurk.
                }, onLongPress: () {
                  widget.onReplurkLongPress?.call();
                }),
              ));
            } else {
              //Normal replurk button.
              rowChildren.add(Expanded(
                flex: 6,
                child: PlurkCardActionButton(
                    Icons.cached, widget.plurk.replurked, Colors.green,
                    count: widget.plurk.replurkersCount, onTap: () async {
                  widget.onReplurkTap?.call();

                  if (!_isReplurking) {
                    _isReplurking = true;
                    Vibrator.vibrateShortNote();

                    if (!widget.plurk.replurked) {
                      //Do replurk
                      Plurdart.RePlurk responseRePlurk =
                          await Plurdart.timelineReplurk(
                              Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
                      if (responseRePlurk != null) {
                        if (responseRePlurk.success) {
                          setState(() {
                            widget.plurk.replurkersCount += 1;
                            widget.plurk.replurked = true;
                          });
                        }
                      }
                    } else {
                      //Do un-replurk
                      Plurdart.RePlurk responseRePlurk =
                          await Plurdart.timelineUnreplurk(
                              Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
                      if (responseRePlurk != null) {
                        if (responseRePlurk.success) {
                          setState(() {
                            widget.plurk.replurkersCount -= 1;
                            widget.plurk.replurked = false;
                          });
                        }
                      }
                    }

                    _isReplurking = false;
                  }
                }, onLongPress: () {
                  widget.onReplurkLongPress.call();
                }),
              ));
            }
          } else if (widget.plurk.limitedTo != null) {
            //Show private numbers.
            List<int> userIds = widget.plurk.getLimitedTo();

            int count;
            if (userIds.length == 1 && userIds[0] == 0) {
              //This is the case only friends can see this Plurk.
              count = 0;
            } else {
              count = userIds.length;
            }

            rowChildren.add(Expanded(
              flex: 6,
              child: PlurkCardActionButton(Icons.lock, true, Colors.purpleAccent,
                  count: count, onTap: () {
                Vibrator.vibrateShortNote();
                widget.onPrivateTap?.call();
              }),
            ));

            // print('limitedTo: ' + plurk.limitedTo);
          } else {
            //Add an empty Extend.
            rowChildren.add(Expanded(
              flex: 6,
              child: Container(),
            ));
          }

          //Response button. (Always show, event no comment)
          rowChildren.add(Expanded(
            flex: 6,
            child: PlurkCardActionButton(
                MdiIcons.cardText, (widget.plurk.isUnread == 1), Colors.amber,
                count: widget.plurk.responseCount, onTap: () {
              Vibrator.vibrateShortNote();
              widget.onResponseTap?.call();
            }),
          ));

          //Try add a divider.
          rowChildren.add(Container(
            width: 1,
            height: 20,
            color: Colors.white.withOpacity(0.5),
          ));

          rowChildren.add(SizedBox(
            width: 6,
          ));

          //Share button.
          rowChildren.add(Expanded(
            flex: 2,
            child: PlurkCardActionButton(Icons.share, true, Colors.blue,
                onTap: () {
                  Vibrator.vibrateShortNote();
                  widget.onShareTap?.call();
                }),
          ));

          rowChildren.add(SizedBox(
            width: 4,
          ));

          //Mute/Unmute button
          rowChildren.add(Expanded(
            flex: 2,
            child: PlurkCardActionButton(
                widget.plurk.isUnread == 2
                    ? Icons.volume_off_outlined
                    : Icons.volume_up_outlined,
                widget.plurk.isUnread == 2 ? true : false,
                Colors.deepOrangeAccent,
                // label: widget.plurk.isUnread == 2 ? 'On' : 'Off',
                onTap: () async {
                  widget.onMuteTap?.call();

                  if (!_isMuting) {
                    _isMuting = true;
                    Vibrator.vibrateShortNote();

                    if (widget.plurk.isUnread == 2) {
                      //Do Unmute
                      Plurdart.Base responseBase =
                      await Plurdart.timelineUnmutePlurks(
                          Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
                      if (responseBase != null) {
                        setState(() {
                          //Not sure if set to 0 is alright. Whatever.
                          widget.plurk.isUnread = 0;
                        });
                      }
                    } else {
                      //Do mute
                      Plurdart.Base responseBase =
                      await Plurdart.timelineMutePlurks(
                          Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
                      if (responseBase != null) {
                        if (responseBase.hasSuccess()) {
                          setState(() {
                            widget.plurk.isUnread = 2;
                          });
                        }
                      }
                    }

                    _isMuting = false;
                  }
                }, onLongPress: () {
              widget.onReplurkLongPress.call();
            }),
          ));

          // //Share button or mute/unmute button?
          // if (Static.settingsDisplayMuteButtonAtBottomRight) {
          //   //Mute/Unmute button
          //   rowChildren.add(Expanded(
          //     flex: 2,
          //     child: PlurkCardActionButton(
          //         widget.plurk.isUnread == 2
          //             ? Icons.volume_off_outlined
          //             : Icons.volume_up_outlined,
          //         widget.plurk.isUnread == 2 ? true : false,
          //         Colors.purple,
          //         // label: widget.plurk.isUnread == 2 ? 'On' : 'Off',
          //         onTap: () async {
          //       widget.onMuteTap?.call();
          //
          //       if (!_isMuting) {
          //         _isMuting = true;
          //         Vibrator.vibrateShortNote();
          //
          //         if (widget.plurk.isUnread == 2) {
          //           //Do Unmute
          //           Plurdart.Base responseBase =
          //               await Plurdart.timelineUnmutePlurks(
          //                   Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
          //           if (responseBase != null) {
          //             setState(() {
          //               //Not sure if set to 0 is alright. Whatever.
          //               widget.plurk.isUnread = 0;
          //             });
          //           }
          //         } else {
          //           //Do mute
          //           Plurdart.Base responseBase =
          //               await Plurdart.timelineMutePlurks(
          //                   Plurdart.PlurkIDs(ids: [widget.plurk.plurkId]));
          //           if (responseBase != null) {
          //             if (responseBase.hasSuccess()) {
          //               setState(() {
          //                 widget.plurk.isUnread = 2;
          //               });
          //             }
          //           }
          //         }
          //
          //         _isMuting = false;
          //       }
          //     }, onLongPress: () {
          //       widget.onReplurkLongPress.call();
          //     }),
          //   ));
          // } else {
          //   //Share button.
          //   rowChildren.add(Expanded(
          //     flex: 2,
          //     child: PlurkCardActionButton(Icons.share, true, Colors.blue,
          //         onTap: () {
          //       Vibrator.vibrateShortNote();
          //       widget.onShareTap?.call();
          //     }),
          //   ));
          // }

          //[Moved to Header]
          //Bookmark button.
          //Do we have coin? The bookmark button require we have a Plurk coin.
          // if (Static.meIsPremium()) {
          //   rowChildren.add(Expanded(
          //     flex: 2,
          //     child: Container(
          //       child: PlurkCardActionButton(
          //         Icons.star,
          //         widget.plurk.bookmark,
          //         Colors.cyan,
          //         onTap: () async {
          //           if (!_isBookmarking) {
          //             Vibrator.vibrateShortNote();
          //             if (!widget.plurk.bookmark) {
          //               //Do set bookmark.
          //               _isBookmarking = true;
          //               Plurdart.Bookmark bookmark =
          //                   await Plurdart.bookmarksSetBookmark(
          //                       Plurdart.BookmarksSetBookmark(
          //                 plurkID: widget.plurk.plurkId,
          //                 bookmark: true,
          //               ));
          //
          //               if (bookmark != null && !bookmark.hasError()) {
          //                 setState(() {
          //                   widget.plurk.bookmark = true;
          //                 });
          //               }
          //               _isBookmarking = false;
          //             } else {
          //               //Do selection tags.
          //               widget.onWantEditBookmarkTags?.call();
          //             }
          //           }
          //         },
          //         onLongPress: () async {
          //           if (!_isBookmarking) {
          //             Vibrator.vibrateShortNote();
          //             if (widget.plurk.bookmark) {
          //               //Do set bookmark.
          //               _isBookmarking = true;
          //               Plurdart.Bookmark bookmark =
          //                   await Plurdart.bookmarksSetBookmark(
          //                       Plurdart.BookmarksSetBookmark(
          //                 plurkID: widget.plurk.plurkId,
          //                 bookmark: false,
          //               ));
          //
          //               if (bookmark != null && !bookmark.hasError()) {
          //                 setState(() {
          //                   widget.plurk.bookmark = false;
          //                 });
          //               }
          //               _isBookmarking = false;
          //             }
          //           }
          //         },
          //       ),
          //     ),
          //   ));
          // }

          //Ready.
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: rowChildren,
          );
        } else {
          //Not ready.
          return Container();
        }
      }),
    );
  }
}
