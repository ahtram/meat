part of 'responses_delete_cubit.dart';

@immutable
abstract class ResponsesDeleteState {}

class ResponsesDeleteInitial extends ResponsesDeleteState {}

class ResponsesDeleteSuccess extends ResponsesDeleteState {
  ResponsesDeleteSuccess(this.responseId);
  final int responseId;
}

class ResponsesDeleteFailed extends ResponsesDeleteState {
  ResponsesDeleteFailed(this.error);
  final String error;
}