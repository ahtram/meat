import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';

//This is an independent locale system directly read locale file from Assets/Locale folder.
//The goal is to just rely on rootbundle to do locale so we may use it without content or widget.
//And thus we can do proper localization in other isolate which need to display localized Strings!

//The loaded String map.
Map<String, dynamic> _localeMap;

//Setup will try to use Platform.localeName to get the job done if no localeName passed in.
//en.json will be the fallback option if no proper locale file found!
Future<bool> initialize({String localeName}) async {
  if (localeName == null || localeName.isEmpty) {
    localeName = Platform.localeName;
  }

  try {
    String jsonStr = await rootBundle.loadString('assets/locales/' + localeName + '.json');
    _localeMap = jsonDecode(jsonStr);
    print('[AssetsLocale] Locale[' + localeName + '] loaded.');
    return true;
  } catch(e) {
    //Error reading the input localeName file.
    print(e.toString());
  }

  //Fallback and try the en.json
  try {
    String jsonStr = await rootBundle.loadString('assets/locales/en.json');
    _localeMap = jsonDecode(jsonStr);
    print('[AssetsLocale] Cannot load locale[' + localeName + ']. Fallback to en.json.');
    return true;
  } catch(e) {
    //Error reading the input localeName file.
    print(e.toString());
  }

  print('[AssetsLocale] initialize failed! No locale file founded!');
  return false;
}

//Check if we are initialized.
bool isInitialized() {
  return _localeMap != null;
}

clear() {
  _localeMap = null;
}

//Mimic the i18n stuff.
String translate(String key) {
  // print('translate [' + key + ']');
  if (_localeMap != null && _localeMap.containsKey(key)) {
    // print(_localeMap[key]);
    return _localeMap[key];
  }
  return '';
}
