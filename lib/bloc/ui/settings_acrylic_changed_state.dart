part of 'settings_acrylic_changed_cubit.dart';

@immutable
abstract class SettingsAcrylicChangedState {}

class SettingsAcrylicChangedInitial extends SettingsAcrylicChangedState {}

class SettingsAcrylicChangedSuccess extends SettingsAcrylicChangedState {}
