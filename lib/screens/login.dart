import 'dart:convert';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:meat/screens/auth_ninja.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/screens/busy.dart' as Busy;
import 'package:meat/models/app/app.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:meat/system/account_keeper.dart' as AccountKeeper;
import 'package:meat/screens/auth_ninja.dart' as AuthNinja;
import 'package:flutter/foundation.dart';

import 'package:meat/screens/md_dialog.dart' as MDDialog;
import 'package:package_info/package_info.dart';

const String _plurkSignUpUrl = 'https://www.plurk.com/signup';
const String _plurkResetPasswordUrl = 'https://www.plurk.com/resetPassword';

//THe login UI used by welcome_login nad add_account.
class Login extends StatefulWidget {
  Login(this.onLoginSuccess, {Key key}) : super(key: key);

  final Function onLoginSuccess;

  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> with AfterLayoutMixin {
  LoginState();

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _usernameKey = GlobalKey<FormState>();
  final _passwordKey = GlobalKey<FormState>();

  final _usernameFocus = FocusNode();
  final _passwordFocus = FocusNode();

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  // Are we currently busying in logging in?
  bool _loggingIn = false;

  String version = '';
  String buildNumber = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    //Read the version number...
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    setState(() {
      version = packageInfo.version +
          ' - ' +
          packageInfo.buildNumber +
          ((kDebugMode) ? ' - Debug' : '') +
          ((kProfileMode) ? ' - Profile' : '') +
          ((kReleaseMode) ? ' - Release' : '');
      buildNumber = packageInfo.buildNumber;
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  //Are we currently in the login waiting progress?
  bool isLoggingIn() {
    return _loggingIn;
  }

  @override
  Widget build(BuildContext context) {
    // Scaffold will be response for keyboard showing resize layout.
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: GestureDetector(
        child: LayoutBuilder(builder:
            (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight,
              ),
              child: IntrinsicHeight(
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      // Big Logo.
                      Flexible(
                        child: Center(
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Image(
                                image: AssetImage(
                                    'assets/images/BKTTokenLogo.png'),
                                width: 256,
                                height: 256,
                              ),
                            ),
                          ),
                        ),
                      ),

                      Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Form(
                            key: _usernameKey,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  icon: Icon(Icons.perm_identity, size: 32),
                                  labelText: FlutterI18n.translate(
                                      context, 'textFieldUsername'),
                                  isDense: true,
                                  suffixIcon:
                                      (_usernameController.text.isEmpty ||
                                              !_usernameFocus.hasFocus)
                                          ? (null)
                                          : IconButton(
                                              onPressed: () =>
                                                  _usernameController.clear(),
                                              icon: Icon(Icons.clear),
                                            )),
                              cursorColor:
                                  Theme.of(context).colorScheme.secondary,
                              focusNode: _usernameFocus,
                              controller: _usernameController,
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              inputFormatters: [
                                new FilteringTextInputFormatter.deny(
                                    new RegExp('[ \t]')),
                              ],
                              validator: _validateUsername,
                              onFieldSubmitted: _onUsernameTextFieldSubmitted,
                            ),
                          ),
                          Form(
                            key: _passwordKey,
                            child: TextFormField(
                              decoration: InputDecoration(
                                icon: Icon(Icons.lock_outline, size: 32),
                                labelText: FlutterI18n.translate(
                                    context, 'textFieldPassword'),
                                isDense: true,
                                suffixIcon: (_passwordController.text.isEmpty ||
                                        !_passwordFocus.hasFocus)
                                    ? (null)
                                    : IconButton(
                                        onPressed: () =>
                                            _passwordController.clear(),
                                        icon: Icon(Icons.clear),
                                      ),
                              ),
                              obscureText: true,
                              cursorColor:
                                  Theme.of(context).colorScheme.secondary,
                              focusNode: _passwordFocus,
                              controller: _passwordController,
                              textInputAction: TextInputAction.next,
                              inputFormatters: [
                                new FilteringTextInputFormatter.deny(
                                    new RegExp('[\t]')),
                              ],
                              validator: _validatePassword,
                              onFieldSubmitted: _onPasswordTextFieldSubmitted,
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          ElevatedButton(
                            child: Text(
                                FlutterI18n.translate(context, 'btnLogin')),
                            onPressed: _pnLoginBtnPressed,
                          ),
                          OutlinedButton(
                            child: Text(FlutterI18n.translate(
                                context, 'btnCreateNewAccount')),
                            onPressed: _onCreateNewAccountBtnPressed,
                          ),
                          InkWell(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  FlutterI18n.translate(
                                      context, 'btnForgotPassword'),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                              onTap: _onForgotPasswordBtnTap),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            version,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.caption,
                          ),
                          SizedBox(height: 8),
                        ],
                      )),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
        onTap: () {
          // Lose focus when tap background.
          setState(() {
            FocusScope.of(context)
                .unfocus(disposition: UnfocusDisposition.scope);
          });
        },
      ),
      // Scaffold(
      //   key: _scaffoldKey,
      //   body: SafeArea(
      //     child: AcrylicContainer(
      //
      //       initialImage: AssetImage('assets/images/background001.jpg'),
      //     ),
      //   ),
      // ),
    );
  }

  _onUsernameTextFieldSubmitted(String value) {
    if (_usernameKey.currentState.validate()) {
      FocusScope.of(context).requestFocus(_passwordFocus);
    }
  }

  _onPasswordTextFieldSubmitted(String value) async {
    if (_passwordKey.currentState.validate()) {
//                                    print('_passwordKey pass validate');
      // Lose focus when tap. (Only if pass validate for better UX)
      setState(() {
        FocusScope.of(context).unfocus(disposition: UnfocusDisposition.scope);
      });

      // Validate returns true if the form is valid, or false
      // otherwise.
      if (_usernameKey.currentState.validate()) {
        //Display TOS dialog...
        print('[Display TOS dialog]');
        String tosMDString = await rootBundle
            .loadString('assets/about/plurk_term_of_service.md');
        MDDialog.show(context, 'End-user License Agreement', tosMDString, (b) {
          //If user accept.
          if (b) {
            // Let's do the auth ninja magic!
            print('[TOS Accepted]');
            _summonAuthNinja();
          }
        });
      }
    }
  }

  _pnLoginBtnPressed() async {
    // Lose focus when tap.
    setState(() {
      FocusScope.of(context).unfocus(disposition: UnfocusDisposition.scope);
    });

    // Validate returns true if the form is valid, or false
    // otherwise.
    if (_usernameKey.currentState.validate()) {
      if (_passwordKey.currentState.validate()) {
        //Display TOS dialog...
        print('[Display TOS dialog]');
        String tosMDString = await rootBundle
            .loadString('assets/about/plurk_term_of_service.md');
        MDDialog.show(context, 'End-user License Agreement', tosMDString, (b) {
          //If user accept.
          if (b) {
            // Let's do the auth ninja magic!
            print('[TOS Accepted]');
            _summonAuthNinja();
          }
        });
      }
    }
  }

  _onCreateNewAccountBtnPressed() async {
    if (await canLaunch(_plurkSignUpUrl)) {
      await launch(_plurkSignUpUrl);
    }

    // Lose focus when tap.
    setState(() {
      FocusScope.of(context).unfocus(disposition: UnfocusDisposition.scope);
    });
  }

  _onForgotPasswordBtnTap() async {
    if (await canLaunch(_plurkResetPasswordUrl)) {
      await launch(_plurkResetPasswordUrl);
    }

    // Lose focus when tap.
    setState(() {
      FocusScope.of(context).unfocus(disposition: UnfocusDisposition.scope);
    });
  }

  //--

  String _validateUsername(String value) {
    if (value.isEmpty) {
      return 'Hey! Who are you?';
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.isEmpty) {
      return 'Are you serious? Come on!';
    } else if (value.length < 4) {
      return 'Password should be at least 4 characters long';
    }
    return null;
  }

  // This will start the grand full automatic auth process.
  void _summonAuthNinja() async {
    print('[_summonAuthNinja]');
    Busy.show(context);
    _loggingIn = true;

    await Future.delayed(Duration(milliseconds: 2500));

    //Read and decode the app stuff from asset file...
    App app;
    try {
      String jsonStr = await rootBundle.loadString('assets/app/app.json');
      dynamic json = jsonDecode(jsonStr);
      app = App.fromJson(json);
    } catch (e) {
      print(e.toString());
    }

    if (app != null) {
      //Clear any exist stuffs for good!
      Plurdart.release();
      //We have the key and the secret!
      Plurdart.initialAuth(app.key, app.secret);

      //Append deviceid and model for good...
      DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
      String deviceID;
      String model;
      try {
        if (Platform.isAndroid) {
          AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
          model = androidInfo.model;
//          deviceVersion = androidInfo .version.toString();
          deviceID = androidInfo.id; //UUID for Android
        } else if (Platform.isIOS) {
          IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
          model = iosInfo.name;
//          deviceVersion = data.systemVersion;
          deviceID = iosInfo.identifierForVendor; //UUID for iOS
        }
      } on PlatformException {
        print('Failed to get platform version');
      }

      // Auth url to open in browser...
      String url =
          await Plurdart.tryGetAuthUrl(deviceId: deviceID, model: model);
      String encodedUrl = Uri.encodeFull(url);

      // print('[encodedUrl]: ' + encodedUrl);

      //Show authNinja the dynamic way.
      AuthNinja.show(
          context,
          new AuthNinjaArgs(
              encodedUrl,
              _onVerifierReceived,
              _onLoginError,
              _onLoginCancel,
              _usernameController.text,
              _passwordController.text));
    } else {
      // Show an error dialog.
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.SCALE,
        title: 'Missing App info file!',
        desc: 'This should not happen...',
        btnOkOnPress: () {},
      )..show();

//      _showSnackBarMessage('Oops! Cannot read the app key and secret?');

      // Hide Busy.
      Navigator.pop(context);
      _loggingIn = false;
    }
  }

  // Login in progress..
  void _onVerifierReceived(String verifier) async {
    // print('_onVerifierReceived: ' + verifier);

    // Get access token. This will also setup the Plurdart client.
    Plurdart.Credentials credentials =
        await Plurdart.tryGetCredentials(verifier);

    //Let's just cache the user me and user Id after first time login. (Remember to clear them when logout)
    Plurdart.User userMe = await Plurdart.usersMe();
    Plurdart.PremiumBalance balance = await Plurdart.premiumGetBalance();
    if (userMe != null && balance != null) {
      // Runtime cache
      Static.me = userMe;
      // Pref meId
      Static.saveMeId(userMe.id);

      //Store this account.
      AccountKeeper.addAccount(userMe.nickName, userMe.id, credentials.token,
          credentials.tokenSecret);

      //Also update the user's balance.
      Static.balance = balance;

      print('[Login Success]: [' + userMe.nickName + ']');

      // Hide Busy and auth ninja
      //Leave this to onLoginSuccess.
      // Navigator.pop(context);
      // Navigator.pop(context);

      _loggingIn = false;

      //Callback to the welcome login or add account.
      widget.onLoginSuccess();
    } else {
      //Cannot get the user data?

      // Hide Busy and auth ninja
      Navigator.pop(context);
      Navigator.pop(context);
      _loggingIn = false;

      // Show an error dialog.
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.SCALE,
        title: 'Cannot read userMe from Plurk!',
        desc: 'This should not happen...',
        btnOkOnPress: () {},
      )..show();
    }
  }

  _onLoginError() async {
    //Hide Busy and auth ninja.
    Navigator.pop(context);
    Navigator.pop(context);
    _loggingIn = false;

    print('[Login failed]');

    // Show an error dialog.
    AwesomeDialog(
      context: context,
      dialogType: DialogType.NO_HEADER,
      animType: AnimType.SCALE,
      headerAnimationLoop: false,
      title: FlutterI18n.translate(context, 'dialogLoginErrorTitle'),
      desc: FlutterI18n.translate(context, 'dialogLoginErrorDesc'),
      btnOkText: FlutterI18n.translate(context, 'dialogLoginErrorHelpMe'),
      btnCancelText: FlutterI18n.translate(context, 'dialogLoginErrorTryAgain'),
      dismissOnTouchOutside: false,
      btnOkOnPress: () async {
        // Do nothing.
        // Navigator.pop(context);
        // Open the forgot password link.
        if (await canLaunch(_plurkResetPasswordUrl)) {
          await launch(_plurkResetPasswordUrl);
        }
      },
      btnCancelOnPress: () {
        // Do nothing.
        // Navigator.pop(context);
      },
    )..show();
  }

  //Cancel from 2FA
  _onLoginCancel() {
    //Hide busy and auth ninja.
    Navigator.pop(context);
    Navigator.pop(context);
    _loggingIn = false;

    print('[Login Cancel]');
  }

//  _showSnackBarMessage(String msg) {
//    _acrylicScaffoldKey.currentState.showSnackBarMessage(msg);
//  }
}
