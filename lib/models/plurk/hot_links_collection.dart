
import 'package:plurdart/plurdart.dart' as Plurdart;

class HotlinksCollection {
  HotlinksCollection();

  // All of the API results goes here.
  List<Plurdart.HotLink> _collection = [];

  // Add a new Plurdart result here.
  void add(List<Plurdart.HotLink> hotlinks,) {
    if (hotlinks.length > 0) {
      _collection.addAll(hotlinks);
    }
  }

  // Clear all.
  void clear() {
    _collection.clear();
  }

  // A convenient function for getting the necessary info for display feed.
  List<Plurdart.HotLink> getFeeds() {
    return _collection;
  }

  int getFeedsLength() {
    return _collection.length;
  }
}
