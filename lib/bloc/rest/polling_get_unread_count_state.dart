part of 'polling_get_unread_count_cubit.dart';

@immutable
abstract class PollingGetUnreadCountState {}

class PollingGetUnreadCountInitial extends PollingGetUnreadCountState {}

class PollingGetUnreadCountSuccess extends PollingGetUnreadCountState {
  PollingGetUnreadCountSuccess(this.pollingUnreadCount);
  final Plurdart.PollingUnreadCount pollingUnreadCount;
}

class PollingGetUnreadCountFailed extends PollingGetUnreadCountState {

}
