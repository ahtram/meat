import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/models/chikkutteh/res/living_channel.dart';
import 'package:meat/models/chikkutteh/res/living_channel_res.dart';
import 'package:meat/theme/app_themes.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/other/themed_badge.dart';
import 'package:meat/system/static_stuffs.dart' as Static;
import 'package:meat/widgets/youtube_living_channels/youtube_living_channel_viewer.dart';

class VtWatchBrowser extends StatefulWidget {
  VtWatchBrowser({Key key}) : super(key: key);

  @override
  _VtWatchBrowserState createState() => _VtWatchBrowserState();
}

class _VtWatchBrowserState extends State<VtWatchBrowser> with AfterLayoutMixin {

  BuildContext scaffoldBodyContext;


  @override
  void dispose() {
    super.dispose();
  }

  @override
  void afterFirstLayout(BuildContext context) async {}

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 3,
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            appBar: AppBar(
              centerTitle: true,
              brightness: Theme.of(context).brightness,
              backgroundColor: Colors.transparent,
              flexibleSpace: AcrylicContainer(
                Container(),
                backgroundColor: Theme.of(context).primaryColor,
                transparentBackLayer: false,
                mode: AcrylicContainerMode.BackdropFilter,
              ),
              elevation: 0,
              titleSpacing: 0.0,
              leading: AppBarActionButton(Icons.arrow_back, onTap: () {
                // context.read<HomeRefreshCubit>().request();
                Navigator.pop(context);
              }),
              automaticallyImplyLeading: false,
              title: TabBar(
                // controller: _tabController,
                tabs: [
                  _tabWithBadge(FlutterI18n.translate(context, 'labelEnglish'), _getLivingCount('en')),
                  _tabWithBadge(FlutterI18n.translate(context, 'labelJapanese'), _getLivingCount('jp')),
                  _tabWithBadge(FlutterI18n.translate(context, 'labelChinese'), _getLivingCount('zh')),
                ],
              ),
            ),
            body: AcrylicContainer(
              Builder(
                builder: (context) {
                  scaffoldBodyContext = context;
                  return Container(
                    // color: Theme.of(context).canvasColor.withOpacity(0.4),
                    child: Column(
                      children: [
                        Expanded(
                          child: Container(
                            child: TabBarView(
                              children: [
                                YoutubeLivingChannelViewer(_getLivingChannels('en')),
                                YoutubeLivingChannelViewer(_getLivingChannels('jp')),
                                YoutubeLivingChannelViewer(_getLivingChannels('zh')),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
              backgroundColor: Theme.of(context).canvasColor.withOpacity(0.4),
              initialImage: AssetImage('assets/images/background001.jpg'),
            )
        )
    );
  }

  Tab _tabWithBadge(String text, int badgeNum) {

    Color dotColor = Theme.of(context).lowContrastPrimaryColor;

    Widget badgeWidget = Container();
    if (badgeNum > 0) {
      badgeWidget = ThemedBadge(dotColor, badgeNum.toString());
    }

    return Tab(
      child: Container(
        padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text(
              text,
              style: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
                fontSize: 13,
              ),
            ),

            Align(
                alignment: Alignment.topRight,
                child: badgeWidget)
          ],
        ),
      ),
    );
  }

  int _getLivingCount(String lang) {
    LivingChannelsRes livingChannelsRes = Static.getLivingChannelsRes();
    if (livingChannelsRes != null) {
      return livingChannelsRes.livingCount(lang);
    }
    return 0;
  }

  List<LivingChannel> _getLivingChannels(String lang) {
    LivingChannelsRes livingChannelsRes = Static.getLivingChannelsRes();
    if (livingChannelsRes != null) {
      return livingChannelsRes.livingChannels(lang);
    }
    return [];
  }

}
