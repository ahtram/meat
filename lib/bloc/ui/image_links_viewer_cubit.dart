import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:meat/screens/image_links_viewer.dart';

part 'image_links_viewer_state.dart';

class ImageLinksViewerCubit extends Cubit<ImageLinksViewerState> {
  ImageLinksViewerCubit() : super(ImageLinksViewerInitial());

  void enter(ImageLinksViewerArgs args) async {
    emit(ImageLinksViewerEnterState(args));
  }
}
