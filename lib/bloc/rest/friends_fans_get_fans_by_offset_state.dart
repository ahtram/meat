part of 'friends_fans_get_fans_by_offset_cubit.dart';

@immutable
abstract class FriendsFansGetFansByOffsetState {}

class FriendsFansGetFansByOffsetInitial extends FriendsFansGetFansByOffsetState {}

class FriendsFansGetFansByOffsetSuccess extends FriendsFansGetFansByOffsetState {
  FriendsFansGetFansByOffsetSuccess(this.users);
  final List<Plurdart.User> users;
}

class FriendsFansGetFansByOffsetFailed extends FriendsFansGetFansByOffsetState {}
