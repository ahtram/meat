import 'package:flutter/material.dart';
import 'package:meat/bloc/ui/image_links_viewer_cubit.dart';
import 'package:meat/bloc/ui/open_uri_cubit.dart';
import 'package:meat/models/plurk/plurk_content_parts.dart';
import 'package:meat/screens/image_links_viewer.dart';
import 'package:meat/widgets/other/acrylic_container.dart';
import 'package:meat/widgets/plurk/plurk_card_action_button.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/plurk_card_link_stack.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meat/system/define.dart' as Define;
import 'package:meat/system/static_stuffs.dart' as Static;

class HotLinkCard extends StatefulWidget {
  HotLinkCard(this.hotlink, {Key key}) : super(key: key);

  final Plurdart.HotLink hotlink;

  @override
  _HotLinkCardState createState() => _HotLinkCardState();
}

class _HotLinkCardState extends State<HotLinkCard> with PlurkContentParts  {
  GlobalKey _acrylicContainerKey = GlobalKey<AcrylicContainerState>();

  @override
  Widget build(BuildContext context) {

    //We'll need to process the link content.
    preProcess(context, widget.hotlink.rendered, Static.settingsRemoveLinksInPlurkContent);

    return Column(
      children: [
        AcrylicContainer(
          InkWell(
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: _buildCardParts(),
                  ),
                ),
              ],
            ),
            onTap: () {
              //Todo: Anything for the hot link card?
            },
          ),
          initialImage: MemoryImage(kTransparentImage),
          key: _acrylicContainerKey,
          mode: AcrylicContainerMode.None,
        ),
        Divider(
          height: 0.5,
          thickness: 0.5,
        ),
      ],
    );
  }

  // This will decide which part will be displayed on the Card.
  // Some of the part maybe empty.
  List<Widget> _buildCardParts() {
    List<Widget> partWidgets = [];

    partWidgets.add(SizedBox(height: Define.verticalPadding));

    //== Text Content == (Maybe empty)
    Widget textContentPart = buildTextContentPart(textContent);
    if (textContentPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: textContentPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Hash tags == (Maybe empty)
    Widget hashTagsPart = buildHashTagsPart(hashTags);
    if (hashTagsPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: hashTagsPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Youtube Video Content == (Maybe empty)
    Widget youtubeVideoPart = buildYoutubeVideoPart(youtubeVideoLinks);
    if (youtubeVideoPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: youtubeVideoPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Image Collection Content == (Maybe empty)
    Widget imageCollectionPart = buildImageCollectionPartByType(plurkImageLinks,
            (heroTag) {
          //Get the initial index
          int initialIndex = 0;
          for (int i = 0; i < plurkImageLinks.length; ++i) {
            if (plurkImageLinks[i].getHeroTag() == heroTag) {
              initialIndex = i;
              break;
            }
          }

          context.read<ImageLinksViewerCubit>().enter(ImageLinksViewerArgs(
            plurkImageLinks,
            initialIndex,
          ));
        }, () {
          //We don't want to set background for the card.
        },
        ImageCollectionType.Summery);

    if (imageCollectionPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: imageCollectionPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Link Stack Content == (Maybe empty)
    Widget linkStackPart = buildLinkStackPartByType(urlLinks, LinkStackType.All, (uri) {
      BlocProvider.of<OpenUriCubit>(context).request(uri);
    });
    if (linkStackPart != null) {
      partWidgets.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
        child: linkStackPart,
      ));
      partWidgets.add(SizedBox(height: Define.verticalPadding));
    }

    //== Action Bar ==
    partWidgets.add(_buildActionBarPart());

    return partWidgets;
  }

  Widget _buildActionBarPart() {
    List<Widget> rowChildren = [];

    rowChildren.add(Expanded(
      flex: 1,
      child: PlurkCardActionButton(
          Icons.whatshot, true, Colors.deepOrange, count: widget.hotlink.numPlurks),
    ));

    return Container(
      padding: EdgeInsets.symmetric(horizontal: Define.horizontalPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: rowChildren,
      ),
    );
  }
}