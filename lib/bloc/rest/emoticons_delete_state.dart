part of 'emoticons_delete_cubit.dart';

@immutable
abstract class EmoticonsDeleteState {}

class EmoticonsDeleteInitial extends EmoticonsDeleteState {}

class EmoticonsDeleteSuccess extends EmoticonsDeleteState {}

class EmoticonsDeleteFailed extends EmoticonsDeleteState {
  EmoticonsDeleteFailed(this.errorText);
  final String errorText;
}