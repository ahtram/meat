import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'responses_track_new_state.dart';

//[Dep]: Replaced by ResponsesCollector.
class ResponsesTrackNewCubit extends Cubit<ResponsesTrackNewState> {
  ResponsesTrackNewCubit() : super(ResponsesTrackNewState(null));

  void get(int plurkId, int fromResponse, int count) async {
    Plurdart.Responses responses = await Plurdart.responsesGet(Plurdart.ResponsesGet(
      plurkId: plurkId,
      fromResponse: fromResponse,
      count: count,
    ));

    //Collect Users to global User Cache.
    if (responses != null) {
      responses.friends.forEach((key, value) {
        Static.addToUserCache(value);
      });
    }

    emit(ResponsesTrackNewState(responses));
  }
}
