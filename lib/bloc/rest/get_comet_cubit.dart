// import 'package:bloc/bloc.dart';
// import 'package:meta/meta.dart';
// import 'package:plurdart/plurdart.dart' as Plurdart;
//
// part 'get_comet_state.dart';
//
// class GetCometCubit extends Cubit<GetCometState> {
//   GetCometCubit() : super(GetCometInitial());
//
//   request() async {
//     Plurdart.Comet comet = await Plurdart.getComet();
//     if (comet != null) {
//       emit(GetCometSuccess(comet));
//     } else {
//       emit(GetCometFailed());
//     }
//   }
// }
