import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'plurk_viewer_modal_prep_state.dart';

class PlurkViewerModalPrepCubit extends Cubit<PlurkViewerModalPrepState> {
  PlurkViewerModalPrepCubit() : super(PlurkViewerModalPrepInitial());

  complete() {
    emit(PlurkViewerModalPrepComplete());
  }

  failed(String errorText) {
    emit(PlurkViewerModalPrepFailed(errorText));
  }
}
