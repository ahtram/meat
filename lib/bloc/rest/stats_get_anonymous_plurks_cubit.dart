import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/system/static_stuffs.dart' as Static;

part 'stats_get_anonymous_plurks_state.dart';

class StatsGetAnonymousPlurksCubit extends Cubit<StatsGetAnonymousPlurksState> {
  StatsGetAnonymousPlurksCubit() : super(StatsGetAnonymousPlurksInitial());

  request() async {
    Plurdart.TopicPlurks topicPlurks = await Plurdart.statsGetAnonymousPlurks(Plurdart.StatsGetAnonymousPlurks(lang: Static.apiLanguage));
    if (topicPlurks != null) {
      emit(StatsGetAnonymousPlurksSuccess(topicPlurks));
    } else {
      emit(StatsGetAnonymousPlurksFailed());
    }
  }
}
