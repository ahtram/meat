import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'alerts_get_active_state.dart';

class AlertsGetActiveCubit extends Cubit<AlertsGetActiveState> {
  AlertsGetActiveCubit() : super(AlertsGetActiveInitial());

  void request() async {
    List<Plurdart.Alert> alerts = await Plurdart.alertsGetActive();

    if (alerts != null) {
      emit(AlertsGetActiveSuccess(alerts));
    } else {
      emit(AlertsGetActiveFailed());
    }
  }
}
