import 'package:flutter/material.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';

import 'login.dart';

void show(BuildContext context, Function onLoginSuccess) {
  //Nope: this will cause a bit glitch.
  Navigator.push(context, new TransparentRoute(
      builder: (BuildContext context) {
        return _AddAccount(onLoginSuccess);
      }
  ));
}

class _AddAccount extends StatefulWidget {
  _AddAccount(this.onLoginSuccess, {Key key}) : super(key: key);

  final Function onLoginSuccess;

  @override
  _AddAccountState createState() => _AddAccountState();
}

class _AddAccountState extends State<_AddAccount> {

  GlobalKey<LoginState> loginKey = GlobalKey<LoginState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (!loginKey.currentState.isLoggingIn()) {
          return true;
        } else {
          return false;
        }
      },
      child: AcrylicScaffold(
        transparentColor: Theme.of(context).canvasColor.withOpacity(0.4),
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          centerTitle: true,
          brightness: Theme.of(context).brightness,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: AppBarActionButton(Icons.arrow_back, onTap: () {
            if (!loginKey.currentState.isLoggingIn()) {
              Navigator.pop(context);
            }
          }),
          title: Text(
            // FlutterI18n.translate(
            //     context, 'menuAbout'),
            'Add Account',
            style: TextStyle(
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
        child: Container(
          child: Login(
            //On login success.
            () {
              //The account has been added and set as active. We'll just callback to
              //inform home.
              widget.onLoginSuccess();
            },
            key: loginKey
          ),
        ),
      ),
    );
  }
}