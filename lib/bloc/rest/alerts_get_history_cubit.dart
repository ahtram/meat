import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'alerts_get_history_state.dart';

class AlertsGetHistoryCubit extends Cubit<AlertsGetHistoryState> {
  AlertsGetHistoryCubit() : super(AlertsGetHistoryInitial());

  void request() async {
    List<Plurdart.Alert> alerts = await Plurdart.alertsGetHistory();

    if (alerts != null) {
      emit(AlertsGetHistorySuccess(alerts));
    } else {
      emit(AlertsGetHistoryFailed());
    }
  }
}
