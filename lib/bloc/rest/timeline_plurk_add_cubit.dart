import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_plurk_add_state.dart';

class TimelinePlurkAddCubit extends Cubit<TimelinePlurkAddState> {
  TimelinePlurkAddCubit() : super(TimelinePlurkAddInitial());

  post(Plurdart.TimelinePlurkAdd timelinePlurkAdd) async {
    Plurdart.Plurk plurk = await Plurdart.timelinePlurkAdd(timelinePlurkAdd);

    //Check post success.
    if (plurk != null) {
      if (!plurk.hasError()) {

        // print('TimelinePlurkAddCubit: ' + prettyJson(plurk.toJson()));

        emit(TimelinePlurkAddSuccess(plurk));
      } else {
        emit(TimelinePlurkAddFailed(plurk.errorText));
      }
    } else {
      emit(TimelinePlurkAddFailed('Unknown error'));
    }
  }
}
