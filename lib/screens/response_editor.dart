import 'package:after_layout/after_layout.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:meat/bloc/rest/responses_response_add_cubit.dart';
import 'package:meat/bloc/rest/responses_response_edit_cubit.dart';
import 'package:meat/bloc/ui/flush_bar_cubit.dart';
import 'package:meat/bloc/ui/response_edit_complete_cubit.dart';
import 'package:meat/bloc/ui/user_viewer_cubit.dart';
import 'package:meat/screens/user_viewer.dart';
import 'package:meat/system/transparent_route.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';
import 'package:meat/widgets/plurk/plurk_card_image_collection.dart';
import 'package:meat/widgets/plurk/response_poster.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;
import 'package:meat/widgets/plurk/response_card.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void show(BuildContext context, ResponseEditorArgs args) {
  Navigator.push(context, new TransparentRoute(builder: (BuildContext context) {
    return _ResponseEditor(args);
  }));
}

class ResponseEditorArgs {
  ResponseEditorArgs(this.plurk, this.response, this.user, this.floor);
  final Plurdart.Plurk plurk;
  final Plurdart.Response response;
  final Plurdart.User user;
  final int floor;
}

class _ResponseEditor extends StatefulWidget {
  _ResponseEditor(this.args, {Key key}) : super(key: key);

  final ResponseEditorArgs args;

  @override
  _ResponseEditorState createState() => _ResponseEditorState();
}

class _ResponseEditorState extends State<_ResponseEditor> with AfterLayoutMixin {

  BuildContext _blocProviderContext;

  //For operate ResponseEditor. (like add nickName to comment)
  GlobalKey<ResponsePosterState> _responseEditorKey =
  GlobalKey<ResponsePosterState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    //Try focus on comment.
    _responseEditorKey.currentState.requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    //What's here?
    //A Response Card for viewing a Response Data.
    //A Response Editor for editing and commit the response.
    return MultiBlocProvider(
      providers: [
        BlocProvider<ResponsesResponseAddCubit>(
          create: (BuildContext context) => ResponsesResponseAddCubit(),
        ),
        BlocProvider<ResponsesResponseEditCubit>(
          create: (BuildContext context) => ResponsesResponseEditCubit(),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<FlushBarCubit, FlushBarState>(
            listener: (context, state) {
              if (state is FlushBarRequest) {
                //Only display FlushBar when I am the current route.
                if (ModalRoute.of(context).isCurrent) {
                  _showFlushBar(_blocProviderContext, state.icon, state.title,
                      state.message);
                }
              }
            },
          ),
          BlocListener<ResponseEditCompleteCubit, ResponseEditCompleteState>(
            listener: (context, state) {
              if (state is ResponseEditCompleteFailed) {
                _showFlushBar(_blocProviderContext, Icons.warning, 'Oops! Response edit failed',
                    state.errorText);
              } else if (state is ResponseEditCompleteSuccess) {
                Navigator.pop(context);
              }
            },
          ),
        ],
        child: AcrylicScaffold(
            resizeToAvoidBottomInset: true,
            appBar: AppBar(
              centerTitle: true,
              backgroundColor:
              Theme.of(context).canvasColor.withOpacity(0.4),
              elevation: 0,
              titleSpacing: 0.0,
              leading: AppBarActionButton(Icons.arrow_back, onTap: () {
                Navigator.pop(context);
              }),
              title: Text(
                FlutterI18n.translate(
                    context, 'titleEditResponse'),
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1.color,
                ),
              ),
            ),
            child: Builder(
              builder: (context) {
                _blocProviderContext = context;
                return SingleChildScrollView(
                  child: Container(
                    color: Theme.of(context).canvasColor.withOpacity(0.4),
                    child: ResponseCard(widget.args.plurk, widget.args.response, widget.args.user, widget.args.floor, (response, user) {

                      String nickNameStr = (response != null && response.handle != null) ? response.handle : '@' + user.nickName;

                      //Add this user's nickName to the comment TextField.
                      _responseEditorKey.currentState.tryInsertNickNameStr(nickNameStr);
                      // print('plurkViewer responseCard onNameTap ');
                    }, (user) {
                      //Bloc to open UserViewer.
                      context.read<UserViewerCubit>().open(UserViewerArgs(
                          user.nickName
                      ));
                    }, null, null, null, false, ImageCollectionType.Summery),
                  ),
                );
              },
            ),
          //Need a ui bloc builder here for checking Plurk downloaded.
          bottomNavigationBar: BottomAppBar(
            elevation: 0,
            color: Colors.transparent,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                // Input UIs here...
                ResponsePoster(
                  widget.args.plurk.plurkId,
                  true,
                  mode: ResponsePosterMode.EditResponse,
                  responseId: widget.args.response.id,
                  initialContent: widget.args.response.contentRaw,
                  key: _responseEditorKey,
                ),

                SizedBox(
                  height: MediaQuery.of(context).viewInsets.bottom,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _showFlushBar(
      BuildContext context, IconData icon, String title, String message) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      messageText: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      icon: Icon(icon),
      margin: EdgeInsets.fromLTRB(64, 12, 64, 56),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      backgroundColor: Theme.of(context).cardColor,
      leftBarIndicatorColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 3),
    )..show(context);
  }
}