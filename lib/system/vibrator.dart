import 'package:flutter/services.dart';

vibrateSuccess() async {
  // if (await Vibration.hasCustomVibrationsSupport()) {
  //   Vibration.vibrate(duration: 30, amplitude: 32);
  //   await Future.delayed(Duration(milliseconds: 100));
  //   Vibration.vibrate(duration: 10, amplitude: 32);
  // }
  HapticFeedback.mediumImpact();
}

vibrateShortNote() async {
  // if (await Vibration.hasCustomVibrationsSupport()) {
  //   Vibration.vibrate(duration: 10, amplitude: 32);
  // }
  HapticFeedback.lightImpact();
}

vibrateError() async {
  // if (await Vibration.hasCustomVibrationsSupport()) {
  //   Vibration.vibrate(duration: 20, amplitude: 32);
  //   await Future.delayed(Duration(milliseconds: 200));
  //   Vibration.vibrate(duration: 20, amplitude: 32);
  // }
  HapticFeedback.mediumImpact();
}