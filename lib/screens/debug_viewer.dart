import 'package:flutter/material.dart';
import 'package:meat/widgets/other/acrylic_scaffold.dart';
import 'package:meat/widgets/other/appbar_action_button.dart';

void show(BuildContext context, String debugMessage) {
  Navigator.push(
      context,
      new MaterialPageRoute(
          fullscreenDialog: true,
          builder: (BuildContext context) {
            return _DebugViewer(debugMessage);
          }));
}

class _DebugViewer extends StatefulWidget {
  _DebugViewer(this.debugMessage, {Key key}) : super(key: key);

  final String debugMessage;

  @override
  _DebugViewerState createState() => _DebugViewerState();
}

class _DebugViewerState extends State<_DebugViewer> {

  BuildContext scaffoldBodyContext;

  @override
  Widget build(BuildContext context) {
    return AcrylicScaffold(
        appBar: AppBar(
          centerTitle: true,
          brightness: Theme.of(context).brightness,
          backgroundColor:
          Theme.of(context).canvasColor.withOpacity(0.4),
          elevation: 0,
          leading: AppBarActionButton(Icons.arrow_back, onTap: () {
            Navigator.pop(context);
          }),
          title: Text(
            'DebugViewer',
            style: TextStyle(
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8),
            color: Theme.of(context).canvasColor.withOpacity(0.4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('請截圖並傳給 BahKutTeh 開發團隊，非常感謝您!'),

                Divider(),

                Text(widget.debugMessage),
              ],
            ),
          ),
        ));
  }
}