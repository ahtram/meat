import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'emoticon_ask_delete_state.dart';

class EmoticonAskDeleteCubit extends Cubit<EmoticonAskDeleteState> {
  EmoticonAskDeleteCubit() : super(EmoticonAskDeleteInitial());

  show(String url) async {
    emit(EmoticonAskDeleteShow(url));
  }
}
