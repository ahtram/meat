import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ResponsesTriLoadButton extends StatelessWidget {
  ResponsesTriLoadButton({this.onLoadTopDown, this.onLoadAll, this.onLoadBottomUp});

  final Function onLoadTopDown;
  final Function onLoadAll;
  final Function onLoadBottomUp;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 4),
          child: Center(
          child: Icon(Icons.more_vert, color: Theme.of(context).textTheme.bodyText1.color),
        ),),

        Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(color: Theme.of(context).textTheme.bodyText1.color),
              left: BorderSide(color: Theme.of(context).textTheme.bodyText1.color),
              right: BorderSide(color: Theme.of(context).textTheme.bodyText1.color),
              bottom: BorderSide(color: Theme.of(context).textTheme.bodyText1.color),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              //1
              Expanded(
                child: InkWell(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.arrow_upward, size: 16, color: Theme.of(context).textTheme.bodyText1.color),
                        SizedBox(width: 4,),
                        Text(FlutterI18n.translate(
                            context, 'btnLabelLoadMore'), style: Theme.of(context).textTheme.caption),
                      ],
                    ),
                  ),
                  onTap: onLoadTopDown,
                ),
              ),

              //2
              Expanded(
                child: InkWell(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(color: Theme.of(context).textTheme.bodyText1.color),
                        right: BorderSide(color: Theme.of(context).textTheme.bodyText1.color),
                      ),
                    ),
                    padding: EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(FlutterI18n.translate(
                            context, 'btnLabelLoadAll'), style: Theme.of(context).textTheme.caption,),
                      ],
                    ),
                  ),
                  onTap: onLoadAll,
                ),
              ),

              //3
              Expanded(
                child: InkWell(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(FlutterI18n.translate(
                            context, 'btnLabelLoadMore'), style: Theme.of(context).textTheme.caption),
                        SizedBox(width: 4,),
                        Icon(Icons.arrow_downward, size: 16, color: Theme.of(context).textTheme.bodyText1.color),
                      ],
                    ),
                  ),
                  onTap: onLoadBottomUp,
                ),
              ),

            ],
          ),
        ),

        Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Center(
            child: Icon(Icons.more_vert, color: Theme.of(context).textTheme.bodyText1.color,),
          ),),

        Divider(height: 0.5, thickness: 0.5),
      ],
    );
  }
}
