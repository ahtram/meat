import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as Http;

import 'sheet_value_range_metadata.dart';
import 'spreadsheets_metadata.dart';

final String workSheetFeedUrlPrefix =
    'https://spreadsheets.google.com/feeds/worksheets/';
final String feedUrlPostfix = '/public/full?alt=json';

// Form a url for a public Google spreadsheet metadata.
String getSpreadsheetsMetadataUrl(String spreadsheetsId, String apiKey) {
  return 'https://sheets.googleapis.com/v4/spreadsheets/' +
      spreadsheetsId +
      '?key=' +
      apiKey;
}

// Form a url for getting data in range of a spreadsheet. The range could be just a sheet title.
String getSheetValueRangeUrl(String spreadsheetsId, String apiKey, String rng) {
  return 'https://sheets.googleapis.com/v4/spreadsheets/'+
      spreadsheetsId +
      '/values/' +
      Uri.encodeComponent(rng) +
      '?key=' +
      apiKey;
}

//Get json content from the uri.
Future<dynamic> getRequestPublicSpreadsheetsUrl(Uri uri) async {
  try {
    Http.Response res = await Http.get(uri);
    if (res.statusCode == HttpStatus.ok) {
      return jsonDecode(res.body);
    } else {
      print('Oops! getRequestPublicSpreadsheetsUrl got status [' +
          res.statusCode.toString() +
          ']');
      return null;
    }
  } catch (e) {
    //The server doesn't define any error code for now!
    print('Oops! getRequestPublicSpreadsheetsUrl got error [' +
        e.toString() +
        ']');
    return null;
  }
}

// Get the Google spreadsheet data we want.
Future<List<WorkSheetData>> fetchGSheetData(
    String spreadsheetId, String apiKey) async {
  List<WorkSheetData> returnDataList = [];

  SpreadsheetsMetadata spreadsheetsMetadata = SpreadsheetsMetadata.fromJson(
      await getRequestPublicSpreadsheetsUrl(
          Uri.parse(getSpreadsheetsMetadataUrl(spreadsheetId, apiKey))));

  if (spreadsheetsMetadata != null) {
    // print('Total sheet [' + spreadsheetsMetadata.sheets.length.toString() + ']...');
    for (int i = 0; i < spreadsheetsMetadata.sheets.length; ++i) {
      String sheetUrl = getSheetValueRangeUrl(spreadsheetId, apiKey,
          spreadsheetsMetadata.sheets[i].properties.title);
      // print('fetching sheet [' + i.toString() + '] from [' + sheetUrl + ']...');
      SheetValueRangeMetadata sheetValueRangeMetadata =
          SheetValueRangeMetadata.fromJson(
              await getRequestPublicSpreadsheetsUrl(Uri.parse(sheetUrl)));
      if (sheetValueRangeMetadata != null) {
        WorkSheetData newWorkSheetData = WorkSheetData(
            title: spreadsheetsMetadata.sheets[i].properties.title,
            stringTable: sheetValueRangeMetadata.values);

        // print('newWorkSheetData: ' + newWorkSheetData.toFormatString('|', ','));

        returnDataList.add(newWorkSheetData);
      }
    }
  }
  return returnDataList;
}

// For sorting out cells.
class WorkSheetData {
  WorkSheetData({this.title, this.stringTable});

  String title = '';
  List<List<String>> stringTable = [];

  //Just for debug.
  String toFormatString(String columSeparator, String rowSeparator) {
    if (columSeparator.isEmpty) {
      return 'Error: No columSeparator assigned.';
    }

    if (rowSeparator.isEmpty) {
      return 'Error: No rowSeparator assigned.';
    }

    String returnString = '';
    for (int i = 0; i < stringTable.length; i++) {
      for (int j = 0; j < stringTable[i].length; j++) {
        returnString += stringTable[i][j];
        if (j < stringTable[i].length - 1) {
          returnString += columSeparator;
        }
      }

      if (i < stringTable.length - 1) {
        returnString += rowSeparator;
      }
    }
    return returnString;
  }
}
