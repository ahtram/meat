part of 'plurk_viewer_cubit.dart';

@immutable
abstract class PlurkViewerState {}

class PlurkViewerInitial extends PlurkViewerState {}

class PlurkViewerOpen extends PlurkViewerState {
  PlurkViewerOpen(this.args);
  final PlurkViewerArgs args;
}
