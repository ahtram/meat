import 'dart:io';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meat/widgets/youtube_video_player/youtube_video_player.dart';

void show(BuildContext context, YoutubeFullscreenViewerArgs args) async {
  await Navigator.push(context, new MaterialPageRoute(
      builder: (BuildContext context) {
        return _YoutubeFullscreenViewer(args);
      }
  ));
}

class YoutubeFullscreenViewerArgs {
  final String youtubeVideoID;
  //InMicroseconds.
  final int startAt;
  final bool autoPlay;
  final YoutubeVideoPlayerState sourceState;

  YoutubeFullscreenViewerArgs(this.youtubeVideoID, this.startAt, this.autoPlay, this.sourceState);
}

// The landscape fullscreen view for flutter_youtube_player.
class _YoutubeFullscreenViewer extends StatefulWidget {
  _YoutubeFullscreenViewer(this.args);

  final YoutubeFullscreenViewerArgs args;

  @override
  _YoutubeFullscreenViewerState createState() => _YoutubeFullscreenViewerState();
}

class _YoutubeFullscreenViewerState extends State<_YoutubeFullscreenViewer> with AfterLayoutMixin {

  @override
  void initState() {
    HapticFeedback.lightImpact();
    super.initState();

    trySetOrientation();
  }

  Future trySetOrientation() async {

    bool shouldChangeOrientation = true;
    if (widget.args.sourceState != null) {
      if (widget.args.sourceState.videoAspectRatio < 1.0) {
        shouldChangeOrientation = false;
      }
    }

    //Check the video aspect ratio to see if this is a vertical video.
    //Only enter landscape mode if this is a Horizontal video.

    if (shouldChangeOrientation) {
      if (Platform.isIOS) {
        //iOS has weired behavior on this. I have to hack a bit to make it work as intent.
        await SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeRight,
        ]);
        await SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeRight,
          DeviceOrientation.landscapeLeft,
        ]);
      } else {
        //Android
        await SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ]);
      }
    }
    await SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
  }

  Future tryRestoreOrientation() async {

    bool shouldChangeOrientation = true;
    if (widget.args.sourceState != null) {
      if (widget.args.sourceState.videoAspectRatio < 1.0) {
        shouldChangeOrientation = false;
      }
    }

    await SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    if (shouldChangeOrientation) {
      await SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    }

  }

  @override
  void afterFirstLayout(BuildContext context) async {

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        HapticFeedback.lightImpact();
        Navigator.pop(context);
        await tryRestoreOrientation();
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: YoutubeVideoPlayer(
          widget.args.youtubeVideoID,
          isInFullScreenSize: true,
          startAt: widget.args.startAt,
          autoPlay: widget.args.autoPlay,
          sourceState: widget.args.sourceState,
          fitVideoAspectRatio: true,
          onFullScreenTap: () async {
            // print('onExitFullScreen');
            HapticFeedback.lightImpact();
            Navigator.pop(context);
            await tryRestoreOrientation();
          },
          onChanged: (value) {
            //We don't need this.
          },
        ),
      ),
    );
  }
}