import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:meat/screens/user_viewer.dart';

part 'user_viewer_state.dart';

class UserViewerCubit extends Cubit<UserViewerState> {
  UserViewerCubit() : super(UserViewerInitial());

  void open(UserViewerArgs args) async {
    emit(UserViewerOpen(args));
  }
}
