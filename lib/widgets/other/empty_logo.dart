import 'package:flutter/material.dart';

class EmptyLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height -
          kToolbarHeight -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/BKTIcon1024.png',
            width: 160,
            height: 160,
          ),
          SizedBox(
            height: 32,
          ),
          Text(
            'As good as a new!',
            style: TextStyle(
              color: Theme.of(context).hintColor,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
