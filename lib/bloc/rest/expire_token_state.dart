part of 'expire_token_cubit.dart';

@immutable
class ExpireTokenState {
  ExpireTokenState(this.checkedExpiredToken);
  final Plurdart.CheckedExpiredToken checkedExpiredToken;
}