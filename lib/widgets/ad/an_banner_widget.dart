// import 'dart:io';
//
// import 'package:facebook_audience_network/facebook_audience_network.dart';
// import 'package:flutter/material.dart';
//
// class AnBannerWidget extends StatefulWidget {
//   AnBannerWidget({Key key}) : super(key: key);
//
//   @override
//   _AnBannerWidgetState createState() => _AnBannerWidgetState();
// }
//
// class _AnBannerWidgetState extends State<AnBannerWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       alignment: Alignment(0.5, 1),
//       child: FacebookBannerAd(
//         placementId: Platform.isAndroid ? "772193767047104_772198730379941" : "772193767047104_772202657046215",
//         bannerSize: BannerSize.STANDARD,
//         listener: (result, value) {
//           switch (result) {
//             case BannerAdResult.ERROR:
//               print("Error: $value");
//               break;
//             case BannerAdResult.LOADED:
//               print("Loaded: $value");
//               break;
//             case BannerAdResult.CLICKED:
//               print("Clicked: $value");
//               break;
//             case BannerAdResult.LOGGING_IMPRESSION:
//               print("Logging Impression: $value");
//               break;
//           }
//         },
//       ),
//     );
//   }
// }