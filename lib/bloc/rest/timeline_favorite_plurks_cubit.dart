import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:plurdart/plurdart.dart' as Plurdart;

part 'timeline_favorite_plurks_state.dart';

//[Dep]: We don't need this anymore.
class TimelineFavoritePlurksCubit extends Cubit<TimelineFavoritePlurksState> {
  TimelineFavoritePlurksCubit() : super(TimelineFavoritePlurksInitial());

  void favoritePlurk(int plurkID) async {
    emit(TimelineFavoritePlurkPressed());
    Plurdart.Base responseBase = await Plurdart.timelineFavoritePlurks(Plurdart.PlurkIDs(ids: [plurkID]));
    if (responseBase != null) {
      if(responseBase.hasSuccess()) {
        emit(TimelineFavoritePlurkSuccess());
      } else {
        emit(TimelineFavoritePlurkFailed());
      }
    } else {
      emit(TimelineFavoritePlurkFailed());
    }
  }

  void unFavoritePlurk(int plurkID) async {
    emit(TimelineUnFavoritePlurkPressed());
    Plurdart.Base responseBase = await Plurdart.timelineUnfavoritePlurks(Plurdart.PlurkIDs(ids: [plurkID]));
    if (responseBase != null) {
      if(responseBase.hasSuccess()) {
        emit(TimelineUnFavoritePlurkSuccess());
      } else {
        emit(TimelineUnFavoritePlurkFailed());
      }
    } else {
      emit(TimelineUnFavoritePlurkFailed());
    }
  }

}
